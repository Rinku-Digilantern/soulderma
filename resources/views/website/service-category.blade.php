@include('website.include.header')
<link rel="stylesheet" href="{{ url('website') }}/css/services-category.css">

<section id="topcommonbanner" class="service_page_banner">
    <div class="commonbannerclinic">
        <img src="{{ url('/backend/service/banner/'.$service->service_banner_image) }}" class="w-100" alt="{{ $service->alt_tag }}">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="headingandbredcrumb">
                    <div class="commonheading notofamily commonheadingfontsize">{{ $service->service_name }}</div>
                    <div class="breadcrumb commonbredcurmb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a></li>
                            @if($sub->service_name != 'General')
                            <li><a href="{{ url('/').'/'.$sub->url }}">{{ $sub->service_name }}</a></li>
                            @endif
                            <li><a href="#">{{ $service->service_name }}</a></li>
                        </ul>
                    </div>
                    <div class="commonrequestform">
                        @include('website.include.requestcallback')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if($service->description)
<section class="twoheadingwrap ">
    <div class="container">
        <div class="row">
            
            <div class="col-12">
                {!! $service->description !!}
            </div>

        </div>
    </div>
</section>
@endif

<section id="homeblogwrapper" class="Category_page">
    <div class="container">
        <div class="row manage-mbwidthparent">
            @foreach($subservice as $ser)
            <div class="col-12 col-md-4 manage-mbwidth">
                <div class="homeblogcontentwrapper">
                    <div class="homeblogimagewrap">
                        <div class="homeblogimage">
                            <img src="{{ url('/backend/service/image')}}/{{ $ser->service_image }}" alt="{{ $ser->alt_tag }}">
                        </div>
                   
                    </div>
                    <div class="homeblogcontenttext">
                        <div class="homeblogcontentheading">{{ $ser->service_name }}</div>
                        <div class="homeblogtextp">
                            {!! Str::limit($ser->short_desc) !!}
                        </div>
                        <div class="homblogview">
                            <a href="{{ url('/') }}/{{ $ser->url }}" class="commonanchor">read more</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
    </div>
</section>
@include('website.include.requestanappointment')
@include('website.include.footer')