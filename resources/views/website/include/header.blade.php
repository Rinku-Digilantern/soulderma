<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@if(isset($seotitle->title_tag)){{ $seotitle->title_tag }}@endif</title>
    <meta name="description" content="@if(isset($seotitle->description_tag)){{ $seotitle->description_tag }}@endif">
    <meta name="keywords" content="@if(isset($seotitle->keyword_tag)){{ $seotitle->keyword_tag }}@endif">
    <link rel="canonical" href="@if(isset($seotitle->canonical_tag)){{ $seotitle->canonical_tag }}@endif"/>

    <!-- OG meta tags -->
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="@if(isset($seotitle->title_tag)){{ $seotitle->title_tag }}@endif">
    <meta property="og:description" content="@if(isset($seotitle->description_tag)){{ $seotitle->description_tag }}@endif">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="@if(isset($seotitle->site_name)){{ $seotitle->site_name }}@endif">
    <meta property="og:image" content="@if(isset($seotitle->image)){{ url('/backend/seo')}}/{{ $seotitle->image }}@endif"/>
    <meta property="og:image:secure_url" content="@if(isset($seotitle->image)){{ url('/backend/seo')}}/{{ $seotitle->image }}@endif"/>
    <meta property="og:image:width" content="928" />
    <meta property="og:image:height" content="585" />
    <meta property="og:url" content="@if(isset($seotitle->canonical_tag)){{ $seotitle->canonical_tag }}@endif"/>
    <meta property="og:image:alt" content="@if(isset($seotitle->title_tag)){{ $seotitle->title_tag }} @endif"/>
    <!-- // OG meta tags -->

    <!-- Twitter Tag -->
    <meta property="twitter:card" content="website">
    <meta property="twitter:description" content="@if(isset($seotitle->description_tag)){{ $seotitle->description_tag }}@endif">
    <meta property="twitter:site" content="@if(isset($seotitle->site_name)){{ $seotitle->site_name }}@endif">
    <meta property="twitter:title" content="@if(isset($seotitle->title_tag)){{ $seotitle->title_tag }}@endif"/>
    <meta property="twitter:image" content="@if(isset($seotitle->image)){{ url('/backend/seo')}}/{{ $seotitle->image }}@endif"/>
    <!-- Twitter Tag -->

    <!-- bs icon -->
    <link rel="icon" href="{{url('/website/img/mobile-logo.png')}}" type="image/x-icon" />
    <!-- Font Awesome -->
    <link rel="preload" as="style" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" onload="this.rel='stylesheet'">
    <link rel="preload" as="font" type="font/woff2" crossorigin href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif:wght@400;700&family=Poppins:wght@300;400;500&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ url('website') }}/css/bt.css" />
    <link rel="stylesheet" href="{{ url('website') }}/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="{{ url('website') }}/css/owl.theme.default.min.css" />
    <link rel="stylesheet" href="{{ url('website') }}/css/date-picker.css" rel="preload" type="text/css" as="style" onload="this.rel='stylesheet'" />
    <link rel="stylesheet" href="{{ url('website') }}/css/lightbox.css">
    <link rel="stylesheet" href="{{ url('website') }}/css/sidebar-menu.css">
    <link rel="stylesheet" href="{{ url('website') }}/css/common.css">
    <!-- <link rel="preload" as="image" fetchpriority="high" type="image/jpg" href="{{url('/website/img/home-topbanner.webp')}}" /> -->
    <link rel="preload" as="image" fetchpriority="high" type="image/jpg" href="{{ url('website') }}/img/banner.jpg" />
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TCDGGBV');
    </script>
    <!-- End Google Tag Manager -->
<!-- referrer -->
<script async>
if (document.referrer && document.referrer != ""){
// console.log('Thanks for visiting this site from' + document.referrer);
if(sessionStorage.getItem("referrer") != '' && sessionStorage.getItem("referrer")){
// sessionStorage.setItem("referrer", document.referrer)
    //   console.log('Thanks for visiting this site from' + sessionStorage.getItem("referrer"));
  } else{
  sessionStorage.setItem("referrer", document.referrer);
  }
}
  </script>
  <!-- referrer -->

  <!-- Local Business Schema -->

  <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "Soul Derma Clinic by Dr.Anika Goel",
  "image": "https://www.souldermaclinic.com/website/img/dr-image.png",
  "@id": "",
  "url": "https://www.souldermaclinic.com/",
  "telephone": "+918595941529",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "58 I, Main Road, Greater Kailash-1, Block W, Greater Kailash I, Greater Kailash",
    "addressLocality": "New Delhi",
    "postalCode": "110048",
    "addressCountry": "91",
    "addressRegion": "IN"
  },
  "sameAs": [
    "https://www.facebook.com/souldermaclinic/",
    "https://www.instagram.com/soul_derma_clinic/?igshid=OGQ5ZDc2ODk2ZA%3D%3D"
  ]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "DiagnosticLab",
  "name": "Soul Derma Clinic",
  "url": "https://www.souldermaclinic.com/",
  "logo": "https://www.souldermaclinic.com/website/img/logo.png",
  "alternateName": "Soul Derma Clinic by Dr.Anika Goel",
  "sameAs": [
    "https://www.facebook.com/souldermaclinic/",
    "https://www.instagram.com/souldermaclinic/?hl=en",
    "https://www.souldermaclinic.com/#"
  ],
  "contactPoint": [
    {
      "@type": "ContactPoint",
      "telephone": "+918595941529",
      "contactType": "customer service",
      "email": "info@souldermaclinic.com",
      "contactOption": "TollFree",
      "areaServed": "IN",
      "availableLanguage": [
        "en",
        "hi"
      ]
    }
  ]
}
</script>

</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TCDGGBV" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <header class="for-desktop commonheaderallpages">
        <div class="main-headerwrap">
            <div class="top-header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <ul class="d-flex justify-content-md-end py-md-2 topul">
                                <li class="me-3"> <a href="tel:+918595941529">CALL: +91-8595941529</a></li>
                                <li><a href="{{ url('/book-an-appointment') }}">BOOK AN APPOINTMENT</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-expand-lg navbar-dark header-spacing" id="navbar_top">
                <div class="container-fluid">
                    <a class="navbar-brand fw-bold" href="{{ url('/') }}"><img src="{{ url('website') }}/img/logo.png"></a>

                    <div class="collapse navbar-collapse header-right justify-content-end" id="navbarSupportedContent">
                        <ul class="navbar-nav mainnave">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#">
                                    About Us
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="{{ url('/skin-specialist') }}">About Doctor</a></li>
                                    <li><a class="dropdown-item" href="{{ url('/skin-clinic') }}">About Clinic</a></li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="{{ url('/skin') }}">
                                    Skin
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item dropend">
                                        <a class="nav-link dropdown-toggle" href="{{ url('/acne') }}">
                                            Acne
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="{{ url('/acne-treatment') }}">Acne Treatment</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/acne-scars-treatment') }}">Acne Scars</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/open-pores-treatment') }}">Open Pores</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item dropend">
                                        <a class="nav-link dropdown-toggle" href="{{ url('/pigmentation') }}">
                                            Pigmentation
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="{{ url('/pigmentation-treatment') }}">Pigmentation Treatment</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/melasma-treatment') }}">Melasma</a></li>
                                            <li><a class="dropdown-item" href="#">Lip, Underarm, Neck </a></li>
                                            <li><a class="dropdown-item" href="{{ url('/dark-circles') }}">Dark Circles</a></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item dropend">
                                        <a class="nav-link dropdown-toggle" href="{{ url('/skin-growth') }}">
                                            Skin Growth</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="#">Wart Removal </a></li>
                                            <li><a class="dropdown-item" href="#">Mole Removal</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/ingrown-toenails') }}">Ingrown Toenails</a></li>
                                            <li><a class="dropdown-item" href="#">Skin Tags</a></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item dropend">
                                        <a class="nav-link dropdown-toggle" href="{{ url('/skin-issues') }}">
                                            Skin Issues</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="{{ url('/keloid') }}">Keloid</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/fungal-infection') }}">Fungal Infection</a></li>
                                            <li><a class="dropdown-item" href="#">Vitiligo </a></li>
                                            <li><a class="dropdown-item" href="#">Psoriasis</a></li>
                                            <li><a class="dropdown-item" href="#">Rosacea</a></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item dropend">
                                        <a class="nav-link dropdown-toggle" href="{{ url('/marks-and-spots') }}">
                                            Marks & Spots</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="#">Stretch Marks</a></li>
                                            <li><a class="dropdown-item" href="#">Dark Spots</a></li>
                                            <li><a class="dropdown-item" href="#">Freckle </a></li>
                                            <li><a class="dropdown-item" href="{{ url('/tattoo-removal') }}">Tattoo Removal</a></li>
                                            <li><a class="dropdown-item" href="#">Sun Damage</a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="{{ url('/aesthetics') }}">
                                    AESTHETICS
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item dropend">
                                        <a class="nav-link dropdown-toggle" href="{{ url('/facial-rejuvenation') }}">
                                            Facial Rejuvenation
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="{{ url('/skin-lightning') }}">Skin Lightning </a></li>
                                            <li><a class="dropdown-item" href="{{ url('/laser-toning') }}">Laser Toning</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/dermal-fillers') }}">Dermal Fillers</a></li>
                                            <li><a class="dropdown-item" href="#">Cheek Enhancment</a></li>
                                            <li><a class="dropdown-item" href="#">Lip Enhancement</a></li>
                                            <li><a class="dropdown-item" href="#">Face Contouring</a></li>
                                            <li><a class="dropdown-item" href="#">Skin Boosters</a></li>
                                            <li><a class="dropdown-item" href="#">Thread Lift</a></li>
                                            <li><a class="dropdown-item" href="#">Double Chin Rejuvenation</a></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item dropend">
                                        <a class="nav-link dropdown-toggle" href="{{ url('/facials') }}">
                                            Facials
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="{{ url('/hydra-facial') }}">Hydra Facial</a></li>
                                            <li><a class="dropdown-item" href="#">Chemical Peeling</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/vampire-facial') }}">Vampire Facial</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/photofacial') }}">Photofacial </a></li>
                                            <li><a class="dropdown-item" href="#">Medi facial</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/carbon-peel') }}">Carbon Peel </a></li>
                                        </ul>
                                    </li>


                                    <li class="nav-item dropend">
                                        <a class="nav-link dropdown-toggle" href="{{ url('/anti-aging') }}">
                                            Anti Aging
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="{{ url('/fine-lines') }}">Fine Lines</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/forehead-lines') }}">Forehead Lines</a></li>
                                            <li><a class="dropdown-item" href="#">Anti wrinkle treatment</a></li>
                                            <li><a class="dropdown-item" href="#">Skin Tightening</a></li>
                                            <li><a class="dropdown-item" href="#">Age Spots</a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>


                            <li class="nav-item dropdown changehoverside">
                                <a class="nav-link dropdown-toggle" href="{{ url('/hair') }}">
                                    HAIR
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item dropend">
                                        <a class="nav-link dropdown-toggle" href="{{ url('/hair-concerns') }}">
                                            Hair Concerns
                                        </a>
                                        <ul class="dropdown-menu subchilddrop">
                                            <li><a class="dropdown-item" href="{{ url('/dandruff') }}">Dandruff</a></li>
                                            <li><a class="dropdown-item" href="{{ url('/hair-loss') }}">Hair Fall/Loss</a></li>
                                            <li><a class="dropdown-item" href="#">Hair Baldness in Male</a></li>
                                            <li><a class="dropdown-item" href="#">Hair Baldness in Female</a></li>
                                            <li><a class="dropdown-item" href="#">Premature Greying</a></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item dropend">
                                        <a class="nav-link dropdown-toggle" href="#">
                                            Hair Growth
                                        </a>
                                        <ul class="dropdown-menu subchilddrop">
                                            <li><a class="dropdown-item" href="#">(GFC) Growth Factor Concentrate</a>
                                            </li>
                                            <li><a class="dropdown-item" href="#">Mesotherapy</a></li>
                                            <li><a class="dropdown-item" href="#">Scalp Microneedling</a></li>
                                            <li><a class="dropdown-item" href="#">Laser Light Therapy</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="{{ url('/featured-services') }}">
                                    Featured Services
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="{{ url('/hair-transplant') }}">Hair Transplant</a></li>
                                    <li><a class="dropdown-item" href="{{ url('/mirapeel') }}">MiraPeel</a></li>
                                    <li><a class="dropdown-item" href="#">Bride & Groom</a></li>
                                    <li><a class="dropdown-item" href="#">Laser Hair Removal</a></li>
                                </ul>
                            </li>
                            <div class="togglemenubar">
                                <div class="btn-group-vertical">
                                    <button id="showRight" class="btn btn-primary second-button d-block">
                                        <div class="animated-icon2"><span></span><span></span><span></span><span></span>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </ul>

                    </div>
                </div>
            </nav>

            <section class="animate-menu animate-menu-right">
                <div class="togglemenubar">
                    <div class="btn-group-vertical">
                        <button id="hideRight" class="btn btn-primary second-button">
                            <div class="animated-icon2"><span></span><span></span><span></span><span></span></div>
                        </button>
                    </div>
                </div>
                <ul class="sidebar-menu">
                    <!-- <li>
                        <a href="#">
                            <span>Testimonials</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="sidebar-submenu">
                            <li><a href="{{ url('/written-testimonials') }}"> Written Testimonials</a></li>
                            <li><a href="{{ url('/video-testimonials') }}"> Videos Testimonials</a></li>
                        </ul>
                    </li> -->
                    <li><a href="{{ url('/written-testimonials') }}"> Written Testimonials</a></li>
                    <li><a href="{{ url('/blogs') }}">Blogs</a></li>
                    <!-- <li><a href="#">Real Results</a></li> -->
                    <li><a href="{{ url('/clinic-gallery') }}">Clinic Gallery</a></li>
                    <li><a href="{{ url('/videos') }}">Videos</a></li>
                    <li><a href="{{ url('/contact-us') }}">Contact Us</a></li>
                    <ul class="topul">
                    <li><a href="{{ url('/book-an-appointment') }}">BOOK AN APPOINTMENT</a></li>
                    </ul>
                    <!-- <li>
                        <a href="#">
                            <span>Hair</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="sidebar-submenu">
                            <li><a href="#"> Hair services</a></li>
                            <li>
                                <a href="#"> Hair services two <i class="fa fa-angle-left pull-right"></i></a>
                                <ul class="sidebar-submenu">
                                    <li><a href="#"> Level Two</a></li>
                                </ul>
                            </li>
                            <li><a href="#"> Level One</a></li>
                        </ul>
                    </li> -->
                </ul>
            </section>

        </div>
    </header>


    <header class="for-mobile commonheaderallpagesmobile">
        <div class="main-headerwrap">
            <nav class="navbar">
                <div class="mobilenavigation">
                    <a class="navbar-brand fw-bold" href="{{ url('/') }}"><img src="{{ url('website') }}/img/mobile-logo.png"></a>
                    <div class="mobileapbuttonwrap"><a href="{{ url('/book-an-appointment') }}">BOOK AN APPOINTMENT</a></div>
                    <div class="nav-container mob_new_menu" tabindex="0">
                        <div class="nav-toggle" id="ham"></div>

                        <!-- <nav class="nav-items">
                            <div class="logomobile"><a class="navbar-brand fw-bold" href="{{ url('/') }}"><img src="{{ url('website') }}/img/mobile-logo.png"></a></div>
                            <div class="accordion accordion-flush" id="accordionFlushExample">
                                <div class="accordion-item">
                                    <p class="accordion-header" id="flush-headingOne">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                            About
                                        </button>
                                    </p>
                                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body">
                                            <ul>
                                                <li><a href="{{ url('/about-doctor') }}">About Doctor</a></li>
                                                <li><a href="{{ url('/about-clinic') }}">About Clinic</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingTwo">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                            Acne
                                        </button>
                                    </h2>
                                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body">
                                            <ul>
                                                <li><a href="{{ url('/about-doctor') }}">About Doctor</a></li>
                                                <li><a href="{{ url('/about-clinic') }}">About Clinic</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingThree">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                            Pigmentation
                                        </button>
                                    </h2>
                                    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body">
                                            <ul>
                                                <li><a href="{{ url('/') }}">About Doctor</a></li>
                                                <li><a href="#">About Clinic</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingThree">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                            Pigmentation
                                        </button>
                                    </h2>
                                    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body">
                                            <ul>
                                                <li><a href="{{ url('/') }}">About Doctor</a></li>
                                                <li><a href="#">About Clinic</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <p class="accordion-header">
                                        <a href="#" class="accordion-button " type="button">
                                            Single Link
                                        </a>
                                    </p>
                                </div>



                            </div>
                            <div class="sidesocialicons">
                                <div class="fotersocialmedia for-mobile">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a> </li>
                                        <li> <a href="#"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav> -->

                        <nav class="nav-drill">
                            <ul class="nav-items nav-level-1">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/') }}">
                                        Home
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/skin-specialist') }}">
                                        About Doctor
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/skin-clinic') }}">
                                        About Clinic
                                    </a>
                                </li>
                                <li class="nav-item nav-expand">
                                    <a class="nav-link nav-expand-link" href="#">
                                        Skin
                                    </a>
                                    <ul class="nav-items nav-expand-content">

                                        <li class="nav-item nav-expand">
                                            <a class="nav-link nav-expand-link" href="#">
                                                Acne
                                            </a>
                                            <ul class="nav-items nav-expand-content">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/acne-treatment') }}">
                                                        Acne Treatment
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/acne-scars-treatment') }}">
                                                        Acne Scars
                                                    </a>
                                                </li>
                                                <!-- <li class="nav-item nav-expand">
                                                    <a class="nav-link nav-expand-link" href="#">
                                                        Acne Scar
                                                    </a>
                                                    <ul class="nav-items nav-expand-content">
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="#">
                                                                Level 4
                                                            </a>
                                                        </li>
                                                        <li class="nav-item nav-expand">
                                                            <a class="nav-link nav-expand-link" href="#">
                                                                Menu
                                                            </a>
                                                            <ul class="nav-items nav-expand-content">
                                                                <li class="nav-item">
                                                                    <a class="nav-link" href="#">
                                                                        Level 5 Directory
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item">
                                                                    <a class="nav-link" href="#">
                                                                        Level 5 Contact
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item">
                                                                    <a class="nav-link" href="#">
                                                                        Level 5 Quick links
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item">
                                                                    <a class="nav-link" href="#">
                                                                        Level 5 Launchpad
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="#">
                                                                Level 4 Directory
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="#">
                                                                Level 4 Contact
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="#">
                                                                Level 4 Quick links
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="#">
                                                                Level 4 Launchpad
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li> -->
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/open-pores-treatment') }}">
                                                        Open Pores
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li class="nav-item nav-expand">
                                            <a class="nav-link nav-expand-link" href="#">
                                                Pigmentation
                                            </a>
                                            <ul class="nav-items nav-expand-content">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/pigmentation-treatment') }}">
                                                        Pigmentation Treatment
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/melasma-treatment') }}">
                                                        Melasma
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Lip, Underarm, Neck
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/dark-circles') }}">
                                                        Dark Circles
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li class="nav-item nav-expand">
                                            <a class="nav-link nav-expand-link" href="#">
                                                Skin Growth
                                            </a>
                                            <ul class="nav-items nav-expand-content">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Wart Removal
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Mole Removal
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/ingrown-toenails') }}">
                                                        Ingrown Toenails
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Skin Tags
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li class="nav-item nav-expand">
                                            <a class="nav-link nav-expand-link" href="#">
                                                Skin Issues
                                            </a>
                                            <ul class="nav-items nav-expand-content">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/keloid') }}">
                                                        Keloid
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/fungal-infection') }}">
                                                        Fungal Infection
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Vitiligo
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Psoriasis
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Rosacea
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li class="nav-item nav-expand">
                                            <a class="nav-link nav-expand-link" href="#">
                                                Marks & Spots
                                            </a>
                                            <ul class="nav-items nav-expand-content">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Stretch Marks
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Dark Spots
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Freckle
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/tattoo-removal') }}">
                                                        Tattoo Removal
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Sun Damage
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item nav-expand">
                                    <a class="nav-link nav-expand-link" href="#">
                                        AESTHETICS
                                    </a>
                                    <ul class="nav-items nav-expand-content">
                                        <li class="nav-item nav-expand">
                                            <a class="nav-link nav-expand-link" href="#">
                                                Facial Rejuvenation
                                            </a>
                                            <ul class="nav-items nav-expand-content">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/skin-lightning') }}">
                                                        Skin Lightning
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/laser-toning') }}">
                                                        Laser Toning
                                                    </a>
                                                </li>

                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/dermal-fillers') }}">
                                                        Dermal Fillers
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Cheek Enhancment
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Lip Enhancement
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Face Contouring
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Skin Boosters
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Thread Lift
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Double Chin Rejuvenation
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li class="nav-item nav-expand">
                                            <a class="nav-link nav-expand-link" href="#">
                                                Facial
                                            </a>
                                            <ul class="nav-items nav-expand-content">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/hydra-facial') }}">
                                                        Hydra Facial
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Chemical Peeling
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/vampire-facial') }}">
                                                        Vampire Facial
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/photofacial') }}">
                                                        Photofacial
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Medi Facial
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/carbon-peel') }}">
                                                        Carbon Peel
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li class="nav-item nav-expand">
                                            <a class="nav-link nav-expand-link" href="#">
                                                Anti Aging
                                            </a>
                                            <ul class="nav-items nav-expand-content">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/fine-lines') }}">
                                                        Fine Lines
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/forehead-lines') }}">
                                                        Forehead Lines
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Anti wrinkle treatment
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Skin Tightening
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Age Spots
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li class="nav-item nav-expand">
                                    <a class="nav-link nav-expand-link" href="#">
                                        HAIR
                                    </a>
                                    <ul class="nav-items nav-expand-content">

                                        <li class="nav-item nav-expand">
                                            <a class="nav-link nav-expand-link" href="#">
                                                Hair Concerns
                                            </a>
                                            <ul class="nav-items nav-expand-content">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/dandruff') }}">
                                                        Dandruff
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('/hair-loss') }}">
                                                        Hair Fall/Loss
                                                    </a>
                                                </li>

                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Hair Baldness in Male
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Hair Baldness in Female
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Premature Greying
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li class="nav-item nav-expand">
                                            <a class="nav-link nav-expand-link" href="#">
                                                Hair Growth
                                            </a>
                                            <ul class="nav-items nav-expand-content">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        (GFC) Growth Factor Concentrate
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Mesotherapy
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Scalp Microneedling
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">
                                                        Laser Light Therapy
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li class="nav-item nav-expand">
                                    <a class="nav-link nav-expand-link" href="#">
                                        Featured Services
                                    </a>
                                    <ul class="nav-items nav-expand-content">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/hair-transplant') }}">
                                                Hair Transplant
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/mirapeel') }}">
                                                MiraPeel
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                Bride & Groom
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                Laser Hair Removal
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/written-testimonials') }}">
                                        Written Testimonials
                                    </a>
                                </li>
                                <!-- <li class="nav-item nav-expand">
                                    <a class="nav-link nav-expand-link" href="#">
                                        Testimonials
                                    </a>
                                    <ul class="nav-items nav-expand-content">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/written-testimonials') }}">
                                                Written Testimonials
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/video-testimonials') }}">
                                                Videos Testimonials
                                            </a>
                                        </li>
                                    </ul>
                                </li> -->
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/blogs') }}">
                                        Blogs
                                    </a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="#">
                                        Real Results
                                    </a>
                                </li> -->
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/clinic-gallery') }}">
                                        Clinic Gallery
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/videos') }}">
                                        Videos
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/contact-us') }}">
                                        Contact Us
                                    </a>
                                </li>
                                <li class="nav-item border_bottom" style="border-right: unset;">
                                    <span class="nav-link">&nbsp;</span>
                                </li>
                                <li class="nav-item border_bottom" style="border-right: unset;">
                                    <span class="nav-link">&nbsp;</span>
                                </li>
                                <li class="nav-item border_bottom" style="border-right: unset;">
                                    <span class="nav-link">&nbsp;</span>
                                </li>
                                <li class="nav-item border_bottom" style="border-right: unset;">
                                    <span class="nav-link">&nbsp;</span>
                                </li>

                            </ul>
                        </nav>
                    </div>
                </div>

            </nav>
        </div>

    </header>
