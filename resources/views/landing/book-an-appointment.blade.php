<form id="appointment_form" class="formBox" action="{{ url('book-an-appointment') }}" method="post">
    <p id="errormsgap" class="text-center" style="color: red; font-weight:bold;"></p>
    <div class="row">

        <div class="col-md-6 col-sm-12 col-12 mb-3"><input name="name" type="text" id="name" class="infld" placeholder="Your Name*" required="">

        </div>

        <div class="col-md-6 col-sm-12 col-12 mb-3">

            <input name="email" type="text" autocomplete="off" class="infld" id="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Your Email*" required="">

        </div>

    </div>

    <div class="row">

        <div class="col-md-6 col-sm-12 col-12 mb-3">

            <input name="phone" id="phone" autocomplete="off" type="text" class="infld" placeholder="Phone*" minlength="10" maxlength="10" required="">

        </div>
        <div class="col-md-6 col-sm-12 col-12 mb-3">
            <input name="date" type="text" autocomplete="off" class="infld dateselect" id="date" autocomplete="off" readonly placeholder="Date*" required="">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-12 mb-3">
            <textarea class="infld" autocomplete="off" rows="2" name="message" placeholder="Message"></textarea>
     

        </div>
        <input type="hidden" name="request_url" value="{{ $_SERVER['REQUEST_URI'] }}">
        
            <input type="hidden" name="referral_url" value="">
        <input type="hidden" name="honey"> 
        <!-- <input type="hidden" class="service_name" name="service_name"> -->
    </div>
    <!-- <div class="row">
        <div class="col-md-12 col-sm-12 col-12 mb-3">
            <div class="g-recaptcha" data-sitekey="6Lcl2kAeAAAAAIjCPywFFvJwO2H98GuAjfDxLM-u" data-callback="verifyCaptcha"></div>
            <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
            <div id="g-recaptcha-error" style="width:100%;"></div>
        </div>
    </div> -->
    <div class="text-center">
        <button id="submit_btn" type="submit" class="btnSubmit">Submit</button>
        <div class="loadme"><img src="{{ url('images/loading_icon.gif') }}" width="40" height="40"></div>
    </div>
</form>