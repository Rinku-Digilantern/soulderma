<?php $rand = rand(9999,99999);
$countparagraph = $_POST['countsec'];
?>

                     <div id="removethreesec{{$rand}}">
                         <div class="form-group col-md-12">
                                <label class="control-label">Select Heading Tag</label>
                                <select class="form-control" id="heading_tag" name="heading_tag[]">
                                    <option value="">Select Heading Tag</option>
                                    <option value="h1" @if(isset($serviceview->button_type))@if($serviceview->button_type=='h1') selected @endif @endif>H1</option>
                                    <option value="h2" @if(isset($serviceview->button_type))@if($serviceview->button_type=='h2') selected @endif @endif>H2</option>
                                    <option value="h3" @if(isset($serviceview->button_type))@if($serviceview->button_type=='h3') selected @endif @endif>H3</option>
                                    <option value="h4" @if(isset($serviceview->button_type))@if($serviceview->button_type=='h4') selected @endif @endif>H4</option>
                                    <option value="h5" @if(isset($serviceview->button_type))@if($serviceview->button_type=='h5') selected @endif @endif>H5</option>
                                    <option value="h6" @if(isset($serviceview->button_type))@if($serviceview->button_type=='h6') selected @endif @endif>H6</option>
                                    <option value="div" @if(isset($serviceview->button_type))@if($serviceview->button_type=='div') selected @endif @endif>div</option>
                                    <option value="p" @if(isset($serviceview->button_type))@if($serviceview->button_type=='p') selected @endif @endif>P</option>
                                </select>
                            </div>
                     <div class="form-group col-md-12">
                      <label class="control-label">Tab Heading</label> 
                      <input class="form-control tabheading{{$rand}}"  name="tab_heading[]" type="text">
                      </div>
                      <div class="form-group col-md-12">
                      <label class="control-label">Tab Image</label> 
                      <input class="form-control tabimage{{$rand}}"  name="tabimage[]" type="file">
                      </div>
                      <div class="form-group col-md-12">
                      <label class="control-label">Tab Heading2</label> 
                      <input class="form-control tabheading2{{$rand}}"  name="tab_heading2[]" type="text">
                      </div>
                      <div class="col-md-11">
        			      	<label class="control-label">Tab Pragraph Section</label>              
                        <textarea class="form-control ckeditor editorleft{{$rand}}" id="editorleft{{$rand}}" name="tabpragraph[]" rows="3"></textarea>
                        <span class="help-block"></span>
                        </div>
                        <div class="col-md-1">
                            <input type="text" class="form-control orderby{{$rand}}" name="orderbytabpragraph[]">
                        <button type="button" class="btn btn-danger marginT38 buttonright" onclick="removethreepragraph('{{$rand}}')">-</button>
                        </div>
                        </div>
                        
                        <script>
    $('.editorleft{{$rand}}').each(function () {
        CKEDITOR.replace($(this).prop('id'));
        $(this).attr('name', 'tabpragraph<?php echo $countparagraph;?>[]');
    });

    $('.orderby{{$rand}}').each(function () {
        $(this).attr('name', 'orderbytabpragraph<?php echo $countparagraph;?>[]');
    });

    $('.tabheading{{$rand}}').each(function () {
        $(this).attr('name', 'tab_heading<?php echo $countparagraph;?>[]');
    });
    $('.tabheading2{{$rand}}').each(function () {
        $(this).attr('name', 'tab_heading2'+count+'[]');
    });
    $('.tabimage{{$rand}}').each(function () {
        $(this).attr('name', 'tabimage<?php echo $countparagraph;?>[]');
    });

    changeeditor('{{$rand}}','{{$countparagraph}}');
</script>