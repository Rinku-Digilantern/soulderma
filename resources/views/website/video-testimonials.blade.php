@include('website.include.header')

    <link rel="stylesheet" href="{{ url('website') }}/css/testimonials.css">


    <section id="topcommonbanner">
        <div class="commonbannerclinic">
            <img src="{{ url('website') }}/img/about-clinic/about-clinic.jpg" class="w-100">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="headingandbredcrumb">
                        <div class="commonheading notofamily commonheadingfontsize">video Testimonials</div>
                        <div class="breadcrumb commonbredcurmb">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a>video Testimonials</a></li>
                            </ul>
                        </div>
                        <div class="commonrequestform">
                            @include('website.include.requestcallback')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="videoteastimonialwrap">
        <div class="container">
            <div class="row">
                @foreach($testimonial as $testi)
                <div class="col-12 col-md-4">
                    <div class="testimonialpagevideowrap">
                        <div class="video-imagetestimona">
                            <img src="{{ url('backend/service_video/inner')}}/{{ $testi->image }}" class="w-100" alt="">
                        </div>
                        <div class="playbutton vid forabsl">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary video-btn" data-bs-toggle="modal"
                                data-src="{{ $testi->video }}" data-bs-target="#myModal"> <span
                                    class="fa fa-play"></span>
                            </button>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>

    <!--common Modal for all video in this page-->
    <section id="commonmodalvideopopup" class="fade-in-out">
        <div class="modal fade modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">x</button>
                        <!-- 16:9 aspect ratio -->
                        <div class="ratio ratio-16x9">
                            <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always"
                                allow="autoplay"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end--common Modal for all video in this page-->
    @include('website.include.requestanappointment')
@include('website.include.footer')
