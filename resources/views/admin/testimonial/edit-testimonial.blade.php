@extends('admin.master')
@section('content')
@php
$primeid = session('primeid');
@endphp
<section id="main-content">
    <section class="wrapper">
        <div class="card cardsec">
            <div class="card-body">
                <div class="page-title pagetitle">
                    <h1>Edit Testimonials</h1>
                    <ul class="breadcrumb side">
                        <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
                        <li><a href="{{url('/admin/testimonials')}}">Testimonials</a></li>
                        <li><a href="{{url('/admin/testimonials')}}">Testimonials List</a></li>
                        <li class="active">Edit Testimonials</li>
                    </ul>
                </div>
            </div>
        </div>
        <form action="{{URL::to('/admin/create_testimonials')}}" id="myForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <div class="card">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="site_id" value="{{session('useradmin')['site_id']}}">
                <input type="hidden" name="org_id" value="{{session('useradmin')['org_id']}}">
                <input type="hidden" name="updated_by" value="{{session('useradmin')['usr_id']}}">
                <input type="hidden" name="test_id" id="test_id" value="@if(isset($primeid)){{$primeid}}@else 0 @endif">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="control-label">Show Type</label>
                            <select class="form-control valid_name" name="test_show_type">
                                <option value="">Select Type</option>
                                <option value="inside" @if(isset($edit->test_show_type))@if($edit->test_show_type=='inside') selected @endif @else selected @endif>Inside</option>
                                <option value="outside" @if(isset($edit->test_show_type))@if($edit->test_show_type=='outside') selected @endif @endif>Outside</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-4 ">
                            <label>Name</label>
                            <input class="form-control valid_name" value="{{$edit->name}}" name="name" type="text">
                        </div>

                        <div class="form-group col-sm-4">
                            <label>Designation</label>
                            <input class="form-control" value="{{$edit->designation}}" name="designation" type="text">
                        </div>
                        <div class="form-group col-sm-4 ">
                            <label>Short Name</label>
                            <input class="form-control" value="{{$edit->short_name}}" name="short_name" type="text">
                        </div>
                        <div class="form-group  col-md-4">
                            <label class="control-label">Testimonial Date</label>
                            <input class="form-control" value="{{$edit->date}}" name="date" type="date">
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Description</label>
                            <textarea class="form-control valid_name" id="textArea" name="description" rows="3">{{$edit->description}}</textarea>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Source</label>
                            <select class="form-control" name="source">
                                <option value="">Select Source</option>
                                <option value="practo" @if(isset($edit->source))@if($edit->source=='practo') selected @endif @endif>Practo</option>
                                <option value="google" @if(isset($edit->source))@if($edit->source=='google') selected @endif @endif>Google</option>
                                <option value="lybrate" @if(isset($edit->source))@if($edit->source=='lybrate') selected @endif @endif>Lybrate</option>
                                <option value="justdial" @if(isset($edit->source))@if($edit->source=='justdial') selected @endif @endif>Justdial</option>
                                <option value="realself" @if(isset($edit->source))@if($edit->source=='realself') selected @endif @endif>Real Self</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Rating</label>
                            <input class="form-control valid_name" value="{{$edit->rating}}" name="rating" type="text">
                        </div>
                        <div class="col-sm-12 marginT30">
                            <button id="myBtn" onclick="testimonial()" class="btn btn-primary icon-btn" type="button">
                                <i class="fa fa-fw fa-lg fa-check-circle"></i>Submit
                            </button>
                            <a class="btn btn-default icon-btn" href="{{url('/admin/testimonials')}}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                        </div>
                    </div>
                </div>
        </form>
    </section>
</section>

<script>
    CKEDITOR.replace('textArea');
</script>



@endsection