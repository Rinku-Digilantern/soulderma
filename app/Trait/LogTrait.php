<?php
namespace App\Trait;
use App\Models\Log;

class LogTrait{

    public static function insertLog($user_id,$name,$email,$url,$remarks){
        $log=Log::create([
            'user_id'=>$user_id,
            'name'=>$name,
            'email'=>$email,
            'url'=>$url,
            'ip'=>$_SERVER['REMOTE_ADDR'],
            'remarks'=>$remarks
        ]);
    }

}
