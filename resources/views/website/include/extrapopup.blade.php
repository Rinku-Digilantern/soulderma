<section id="popupappointmentform">
        <div class="modal fade" id="appointmentPop" tabindex="-1" aria-labelledby="appointmentPopLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="appointmentPopLabel">Book An Appointment</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                 
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="requestpopup">
        <!-- Modal -->
        <div class="modal fade" id="requestPop" tabindex="-1" aria-labelledby="requestPopLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="requestPopLabel">Request a Callback</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form class="row g-3 from-wrap">
    <input type="hidden" id="uniq" name="uncode">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div class="col-auto">
        <input type="text" name="name" autocomplete="off" class="form-control" id="name" onkeyup="validateUsername()" placeholder="Name*" required="">
        <div id="validname" class="error"></div>
    </div>
    <div class="col-auto">
        <input type="text" class="form-control" name="mobile" autocomplete="off" id="mobile" onkeyup="validatePhoneNumber()" minlength="10" maxlength="10" placeholder="Phone*" required="">
        <div id="showmobilemsg" class="error"></div>
    </div>
    <input type="hidden" class="form-control" name="request_url" id="request_call" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <input type="hidden" class="form-control" id="referrer_call" name="referrer">
    <div class="col-auto">
        <div class="d-flex align-items-center commoncaptcawrap ">
            <div class="req-captcha-box  captcha">
                <div class="captchacodeleftpic" id="capt">
                </div>
            </div>
            <div class="button-box">
                <button type="button" class="btnBtnSubmits" onClick="captchareload()"><i class="bi bi-arrow-counterclockwise fa fa-refresh"></i></button>
            </div>
        </div>

    </div>
    <div class="col-auto">

        <input type="text" class="form-control captcha_field" name="captcha" id="captchacode" autocomplete="off" placeholder="Captcha*" maxlength="6">
        <div id="validcaptcha" class="error"></div>

    </div>
    <div class="col-auto">
        <button id="submitcall" type="button" onclick="checkvalidationcallback()" class="showsubmitbutton">SUBMIT</button>
    </div>
</form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--common Modal for all video in this page-->
    <section id="commonmodalvideopopup" class="fade-in-out">
        <div class="modal fade modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">x</button>
                        <!-- 16:9 aspect ratio -->
                        <div class="ratio ratio-16x9">
                            <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always"
                                allow="autoplay"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end--common Modal for all video in this page-->
