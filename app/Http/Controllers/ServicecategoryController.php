<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Models\FrontMenuModel;
use App\Models\SeoPageModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\ServiceModel;
use App\Trait\CategoryTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class ServicecategoryController extends Controller
{
    public function __construct(Helpers $siteurl)
    {
       $this->siteurl = $siteurl;
    }

    public function index()
    {
        session()->forget('primeid');
        $uri = request()->segments()[1];
        $uripermission = [];
        foreach(session('userinfo')['user_operation_permissions'] as $oplist){
           if($oplist->op_link == $uri){
            $uripermission = $oplist->oper_act_id;
            $uripermission = explode(',',$uripermission);
           }
        }
        $data['permission'] = $uripermission;
        $select_table = ['ser_id','parent_id','video_type','service_type', 'service_name','service_image','service_banner_image','video_link','alt_tag','description','short_desc','title_tag','keyword_tag','description_tag','canonical_tag','url', 'order_by', 'status','ser_status'];
        $data['view'] = ServiceModel::select($select_table)->whereNull('parent_id')->get();
        return view('admin.service-category.list-service-category')->with($data);
    }

    public function getactive(Request $request)
    {
        $id = $request->input('id');
        $servicelist = ServiceModel::find($id);
        $servicelist->update($request->input());
    }

    public function orderby_service_category(Request $request)
    {
        $id = $request->input('id');
        $servicelist = ServiceModel::find($id);
        $servicelist->update($request->input());
    }

    public function add()
    {
        $data['view'] = ServiceModel::find(session('primeid'));
        if(isset($data['view']->url)){
            $data['seotag'] = SeoPageModel::select('title_tag', 'keyword_tag','description_tag','canonical_tag','url')
        ->where('url', $data['view']->url)->where('seo_type', 'service'.$data['view']->category_type)->first();
            }
        return view('admin.service-category.add-service-category')->with($data);
    }

        public function ServiceRemoveseccat(Request $request){

            $view = ServiceModel::find(session('primeid'));
    
            $keydata = $request->input('keydata');
    
            $datalistsec = [];
    
            $datalist = json_decode($view->section);
    
            unset($datalist[$keydata]);
    
            foreach($datalist as $key => $datalistsl){
    
               $threepar = $datalistsl->threepragraph;
    
               $tabpra = $datalistsl->tabpragraph;
    
    
    
                if(isset($threepar->threeparagraphdata)){
    
                foreach($threepar->threeparagraphdata as $threekey => $datalistslorder){
    
                    $threepra = [
    
                        'sec_heading' => $threepar->sec_heading,
    
                        'threeparagraphdata' => $threepar->threeparagraphdata,
    
                        'orderby' => $threepar->orderby
    
                    ];
    
                };
    
            }else{
    
                $threepra = [
    
                    'sec_heading' => null,
    
                    'threeparagraphdata' => null,
    
                    'orderby' => null
    
                ];
    
            }
    
    
    
            if(isset($tabpra->tabheadingdata)){
    
                foreach($tabpra->tabheadingdata as $threekey => $datalistslorder){
                    // print_r($tabpra);
                    $tabpragraph = [
    
                        'tabheadingdata' => $tabpra->tabheadingdata,
                        'tabheadingdata2' => $tabpra->tabheadingdata2,
    
                        'tabimage' => $tabpra->tabimage,
    
                        'tab_alttag' => $tabpra->tab_alttag,
    
                        'tabparagraphdata' => $tabpra->tabparagraphdata,
    
                        'taborderby' => $tabpra->taborderby
    
                    ];
    
                };
    
            }else{
    
                $tabpragraph = [
    
                    'tabheadingdata' => null,
    
                    'tabheadingdata2' => null,
    
                    'tabimage' => null,
    
                    'tab_alttag' => null,
    
                    'tabparagraphdata' => null,
    
                    'taborderby' => null
    
                ];
    
            }
    
    
    
            if(isset($datalistsl->secorderby)){
    
                $secorderby = $datalistsl->secorderby;
    
            }else{
    
                $secorderby = null;
    
            }
    
    
    
            if(isset($datalistsl->service_comman)){
    
                $service_comman = $datalistsl->service_comman;
    
            }else{
    
                $service_comman = null;
    
            }
    
            if(isset($datalistsl->service_comman2)){
    
                $service_comman2 = $datalistsl->service_comman2;
    
            }else{
    
                $service_comman2 = null;
    
            }
    
    
    
            if(isset($datalistsl->servicealt_tag)){
    
                $servicealt_tag = $datalistsl->servicealt_tag;
    
            }else{
    
                $servicealt_tag = null;
    
            }
    
    
    
                $datalistsec[] = [
    
                    'type' => $datalistsl->type,
    
                    'secorderby' => $secorderby,
    
                    'heading_tag' => $datalistsl->heading_tag,
    
                    'class_add' => $datalistsl->class_add,
    
                    'section_heading' => $datalistsl->section_heading,
    
                    'service_heading' => $datalistsl->service_heading,
                    'service_heading1' => $datalistsl->service_heading1,
    
                    'button_type' => $datalistsl->button_type,
    
                    'button_name' => $datalistsl->button_name,
    
                    'button_url' => $datalistsl->button_url,
    
                    'button_style' => $datalistsl->button_style,
    
                    'appointment_side' => $datalistsl->appointment_side,
    
                    'bg_type' => $datalistsl->bg_type,
    
                    'bgcolor_style' => $datalistsl->bgcolor_style,
    
                    'bgimage' => $datalistsl->bgimage,
    
                    'tab_type' => $datalistsl->tab_type,
    
                    'section1' => $datalistsl->section1,
    
                    'section2' => $datalistsl->section2,
    
                    'service_comman' => $service_comman,
    
                    'service_comman2' => $service_comman2,
    
                    'image' => $datalistsl->image,
    
                    'alttag' => $datalistsl->alttag,
    
                    'servicealt_tag' => $servicealt_tag,
    
                    'threepragraph' => $threepra,
    
                    'tabpragraph' => $tabpragraph,
    
                ];
    
            }
    
    
    
            // echo '<pre>';print_r($datalistsec);echo '</pre>';die();
    
    
    
            $view->update([
    
                'section' => json_encode($datalistsec)
    
            ]);
    
    
    
        }
    

    public function create_service(Request $request)
    {
        
        $destinationPath = 'backend/service/image';
        $destinationPathbanner = 'backend/service/banner';


        $destinationPathsection = 'backend/service/section';

        $destinationPathsectionbanner = 'backend/service/section_banner';

        if (!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath, $mode = 0777, true, true);
        }

        if (!File::exists($destinationPathbanner)) {
            File::makeDirectory($destinationPathbanner, $mode = 0777, true, true);
        }

        if (!File::exists($destinationPathsection)) {

            File::makeDirectory($destinationPathsection, $mode = 0777, true, true);

        }



        if (!File::exists($destinationPathsectionbanner)) {

            File::makeDirectory($destinationPathsectionbanner, $mode = 0777, true, true);

        }

        $categorytype = $request->input('category_type');
        if($categorytype=='firstcategory'){
            $redirect_blade = 'admin/service-category-preview';
            $redirect_blade_fail = 'admin/add-service-category';
            $redirect_blade_list = 'admin/service-category';
        }elseif($categorytype=='secondcategory'){
            $redirect_blade = 'admin/second-category-preview';
            $redirect_blade_fail = 'admin/add-second-service-category';
            $redirect_blade_list = 'admin/second-category';
        }elseif($categorytype=='thirdcategory'){
            $redirect_blade = 'admin/third-category-preview';
            $redirect_blade_fail = 'admin/add-third-service-category';
            $redirect_blade_list = 'admin/third-category';
        }elseif($categorytype=='fourthcategory'){
            $redirect_blade = 'admin/fourth-category-preview';
            $redirect_blade_fail = 'admin/add-fourth-service-category';
            $redirect_blade_list = 'admin/fourth-category';
        }elseif($categorytype=='fifthcategory'){
            $redirect_blade = 'admin/fifth-category-preview';
            $redirect_blade_fail = 'admin/add-fifth-service-category';
            $redirect_blade_list = 'admin/fifth-category';
        }

        //this is assign plan required field
        $validator = Validator::make($request->input(), [
            'service_name' => 'required',
        ]);
        //Will get redirected if validator is not matched
        if ($validator->fails()) {
            return redirect($redirect_blade_fail);
        }

        if ($request->hasFile('service_image')) {
            $image = $request->file('service_image');
            $name = $categorytype.'_image_'.time().'.'. $image->getClientOriginalExtension();
            $image->move($destinationPath, $name);
        } else {
            $name = $request->input('oldservice_image');
        }

        if ($request->hasFile('service_icon')) {
            $image = $request->file('service_icon');
            $name_icon = $categorytype.'_image_icon_'.time().'.'. $image->getClientOriginalExtension();
            $image->move($destinationPath, $name_icon);
        } else {
            $name_icon = $request->input('oldservice_icon');
        }

        if ($request->hasFile('service_banner_image')) {
            $image = $request->file('service_banner_image');
            $name2 = $categorytype.'_banner_'.time().'.'. $image->getClientOriginalExtension();
            $image->move($destinationPathbanner, $name2);
        } else {
            $name2 = $request->input('oldservice_banner_image');
        }


$servicesection = $request->input('servicesection');
$datalist  = [];
// echo "<pre>";
// print_r($request->input()); echo "</pre>"; die;
if(isset($servicesection)){

    foreach ($request->input('servicesection') as $key => $servicesection) {

    

        $rand = rand(99, 999);
        
    
                if (isset($request->input('service_section_id')[$key])) {

                    $sersecid = $request->input('service_section_id')[$key];

                } else {

                    $sersecid = '';

                }

                  //for heading  tags
                if (isset($request->input('heading_tag')[$key])) {
                    $heading_tag = $request->input('heading_tag')[$key];
                } else {
                    $heading_tag = null;
                }

                // print_r($request->file('serviceimage'));

                if (isset($request->file('serviceimage')[$key])) {

                    $filename = time() . $rand;

                    $image = $request->file('serviceimage')[$key];

                    $name3 = $filename . '.' . $image->getClientOriginalExtension();

                    $image->move($destinationPathsection, $name3);



                } else {

                    if (isset($request->input('oldserviceimage')[$key])) {

                        $name3 = $request->input('oldserviceimage')[$key];

                    } else {

                        $name3 = null;

                    }

                }

                // echo $name3;die;

                if(isset($request->input('bg_type')[$key])){

                if ($request->input('bg_type')[$key] == 'bgimage') {

                if (isset($request->file('bgimage')[$key])) {

                    $filename = time() . $rand;

                    $image4 = $request->file('bgimage')[$key];

                    $name4 = $filename . '_bannersction.' . $image4->getClientOriginalExtension();

                    $image4->move($destinationPathsectionbanner, $name4);



                } else {

                    if (isset($request->input('oldbgimage')[$key])) {

                        $name4 = $request->input('oldbgimage')[$key];

                    } else {

                        $name4 = null;

                    }

                }

            }else{

                $name4 = null;

            }

        }else{

            $name4 = null;

        }





                if ($request->input('button_type')[$key] == 'Yes') {

                    $button_name = $request->input('button_name')[$key];

                    $button_url = $request->input('button_url')[$key];

                    $button_style = $request->input('button_style')[$key];

                } else {

                    $button_name = null;

                    $button_url = null;

                    $button_style = null;

                }



                if(isset($request->input('bg_type')[$key])){

                if ($request->input('bg_type')[$key] == 'bgcolor') {

                    $bgcolor_style = $request->input('bgcolor_style')[$key];

                } else {

                    $bgcolor_style = null;

                }

            }else{

                $bgcolor_style = null;

            }



            if(isset($request->input('appointment_side')[$key])){

                    $appointment_side = $request->input('appointment_side')[$key];

            }else{

                $appointment_side = null;

            }



            if(isset($request->input('bg_type')[$key])){

                $bg_type = $request->input('bg_type')[$key];

            }else{

                $bg_type = null;

            }





                if(isset($request->input('service1')[$key])){

                    if($request->input('service1')[$key] != ''){

                    $service1 = $request->input('service1')[$key];

                    }else{

                        $service1 = null;

                    }

                } else {

                    $service1 = null;

                }





                if($request->input('service_type')[$key] == 'threeparagraph'){

                    $service2 = null;

            }else{

                if(isset($request->input('service2')[$key])){

                    $service2 = $request->input('service2')[$key];

                } else {

                    $service2 = null;

                }



            }

            if(isset($request->input('service_comman')[$key])){

                   $service_comman = $request->input('service_comman')[$key];

                }else{

                    $service_comman = null;

                }

                if(isset($request->input('service_comman2')[$key])){

                   $service_comman2 = $request->input('service_comman2')[$key];

                }else{

                    $service_comman2 = null;

                }

                if(isset($request->input('secorderby')[$key])){

                   $secorderby = $request->input('secorderby')[$key];

                }else{

                    $secorderby = null;

                }



                if(isset($request->input('service_heading1')[$key])){

                    $service_heading1 = $request->input('service_heading1')[$key];

                 }else{

                     $service_heading1 = null;

                 }



                 if(isset($request->input('service_heading1')[$key])){

                    $service_heading1 = $request->input('service_heading1')[$key];

                 }else{

                     $service_heading1 = null;

                 }



                 if(isset($request->input('tab_type')[$key])){

                    $tab_type = $request->input('tab_type')[$key];

                 }else{

                     $tab_type = null;

                 }

                 if(isset($request->input('alttag')[$key])){

                    $alttag = $request->input('alttag')[$key];

                 }else{

                     $alttag = null;

                 }



                 if(isset($request->input('class_add')[$key])){

                    $classadd = $request->input('class_add')[$key];

                 }else{

                     $classadd = null;

                 }



                 if(isset($request->input('section_heading')[$key])){

                    $section_heading = $request->input('section_heading')[$key];

                 }else{

                     $section_heading = null;

                 }



                 $ders = $request->input('threepragraph'.$key+1);

                 if(isset($ders)){

                    $sec_heading = $request->input('sec_heading'.$key+1);

                    $threepragraph = $request->input('threepragraph'.$key+1);

                    $threedesignstart = $request->input('threedesignstart'.$key+1);

                    $orderbypragraph = $request->input('orderbypragraph'.$key+1);

                 }else{

                    $sec_heading = null;

                    $threepragraph = null;

                    $threedesignstart = null;

                    $orderbypragraph = null;

                }

                $dff = $request->input('tab_heading'.$key+1);

                // print_r($dff);

                if(isset($dff)){

                    $tabheading = $request->input('tab_heading'.$key+1);

                    $tabheading2 = $request->input('tab_heading2'.$key+1);

                    $tab_alttag = $request->input('tab_alttag'.$key+1);

                    $tabpragraph = $request->input('tabpragraph'.$key+1);

                    $orderbytabpragraph = $request->input('orderbytabpragraph'.$key+1);

                 }else{

                    $tabheading = null;

                    $tabheading2 = null;

                    $tab_alttag = null;

                    $tabpragraph = null;

                    $orderbytabpragraph = null;

                }



                if(isset($dff)){

                    foreach($request->input('tab_heading'.$key+1) as $threekey1 => $datalistslorder){

                        if (isset($request->file('tabimage'.$key+1)[$threekey1])) {

                            $randtab = rand('99999','999999');

                            $filename = time() . $randtab;

                            $image5 = $request->file('tabimage'.$key+1)[$threekey1];

                            $name5 = $filename . '.' . $image5->getClientOriginalExtension();

                            $image5->move($destinationPathsection, $name5);

                            $tabimg[] = $name5;

                        } else {

                            if (isset($request->input('oldtabimage'.$key+1)[$threekey1])) {

                                $tabimg[] = $request->input('oldtabimage'.$key+1)[$threekey1];

                            } else {

                                $tabimg = null;

                            }

                        }

                    }



                }else{

                    $tabimg = null;

                }



        // print_r($service_comman);

            $service_type = $request->input('service_type')[$key];

            $datalist[] = [

            'type' => $request->input('service_type')[$key],
            'heading_tag'=>$heading_tag,//new code
            'secorderby' => $secorderby,

            'section_heading' => $section_heading,

            'service_heading' => $request->input('service_heading')[$key],

            'service_heading1' => $service_heading1,

            'button_type' => $request->input('button_type')[$key],

            'button_name' => $button_name,

            'button_url' => $button_url,

            'button_style' => $button_style,

            'class_add' => $classadd,

            'appointment_side' => $appointment_side,

            'bg_type' => $bg_type,

            'bgcolor_style' => $bgcolor_style,

            'bgimage' => $name4,

            'tab_type' => $tab_type,

            'section1' => $service1,

            'section2' => $service2,

            'service_comman' => $service_comman,

            'service_comman2' => $service_comman2,

            'image' => $name3,

            'alttag' => $alttag,

            'threepragraph' => [

                'sec_heading' => $sec_heading,

                'threeparagraphdata' => $threepragraph,

                'threedesignstart' => $threedesignstart,

                'orderby' => $orderbypragraph,

            ],

            'tabpragraph' => [

                'tabheadingdata' => $tabheading,

                'tabheadingdata2' => $tabheading2,

                'tabimage' => $tabimg,

                'tab_alttag' => $tab_alttag,

                'tabparagraphdata' => $tabpragraph,

                'taborderby' => $orderbytabpragraph,

            ],

            ];

            }

        }
        
        // die;

    //         echo '<pre>';print_r($datalist);echo '</pre>';

    // die();

        $parent_id = null;

        $permissionoprid = session('userinfo')['user_category_operation_permissions'][0]['opid'];

        $permissionoprid = explode(',', $permissionoprid);

        // if (in_array('39', $permissionoprid)) {

        //     $parent_id = $request->input('fifth_cat');

        // } else {

        //     if (in_array('38', $permissionoprid)) {

        //         $parent_id = $request->input('fourt_cat');

        //     } else {

        //         if (in_array('37', $permissionoprid)) {

        //             $parent_id = $request->input('third_cat');

        //         } else {

        //             if (in_array('36', $permissionoprid)) {

        //                 $parent_id = $request->input('second_cat');

        //             } else {

        //                 if (in_array('16', $permissionoprid)) {

        //                     $parent_id = $request->input('service_cat');

        //                 }

        //             }

        //         }

        //     }

        // }



        // if($request->input('show_type')=='outside'){

        //     $homedescription = $request->input('homedescription');

        // }else{

        //     $homedescription = null;

        // }
        //

      $ara =  array_merge(
            $request->input(),
            [
                'service_image' => $name,
                'service_banner_image' => $name2,
                'service_icon' => $name_icon,
                'section' => json_encode($datalist)
            ]);
        //    echo '<pre>'; print_r($ara);die;echo '</pre>';
        $ser_id = $request->input('ser_id');
        if($ser_id < 1){
            // print_r($ara);die;
            $service = ServiceModel::create([
                'category_type' => $request->input('category_type'),
                'design_type' => $request->input('design_type'),
                'parent_id' => $request->input('parent_id'),
                'service_name' => $request->input('service_name'),
                'video_type' => $request->input('video_type'),
                'video_link' => $request->input('video_link'),
                'alt_tag' => $request->input('alt_tag'),
                'short_desc' => $request->input('short_desc'),
                'description' => $request->input('description'),
                'title_tag' => $request->input('title_tag'),
                'keyword_tag' => $request->input('keyword_tag'),
                'description_tag' => $request->input('description_tag'),
                'canonical' => $request->input('canonical'),
                'url' => $request->input('url'),
                'service_image' => $name,
                'service_banner_image' => $name2,
                'service_icon' => $name_icon,
                'created_by' => $request->input('created_by'),
                'section' => json_encode($datalist)
            ]);
        Session::put('primeid', $service->ser_id);
    }else{
        $servicedata = ServiceModel::find($ser_id);
        $servicedata->update([
            'category_type' => $request->input('category_type'),
            'design_type' => $request->input('design_type'),
            'parent_id' => $request->input('parent_id'),
            'service_name' => $request->input('service_name'),
            'video_type' => $request->input('video_type'),
            'video_link' => $request->input('video_link'),
            'alt_tag' => $request->input('alt_tag'),
            'short_desc' => $request->input('short_desc'),
            'description' => $request->input('description'),
            'title_tag' => $request->input('title_tag'),
            'keyword_tag' => $request->input('keyword_tag'),
            'description_tag' => $request->input('description_tag'),
            'canonical' => $request->input('canonical'),
            'url' => $request->input('url'),
            'service_image' => $name,
            'service_banner_image' => $name2,
            'service_icon' => $name_icon,
            'created_by' => $request->input('created_by'),
            'section' => json_encode($datalist)
        ]);
        }
        // echo '<pre>';print_r(json_encode($datalist));echo '</pre>';die;
        if($request->input('url') != '#'){
            $dataseo = [
                'seo_type' => 'service'.$categorytype,
                'page_name' => $request->input('service_name'),
                'title_tag' => $request->input('title_tag'),
                'keyword_tag' => $request->input('keyword_tag'),
                'description_tag' => $request->input('description_tag'),
                'canonical_tag' => $request->input('canonical_tag'),
                'image' => $name,
                'type' => $request->input('type'),
                'site_name' => $request->input('site_name'),
                'url' => $request->input('url')
            ];
            $seo = SeoPageModel::where('url',$request->input('oldurl'))->where('seo_type','service'.$categorytype)->where('seo_status','preview');
            // echo $seo->count();
            // die();
            if($seo->count() < 1){
            SeoPageModel::create(array_merge($dataseo,['created_by' => session('useradmin')['usr_id']]));
            }else{
                $seo->update(array_merge($dataseo,[
                    'updated_by' => session('useradmin')['usr_id'],
                    'seo_key' => $seo->first()->seo_key,
                    'seo_type' => 'service'.$categorytype,
                    ]));
            }
        }

        $frontmenu = FrontMenuModel::where('urllink',$request->input('oldurl'))->where('menu_status','preview');

            if($frontmenu->count() > 0){
                $frontmenu->update(array_merge([
                    'updated_by' => session('useradmin')['usr_id'],
                    'urllink' => $request->input('url')
                ]));
            }

        return redirect($redirect_blade_list);
    }

    public function edit_service($id)
    {
        Session::put('primeid', $id);
        $data['edit'] = ServiceModel::find($id);
        $data['firstcategory'] = ServiceModel::select('ser_id','service_name')->whereNull('parent_id')
        ->where('category_type','firstcategory')->get();

        $data['seotag'] = SeoPageModel::select('title_tag', 'keyword_tag','description_tag','canonical_tag','url')
        ->where('url', $data['edit']->url)->where('seo_type', 'service'.$data['edit']->category_type)->first();

        if($data['edit']->category_type=='firstcategory'){
            $redirect_blade = 'admin.service-category.edit-service-category';
        }elseif($data['edit']->category_type=='secondcategory'){
            $redirect_blade = 'admin.service-category-second.edit-service-category-second';
        }

        return view($redirect_blade)->with($data);
    }

    public function SecondIndex()
    {
        session()->forget('primeid');
        $uri = request()->segments()[1];
        $uripermission = [];
        foreach(session('userinfo')['user_operation_permissions'] as $oplist){
           if($oplist->op_link == $uri){
            $uripermission = $oplist->oper_act_id;
            $uripermission = explode(',',$uripermission);
           }
        }
        $data['permission'] = $uripermission;
        $select_table = ['ser_id','parent_id','video_type','service_type', 'service_name','service_image','service_banner_image','video_link','alt_tag','description','short_desc','title_tag','keyword_tag','description_tag','canonical_tag','url', 'order_by', 'status','ser_status'];
        $data['view'] = ServiceModel::select($select_table)->whereNull('parent_id')->where('category_type','firstcategory')->get();
        $data['secondcatview'] = ServiceModel::select($select_table)->whereNotNull('parent_id')->where('category_type','secondcategory')->get();

        return view('admin.service-category-second.list-service-category-second')->with($data);
    }

    public function SecondAdd()
    {
        $data['firstcategory'] = ServiceModel::select('ser_id','service_name')->whereNull('parent_id')
        ->where('category_type','firstcategory')->get();
        $data['view'] = ServiceModel::find(session('primeid'));
        if(isset($data['view']->url)){
            $data['seotag'] = SeoPageModel::select('title_tag', 'keyword_tag','description_tag','canonical_tag','url')
            ->where('url', $data['view']->url)->where('seo_type', 'service'.$data['view']->category_type)->first();
            }
        return view('admin.service-category-second.add-service-category-second')->with($data);
    }

    public function ThirdIndex()
    {
        session()->forget('primeid');
        $uri = request()->segments()[1];
        $uripermission = [];
        foreach(session('userinfo')['user_operation_permissions'] as $oplist){
           if($oplist->op_link == $uri){
            $uripermission = $oplist->oper_act_id;
            $uripermission = explode(',',$uripermission);
           }
        }
        $data['permission'] = $uripermission;
        $select_table = ['ser_id','parent_id','video_type','service_type', 'service_name','service_image','service_banner_image','video_link','alt_tag','description','short_desc','title_tag','keyword_tag','description_tag','canonical_tag','url', 'order_by', 'status','ser_status'];
        $data['secondcatview'] = ServiceModel::select($select_table)->whereNotNull('parent_id')->where('category_type','secondcategory')->get();
        $data['thirdcatview'] = ServiceModel::select($select_table)->whereNotNull('parent_id')->where('category_type','thirdcategory')->get();
        return view('admin.service-category-third.list-service-category-third')->with($data);
    }

    public function ThirdAdd()
    {
        $data['firstcategory'] = ServiceModel::select('ser_id','service_name')->whereNull('parent_id')
        ->where('category_type','firstcategory')->get();
        $data['view'] = ServiceModel::find(session('primeid'));
        if(isset($data['view']->parent_id)){
            $data['secondsec'] = CategoryTrait::allcategorylist($data['view']->parent_id);
        }
        if(isset($data['view']->url)){
            $data['seotag'] = SeoPageModel::select('title_tag', 'keyword_tag','description_tag','canonical_tag','url')
        ->where('url', $data['view']->url)->where('seo_type', 'service'.$data['view']->category_type)->first();
            }
        return view('admin.service-category-third.add-service-category-third')->with($data);
    }

    public function ThirdEditService($id)
    {
        Session::put('primeid', $id);
        $data['firstcategory'] = ServiceModel::select('ser_id','service_name')->whereNull('parent_id')
        ->where('category_type','firstcategory')->get();
        $data['edit'] = ServiceModel::find($id);

        $data['seotag'] = SeoPageModel::select('title_tag', 'keyword_tag','description_tag','canonical_tag','url')
        ->where('url', $data['edit']->url)->where('seo_type', 'service'.$data['edit']->category_type)->first();

        if(isset($data['edit']->parent_id)){
        $data['secondsec'] = CategoryTrait::allcategorylist($data['edit']->parent_id);
        }
        return view('admin.service-category-third.edit-service-category-third')->with($data);
    }

    public function ThirdCategoryPreview()
    {
        $data['view'] = ServiceModel::find(session('primeid'));
        $data['secondsec'] = CategoryTrait::allcategorylist($data['view']->parent_id);
        return view('admin.service-category-third.preview-service-category-third')->with($data);
    }

    public function FourthIndex()
    {
        session()->forget('primeid');
        $uri = request()->segments()[1];
        $uripermission = [];
        foreach(session('userinfo')['user_operation_permissions'] as $oplist){
           if($oplist->op_link == $uri){
            $uripermission = $oplist->oper_act_id;
            $uripermission = explode(',',$uripermission);
           }
        }
        $data['permission'] = $uripermission;
        $select_table = ['ser_id','parent_id','video_type','service_type', 'service_name','service_image','service_banner_image','video_link','alt_tag','description','short_desc','title_tag','keyword_tag','description_tag','canonical_tag','url', 'order_by', 'status','ser_status'];
        $data['thirdcatview'] = ServiceModel::select($select_table)->whereNotNull('parent_id')->where('category_type','thirdcategory')->get();
        $data['fourthcatview'] = ServiceModel::select($select_table)->whereNotNull('parent_id')->where('category_type','fourthcategory')->get();
        return view('admin.service-category-fourth.list-service-category-fourth')->with($data);
    }

    public function FourthAdd()
    {
        $data['firstcategory'] = ServiceModel::select('ser_id','service_name')->whereNull('parent_id')
        ->where('category_type','firstcategory')->get();
        $data['view'] = ServiceModel::find(session('primeid'));
        if(isset($data['view']->parent_id)){
            $data['secondsec'] = CategoryTrait::allcategorylist($data['view']->parent_id);
        }

        if(isset($data['view']->url)){
            $data['seotag'] = SeoPageModel::select('title_tag', 'keyword_tag','description_tag','canonical_tag','url')
            ->where('url', $data['view']->url)->where('seo_type', 'service'.$data['view']->category_type)->first();
        }

        return view('admin.service-category-fourth.add-service-category-fourth')->with($data);
    }

    public function FourthEditService($id)
    {
        Session::put('primeid', $id);
        $data['firstcategory'] = ServiceModel::select('ser_id','service_name')->whereNull('parent_id')
        ->where('category_type','firstcategory')->get();
        $data['edit'] = ServiceModel::find($id);
        if(isset($data['edit']->parent_id)){
            $data['secondsec'] = CategoryTrait::allcategorylist($data['edit']->parent_id);
        }

        $data['seotag'] = SeoPageModel::select('title_tag', 'keyword_tag','description_tag','canonical_tag','url')
        ->where('url', $data['edit']->url)->where('seo_type', 'service'.$data['edit']->category_type)->first();

        return view('admin.service-category-fourth.edit-service-category-fourth')->with($data);
    }

    public function FourthCategoryPreview()
    {
        $data['view'] = ServiceModel::find(session('primeid'));
        $data['secondsec'] = CategoryTrait::allcategorylist($data['view']->parent_id);
        return view('admin.service-category-fourth.preview-service-category-fourth')->with($data);
    }

    public function FifthIndex()
    {
        session()->forget('primeid');
        $uri = request()->segments()[1];
        $uripermission = [];
        foreach(session('userinfo')['user_operation_permissions'] as $oplist){
           if($oplist->op_link == $uri){
            $uripermission = $oplist->oper_act_id;
            $uripermission = explode(',',$uripermission);
           }
        }
        $data['permission'] = $uripermission;
        $select_table = ['ser_id','parent_id','video_type','service_type', 'service_name','service_image','service_banner_image','video_link','alt_tag','description','short_desc','title_tag','keyword_tag','description_tag','canonical_tag','url', 'order_by', 'status','ser_status'];
        $data['fourthcatview'] = ServiceModel::select($select_table)->whereNotNull('parent_id')->where('category_type','fourthcategory')->get();
        $data['fifthcatview'] = ServiceModel::select($select_table)->whereNotNull('parent_id')->where('category_type','fifthcategory')->get();
        return view('admin.service-category-fifth.list-service-category-fifth')->with($data);
    }

    public function FifthAdd()
    {
        $data['firstcategory'] = ServiceModel::select('ser_id','service_name')->whereNull('parent_id')
        ->where('category_type','firstcategory')->get();
        $data['view'] = ServiceModel::find(session('primeid'));
        if(isset($data['view']->parent_id)){
            $data['secondsec'] = CategoryTrait::allcategorylist($data['view']->parent_id);
        }
        if(isset($data['view']->url)){
            $data['seotag'] = SeoPageModel::select('title_tag', 'keyword_tag','description_tag','canonical_tag','url')
        ->where('url', $data['view']->url)->where('seo_type', 'service'.$data['view']->category_type)->first();
            }
        return view('admin.service-category-fifth.add-service-category-fifth')->with($data);
    }

    public function FifthEditService($id)
    {
        Session::put('primeid', $id);
        $data['firstcategory'] = ServiceModel::select('ser_id','service_name')->whereNull('parent_id')
        ->where('category_type','firstcategory')->get();
        $data['edit'] = ServiceModel::find($id);

        $data['seotag'] = SeoPageModel::select('title_tag', 'keyword_tag','description_tag','canonical_tag','url')
        ->where('url', $data['edit']->url)->where('seo_type', 'service'.$data['edit']->category_type)->first();

        if(isset($data['edit']->parent_id)){
            $data['secondsec'] = CategoryTrait::allcategorylist($data['edit']->parent_id);
        }
        return view('admin.service-category-fifth.edit-service-category-fifth')->with($data);
    }

    public function FifthCategoryPreview()
    {
        $data['view'] = ServiceModel::find(session('primeid'));
        $data['secondsec'] = CategoryTrait::allcategorylist($data['view']->parent_id);
        return view('admin.service-category-fifth.preview-service-category-fifth')->with($data);
    }

    // public function SecondCategory(Request $request)
    // {
    //     $ser_id = $request->input('servicecategory');
    //     // $secondsec = CategoryTrait::allcategorylist($ser_id);
    //     $secondsec = ServiceModel::tree();
    //     return response()->json(['data'=>$secondsec]);
    // }

    public function SecondCategory(Request $request)
    {
        $ser_id = $request->input('servicecategory');
        $select_table = ['ser_id', 'service_name'];
        $data = ServiceModel::select($select_table)->where('parent_id', $ser_id)->get();
        echo "<option value=''>Select Service</option>";
        foreach ($data as $datalist) {
            echo "<option value='{$datalist->ser_id}'>{$datalist->service_name}</option>";
        }
    }

    public function ThirdCategory(Request $request)
    {
        $ser_id = $request->input('secondcategory');
        $select_table = ['ser_id', 'service_name'];
        $data = ServiceModel::select($select_table)->where('parent_id', $ser_id)->get();
        echo "<option value=''>Select Service</option>";
        foreach ($data as $datalist) {
            echo "<option value='{$datalist->ser_id}'>{$datalist->service_name}</option>";
        }
    }

    public function FourthCategory(Request $request)
    {
        $ser_id = $request->input('thirdcategory');
        $select_table = ['ser_id', 'service_name'];
        $data = ServiceModel::select($select_table)->where('parent_id', $ser_id)->get();
        echo "<option value=''>Select Service</option>";
        foreach ($data as $datalist) {
            echo "<option value='{$datalist->ser_id}'>{$datalist->service_name}</option>";
        }
    }

    public function FifthCategory(Request $request)
    {
        $ser_id = $request->input('fourthcategory');
        $select_table = ['ser_id', 'service_name'];
        $data = ServiceModel::select($select_table)->where('parent_id', $ser_id)->get();
        echo "<option value=''>Select Service</option>";
        foreach ($data as $datalist) {
            echo "<option value='{$datalist->ser_id}'>{$datalist->service_name}</option>";
        }
    }

    public function ServiceSection(Request $request)
    {
        $ser_id = $request->input('fifthcategory');
        $select_table = ['ser_id', 'service_name'];
        $data = ServiceModel::select($select_table)->where('parent_id', $ser_id)->get();
        echo "<option value=''>Select Service</option>";
        foreach ($data as $datalist) {
            echo "<option value='{$datalist->ser_id}'>{$datalist->service_name}</option>";
        }
    }

    // public function SecondEditService($id)
    // {
    //     Session::put('primeid', $id);
    //     $data = ServiceModel::find($id);
    //     return view('admin.service-category-second.edit-service-category-second', ['edit' => $data]);
    // }

    // public function CategoryPreviewSecond()
    // {
    //     $data = ServiceModel::find(session('primeid'));
    //     return view('admin.service-category-second.preview-service-category-second', ['view' => $data]);
    // }

    // public function update_service(Request $request, $id)
    // {
    //     $servicecatlist = ServiceCategoryModel::find($id);

    //     if ($request->hasFile('service_image')) {
    //         $image = $request->file('service_image');
    //         $name = time() . '_banner.' . $image->getClientOriginalExtension();
    //         $destinationPath = Session::get('useradmin')['site_url'].'backend/service_category/image';
    //         $image->move($destinationPath, $name);
    //     } else {
    //         $name = $request->input('oldservice_image');
    //     }

    //     if ($request->hasFile('image')) {
    //         $image = $request->file('image');
    //         $name2 = time() . '.' . $image->getClientOriginalExtension();
    //         $destinationPath = Session::get('useradmin')['site_url'].'backend/service_category/image';
    //         $image->move($destinationPath, $name2);
    //     } else {
    //         $name2 = $request->input('oldimage');
    //     }

    //     if ($request->input('video_type') == 'image') {
    //         $videolink = '';
    //     } else {
    //         $videolink = $request->input('video_link');
    //     }
    //     $servicecatlist->update(array_merge(
    //         $request->input(),
    //         [
    //             'service_video' =>  $videolink,
    //             'service_image' => $name,
    //             'image' => $name2,
    //         ]
    //     ));

    //     return redirect('admin/service-category');
    // }
    public function delete_service($id)
    {
        $servicelist = ServiceModel::find($id);
        $servicelist->update(['deleted_by' => session('useradmin')['usr_id']]);
        $servicelist->delete();
        return redirect('admin/service-category');
    }
}
