<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-4 col-md-12 commonborderright">
                <div class="footerlogocontent commonfooterpadding">
                    <div class="footerlogo">
                        <a href="{{ url('/') }}"><img src="{{ url('website') }}/img/footerlogo.png"></a>
                    </div>
                    <div class="footertext">
                        <p>Dr Anika Goel is a renowned cosmetic dermatologist and hair transplant surgeon. She has extensive experience of more than 10 years. She has done her M.D. Dermatology, Venereology and Leprosy - Sri Guru Ram Das Institute of Medical Science and Research, Amritsar.</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-2 col-md-3 commonborderright">
                <div class="footerlinkwrap commonfooterpadding">
                    <div class="footerlinksheading notofamily">SKIN</div>
                    <ul>
                        <li><a href="{{ url('/acne') }}">ACNE</a></li>
                        <li><a href="{{ url('/pigmentation') }}">PIGMENTATION</a></li>
                        <li><a href="{{ url('/skin-growth') }}">SKIN GROWTH</a></li>
                        <li><a href="{{ url('/skin-issues') }}">SKIN ISSUES</a></li>
                        <li><a href="{{ url('/marks-and-spots') }}">MARKS & SPOTS</a></li>
                    </ul>
                </div>
            </div>

            <div class=" col-6 col-lg-2 col-md-3 commonborderright">
                <div class="footerlinkwrap commonfooterpadding">
                    <div class="footerlinksheading notofamily">AESTHETICS</div>
                    <ul>
                        <li><a href="{{ url('/facial-rejuvenation') }}">FACIAL REJUVENATION</a></li>
                        <li><a href="{{ url('/facials') }}">FACIALS</a></li>
                        <li><a href="{{ url('/anti-aging') }}">ANTI AGING</a></li>
                    </ul>
                </div>
            </div>


            <div class="col-6 col-lg-2 col-md-3 commonborderright">
                <div class="footerlinkwrap commonfooterpadding">
                    <div class="footerlinksheading notofamily">HAIR</div>
                    <ul>
                        <li><a href="{{ url('/hair-concerns') }}">HAIR CONCERNS</a></li>
                        <li><a href="#">HAIR GROWTH</a></li>
                    </ul>
                </div>
            </div>

            <div class=" col-lg-2 col-md-3 col-sm-12 commonborderright our_usefull_link ">
                <div class="footerlinkwrap commonfooterpadding">
                    <div class="footerlinksheading notofamily">USEFUL LINKS</div>
                    <ul>
                        <li><a href="{{ url('skin-specialist') }}">ABOUT DOCTOR</a></li>
                        <li><a href="{{ url('/skin-clinic') }}">ABOUT CLINIC</a></li>
                        <li><a href="{{ url('/videos') }}">VIDEOS</a></li>
                        <li><a href="{{ url('/written-testimonials') }}">WRITTEN TESTIMONIALS</a></li>
                        <li><a href="{{ url('/blogs') }}">BLOGS</a></li>
                        <li><a href="{{ url('/contact-us') }}">CONTACT US</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="footerlinks">
                    <div class="fotersocialmedia for-desktop">
                        <ul>
                            <li><a href="https://www.facebook.com/souldermaclinic/" target="_blank"><i class="fa fa-facebook"></i></a> </li>
                            <li> <a href="https://www.instagram.com/soul_derma_clinic/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://www.youtube.com/@SoulDermaClinic"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <section id="footercontent" class="for-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-4 col-md-12">
                        <div class="appointaddresswrap">
                            <div class="apadressicon">
                                <img src="{{ url('website') }}/img/location-icon.png">
                            </div>
                            <div class="aplocationcontentwrap">
                                <div class="aplocationheading">ADDRESS</div>
                                <div class="detailaddress">
                                    <p>Ground Floor W-58, Greater Kailash I, New Delhi, Delhi 110048 India</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 col-md-12">
                        <div class="appointaddresswrap">
                            <div class="apadressicon">
                                <img src="{{ url('website') }}/img/content-icon.png">
                            </div>
                            <div class="aplocationcontentwrap">
                                <div class="aplocationheading">MOBILE NO</div>
                                <div class="apmobilenumber">
                                    <a href="tel:+91-8595941529">+91-8595941529</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 col-md-12">
                        <div class="appointaddresswrap">
                            <div class="apadressicon">
                                <img src="{{ url('website') }}/img/email-icon.png">
                            </div>
                            <div class="aplocationcontentwrap">
                                <div class="aplocationheading">EMAIL ID</div>
                                <div class="detailaddress">
                                    <p><a href="mailto:info@souldermaclinic.com">info@souldermaclinic.com</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="row managebottomfooter">
            <div class="col-12 col-lg-6 col-md-12">
                <div class="fotersocialmedia for-mobile">
                    <ul>
                        <li><a href="https://www.facebook.com/souldermaclinic/" target="_blank"><i class="fa fa-facebook"></i></a> </li>
                        <li> <a href="https://www.instagram.com/souldermaclinic/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                </div>
                <div class="copyrightwrap">
                    <p>&copy; 2023 - 2024, Soul Derma. All Rights Reserved.</p>
                </div>
            </div>

            <div class="col-12 col-lg-6 col-md-12">
                <div class="treamsconditionwrap">
                    <ul>
                        <li><a href="#">PRIVACY POLICY</a></li>
                        <li><a href="#">TERMS & CONDITIONS</a></li>
                        <li><a href="#">DISCLAIMER</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</footer>


<!---------------------------------------------Cta----------------------------->

<a id="whatsappicon" target="_blank" href="https://api.whatsapp.com/send?phone=918595941529&amp;text=Hello Doctor, I have contacted you through Soulderma website."></a>

<a id="button"> </a>


<section id="mobilecta" class="for-mobile">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta-wrap">
                    <div class="mail-cta commonmobilecta">
                        <a href="mailto:info@souldermaclinic.com">
                            <img src="{{ url('website') }}/img/mail-icon.png">
                            <p>Email</p>
                        </a>
                    </div>
                    <div class="calender-cta commonmobilecta">
                        <a href="{{ url('/book-an-appointment') }}">
                            <img src="{{ url('website') }}/img/calender-icon.png">
                            <p>Appointment</p>
                        </a>
                    </div>
                    <div class="call-cta commonmobilecta">
                        <a href="tel:+91-8595941529">
                            <img src="{{ url('website') }}/img/call-icon.png">
                            <p>Call Us</p>
                        </a>
                    </div>
                    <!-- <div class="service-cta commonmobilecta">
                        <a href="#">
                            <img src="{{ url('website') }}/img/services-icon.png">
                            <p>Services</p>
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-------------------------------------------end--Cta----------------------------->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ url('website') }}/js/bs.js" defer></script>
<script src="{{ url('website') }}/js/fiveservices.js"></script>
<script type="text/javascript" src="{{ url('website') }}/js/owl.carousel.min.js" defer></script>
<script type="text/javascript" src="{{ url('website') }}/js/lightbox.js" defer></script>
<script src="{{ url('website') }}/js/sidebar-menu.js"></script>
<script type="text/javascript" src="{{ url('website') }}/js/custom.js" defer></script>
<script type="text/javascript">
    (() => {
        var p = t => {
                let o = document.readyState;
                if (typeof $ != "function") {
                    setTimeout(() => {
                        p(t)
                    }, 100);
                    return
                }
                o === "complete" || o !== "loading" && !document.documentElement.doScroll ? t() : document.addEventListener("DOMContentLoaded", t)
            },
            m = p;
        var g = () => {
                [...document.querySelectorAll("video")].forEach((t, o) => {
                    let e = t.getAttribute("data-lazyload"),
                        n = t.getAttribute("data-lazyload-loaded");
                    if (!e || n) {
                        let a = t.getAttribute("src"),
                            s = window.innerWidth,
                            r = t.getAttribute("data-desktop-vid"),
                            l = t.getAttribute("data-tablet-vid"),
                            u = t.getAttribute("data-mobile-vid"),
                            d = s <= 640 ? u : s <= 920 ? l : r;
                        d !== null && d !== a && t.setAttribute("src", d)
                    }
                })
            },
            E = t => "scrollTop" in t ? t.scrollTop : t.pageYOffset,
            y = t => {
                let o = t.getBoundingClientRect();
                return {
                    top: o.top + window.scrollY,
                    left: o.left + window.scrollX
                }
            },
            C = t => {
                for (var o = [], e = t.parentNode.firstChild; e;) e.nodeType === 1 && e !== t && o.push(e), e = e.nextSibling;
                return o
            },
            c = {
                run() {
                    g(), [...document.querySelectorAll("[data-lazyload]")].forEach(function(t, o) {
                        var e = t.getAttribute("data-lazyload"),
                            n = t.getAttribute("data-lazyload-src");
                        let a = (window.tresioConfig || {}).lazyload || {};
                        var s = a.homeOffset !== void 0 ? a.homeOffset : 300,
                            r = a.offset !== void 0 ? a.offset : 300;
                        e === "bg" && (r = a.backgroundOffset !== void 0 ? a.backgroundOffset : 300);
                        var l = E(window),
                            u = t.nodeName === "SOURCE" ? y(t.parentNode).top : y(t).top;
                        window.location.pathname === "/" && s !== void 0 && l < 300 && (r = s);
                        var d = u - r,
                            v = d + t.getBoundingClientRect().height,
                            h = l + document.querySelector("html").clientHeight,
                            w = u - l,
                            A = d < -r,
                            S = v > l && d < h || w < r;
                        let z = t.style.display;
                        if (S && z !== "none" && t.getAttribute("data-lazyload-loaded") !== "true" && !A) {
                            if (e === "bg" && (t.style.backgroundImage = `url(${n})`), e === "img" && (t.getAttribute("src") !== n && t.setAttribute("src", n), C(t).forEach((i, x) => {
                                    if (i.nodeName === "SOURCE") {
                                        let f = i.getAttribute("data-lazyload-srcset"),
                                            T = i.getAttribute("srcset");
                                        f && f !== T && i.setAttribute("srcset", f)
                                    }
                                })), e === "video") {
                                if (t.getAttribute("data-desktop-vid")) t.setAttribute("data-lazyload-loaded", "true"), g();
                                else if (t.setAttribute("src", n), t.nodeName === "SOURCE") {
                                    let i = t.parentNode;
                                    i.load && i.load()
                                }
                            }
                            t.setAttribute("data-lazyload-loaded", "true")
                        }
                    })
                },
                init() {
                    document.readyState === "loading" && c.run(), m(() => {
                        window.addEventListener("resize", c.run, !1), window.addEventListener("scroll", c.run, !1), c.run()
                    })
                }
            },
            b = c;
        b.init();
    })();
</script>
<!-- For refrrer url -->
<script async type="text/javascript">
    var ref = sessionStorage.getItem("referrer");
    document.getElementById("referrer").value = ref;
</script>
<script>
    var ref = sessionStorage.getItem("referrer");
    document.getElementById("referrer_call").value = ref;
</script>

<!-- Mobile_Menu_js -->
<script>
    console.clear();
    const navExpand = [].slice.call(document.querySelectorAll('.nav-expand'));
    const backLink = `<li class="nav-item">
	<a class="nav-link nav-back-link" href="javascript:;">
		Back
	</a>
</li>`;
    navExpand.forEach(item => {
        item.querySelector('.nav-expand-content').insertAdjacentHTML('afterbegin', backLink);
        item.querySelector('.nav-link').addEventListener('click', () => item.classList.add('active'));
        item.querySelector('.nav-back-link').addEventListener('click', () => item.classList.remove('active'));
    });

    // ---------------------------------------
    // not-so-important stuff starts here

    const ham = document.getElementById('ham');
    ham.addEventListener('click', function() {
        document.body.classList.toggle('nav-is-toggled');
    });
</script>

<!-- For refrrer url -->
@include('website.include.customvalidation')



</body>

</html>