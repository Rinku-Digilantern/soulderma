@extends('admin.master')

@section('content')



<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="page-title">
      <div>
        <h1>Appointment List</h1>
        <ul class="breadcrumb side">
          <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/admin/appointment')}}">Forms</a></li>
          <li class="active">List Appointment</li>
        </ul>
      </div>
      <!--<div><button id="export" class="btn btn-primary">Export</button></div>-->
    </div>
    <div class="appointment_strip">
      <div class="container-fluid">
        <div class="row ">
          <div class="strip_form">
            <div class="col-md-3">
              <div class="design_daterange">
                <label>Select Date</label>
                <div id="reportrange" class="pull-left form-control">
                  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                  <span></span> <b class="caret"></b>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="formtype">
                <label>Form Type</label>
                <select class="form-control select_form ">
                  <option value="All">All</option>
                  <option value="Appointment">Appointment</option>
                  <option value="Contact">Contact</option>
                </select>
              </div>


            </div>
            <div class="col-md-3">
              <div class="todays_appointment">
                <label>Today Appointment</label>
                <div class="count">
                  <span class="numbr_appointment">
                    0
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-3"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row appointment_list">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <table class="table table-hover table-bordered" id="sampleTable">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Appointment</th>
                  <th>Message</th>
                  <th>Source</th>
                  <th>Referer Url</th>
                  <th>Lead Type</th>
                  <th>Created Date</th>
                </tr>
              </thead>
              <tbody>
                <!-- Start foreach loop -->
                @foreach($view as $viewlist)
                <tr>
                  <td>
                    {{$viewlist->app_id}}
                  </td>
                  <td>
                    {{$viewlist->name}}
                  </td>
                  <td>
                    {{$viewlist->email}}
                  </td>
                  <td>
                    {{$viewlist->phone}}
                  </td>
                  <td>
                    {{$viewlist->date}}
                  </td>
                  <td>
                    {{$viewlist->message}}
                  </td>
                  <td>
                    {{$viewlist->request_url}}
                  </td>
                  <td>
                    {{$viewlist->referral_url}}
                  </td>
                  <td>
                    {{$viewlist->source_type}}
                  </td>
                  <td>
                    {{$viewlist->created_at}}
                  </td>
                </tr>
                @endforeach
                <!-- End foreach loop -->
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>

<script>
  $(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, cb);

    cb(start, end);

  });
</script>
<script type="text/javascript">
  $("button").click(function() {
    $("#sampleTable").table2excel({
      // exclude CSS class
      exclude: ".noExl",
      name: "Worksheet Name",
      filename: "Appointment.xls" //do not include extension
    });
  });
</script>

<!--main content end-->

@endsection