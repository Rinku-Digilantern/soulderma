@include('admin.include.header')
<div class="New_login">
    <div class=" main_form_div">
        <div class="hospital_img">
            <img src="https://i.pinimg.com/originals/57/1f/d2/571fd28ce80b7902bfc61ca7e6119fe9.jpg" alt="Hospital Image" class="img-fluid ">
        </div>
        <div class="Form_container">
            <div class="left_form">
                <div class="login_logo">
                    <img class="f-logo" src="{{ url('/admin/images/Arete_logo.png') }}" alt="logo">
                </div>
                <div class="forgottxt">Please enter the email address you used to create your account, and we'll send you a link to reset your password.</div>

                <form class="form_fields">
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <input type="email" class="ggg" autocomplete="off" id="email" onkeyup="validateEmail()" name="email" placeholder="Email">
                    <div id="showemailmsg" class="error"></div>
                    <div class="submit_btn">
                        <button id="submit" type="button" class="btn btn-primary" onclick="checkvalidation()">Submit</button>
                    </div>
                </form>
                <div id="validuser"></div>

            </div>
        </div>
    </div>
</div>

<!-- <div class="log-w3">
    <div class="w3layouts-main" id="forgot">
        <div class="logo-block login_logo" style="text-align: center;">
            <img class="f-logo" src="{{ url('/admin/images/Arete_logo.png') }}" alt="logo">
        </div>
        <div class="forgottxt">Please enter the email address you used to create your account, and we'll send you a link to reset your password.</div>
        <form>
            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
            <input type="email" class="ggg" autocomplete="off" id="email" onkeyup="validateEmail()" name="email" placeholder="Email">
            <div id="showemailmsg" class="error"></div>
            <div class="submit_btn">
                <button id="submit" type="button" class="btn btn-primary" onclick="checkvalidation()">Submit</button>
            </div>
        </form>
        <div id="validuser"></div>
    </div>
    <div class="w3layouts-main" id="messagecnf" style="display:none">
        <div id="msg"></div>
        <div id="commonmsg"></div>
    </div>
</div> -->

<style>
    #togglePassword {
        cursor: pointer;
        color: #000;
    }
</style>

<script>
    // Login Validation
    function checkvalidation(getid) {
        var email = document.getElementById("email").value
        var _token = document.getElementById("token").value

        var checkemail = false;

        var emailExp = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        emailExp.test(email);
        if (email == '') {
            document.getElementById("showemailmsg").innerHTML = 'This field is required.';
            document.getElementById("email").classList.add("errorsection");
            document.getElementById("email").classList.remove("validsection");
            // e.preventDefault();
        } else if (!emailExp.test(email)) {
            document.getElementById("showemailmsg").innerHTML = 'Please Enter Valid Email ID.';
            document.getElementById("email").classList.add("errorsection");
            document.getElementById("email").classList.remove("validsection");
            // e.preventDefault();
        } else {
            var checkemail = true;
            document.getElementById("showemailmsg").innerHTML = ' ';
            document.getElementById("email").classList.add("validsection");
            document.getElementById("email").classList.remove("errorsection");
        }

        if (checkemail == true) {
            var xhr = new XMLHttpRequest();

            // set up the request
            xhr.open("POST", "{{ url('/admin/submitforgotpassword') }}");
            xhr.setRequestHeader("Content-Type", "application/json");

            // handle the response
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        document.getElementById("forgot").style.display = "none";
                        document.getElementById("messagecnf").style.display = "block";
                        document.getElementById("msg").innerHTML = "Thank you";
                        document.getElementById("commonmsg").innerHTML = "Please check your mail for the link to reset your password.";
                    } else {
                        if (xhr.status == 400) {
                            document.getElementById("submit").disabled = false;
                            document.getElementById("submit").innerText = "Submit";
                            document.getElementById("validuser").innerHTML = 'Email Not exist in our database ';
                        }
                        if (xhr.status == 404) {
                            document.getElementById("submit").disabled = false;
                            document.getElementById("submit").innerText = "Submit";
                            document.getElementById("validuser").innerHTML = 'Email Not exist in our database ';
                        }
                        document.getElementById("submit").disabled = false;
                    }
                }
            };
            var data = {
                'email': email,
                '_token': _token,
            };
            // send the request
            document.getElementById("submit").disabled = true;
            document.getElementById("submit").innerText = "Submiting..";
            xhr.send(JSON.stringify(data));
            // }
        }
    }


    // for Email OnkeyUp Validate
    function validateEmail() {
        var emailInput = document.getElementById('email');
        var email = emailInput.value;
        var errorSpan = document.getElementById('showemailmsg');

        // Regular expression pattern for email validation
        var pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        if (pattern.test(email)) {
            errorSpan.textContent = ""; // Clear any existing error message
            // Do something with the valid email, such as submitting the form
        } else {
            errorSpan.textContent = "Please enter a valid email address";
        }
    }
    // For Email validate
</script>

@include('admin.include.footer')