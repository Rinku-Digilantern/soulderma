@include('website.include.header')

    <link rel="stylesheet" href="{{ url('website') }}/css/error.css">

    <div class="jumbotron text-center">
        <h1 class="display-3">404</h1>
        <p class="lead">OOPS! PAGE NOT FOUND</p>
        <p class="lead">Sorry, the page you are looking for doesn't exist
            something broken , report a problem</p>

        <p class="lead">
          <a class="commongradientbutton"  href="{{ url('/') }}" role="button">Continue to homepage</a>
        </p>
      </div>
@include('website.include.footer')
