<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Mail\UserMail;
use App\Models\CaptchaModel;
use App\Models\CfgRoleMenuModel;
use App\Models\CfgRoleOperationsModel;
use App\Models\CfgUsrRoleModel;
use App\Models\Loginlog;
use App\Models\MasterMenuModel;
use App\Models\MasterOperationsModel;
use App\Models\MasterRoleModel;
use App\Models\MasterUsersModel;
use Faker\Extension\Helper;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class LoginController extends Controller
{
    public function index()
    {
        if (session('isLogin') != 'yes') {
          $url = Helpers::adminurlget();
            return view('admin.adminlogin');
        }

        // print_r(session('useradmin'));
        return view('admin.dashboard.dashboard');
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
// die('bhh');
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => 'validation failed'], 400);
        }
        $cap = $request->input('captcha');
        $capuid = $request->input('uncode');

        $cp = CaptchaModel::where('capchacode',$cap)->where('uid',$capuid);

        if($cp->count() < 1){
            return response()->json(['success' => false, 'data' => []], 401);
        }
        $data = MasterUsersModel::login($request->input());
        if ($data->count() < 1) {
                //go back
                return response()->json(['success' => false, 'data' => []], 404);
                // return redirect('/admin/login');
            }

        //Add user info into session
        $this->initializeSession($data->first());

        $logindetails=session()->get('userinfo');
        $redirect_blade = session('isLogin') == 'yes' ? '/admin/dashboard' : '/admin/login';
        $this->saveLoginLog($logindetails['usr_id'],$logindetails['user_name'], $logindetails['usr_email']);
        return redirect($redirect_blade);
    }

    private function initializeSession($result)
    {
        // dd($result);
        // $surlget = str_replace(".", "_",  $result->site_url);
		// $surlget = 'template/'.str_replace(":", "_",  $surlget).'/';
        $backend = 'backend';
        $active = 'active';
        //get user role list
        // $usrrolelist = CfgUsrRoleModel::where('usr_id', $result->usr_id)
        //     ->where('cfg_org_id', $result->usr_org_id);
            //echo $usrrolelist->count();die();
            // if($usrrolelist->count() < 1){
            //     return redirect('/no-authorization');
            // }

            // $usrrole = $usrrolelist->first();
        // If not super admin condition will run and set plan id,role name, role id
        // if ($result->usr_org_id > SUPER_ADMIN) {
            // $role = MasterRoleModel::find($usrrole->role_id);
            // $plan_id = $usrrole->plan_id;
            // $role_name = $role->role_name;
            // $role_id = $role->role_id;
        // }
        if($result->usr_id == '1'){
            $role_id = '1';
            $categoryidmenus = [];
            $categoryidoperations = [];
            $menus  = MasterMenuModel::select('mnu_name', 'mnu_id', 'mnu_url','mnu_dropdown', 'mnu_icon')
            ->where('mnu_type', '=', 'backend')
            ->where('mnu_status', '=', 'active')
            ->orderBy('master_menu.mnu_order', 'asc')
            ->groupBy('mnu_name', 'mnu_id', 'mnu_url','mnu_dropdown', 'mnu_icon','mnu_order')
            ->get();

            $operations = MasterOperationsModel::select('op_name', 'op_id', 'op_link', 'op_icon_link', 'mnu_id', 'cfg_mun_id')
            ->where('op_type', '=', 'backend')
            ->where('op_status', '=', 'active')
            ->join('cfg_mnu_operations', 'cfg_op_id', '=', 'master_operations.op_id')
            ->join('master_menu', 'mnu_id', '=', 'cfg_mnu_operations.cfg_mun_id')
            ->orderBy('master_operations.op_view_order', 'asc')
            ->groupBy('op_name', 'op_id', 'op_link', 'op_icon_link', 'mnu_id', 'cfg_mun_id','op_view_order')
            ->get();

            $categoryidmenus  = MasterMenuModel::selectRaw('GROUP_CONCAT(mnu_id) as mnuid')
            ->where('mnu_type', '=', 'backend')
            ->where('mnu_status', '=', 'active')
            ->get();

            $categoryidoperations = MasterOperationsModel::select(DB::raw('GROUP_CONCAT(op_id) as opid'))
            ->where('op_type', '=', 'backend')
            ->where('op_status', '=', 'active')
            ->join('cfg_mnu_operations', 'cfg_op_id', '=', 'master_operations.op_id')
            ->join('master_menu', 'mnu_id', '=', 'cfg_mnu_operations.cfg_mun_id')
            ->get();

        }else{

           $usrrole = CfgUsrRoleModel::where('usr_id',$result->usr_id);
           if($usrrole->count() < 1){
                return redirect('admin/login');
           }
          $usrroles = $usrrole->first();
            $role_id = $usrroles->role_id;
// $role_id = '1';
        //Taking Data from Role Menu Bag Through Role Id Matching
        $menus  = CfgRoleMenuModel::select(DB::raw('group_concat(cfgmnu_act_id) as cfgmnu_act_id'),'mnu_name', 'mnu_id', 'mnu_url','mnu_dropdown', 'mnu_icon')
            ->where('cfgmnu_role_id', '=', $role_id)
            ->where('mnu_type', '=', 'backend')
            ->where('mnu_status', '=', 'active')
            ->join('master_menu', 'mnu_id', '=', 'cfgmnu_mnu_id')
            ->orderBy('master_menu.mnu_order', 'asc')
            ->groupBy('mnu_name', 'mnu_id', 'mnu_url','mnu_dropdown', 'mnu_icon','mnu_order')
            ->get();


        //Taking Data from Role operation Bag Through Role Id Matching
        $operations = CfgRoleOperationsModel::select(DB::raw('group_concat(oper_act_id) as oper_act_id'),'op_name', 'op_id', 'op_link', 'op_icon_link', 'mnu_id', 'cfg_mun_id')
            ->where('oper_role_id', '=',  $role_id)
            ->where('op_type', '=', 'backend')
            ->where('op_status', '=', 'active')
            ->join('master_operations', 'op_id', '=', 'oper_op_id')
            ->join('cfg_mnu_operations', 'cfg_op_id', '=', 'master_operations.op_id')
            ->join('master_menu', 'mnu_id', '=', 'cfg_mnu_operations.cfg_mun_id')
            ->orderBy('master_operations.op_view_order', 'asc')
            ->groupBy('op_name', 'op_id', 'op_link', 'op_icon_link', 'mnu_id', 'cfg_mun_id','op_view_order')
            ->get();

               //find to data service category menu id
            //Taking Data from Role Menu Bag Through Role Id Matching
            $categoryidmenus  = CfgRoleMenuModel::select(DB::raw('GROUP_CONCAT(DISTINCT(mnu_id)) as mnuid'))
            ->join('master_menu', 'mnu_id', '=', 'cfgmnu_mnu_id')
            ->where('cfgmnu_role_id', $role_id)
            ->where('master_menu.mnu_type', $backend)
            ->where('mnu_status', $active)
            ->get();


        //Taking Data from Role operation Bag Through Role Id Matching
        $categoryidoperations = CfgRoleOperationsModel::select(DB::raw('GROUP_CONCAT(DISTINCT(op_id)) as opid'))
            ->join('master_operations', 'op_id', '=', 'oper_op_id')
            ->join('cfg_mnu_operations', 'cfg_op_id', '=', 'master_operations.op_id')
            ->join('master_menu', 'mnu_id', '=', 'cfg_mnu_operations.cfg_mun_id')
            ->where('oper_role_id',  $role_id)
            ->where('master_operations.op_type', $backend)
            ->where('op_status', $active)
            ->get();
        }
            // echo '<pre>';print_r($categoryidoperations); echo '</pre>';
            // die();
        Session::put('isLogin', 'yes');
$role_name = 'Super User';
        //set session userinfo
        Session::put('userinfo', [
            'usr_id' => $result->usr_id,
            'usr_email' => $result->usr_email,
            'user_name' => $result->usr_first_name . ' ' . $result->usr_last_name,
            'user_org_id' => $result->usr_org_id,
            'user_org_name' => $result->org_name,
            'org_person_name' => $result->org_person_name,
            'org_logo' => $result->org_logo,
            'user_role_name' => $role_name,
            'user_role_id' => $role_id,
            'client_usr_id' => $result->usr_id,
            'user_home_page' => '',
            'user_category_menu_permissions' => $categoryidmenus,
            'user_category_operation_permissions' => $categoryidoperations,
            'user_menu_permissions' => $menus,
            'user_operation_permissions' => $operations
        ]);

        //set session userinfo
        Session::put('useradmin', [
            'org_id' => $result->org_id,
            'super_org_id' => $result->usr_id,
            'site_id' => $result->site_id,
            'usr_id' => $result->usr_id,
            'site_url' => ''
        ]);
    }

    public function saveLoginLog($userId,$userName,$userEmail){
        $loginLog=Loginlog::create([
               'user_id'=>$userId,
               'name'=>$userName,
               'email'=>$userEmail,
               'ip'=>$_SERVER['REMOTE_ADDR']
        ]);
    }

    public function forgot_password()
    {
        if (session('isLogin') == 'yes') {
            $url = Helpers::adminurlget();
              return view('admin.forgotpassword');
          }

          // print_r(session('useradmin'));
          return view('admin.forgotpassword');
    }

    public function changePassword()
    {
        if (session('isLogin') == 'yes') {
            $url = Helpers::adminurlget();
              return view('admin.forgotpassword');
          }

          // print_r(session('useradmin'));
          return view('admin.changepassword');
    }

    public function submit_forgot_password(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'email' => 'required|email',
        ]);
// die('bhh');
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => 'validation failed'], 400);
        }

        $data = MasterUsersModel::select('*')->where('usr_email',$request->input('email'))->where('deleted_at',NULL);
        if ($data->count() < 1) {
            return response()->json(['success' => false, 'data' => []], 404);
        }

        $user = $data->first();
        // dd($user->usr_first_name);
            $email = md5($user->usr_email);
            $url = url('/admin/change-password?requestid='.$email);
            $name = $user->usr_first_name;
            $details = [
            'name' => $name,
            'email' =>  $request->input('user_email'),
            'page' => 'forgot-password',
            'url' => $url,
        ];
        if($user)
        {

            Mail::to($request->input('email'))->send(new UserMail($details));
            return response()->json(['success' => true], 200);
        }else{
            return response()->json(['success' => false, 'error' => 'validation failed'], 404);
        }
    }

    public function submitchangePassword(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => 'validation failed'], 400);
        }

        $email = $request->input('email');
        $data = MasterUsersModel::whereRaw('md5(usr_email) = "' .$email. '"');
        if($data->count() > 0)
        {
            $data->update([
                'usr_password' => md5($request->input('password')),
            ]);
            return response()->json(['success' => true], 200);
        }else{
            return response()->json(['success' => false, 'error' => 'validation failed'], 404);
        }
    }
}
