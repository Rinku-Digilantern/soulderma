@include('website.include.header')

<link rel="stylesheet" href="{{ url('website') }}/css/blog.css">


<section id="topcommonbanner">
    <div class="commonbannerclinic">
        <img src="{{ url('website') }}/img/about-clinic/about-clinic.jpg" class="w-100">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="headingandbredcrumb">
                    <div class="commonheading notofamily commonheadingfontsize">Blogs</div>
                    <div class="breadcrumb commonbredcurmb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a>Blogs</a></li>
                        </ul>
                    </div>
                    <div class="commonrequestform blog_main_form">
                        @include('website.include.requestcallback')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="blogwrapper" style="margin:20px 0">
    <div class="container">
        <div class="row">
            @foreach($blog as $blg)
            <div class="col-12 col-md-4">
                <div class="blogcontentwrap">
                    <a href="{{ url('blogs') }}/{{ $blg->url }}" class="bloglink">
                        <div class="blogimagewrap">
                            <img src="{{ url('backend/blog') }}/{{ $blg->blog_image }}" class="w-100" alt="{{ $blg->alt_image_name }}">
                        </div>
                        <div class="detailwrap">
                            <div class="blogheading">
                                {{ $blg->blog_name }}
                            </div>
                            <div class="blogcontentwrapgr">
                                {!! Str::limit($blg->short_desc,75) !!}
                            </div>
                            <div class="blogadminame">
                                <div class="blogadminname">
                                    <!-- <p class="adminn">Kalvin</p> -->
                                    <p>{{ $blg->date }}</p>
                                </div>
                                <div class="blogviewbutton">
                                    <a href="{{ url('blogs') }}/{{ $blg->url }}">
                                        <button class="blogsubmit commonbgcolorbutton">Read More</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@include('website.include.requestanappointment')

@include('website.include.footer')