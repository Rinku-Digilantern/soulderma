<!-- Header -->
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="<?php echo base_url() ?>assets/soulderma/images/favicon.png">
    <title>Dr. Anika Goel</title>
    <link rel="preload" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/soulderma/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="preload" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" as='font' onload="this.onload=null;this.rel='stylesheet'" crossorigin>
    <link rel="preload" href="<?php echo base_url() ?>assets/soulderma/css/datepicker.css" as="style" onload="this.rel='stylesheet'">
    <link rel="preload" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" crossorigin />
    <meta name="facebook-domain-verification" content="wam750j9i0jm83wg99h2q04ctwtamn" />
    <!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1319675791941517');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1319675791941517&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TCDGGBV');</script>
<!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TCDGGBV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <header class="hdrnobg">
        <div class="row align-items-center">
            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                <a><img src="<?php echo base_url() ?>assets/soulderma/images/logo.png" data-src="<?php echo base_url() ?>assets/soulderma/images/logo.png" class="logoicn lazyload" alt="logo" /></a>
            </div>
            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6" style="text-align:right;">
                <a class="topCallBtn" href="Tel:+91-8595941529">Call Now</a>
            </div>
        </div>
    </header>
    <section class="innerHeader">
<div class="hdrHd">Thank You</div>
</section>
<section class="innerSpace">
<div class="container text-center">
<p class="accept-para">We have received your appointment request.<br>We will get in touch with you shortly.</p>

</div>
</section>





    <!-- Footer -->
    <footer class="blck">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 my-3">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.6345934417313!2d77.23130931500711!3d28.550700982450234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce3dfd43e3265%3A0x2bed17d86139c92d!2sSoul%20Derma%20Clinic%20by%20Dr.Anika%20Goel!5e0!3m2!1sen!2sin!4v1665042571080!5m2!1sen!2sin" style="border:0;width:100%;height:100%" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div class="col-lg-6 my-3">
                    <ul class="footer-ul bg-light">
                        <li><i class="fa fa-envelope" aria-hidden="true"></i>souldermaclinic@gmail.com</li>
                        <li><i class="fa fa-phone" aria-hidden="true"></i> +91-8595941529</li>
                        <li><i class="fa fa-address-book" aria-hidden="true"></i>Ground Floor W-58,
                            Greater Kailash I, New Delhi,
                            Delhi 110048 India</li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <div class="container-fluid disclaimer-section px-0 blck">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mydis">
                    Disclaimer: All the general information, graphics, and videos provided on the website
                    (www.soulderma.com) are solely to create awareness among people and is not intended to be
                    taken as medical advice. The results also vary from patient to patient, depending upon their health
                    condition and body anatomy. If you are interested in finding out more, please contact us today for a
                    personal consultation.
                </div>
            </div>
        </div>
    </div>
    <div class="txtCprgt text-center">© Copyright 2022 | All Rights Reserved</div>
    <div class="d-md-none d-lg-none d-xl-none col-sm-block ftrFixMenu">
        <div class="row align-items-center">
            <div class="col"><a href="tel:+91-8595941529"><i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Call Us</p>
                </a></div>
            <div class="col d-none"><a href="mailto:info@delhiurologyhospital.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                    <p>Email Us</p>
                </a></div>
            <div class="col"><a href="#AppForm"><i class="fa fa-calendar" aria-hidden="true"></i>
                    <p>Appointment</p>
                </a></div>
        </div>
    </div>

</body>

</html>