<?php

namespace App\Http\Controllers;

use App\Models\Loginlog;
use App\Models\MasterUsersModel;
use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;

class LoginlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

public function lastlogindata(Request $request){
    return Loginlog::where('user_id',$request->usr_id)->get();
   
}
     public function last_login(){
    $logindata=Loginlog::get();
    $users=MasterUsersModel::get();
    return view('admin.log.lastlogin',['data'=>$logindata,'users'=>$users]);

}

     public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Loginlog  $loginlog
     * @return \Illuminate\Http\Response
     */
    public function show(Loginlog $loginlog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Loginlog  $loginlog
     * @return \Illuminate\Http\Response
     */
    public function edit(Loginlog $loginlog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loginlog  $loginlog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loginlog $loginlog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Loginlog  $loginlog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loginlog $loginlog)
    {
        //
    }
}
