<form  id="callback" action="{{ url('callback-form') }}" method="post">
    <p id="errormsg" class="text-center" style="color: red; font-weight:bold;"></p>
    <div class="form-group row form_group_justify">
        <div class="col-lg-3 col-sm-12 form_headingg">
            <div class="heading_form">Request A Call Back</div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 w50 common_inputs">
            <input class="form-control " name="name" id="name" type="text" placeholder="Name*" required=""  autocomplete="off">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 w50 common_inputs">
            <input class="form-control " id="phone" type="text" name="phone" placeholder="Phone*"  autocomplete="off" minlength="10" maxlength="10" required="">
        </div>
        <input type="hidden" name="request_url" value="{{ $_SERVER['REQUEST_URI'] }}">
    
        <input type="hidden" name="referral_url" value="">
    <input type="hidden" name="honey"> 
        <!-- <div class="col-lg-2 col-md-2 col-sm-6 w50 common_inputs cptcha">
           <img src="{{ () ?>assets/soulderma/images/captcha.jpg" alt="">
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 w50 common_inputs">
            <input class="form-control" id="phone" type="text" placeholder="Enter Code*">
        </div> -->
        <div class="col-lg-3 col-md-4  col-sm-12 common_inputs mob-center">
            <button type="submit" id="call_submit" class="btn-submit ">Submit</button>
        </div>
         <div class="loadme"><img src="{{ url('assets/images/loading_icon.gif') }}" width="40" height="40"></div>
    </div>
</form>