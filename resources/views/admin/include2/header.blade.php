<title>Subscription | Admin Panel</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />


<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/vendors.min.css') }}">
<!-- BEGIN: VENDOR CSS-->


<link rel="stylesheet" type="text/css"
  href="{{asset('css/themes/vertical-modern-menu-template/materialize.css')}}">
<link rel="stylesheet" type="text/css"
  href="{{asset('css/themes/vertical-modern-menu-template/style.css')}}">


<!-- <link rel="stylesheet" type="text/css" href="{{asset('css/layouts/style-horizontal.css')}}"> -->



@yield('vendor-style')
<!-- END: VENDOR CSS-->
<!-- BEGIN: Page Level CSS-->
<!-- 
<link rel="stylesheet" type="text/css"
href="{{asset('css/themes/template/materialize.css')}}">
<link rel="stylesheet" type="text/css"  href="{{asset('css/themes/template/style.css')}}"> -->


<!-- END: Page Level CSS-->
<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('css/laravel-custom.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/custom/custom.css')}}">
<!-- END: Custom CSS-->

<link rel="stylesheet" type="text/css" href="{{asset('vendors/flag-icon/css/flag-icon.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/data-tables/css/jquery.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css"
href="{{asset('vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/data-tables/css/select.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/select2/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/select2/select2-materialize.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/data-tables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/form-select2.css')}}">




<script src="{{ url('/admin/js/jquery2.0.3.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="{{ url('/admin/ckeditor/ckeditor.js')}}"></script>
<script src="{{ url('/admin/js/raphael-min.js')}}"></script>
<script src="{{ url('/admin/js/morris.js')}}"></script>
<script src="{{ url('/admin/ckeditor/samples/js/sample.js')}}"></script>



<?php //print_r(session('useradmin')); ?>