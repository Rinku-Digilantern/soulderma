 <section id="appointmentwrapper" class="for-desktop">

     <div class="container">
         <div class="homeappointmentheading notofamily commonheadingfontsize">BOOK AN APPOINTMENT</div>
         <div class="row">
             <div class="col-12 col-md-4">
                 <div class="appointaddresswrap">
                     <div class="apadressicon">
                         <img src="{{ url('website') }}/img/location-icon.png">
                     </div>
                     <div class="aplocationcontentwrap">
                         <div class="aplocationheading">ADDRESS</div>
                         <div class="detailaddress">
                             <p>Ground Floor W-58, Greater Kailash I, New Delhi, Delhi 110048 India</p>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="col-12 col-md-4">
                 <div class="appointaddresswrap">
                     <div class="apadressicon">
                         <img src="{{ url('website') }}/img/content-icon.png">
                     </div>
                     <div class="aplocationcontentwrap">
                         <div class="aplocationheading">MOBILE NO</div>
                         <div class="apmobilenumber">
                             <a href="tel:+91-8595941529">+91-8595941529</a>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="col-12 col-md-4">
                 <div class="appointaddresswrap">
                     <div class="apadressicon">
                         <img src="{{ url('website') }}/img/email-icon.png">
                     </div>
                     <div class="aplocationcontentwrap">
                         <div class="aplocationheading">EMAIL ID</div>
                         <div class="detailaddress">
                             <p><a href="mailto:info@souldermaclinic.com">info@souldermaclinic.com</a></p>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>

 <section id="bigappointwrap">
     <div class="container">
         <div class="homeappointmentheading notofamily commonheadingfontsize for-mobile">BOOK AN APPOINTMENT</div>
         <div class="row appointbg">
             <form method="post" class="bottom_form">
                 <input type="hidden" id="uniq_app" name="uncode">
                 <input type="hidden" name="_token" id="token_app" value="{{ csrf_token() }}">
                 <div class="form-inputs row">
                     <div class="col-12 col-md-4">
                         <div class="form-input">
                             <div class="bigapform">
                                 <input type="text" name="name" class="form-control" autocomplete="off" id="name_app" onkeyup="validateUsernameapp()" placeholder="Name*" required="">
                                 <div id="validname_app" class="error"></div>
                                 <!-- <input type="text" id="apone" placeholder="Name*"> -->
                             </div>
                         </div>
                     </div>
                     <div class="col-12 col-md-4">
                         <div class="form-input">
                             <div class="bigapform">
                                 <input type="email" class="form-control" name="email" autocomplete="off" id="email_app" onkeyup="validateEmail()" placeholder="Email*" required="">
                                 <div id="showemailmsg_app" class="error"></div>
                                 <!-- <input type="text" id="aptwo" placeholder="Phone Number*"> -->
                             </div>
                         </div>
                     </div>
                     <div class="col-12 col-md-4">
                         <div class="form-input">
                             <div class="bigapform">
                                 <input type="text" class="form-control" name="phone" autocomplete="off" minlength="10" onkeyup="validatePhoneNumberapp()" maxlength="10" id="phone_app" placeholder="Phone*" required="">
                                 <div id="validphone_app" class="error"></div>
                                 <!-- <input type="text" class="form-control" id="aptwo" placeholder="Phone Number*"> -->
                             </div>
                         </div>
                     </div>
                     <div class="col-12 col-md-4">
                         <div class="form-input">
                             <div class="bigapform bigapformrelative">
                                 <input type="text" id="date_app" onfocus="(this.type = 'date')" name="date" id="inputdate" autocomplete="off" placeholder="Select Date*" class="form-control for-icon" min="{{ date('Y-m-d') }}">
                                 <!-- <span class="fa fa-calendar"></span> -->
                                 <div id="validdate_app" class="error"></div>

                             </div>
                         </div>
                     </div>
                     <div class="col-12 col-md-8">
                         <div class="form-input">
                             <div class="bigapform">
                                 <textarea  name="message" autocomplete="off" rows="1" id="message" placeholder="Message" required="" class="form-control"></textarea>
                             </div>
                         </div>
                     </div>
                     <input type="hidden" name="request_url" id="request" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
                     <input type="hidden" id="referrer" name="referrer">
                     <div class="col-12 col-md-12">
                         <div class="form-input">
                             <div class="big-apsubmitwrapper">
                                 <div class="bigcaptchawrap">
                                     <div class="captcgawrap">
                                         <div class="captchbgimg req-captcha-box">
                                             <div class="captchacodeleftpic" id="capt_app"></div>
                                         </div>
                                         <div class="apcaptchabutton button-box">
                                             <button type="button" class="btnBtnSubmits" onClick="captchareload_app()"><i class="fa fa-refresh"></i></i></button>
                                         </div>
                                     </div>
                                     <div class="inputfillcaptcha ">
                                         <input type="text" class="form-control captcha_field" formcontrolname="captcha" id="captchacode_app" autocomplete="off" placeholder="Captcha*" maxlength="6">
                                         <div id="validcaptcha_app" class="error"></div>
                                         <!-- <input type="text" placeholder="Captcha*"> -->
                                     </div>
                                 </div>
                                 <div class="bgcaptchasubmit">
                                     <button id="submitapp" type="button" onclick="checkvalidation()">Submit</button>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!-- <div class="form-input input-submit">
                         <button id="submitapp" type="button" onclick="checkvalidation()">Submit</button>
                     </div> -->
                 </div>
             </form>
         </div>
     </div>
 </section>



 <!-- backup_form -->
 <!-- <section id="bigappointwrap">
        <div class="container">
            <div class="homeappointmentheading notofamily commonheadingfontsize for-mobile">BOOK AN APPOINTMENT</div>
            <div class="row appointbg">
            <form method="post">
                <input type="hidden" id="uniq_app" name="uncode">
                <input type="hidden" name="_token" id="token_app" value="{{ csrf_token() }}">
                <div class="form-inputs">
                    <div class="form-input">
                        <input type="text" name="name" class="" autocomplete="off" id="name_app" onkeyup="validateUsernameapp()" placeholder="Name*" required="">
                        <div id="validname_app" class="error"></div>
                    </div>
                    <div class="form-input">
                        <input type="email" class="" name="email" autocomplete="off" id="email_app" onkeyup="validateEmail()" placeholder="Email*" required="">
                        <div id="showemailmsg_app" class="error"></div>
                    </div>
                    <div class="form-input">
                        <input type="text" class="" name="phone" autocomplete="off" minlength="10" onkeyup="validatePhoneNumberapp()" maxlength="10" id="phone_app" placeholder="Phone*" required="">
                        <div id="validphone_app" class="error"></div>
                    </div>
                    <div class="form-input">
                        <input type="text" onfocus="(this.type = 'date')" id="date_app" name="date" id="inputdate" autocomplete="off" placeholder="Select Date*">
                        <div id="validdate_app" class="error"></div>
                    </div>
                    <div class="form-input">
                        <textarea class="" name="message" autocomplete="off" rows="8" id="message" placeholder="Message" required=""></textarea>
                    </div>
                    <input type="hidden" name="request_url" id="request" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
                    <input type="hidden" id="referrer" name="referrer">
                    <div class="form-input">
                        <div class="captcha-box d-flex align-items-center">
                            <div class="w-50 d-flex">
                                <div class="req-captcha-box">
                                <div class="captchacodeleftpic" id="capt_app"></div>
                                </div>
                                <div class="button-box">
                                <button type="button" class="btnBtnSubmits" onClick="captchareload_app()"><i class="bi bi-arrow-counterclockwise"></i></button>
                                </div>
                            </div>
                            <div class="w-50">
                                <input type="text" class="" formcontrolname="captcha" id="captchacode_app" autocomplete="off" placeholder="Captcha*" maxlength="6">
                                <div id="validcaptcha_app" class="error"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-input input-submit">
                        <button id="submitapp" type="button" onclick="checkvalidation()">Submit</button>
                    </div>
            </form>
            </div>
        </div>
    </section> -->