@include('website.include.header')
<link rel="stylesheet" href="{{ url('website') }}/css/services-category.css">

<section id="topcommonbanner" class="service_page_banner">
    <div class="commonbannerclinic">
        <img src="{{ url('/backend/service/banner/'.$service->service_banner_image) }}" class="w-100" alt="{{ $service->alt_tag }}">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="headingandbredcrumb">
                    <div class="commonheading notofamily commonheadingfontsize">{{ $service->service_name }}</div>
                    <div class="breadcrumb commonbredcurmb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="#">{{ $service->service_name }}</a></li>
                        </ul>
                    </div>
                    <div class="commonrequestform">
                        @include('website.include.requestcallback')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--<section class="twoheadingwrap ">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-12 col-md-12">-->
<!--                <div class="commonservicesheading">-->
<!--                    Get Acne Scar Removal done by the best team in India.-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-12">-->
<!--                <div class="twosubheading">What Is acne</div>-->
<!--                <p>Acne is a common skin condition that affects the hair follicles and oil glands in the skin. It typically occurs during adolescence but can also affect adults. Acne is characterized by the presence of pimples, blackheads, whiteheads, cysts, and nodules on the skin, most commonly on the face, neck, chest, and back.</p>-->
<!--                <div class="commonpointers">-->
<!--                    <ul>-->
<!--                        <li>Acne scars can be effectively removed by the best team in India.</li>-->
<!--                        <li>The skilled professionals have the experience and expertise to provide you with excellent results.</li>-->
<!--                    </ul>-->
<!--                </div>-->

<!--            </div>-->

<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<section id="homeblogwrapper" class="Category_page">
    <div class="container">
        <!--<div class="homeblogheading notofamily commonheadingfontsize">All Category</div>-->
        <div class="row manage-mbwidthparent">
            @foreach($subservice as $ser)
            <div class="col-12 col-md-4 manage-mbwidth">
                <div class="homeblogcontentwrapper">
                    <div class="homeblogimagewrap">
                        <div class="homeblogimage">
                            <img src="{{ url('/backend/service/image')}}/{{ $ser->service_image }}" alt="{{ $ser->alt_tag }}">
                        </div>
                   
                    </div>
                    <div class="homeblogcontenttext">
                        <div class="homeblogcontentheading">{{ $ser->service_name }}</div>
                        <div class="homeblogtextp">
                            {!! Str::limit($ser->short_desc) !!}
                        </div>
                        <div class="homblogview">
                            <a href="{{ url('/') }}/{{ $ser->url }}" class="commonanchor">read more</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
    </div>
</section>






@include('website.include.requestanappointment')
@include('website.include.footer')