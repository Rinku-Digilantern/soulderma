<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Models\AboutModel;
use App\Models\BlogModel;
use App\Models\CaseStudiesModel;
use App\Models\ContactDetailModel;
use App\Models\ContactUsModel;
use App\Models\DoctorCertificateModel;
use App\Models\DoctorModel;
use App\Models\FooterModel;
use App\Models\FrontMenuModel;
use App\Models\GalleryCategoryModel;
use App\Models\GalleryModel;
use App\Models\GoogleRecaptchaModel;
use App\Models\MembershipModel;
use App\Models\PressMediaCategoryModel;
use App\Models\PressMediaModel;
use App\Models\ResultInnerModel;
use App\Models\ResultServiceModel;
use App\Models\SeoPageModel;
use App\Models\ServiceFaqModel;
use App\Models\ServiceModel;
use Illuminate\Support\Facades\File;
// use App\Models\ServiceSectionModel;
use Illuminate\Http\Request;
use App\Models\SliderModel;
use App\Models\TestimonialModel;
use App\Models\TestimonialVideoModel;
use App\Models\VideoServiceModel;
use App\Models\VideoInnerModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;

class AppLaunchController extends Controller
{
    public function __construct(Helpers $siteurl)
    {
       $this->siteurl = $siteurl;
    }

    public function appLaunch()
    {
        // die();
        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();

        // $data['datadic'] = $resdirectory.$frontpathurl;
        // View::addLocation($data['datadic']);
        $seourl = '';
        // find org id from site url
        // Pick all records of this org list
        // $siteview = Helpers::orgnizationlist();
        // $this->initializeSession($siteview->first());

       // $data['orgdata'] = $siteview->first();

        // Pick all records of this testimonial list
        $select_fields = ['test_id','source','rating','description','short_name','name'];
        $data['testimonial'] = TestimonialModel::select($select_fields)->where('status','active')
        ->where('test_show_type','outside')
        ->limit(3)->orderBy('test_id','desc')->get();

        // Pick all records of this blog list
        $select_fields = ['blg_id','blog_type','blog_name','name','short_desc','blog_description','blog_image','alt_image_name','date','url'];
        $data['blog'] = BlogModel::select($select_fields)->where('status','active')
        // ->where('blog_show_type','outside')
        ->limit(3)->orderBy('blg_id','desc')->get();

        $data['seotitle'] = self::seopage();
        // print_r('hi',$data['seotitle']);
        // die('hj');
        // $data['slider'] = $this->homesliderquery();
        // $data['service'] = $this->homeservicerquery();
        // $data['aboutclinic'] = $this->homeaboutquery();
        // $data['doctorlist'] = $this->homedoctorquery();
       return view('website.index')->with($data);
    }

    private static function homesliderquery(){
        // Pick all records of this slider list
        $select_fields = ['slider_id','name','image','alt_tag','description','button_type','button_name','button_url','button_style','url'];
      return  SliderModel::select($select_fields)->where('status','active')
        ->orderBy('order_by','asc')->get();
    }

    private static function homeservicerquery(){
        // Pick all records of this service list
        $select_fields = ['ser_id','service_name','home_banner_image','homedescription','url'];
      return  ServiceModel::select($select_fields)->where('status','active')
        ->where('category_type','service')->orderBy('order_by','asc')->get();
    }

    private static function homeaboutquery(){
        // Pick all records of this service list
        $select_fields = ['abt_id','name','image','home_image','home_section','heading1','section1','section2','heading2','section3','heading3','section4','heading4','section5'];
        return AboutModel::select($select_fields)->where('status','active')
        ->get();
    }

    private static function homedoctorquery(){
        // Pick all records of this service list
        $select_fields = ['doc_id','name','sort_desc','short_degree','home_desc','home_image','description','image','banner_image','education_desc','education_detail','experience_desc','experience_detail','alt_tag','url'];
        return DoctorModel::select($select_fields)->where('status','active')
        ->where('show_type','outside')->orderBy('order_by','asc')->get();
    }

    public static function CommanAppointment(){
        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        $select_fields = ['google_id','site_key','secret_key'];
        $data['googlerecaptcha'] = GoogleRecaptchaModel::select($select_fields)->where('status','active')
        ->first();

        // View::addLocation($data['datadic']);
        return view('website.common-appointment')->with($data);
    }

    public static function GetAQuote(){
        $url = '';

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        if(isset(request()->segments()[0])){
            $url = request()->segments()[0];
            $select_table = ['ser_id','service_name'];
        $data['servicedetail'] = ServiceModel::select($select_table)->where('status','active')
        ->where('url',$url)->first();
            }else{
                $select_table = ['ser_id','service_name'];
        $data['servicedetail'] = ServiceModel::select($select_table)->where('category_type','service')->where('status','active')->get();
            }


        $select_fields = ['google_id','site_key','secret_key'];
        $data['googlerecaptcha'] = GoogleRecaptchaModel::select($select_fields)->where('status','active')
        ->first();

        // View::addLocation($data['datadic']);
        return view('website.get-a-quote')->with($data);
    }

    public static function CommanTestimonials(){
        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        $select_fields = ['test_id','source','rating','description','short_name','name'];
        $data['testimonial'] = TestimonialModel::select($select_fields)->where('status','active')
        ->where('test_show_type','outside')
        ->limit(2)->orderBy('test_id','desc')->get();

        $data['seotitle'] = self::seopage();

        // print_r($data['testimonial']);
        View::addLocation($data['datadic']);
        return view('website.common-testimonials')->with($data);
    }

    public static function LeftsideCommanAppointment(){
        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        $select_fields = ['google_id','site_key','secret_key'];
        $data['googlerecaptcha'] = GoogleRecaptchaModel::select($select_fields)->where('status','active')
        ->first();


        View::addLocation($data['datadic']);
        return view('website.left-side-common-appointment')->with($data);
    }

    // public static function CommanHeader(){
    //     //pick resource directory path
    //     $resdirectory = env('RES_DIRECTORY');
    //     $frontpathurl = Helpers::frontpathurl();
    //     $data['datadic'] = $resdirectory.$frontpathurl;

    //     // Pick all records of this menu list
    //    $data['firstmenu'] = FrontMenuModel::frontmenu();

    //    $select_fields = ['footer_id','header_logo','header_logo2','alt_tag'];
    //     $data['headerlogo'] = FooterModel::select($select_fields)->where('status','active')
    //     ->where('footer_type','leftside')->first();

    //     $select_fields = ['contact_id','name','address','address1','address2','email_id','phone','timings'];
    //     $data['contactus'] = ContactDetailModel::select($select_fields)->where('status','active')
    //     ->where('show_type','outside')->first();

    //     // Pick records of this seo list

    //     // die();
    //     $data['seotitle'] = self::seopage();
    //     // echo '<pre>';print_r($data['seotitle']);echo '</pre>';die();
    //     // print_r($data['seotitle']);

    //     View::addLocation($data['datadic']);
    //     return view('home.header')->with($data);
    // }

    // public static function CommanFooter(){
    //     //pick resource directory path
    //     $resdirectory = env('RES_DIRECTORY');
    //     $frontpathurl = Helpers::frontpathurl();
    //     $data['datadic'] = $resdirectory.$frontpathurl;

    //     $select_fields = ['contact_id','name','address','address1','address2','pincode','email_id','phone','timings'];
    //     $data['contactus'] = ContactDetailModel::select($select_fields)->where('status','active')
    //     ->where('show_type','outside')->first();
    //     // print_r($data['contactus']);die();->where('show_type','outside')
    //     $select_fields = ['footer_id','footer_type','footer_name','footer_logo','description','menu_name','alt_tag'];
    //     $data['footer'] = FooterModel::select($select_fields)->where('status','active')
    //     ->get();

    //     View::addLocation($data['datadic']);
    //     return view('home.footer')->with($data);
    // }

    public function AboutClinic(){
        // Pick all records of this org list
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        $data['aboutlist'] = $this->homeaboutquery();
        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this gallery list
        $select_table = ['gallery_id','gallery_name','gallery_cat_id','gallery_image', 'full_image', 'alt_tag'];
        $data['gallery'] = GalleryModel::select($select_table)->where('status','active')
        ->where('gallery_show_type','outside')->limit(3)->orderBy('gallery_id','desc')->get();

        $data['seotitle'] = self::seopage();

        // View::addLocation($data['datadic']);
        return view('website.about-clinic')->with($data);
    }

    public function AboutClinicTwo(){
        // Pick all records of this org list
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        $data['aboutlist'] = $this->homeaboutquery();
        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this gallery list
        $select_table = ['gallery_id','gallery_name','gallery_cat_id','gallery_image', 'full_image', 'alt_tag'];
        $data['gallery'] = GalleryModel::select($select_table)->where('status','active')
        ->where('gallery_show_type','outside')->limit(3)->orderBy('gallery_id','desc')->get();

        $data['seotitle'] = self::seopage();

        // View::addLocation($data['datadic']);
        return view('website.about-clinic-two')->with($data);
    }

    public function AboutDoctor(){
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        $select_fields = ['doc_id','name','short_degree','sort_desc','description','image','banner_image','education_desc','education_detail','experience_desc','experience_detail','alt_tag','url'];
        $data['doctorlist'] = DoctorModel::select($select_fields)->where('status','active')
        ->orderBy('order_by','asc')->get();

        $select_fields = ['mem_id','image','full_image','alt_tag'];
        $data['membership'] = MembershipModel::select($select_fields)->where('status','active')
        ->get();

        $select_fields = ['certificate_id','doctor_id','certificate_name','certificate_image','full_image','alt_tag'];
        $data['doctorcertificate'] = DoctorCertificateModel::select($select_fields)->where('status','active')
        ->orderBy('order_by','asc')->get();

        $data['seotitle'] = self::seopage();

        // print_r($data['doctorcertificate']);
        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;
        // View::addLocation($data['datadic']);
        return view('website.about-doctor')->with($data);
    }

    public static function ContactDetail(){
        $select_fields = ['contact_id','name','address','address1','address2','pincode','email_id','phone','timings'];
        $data['contactus'] = ContactDetailModel::select($select_fields)->where('status','active')
        ->where('show_type','outside')->first();

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        $data['seotitle'] = self::seopage();

        // View::addLocation($data['datadic']);
        return view('website.clinic-address')->with($data);
    }

    public function Services($url){
        
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());
       
        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this service list
        $select_table = ['ser_id','design_type','video_type','service_cat_id','category_type','service_type','service_name','service_image','service_banner_image','alt_tag','short_desc','url'];
        $service = ServiceModel::select($select_table)->where('url',$url)->where('category_type','firstcategory')->where('status','active')->where('service_name','!=','General');

        if($service->count() < 1){
            return $this->SubServices($url);
        }
        $data['service'] = $service->first();
        
        $ser_id = '';
            if(isset($data['service']->ser_id)){
                $ser_id = $data['service']->ser_id;
            }
        // if(isset($data['servicegen']->ser_id)){
        $select_table = ['ser_id','design_type','video_type','service_cat_id','service_type','service_name','service_image','alt_tag','short_desc','url'];
        $data['subservice'] = ServiceModel::select($select_table)->where('parent_id',$data['service']->ser_id)->where('service_name','!=','General')->where('deleted_at', NULL)->where('status','active')->get();
        // print_r($data['service']); die;
        // }

        $data['seotitle'] = self::seopage();

        // View::addLocation($data['datadic']);
        return view('website.service')->with($data);
    }

    public function SubServices($url){
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        $select_table = ['ser_id','parent_id','service_name','banner_show','short_desc','description','service_image','service_banner_image','section','title_tag','keyword_tag','description_tag','canonical_tag'];
        $service = ServiceModel::select($select_table)->where('url',$url)->where('category_type','secondcategory')->where('status','active');
        if($service->count() < 1){
            return $this->ServiceDetails($url);
        }
        $data['service'] = $service->first();
        
        $ser_id = '';
            if(isset($data['service']->ser_id)){
                $ser_id = $data['service']->ser_id;
            }
            
            $select_table = ['ser_id','parent_id','service_name','banner_show','short_desc','description','service_image','service_banner_image','section','title_tag','keyword_tag','description_tag','canonical_tag','url'];
        $data['sub'] = ServiceModel::select($select_table)->where('ser_id',$data['service']->parent_id)->where('status','active')->first();
            
        // Pick all records of this Service faq list
        $select_table = ['ser_faq_id', 'service_cat_id','service_id','question', 'answer','order_by', 'status','ser_faq_status'];
        $data['servicefaqs'] = ServiceFaqModel::select($select_table)->where('status','active')
            ->where('service_id',$ser_id)->get();
        // Pick all records of this service list
         $select_table = ['ser_id','parent_id','service_name','banner_show','short_desc','description','service_image','service_banner_image','section','title_tag','keyword_tag','description_tag','canonical_tag','url'];
        $data['subservice'] = ServiceModel::select($select_table)->where('parent_id',$data['service']->ser_id)->where('status','active')->get();
        // View::addLocation($data['datadic']);
        $data['seotitle'] = self::seopage();

        return view('website.service-category')->with($data);
    }

    public function ServiceDetails($url){
        // echo $url = ;
        // print_r($url);die();
        // Pick all records of this org list
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this service list
        $select_table = ['ser_id','parent_id','service_name','banner_show','short_desc','description','service_image','service_banner_image','section','title_tag','keyword_tag','description_tag','canonical_tag'];
        $servicedetail = ServiceModel::select($select_table)->where('status','active')->where('category_type','service')->where('url',$url);
        // echo $servicedetail->count();die();
        if($servicedetail->count() < 1){
            return redirect('404', 301);
        }
        $data['servicedetail'] = $servicedetail->first();
        $data['seotitle'] = self::seopage();
            $ser_id = '';
            if(isset($data['servicedetail']->ser_id)){
                $ser_id = $data['servicedetail']->ser_id;
            }
            
        
            $cat_table = ['ser_id','service_name','url','parent_id'];
            $data['cat'] = ServiceModel::select($cat_table)->where('status','active')->where('ser_id',$data['servicedetail']->parent_id)->first();

            $select_table = ['ser_id','parent_id','service_name','banner_show','short_desc','description','service_image','service_banner_image','section','title_tag','keyword_tag','description_tag','canonical_tag','url'];
            $data['sub'] = ServiceModel::select($select_table)->where('ser_id',$data['cat']->parent_id)->where('status','active')->first();
            
            // Pick all records of this Service faq list
            $select_table = ['ser_faq_id', 'service_cat_id','service_id','question', 'answer','order_by', 'status','ser_faq_status'];
            $data['servicefaqs'] = ServiceFaqModel::select($select_table)->where('status','active')
            ->where('service_id',$ser_id)->get();

            $data['realresult'] = DB::table('result_inner')->select('*')->where('service_id', $data['servicedetail']->ser_id)->where('status', 'active')->where('deleted_at', NULL);
            $data['videolist'] = DB::table('video_inner')->select('*')->where('service_id', $data['servicedetail']->ser_id)->where('status', 'active')->where('deleted_at', NULL);

            $real_cat = $data['realresult']->first();
            $data['result'] = $data['realresult']->limit(3)->get();
            if($real_cat){
            $data['real_cat'] = DB::table('result_service')->select('url')->where('res_ser_id', $real_cat->result_service_id)->where('deleted_at', NULL)->first();
            }else{
                $data['realresult'] = "";
                $data['real_cat'] = "";
            }

            $video_cat = $data['videolist']->first();
            $data['video'] = $data['videolist']->limit(3)->get();
            if($video_cat){
                $data['video_cat'] = DB::table('video_service')->select('url')->where('vid_ser_id', $video_cat->video_cat_id)->where('deleted_at', NULL)->first();
            }else{
                $data['videolist'] = "";
                $data['video_cat'] = "";
            }
            // View::addLocation($data['datadic']);
            return view('website.service-inner')->with($data);
    }

    public function ErrorPage(){
        $data['seotitle'] = self::seopage();
        return view('website.404')->with($data);
    }

    public function Blogs(){
        // Pick all records of this org list
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this blog list
        $select_table = ['blg_id','blog_type','name','blog_name','short_desc','blog_image','alt_image_name','url','date'];
        $data['blog'] = BlogModel::select($select_table)->where('status','active')->Paginate(6);
        // View::addLocation($data['datadic']);

        $data['seotitle'] = self::seopage();

        return view('website.blogs')->with($data);
    }

    public function BlogDetails($url){
        // Pick all records of this org list
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this blog detail list
        $select_table = ['blg_id','blog_type','name','blog_name','blog_description','blog_image_inner','alt_image_name','url','date','title_tag','keyword_tag','description_tag','canonical'];
        $blogdetail = BlogModel::select($select_table)->where('url',$url)
        ->where('status','active');
        if($blogdetail->count() < 1){
            return redirect('/', 301);
        }
        $data['bloglist'] = $blogdetail->first();
        // Pick five records of this blog list
        $select_table = ['blg_id','blog_type','name','blog_name','blog_description','short_desc','blog_image','alt_image_name','url','date'];
        $data['blog'] = BlogModel::select($select_table)->where('status','active')
        ->limit(3)->orderBy('blg_id','desc')->get();

        $select_fields = ['contact_id','name','address','address1','address2','pincode','email_id','phone','timings'];
        $data['contactus'] = ContactDetailModel::select($select_fields)->where('status','active')
        ->where('show_type','outside')->first();

        $meta = ['seo_id','page_name','title_tag','keyword_tag','description_tag','canonical_tag','url','image','type','site_name','seo_schema'];
        $data['seotitle'] = SeoPageModel::select($meta)->where('url',$url)->where('seo_type','blogcategory')->where('status','active')->first();

        // View::addLocation($data['datadic']);
        return view('website.blog-details')->with($data);
    }

    public function PressMedia(){
        $select_table = ['press_id','name','reference','image','banner_image','url','alt_tag'];

        $data['press'] = PressMediaModel::select($select_table)->where('status','active')->orderBy('press_id','desc')
        ->paginate(6);

        $data['seotitle'] = self::seopage();

        // echo '<pre>';print_r(count($data['casestudies']));echo '</pre>';
        // View::addLocation($data['datadic']);
        return view('website.media')->with($data);

    }

    public function PressMediaDetails($url){
        $select_table = ['press_id','name','reference','image','description','banner_image','url','alt_tag'];

        $data['press'] = PressMediaModel::select($select_table)->where('status','active')->where('url',$url)->orderBy('press_id','desc')
        ->first();

        $data['seotitle'] = self::seopage();

        // echo '<pre>';print_r(count($data['casestudies']));echo '</pre>';
        // View::addLocation($data['datadic']);
        return view('website.press-media-inner')->with($data);

    }

    public function CaseCtudies(){
        // die('hi');
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        // echo $parent_id;die();
        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this case studies list
        $select_table = ['case_id','case_name','case_image','case_banner_image','video_link','alt_tag','description','short_desc','section','url'];
        $data['casestudiesone'] = CaseStudiesModel::select($select_table)->where('status','active')->orderBy('case_id','desc')->limit(1)->first();

        $select_table = ['case_id','case_name','case_image','case_banner_image','video_link','alt_tag','description','short_desc','section','url'];

        $data['casestudies'] = CaseStudiesModel::select($select_table)->where('case_id','!=',$data['casestudiesone']->case_id)->where('status','active')->orderBy('case_id','desc')
        ->paginate(1);

        $data['seotitle'] = self::seopage();

        // echo '<pre>';print_r(count($data['casestudies']));echo '</pre>';
        // View::addLocation($data['datadic']);
        return view('website.case-studies')->with($data);
    }

    public function CaseCtudiesDetails($url){
        // Pick all records of this org list
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this blog detail list
        $select_table = ['blg_id','blog_type','name','blog_name','blog_description','blog_image_inner','alt_image_name','url','date','title_tag','keyword_tag','description_tag','canonical'];
        $blogdetail = BlogModel::select($select_table)->where('url',$url)
        ->where('status','active');
        if($blogdetail->count() < 1){
            return redirect('/', 301);
        }
        $data['bloglist'] = $blogdetail->first();
        // Pick five records of this blog list
        $select_table = ['blg_id','blog_type','name','blog_name','short_desc','blog_image','alt_image_name','url','date'];
        $data['blog'] = BlogModel::select($select_table)->where('status','active')
        ->limit(5)->orderBy('blg_id','desc')->get();

        $select_fields = ['contact_id','name','address','address1','address2','pincode','email_id','phone','timings'];
        $data['contactus'] = ContactDetailModel::select($select_fields)->where('status','active')
        ->where('show_type','outside')->first();

        $data['seotitle'] = self::seopage();

        // View::addLocation($data['datadic']);
        return view('website.blog-details')->with($data);
    }

    public function BookAnAppointment(){
        // Pick all records of this org list
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // $select_fields = ['google_id','site_key','secret_key'];
        // $data['googlerecaptcha'] = GoogleRecaptchaModel::select($select_fields)->where('status','active')->first();

        // View::addLocation($data['datadic']);

        $data['seotitle'] = self::seopage();

        return view('website.request-an-appointment')->with($data);
    }

    public function ContactUs(){
        // Pick all records of this org list
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        $select_fields = ['contact_id','name','address','address1','address2','email_id','phone','timings'];
        $data['contactus'] = ContactDetailModel::select($select_fields)->where('status','active')
        ->get();

        $select_fields = ['google_id','site_key','secret_key'];
        $data['googlerecaptcha'] = GoogleRecaptchaModel::select($select_fields)->where('status','active')
        ->first();

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;
        // View::addLocation($data['datadic']);

        $data['seotitle'] = self::seopage();

        return view('website.contact-us')->with($data);
    }
    public function Testimonials(){
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this testimonial list
        $select_table = ['test_id','name','short_name','designation','source','description','testimonial_image','alt_image_name','rating'];
        $data['testimonial'] = TestimonialModel::select($select_table)->where('status','active')->Paginate(12);
        // View::addLocation($data['datadic']);

        $data['seotitle'] = self::seopage();

        return view('website.testimonials')->with($data);
    }

    public function Gallery(){
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this testimonial list
        $select_table = ['gallery_id','gallery_name','gallery_cat_id','gallery_image', 'full_image', 'alt_tag'];
        $data['gallery'] = GalleryModel::select($select_table)->where('status','active')->orderBy('gallery_id','desc')->paginate(9);
        // View::addLocation($data['datadic']);

        $data['seotitle'] = self::seopage();

        return view('website.gallery')->with($data);
    }

    public function VideoTestimonials(){
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this testimonial list
        $select_table = ['test_video_id','name','image','video','alt_img'];
        $data['testimonial'] = TestimonialVideoModel::select($select_table)->where('status','active')->Paginate(12);
        // View::addLocation($data['datadic']);
        $data['seotitle'] = self::seopage();

        return view('website.video-testimonials')->with($data);
    }

    public function Results(){
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this video list
        $select_table = ['res_ser_id','name','image','alt_tag','url'];
        $data['result'] = ResultServiceModel::select($select_table)->where('status','active')->Paginate(9);
        // print_r($data['video']); die;
        // View::addLocation($data['datadic']);

        $data['seotitle'] = self::seopage();

        return view('website.results')->with($data);
    }

    public function Results_inner($url){
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        $select_table1 = ['res_ser_id','name','image','alt_tag','url'];
        $data['cat'] = ResultServiceModel::select($select_table1)->where('status','active')->where('url',$url)->first();
        // Pick all records of this video list
        if($data['cat']){
            $select_table = ['res_inn_id','name','beforeimg','alt_img'];
            $data['result'] = ResultInnerModel::select($select_table)->where('result_service_id',$data['cat']->res_ser_id)->where('status','active')->get();
        }
        // print_r($data['videos']);
        // View::addLocation($data['datadic']);

        $data['seotitle'] = self::seopage();

        return view('website.results-inner')->with($data);
    }

    public function Videos(){
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // Pick all records of this video list
        $select_table = ['vid_ser_id','name','image','alt_tag','url'];
        $data['video'] = VideoServiceModel::select($select_table)->where('status','active')->Paginate(9);
        // print_r($data['video']); die;
        // View::addLocation($data['datadic']);

        $data['seotitle'] = self::seopage();

        return view('website.videos')->with($data);
    }

    public function Videos_inner($url){
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;
        $select_table1 = ['vid_ser_id','name','image','alt_tag','url'];
        $data['cat'] = VideoServiceModel::select($select_table1)->where('status','active')->where('url',$url)->first();
        // Pick all records of this video list
        if($data['cat']){
            $select_table = ['vid_inn_id','name','image','video','alt_img'];
            $data['video'] = VideoInnerModel::select($select_table)->where('video_cat_id',$data['cat']->vid_ser_id)->where('video_type','video')->where('status','active')->get();
            $data['short'] = VideoInnerModel::select($select_table)->where('video_cat_id',$data['cat']->vid_ser_id)->where('video_type','short')->where('status','active')->get();
            $data['video_type'] = VideoInnerModel::select($select_table)->where('video_cat_id',$data['cat']->vid_ser_id)->where('video_type','video')->where('status','active')->count();
            $data['short_type'] = VideoInnerModel::select($select_table)->where('video_cat_id',$data['cat']->vid_ser_id)->where('video_type','short')->where('status','active')->count();
        }
        // print_r($data['videos']);
        // View::addLocation($data['datadic']);
        $select_fields = ['seo_id','page_name','title_tag','keyword_tag','description_tag','canonical_tag','url','image','type','site_name','seo_schema'];
        $data['seotitle'] = SeoPageModel::select($select_fields)->where('url',$url)->where('seo_type','videofirstcategory')->where('status','active')->first();

        return view('website.video-inner')->with($data);
    }

    public function ThankYou(){
        // Pick all records of this org id
        $siteview = Helpers::orgnizationlist();
        $this->initializeSession($siteview->first());

        //pick resource directory path
        $resdirectory = env('RES_DIRECTORY');
        $frontpathurl = Helpers::frontpathurl();
        $data['datadic'] = $resdirectory.$frontpathurl;

        // print_r($data['videos']);
        // View::addLocation($data['datadic']);

        $data['seotitle'] = self::seopage();

        return view('website.thank-you')->with($data);
    }

    private function initializeSession($result)
    {
        $urlpathget = Helpers::frontpathurl();

        $site_url = request()->root().'/template/'.$urlpathget.'/';
        //set session userinfo
        Session::put('userorginfo', [
            'org_id' => '1',
            'site_id' => '1',
            'site_url' => '1',
        ]);
    }

    public static function seopage()
    {
        $seourl = request()->segment(1);
        $seourl2 = request()->segment(2);
        if($seourl == '' || $seourl == '/'){
            $seourl = 'home';
        }
        // echo $seourl;
        // Pick records of this seo list
        $select_fields = ['seo_id','page_name','title_tag','keyword_tag','description_tag','canonical_tag','url','image','type','site_name','seo_schema'];
        $metatag = SeoPageModel::select($select_fields)->where('url',$seourl)
       ->where('status','active');
        // echo $metatag->count();
       if($metatag->count() > 0){
          return $data['metatag'] = $metatag->first();
       }else{
           $select_fields = ['seo_id','page_name','title_tag','keyword_tag','description_tag','canonical_tag','url','image','type','site_name','seo_schema'];
        $metatag = SeoPageModel::select($select_fields)->where('url',$seourl2)
       ->where('status','active');
       return $data['metatag'] = $metatag->first();
       }
    }

    public function NoAuthorization(){
        return view("401");
    }

    public function uploadimage(Request $request)

    {

        $destinationPath = $this->siteurl->sessionget().'backend/comman';
    //     $destinationPath = 'upload';


        if (!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath, $mode = 0777, true, true);
        }

        if($request->hasFile('upload')) {

            $originName = $request->file('upload')->getClientOriginalName();

            $fileName = pathinfo($originName, PATHINFO_FILENAME);

            $extension = $request->file('upload')->getClientOriginalExtension();

            $fileName = $fileName.'_'.time().'.'.$extension;

        

            $request->file('upload')->move($destinationPath, $fileName);

   

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');

            // $url = asset('images/'.$fileName); 
            $url = '/'.$destinationPath.'/'.$fileName;
            $msg = 'Image uploaded successfully'; 

            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url')</script>";

               

            @header('Content-type: text/html; charset=utf-8'); 

            echo $response;

        }

    }

}
