@extends('admin.master')

@section('content')

@php

$primeid = session('primeid');

$rand = rand(9999, 99999);



@endphp

<section id="main-content">

    <section class="wrapper">

        <div class="card cardsec">

            <div class="card-body">

                <div class="page-title pagetitle">

                    <h1>Add Case Studies</h1>

                    <ul class="breadcrumb side">

                        <li><a href="{{url('Admin/dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

                        <li><a href="{{url('/admin/case-studies')}}">Case Studies</a></li>

                        <li><a href="{{url('/admin/case-studies')}}">Case Studies List</a></li>

                        <li class="active">Add Case Studies</li>

                    </ul>

                </div>

            </div>

        </div>

        <!-- Start form here -->

        <form action="{{URL::to('/admin/create_case_studies')}}" id="myForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">

            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <input type="text" name="case_id" id="case_id" value="@if(isset($primeid)){{$primeid}}@else 0 @endif" style="background: transparent;">

            <input type="hidden" name="category_type" value="caseservice">

            <div class="card">

                <div class="card-body">

                    <?php //echo '<pre>';print_r(session('userinfo')); echo '</pre>';

                    ?>

                    <div class="row">

                        <div class="form-group col-md-4">

                            <label class="control-label">Show Type</label>

                            <select class="form-control valid_name" name="show_type" id="showtype" onclick="serviceshowtype()">

                                <option value="">Select Type</option>

                                <option value="inside" @if(isset($view->show_type))@if($view->show_type=='inside') selected @endif @else selected @endif>Inside</option>

                                <option value="outside" @if(isset($view->show_type))@if($view->show_type=='outside') selected @endif @endif>Outside</option>

                            </select>

                        </div>

                        <div class="form-group col-md-4">

                            <label class="control-label">Select Design Type</label>

                            <select class="form-control" id="design_type" name="design_type">

                                <option value="">Select Design Type</option>

                                <option value="left" @if(isset($view->design_type))@if($view->design_type=='left') selected @endif @else selected @endif>Left Side Image</option>

                                <option value="right" @if(isset($view->design_type))@if($view->design_type=='right') selected @endif @endif>Right Side Image</option>

                            </select>

                        </div>



                        <div class="form-group col-md-4">

                            <label class="control-label">Case Studies Name</label>

                            <input class="form-control valid_name" name="case_name" value="@if(isset($view->case_name)) {{$view->case_name}} @endif" type="text">

                        </div>

                        <div class="form-group  col-md-4">
                            <div class="admin_img_card">

                                @if(isset($view->case_image))

                                @if($view->case_image!='')

                                <img src="{{url('backend/casestudies/image/'.$view->case_image)}}" style="width: 85px;height: 85px;"><br>

                                @else

                                <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">

                                @endif

                                @else

                                <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;"><br>

                                @endif

                                <div class="form-group">

                                    <label class="control-label">Image</label>

                                    <input class="form-control" name="case_image" type="file">

                                    <input class="form-control" name="oldcase_image" value="@if(isset($view->case_image)){{$view->case_image}}@endif" type="hidden">
                                    <span class="image_tile">Image type: jpeg, jpg, png, webp | max-size: 100kb</span>


                                </div>
                            </div>


                        </div>



                        <div class="form-group col-md-4">

                            <label class="control-label">Case Studies Banner Type</label>

                            <select class="form-control valid_name" name="video_type" id="video_type" onchange="servicetype()">

                                <option value="">Select Type</option>

                                <option value="image" @if(isset($view->video_type))@if($view->video_type=='image') selected @endif @else selected @endif>Image</option>

                                <option value="link" @if(isset($view->video_type))@if($view->video_type=='link') selected @endif @endif>Youtube Link</option>

                            </select>

                        </div>

                        <div class="form-group col-md-4">
                            <div class="admin_img_card">

                                @if(isset($view->case_banner_image))

                                @if($view->case_banner_image!='')

                                <img src="{{url('backend/casestudies/banner/'.$view->case_banner_image)}}" style="width: 85px;height: 85px;"><br>

                                @else

                                <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">

                                @endif

                                @else

                                <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">

                                @endif

                                <div class="form-group">

                                    @if(isset($view->video_type))

                                    @if($view->video_type=='image')

                                    <label class="control-label videoupload">Banner Image</label>

                                    <label class="control-label videolink" style="display:none;">Banner Thumbnail Image</label>

                                    @else

                                    <label class="control-label videoupload">Banner Image</label>

                                    <label class="control-label videolink" style="display:none;">Banner Thumbnail Image</label>

                                    @endif

                                    @else

                                    <label class="control-label videoupload">Banner Image</label>

                                    <label class="control-label videolink" style="display:none;">Banner Thumbnail Image</label>

                                    @endif

                                    <input class="form-control" name="case_banner_image" type="file">

                                    <input class="form-control" name="oldcase_banner_image" value="@if(isset($view->case_banner_image)) {{$view->case_banner_image}} @endif" type="hidden">
                                    <span class="image_tile">Image type: jpeg, jpg, png, webp | max-size: 100kb</span>


                                </div>
                            </div>


                        </div>





                        @if(isset($view->video_type))

                        @if($view->video_type=='link')

                        <div class="col-sm-4 videolink">

                            @if($view->video_link!='')

                            <iframe width="100%" height="76" src="{{$view->video_link}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                            @else

                            <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">

                            @endif

                            <div class="form-group">

                                <label class="control-label">Case Studies Youtube Link</label>

                                <input class="form-control" name="video_link" value="@if(isset($view->video_link)){{$view->video_link}}@endif" type="text">

                            </div>

                        </div>

                        @else

                        <div class="form-group col-md-4 videolink" style="display:none;">

                            <label class="control-label">Case Studies Youtube Link</label>

                            <input class="form-control" name="video_link" value="@if(isset($view->video_link)) {{$view->video_link}} @endif" type="text">

                        </div>

                        @endif

                        @endif



                        <div class="form-group col-md-4">

                            <label class="control-label">Image Alt Tag</label>

                            <input class="form-control" name="alt_tag" value="@if(isset($view->alt_tag)) {{$view->alt_tag}} @endif" type="text">

                        </div>

                        <div class="form-group col-md-12">

                            <label class="control-label">Short Description</label>

                            <textarea class="form-control ckeditor" name="short_desc" rows="3">@if(isset($view->short_desc)) {{$view->short_desc}} @endif</textarea>

                            <span class="help-block"></span>

                        </div>

                        <div class="form-group col-md-12">

                            <label class="control-label">Description</label>

                            <textarea class="form-control ckeditor" name="description" rows="3">@if(isset($view->description)) {{$view->description}} @endif</textarea>

                            <span class="help-block"></span>

                        </div>

                        @if(isset($view->show_type))

                        @if($view->show_type=='outside')

                        <div class="form-group col-md-4">
                            <div class="admin_img_card">

                                @if(isset($view->home_banner_image))

                                @if($view->home_banner_image!='')

                                <img src="{{url('backend/casestudies/banner/'.$view->home_banner_image)}}" style="width: 85px;height: 85px;"><br>

                                @else

                                <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">

                                @endif

                                @else

                                <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">

                                @endif

                                <div class="form-group">

                                    <label class="control-label">Home Case Studies Image</label>

                                    <input class="form-control" name="home_banner_image" type="file">

                                    <input class="form-control" name="oldhome_banner_image" value="@if(isset($view->home_banner_image)) {{$view->home_banner_image}} @endif" type="hidden">
                                    <span class="image_tile">Image type: jpeg, jpg, png, webp | max-size: 100kb</span>


                                </div>
                            </div>


                        </div>

                        <div class="form-group col-md-12">

                            <label class="control-label">Home Case Studies Description</label>

                            <textarea class="form-control ckeditor" name="homedescription" rows="3">@if(isset($view->homedescription)) {{$view->homedescription}} @endif</textarea>

                            <span class="help-block"></span>

                        </div>

                        @endif

                        @else

                        <div class="form-group col-md-4 homeservice" style="display: none;">
                            <div class="admin_img_card">

                                <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">

                                <div class="form-group">

                                    <label class="control-label">Home Case Studies Image</label>

                                    <input class="form-control" name="home_banner_image" type="file">

                                    <input class="form-control" name="oldhome_banner_image" value="@if(isset($view->home_banner_image)) {{$view->home_banner_image}} @endif" type="hidden">
                                    <span class="image_tile">Image type: jpeg, jpg, png, webp | max-size: 100kb</span>
                                </div>
                            </div>


                        </div>

                        <div class="form-group col-md-12 homeservice" style="display: none;">

                            <label class="control-label">Home Case Studies Description</label>

                            <textarea class="form-control ckeditor" name="homedescription" rows="3">@if(isset($view->homedescription)) {{$view->homedescription}} @endif</textarea>

                            <span class="help-block"></span>

                        </div>

                        @endif



                    </div>

                    <?php $seccount = '0'; ?>

                    @if(isset($view->section))

                    <?php

                    $sersection = json_decode($view->section);

                    $sersectionorder = collect($sersection)->sortBy('secorderby');

                    $seccount = count($sersection);

                    $i = 1;

                    ?>

                    @foreach($sersectionorder as $key => $sersectionlist)

                    <?php

                    $rand = rand('9999', '99999');



                    ?>

                    <div id="removesec{{$rand}}" class="sectionservice">

                        <input class="form-control" name="service_type[]" value="@if(isset($sersectionlist->type)){{$sersectionlist->type}}@endif" type="hidden">

                        <div class="form-group col-md-12 col s12">

                            <input type="text" class="form-control orderby{{$rand}}" value="@if(isset($sersectionlist->secorderby)){{$sersectionlist->secorderby}}@endif" name="secorderby[]">

                            <button type="button" class="btn btn-danger marginT38 buttonright" onclick="removesec('{{$rand}}','{{$key}}')">-</button>

                        </div>

                        <div class="form-group col-md-12 col s12">

                            <label class="control-label">Section Heading</label>

                            <input class="form-control" name="service_heading[]" value="@if(isset($sersectionlist->service_heading)){{$sersectionlist->service_heading}}@endif" type="text">



                            <input type="hidden" name="servicesection[]" value="servicesection">

                        </div>

                        <div class="form-group col-md-12 col s12">

                            <div class="form-group col-md-4 col s4">

                                <label class="control-label">Select Button Type</label>

                                <select class="form-control" id="button_type{{$rand}}" name="button_type[]" onchange="buttontype('{{$rand}}')">

                                    <option value="">Select Button Type</option>

                                    <option value="Yes" @if(isset($sersectionlist->button_type)) @if($sersectionlist->button_type == 'Yes') selected @endif @endif>Yes</option>

                                    <option value="No" @if(isset($sersectionlist->button_type)) @if($sersectionlist->button_type == 'No') selected @endif @else selected @endif>No</option>

                                </select>

                            </div>

                            <div class="showtype{{$rand}}" style="<?php if (isset($sersectionlist->button_type)) {
                                                                        if ($sersectionlist->button_type == 'Yes') {
                                                                            echo 'display: block';
                                                                        } else {
                                                                            echo 'display: none';
                                                                        }
                                                                    } else {
                                                                        echo 'display: none';
                                                                    } ?>">

                                <div class="form-group col-md-4 col s4">

                                    <label class="control-label">Button Name</label>

                                    <input class="form-control" name="button_name[]" value="@if(isset($sersectionlist->button_name)){{$sersectionlist->button_name}}@endif" type="text">

                                </div>

                                <div class="form-group col-md-4 col s4">

                                    <label class="control-label">Button Url</label>

                                    <input class="form-control" name="button_url[]" value="@if(isset($sersectionlist->button_url)){{$sersectionlist->button_url}}@endif" type="text">

                                </div>

                                <div class="form-group col-md-12 col s12">

                                    <label class="control-label">Button Style</label>

                                    <textarea class="form-control" name="button_style[]">@if(isset($sersectionlist->button_style)){{$sersectionlist->button_style}}@endif</textarea>

                                </div>

                            </div>



                            <div class="form-group col-md-4 col s4">

                                <label class="control-label">Select Section Appointment Form</label>

                                <select class="form-control" name="appointment_side[]">

                                    <option value="">Select Button Type</option>

                                    <option value="upper" @if(isset($sersectionlist->appointment_side)) @if($sersectionlist->appointment_side == 'upper') selected @endif @else selected @endif>Upper Side</option>

                                    <option value="down" @if(isset($sersectionlist->appointment_side)) @if($sersectionlist->appointment_side == 'down') selected @endif @endif>Down Side</option>

                                </select>

                            </div>

                        </div>

                        <div class="form-group col-md-12 col s12">

                            <div class="form-group col-md-4 col s4">

                                <label class="control-label">Select Background Type</label>

                                <select class="form-control" id="bg_type{{$rand}}" name="bg_type[]" onchange="bgtypeimage('{{$rand}}')">

                                    <option value="">Select Background Type</option>

                                    <option value="white" @if(isset($sersectionlist->bg_type)) @if($sersectionlist->bg_type == 'white') selected @endif @else selected @endif>White</option>

                                    <option value="bgcolor" @if(isset($sersectionlist->bg_type)) @if($sersectionlist->bg_type == 'bgcolor') selected @endif @endif>Background Color</option>

                                    <option value="bgimage" @if(isset($sersectionlist->bg_type)) @if($sersectionlist->bg_type == 'bgimage') selected @endif @endif>Background Image</option>

                                </select>

                            </div>



                            <div class="bgcolor{{$rand}}" style="<?php if (isset($sersectionlist->bg_type)) {
                                                                        if ($sersectionlist->bg_type == 'bgcolor') {
                                                                            echo 'display: block';
                                                                        } else {
                                                                            echo 'display: none';
                                                                        }
                                                                    } else {
                                                                        echo 'display: none';
                                                                    } ?>">

                                <div class="form-group col-md-12 col s12">

                                    <label class="control-label">Background Color Style</label>

                                    <textarea class="form-control" name="bgcolor_style[]">{{$sersectionlist->bgcolor_style}}</textarea>

                                </div>

                            </div>



                            <div class="bgimagetype{{$rand}}" style="<?php if (isset($sersectionlist->bg_type)) {
                                                                            if ($sersectionlist->bg_type == 'bgimage') {
                                                                                echo 'display: block';
                                                                            } else {
                                                                                echo 'display: none';
                                                                            }
                                                                        } else {
                                                                            echo 'display: none';
                                                                        } ?>">

                                <div class="form-group col-md-6 col s6">

                                    @if($sersectionlist->bgimage==NULL || $sersectionlist->bgimage=='')

                                    <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">

                                    @else

                                    <img src="{{url('/backend/service/section_banner/'.$sersectionlist->bgimage)}}" style="width:85px;height:85px;">

                                    @endif



                                    <label class="control-label">Bg Image</label>

                                    <input type="file" name="bgimage[]">

                                    <input class="form-control" name="oldbgimage[]" value="{{$sersectionlist->bgimage}}" type="hidden">

                                </div>

                            </div>

                        </div>

                        @if(isset($sersectionlist->image))

                        <div class="form-group col-md-12 col s12">

                            @if($sersectionlist->image==NULL || $sersectionlist->image=='')

                            <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">

                            @else

                            <img src="{{url('/backend/service/section/'.$sersectionlist->image)}}" style="width:85px;height:85px;">

                            @endif



                            <label class="control-label">Image</label>

                            <input type="file" name="serviceimage[]">

                            <input class="form-control" name="oldserviceimage[]" value="{{$sersectionlist->image}}" type="hidden">

                        </div>

                        @endif

                        @if(isset($sersectionlist->section1))

                        <div class="col-md-12 col s12">

                            <label class="control-label">Section 1</label>

                            <textarea class="form-control ckeditor editorleft{{$rand}}" id="editorleft{{$rand}}" name="service1[]" rows="3">{{$sersectionlist->section1}}</textarea>

                            <span class="help-block"></span>

                        </div>

                        @else

                        <input type="hidden" name="service1[]">

                        @endif

                        @if(isset($sersectionlist->section2))

                        <div class="col-md-12 col s12">

                            <label class="control-label">Section 1</label>

                            <textarea class="form-control ckeditor editorleft{{$rand}}" id="editorleft{{$rand}}" name="service2[]" rows="3">{{$sersectionlist->section2}}</textarea>

                            <span class="help-block"></span>

                        </div>

                        @else

                        <input type="hidden" name="service2[]">

                        @endif

                        <!-- @if($sersectionlist->type=='threeparagraph' || $sersectionlist->type=='tabparagraph')

                      <input type="text" name="service1[]">

                      <input type="text" name="service2[]">

                      @endif



                      @if($sersectionlist->type=='sectionthreeparagraph' || $sersectionlist->type=='sectiontabparagraph')

                      <input type="text" name="service2[]">

                      @endif -->

                        <?php

                        $datasec = $sersectionlist->threepragraph;

                        ?>

                        @if(isset($datasec->orderby))

                        <?php

                        $orderbypragraphsec = collect($datasec->orderby)->sortBy('orderbypragraph');

                        ?>

                        @foreach($orderbypragraphsec as $orderkey => $threelist)

                        <?php $randsec = rand('9999', '99999'); ?>

                        <div id="removethreesec{{$randsec}}">

                            <div class="col-md-11 col s11 pragraphsec">

                                <label class="control-label">Three Pragraph Section</label>

                                <textarea class="form-control ckeditor editorleft{{$randsec}}" id="editorleft{{$randsec}}" name="threepragraph{{$i}}[]" rows="3">{{$datasec->threeparagraphdata[$orderkey]}}</textarea>

                                <span class="help-block"></span>

                            </div>

                            <div class="col-md-1 col s1">

                                <input type="text" class="form-control orderby{{$randsec}}" value="{{$threelist}}" name="orderbypragraph{{$i}}[]">

                                @if($orderkey == '0')

                                <button type="button" class="btn btn-primary marginT38 buttonright" onclick="editthreepragraph('{{$rand}}','{{$i}}')">+</button>

                                @else

                                <button type="button" class="btn btn-danger marginT38 buttonright" onclick="removethreepragraph('{{$randsec}}')">-</button>

                                @endif

                            </div>

                        </div>



                        @endforeach

                        @endif



                        <?php

                        $datasec = $sersectionlist->tabpragraph;

                        ?>

                        @if(isset($datasec->taborderby))

                        <?php

                        $orderbytabpragraphsec = collect($datasec->taborderby)->sortBy('taborderby');

                        ?>

                        @foreach($orderbytabpragraphsec as $taborderkey => $tablist)

                        <?php $randsec = rand('9999', '99999'); ?>

                        <div id="removethreesec{{$randsec}}">

                            <div class="form-group col-md-12">

                                <label class="control-label">Tab Heading</label>

                                <input class="form-control tabheading{{$randsec}}" name="tab_heading{{$i}}[]" value="{{$datasec->tabheadingdata[$taborderkey]}}" type="text">

                            </div>

                            <?php $serviceimg = ''; ?>

                            @if(isset($datasec->tabimage[$taborderkey]))

                            @php

                            $serviceimg = $datasec->tabimage[$taborderkey];

                            @endphp

                            @endif

                            <div class="form-group col-md-12 col s12">

                                @if($serviceimg==NULL || $serviceimg=='')

                                <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">

                                @else

                                <img src="{{url('/backend/service/section/'.$serviceimg)}}" style="width:85px;height:85px;">

                                @endif



                                <label class="control-label">Image</label>

                                <input type="file" name="tabimage{{$i}}[]">

                                <input class="form-control" name="oldtabimage{{$i}}[]" value="hi" type="hidden">

                            </div>

                            <div class="form-group col-md-12">

                                <label class="control-label">Heading2</label>

                                <input class="form-control tabheading{{$randsec}}" name="tab_heading2{{$i}}[]" value="{{$datasec->tabheadingdata2[$taborderkey]}}" type="text">

                            </div>

                            <div class="col-md-11 col s11 pragraphsec">

                                <label class="control-label">Tab Pragraph Section</label>

                                <textarea class="form-control ckeditor editorleft{{$randsec}}" id="editorleft{{$randsec}}" name="tabpragraph{{$i}}[]" rows="3">{{$datasec->tabparagraphdata[$taborderkey]}}</textarea>

                                <span class="help-block"></span>

                            </div>



                            <div class="col-md-1 col s1">

                                <input type="text" class="form-control orderby{{$randsec}}" value="{{$tablist}}" name="orderbytabpragraph{{$i}}[]">

                                @if($taborderkey == '0')

                                <button type="button" class="btn btn-primary marginT38 buttonright" onclick="editthreepragraph('{{$rand}}','{{$i}}')">+</button>

                                @else

                                <button type="button" class="btn btn-danger marginT38 buttonright" onclick="removethreepragraph('{{$randsec}}')">-</button>

                                @endif

                            </div>

                        </div>



                        @endforeach

                        @endif

                        <div id="editthreeparagraph{{$rand}}"></div>

                    </div>

                    <?php ++$i; ?>

                    @endforeach

                    @endif









                    <div class="row">

                        <div class="form-group col-sm-12 col s12">

                            <label>Add More Section</label>

                            <select class="form-control" id="service_list{{$rand}}" name="service_type[]" onchange="servicelist('{{$rand}}')">

                                <option value="">Add More Section</option>

                                <option value="fulltext">Full Text</option>

                                <option value="imagetext">Left Image and Right Text</option>

                                <option value="rightimagetext">Right Image and Left Text</option>

                                <option value="leftheading">Left Heading and Right Text</option>

                                <option value="twoparagraph">Two Paragraph</option>

                                <option value="twoparagraphbgcolor">Two Paragraph BG Color</option>

                                <option value="threeparagraph">Three Paragraph or More</option>

                                <option value="sectionthreeparagraph">Section Three Paragraph or More</option>

                                <option value="tabparagraph">Tab Paragraph or More</option>

                                <option value="sectiontabparagraph">Section Tab Paragraph or More</option>

                            </select>

                        </div>



                        <div id="servicelist{{$rand}}"></div>

                    </div>



                    <input type="hidden" value="{{$seccount}}" id="countparagraph">

                    <input type="hidden" value="00000" id="allarraycount">



                    <div class="row">

                        <div class="form-group col-sm-4">

                            <label>Title Tags</label>

                            <input class="form-control" name="title_tag" value="@if(isset($view->title_tag)) {{$view->title_tag}} @endif" type="text">

                        </div>



                        <div class="form-group col-sm-4">

                            <label>Keyword Tag</label>

                            <input class="form-control" name="keyword_tag" value="@if(isset($view->keyword_tag)) {{$view->keyword_tag}} @endif" type="text">

                        </div>



                        <div class="form-group col-sm-4">

                            <label>Description Tag</label>

                            <input class="form-control" name="description_tag" value="@if(isset($view->description_tag)) {{$view->description_tag}} @endif" type="text">

                        </div>



                        <div class="form-group col-sm-4">

                            <label>Canonical</label>

                            <input class="form-control" name="canonical_tag" value="@if(isset($view->canonical_tag)) {{$view->canonical_tag}} @endif" type="text">

                        </div>



                        <div class="form-group col-sm-4">

                            <label class="control-label">Url</label>

                            <input class="form-control" name="url" value="@if(isset($view->url)) {{$view->url}} @endif" type="text">

                        </div>



                        <div class="col-sm-12 marginT30">

                            <button type="button" id="myBtn" class="btn btn-primary icon-btn" >

                                <i class="fa fa-fw fa-lg fa-check-circle"></i>Submit

                            </button>&nbsp;&nbsp;&nbsp;

                            <a class="btn btn-default icon-btn" href="{{url('/admin/case-studies')}}">

                                <i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel

                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </form>

        <!-- End form -->

        <?php

        if (isset($view->ser_id))

            $ser_id = $view->ser_id;

        else

            $ser_id = '';

        ?>

    </section>

</section>

<script>
    CKEDITOR.replace('textArea');

    //     function removedata(data){

    //        // return data.filter((value, index) => data.indexOf(value) === index);

    //        uniqueArray = data.filter(function(item, pos) {

    //     return data.indexOf(item) == pos;

    // })

    // // console.log(uniqueArray);

    //     }



    function getUnique(array) {

        var uniqueArray = [];



        // Loop through array values

        for (var value of array) {

            if (uniqueArray.indexOf(value) === -1) {

                uniqueArray.push(value);

            }

        }

        return uniqueArray;

    }



    function servicelist(rand) {

        var servicelist = $("#service_list" + rand).val();

        var allarray = $('#allarraycount').val();

        var allarraydata = allarray.split(',');

        var newarray = [rand];

        var uniqueArrayl = [];

        var alldata = uniqueArrayl.concat(allarraydata, newarray);

        var allnewarray = getUnique(alldata);

        var allarray = $('#allarraycount').val(allnewarray);

        // console.log(rand);

        // console.log(allnewarray.includes(rand));

        if (allarraydata.includes(rand) === false) {

            var countsec = $('#countparagraph').val();

            var no = 1;

            var serial = parseInt(countsec) + parseInt(no);

            $('#countparagraph').val(serial);

        }

        $.ajax({

            type: "post",

            // cache: false,

            // async: false,

            url: "{{url('/admin/case-studies-list')}}",

            data: {

                'servicelist': servicelist

            },

            success: function(result) {

                $("#servicelist" + rand).html(result);

            },

            complete: function() {},

        });

    }





    function threepragraph(rand) {

        var token = $("#token").val();

        //   alert(servicelist)

        var countsec = $('#countparagraphlist' + rand).val();

        // $('#totalseccount').val(countsec);

        $.ajax({

            type: "post",

            // cache: false,

            // async: false,

            url: "{{url('/admin/casethreepragraph-list')}}",

            data: {

                '_token': token,
                'countsec': countsec

            },

            success: function(result) {

                $("#addthreeparagraph" + rand).append(result);

            },

            complete: function() {},

        });

    }



    function tabpragraph(rand) {

        var token = $("#token").val();

        //   alert(servicelist)

        var countsec = $('#countparagraphlist' + rand).val();

        // $('#totalseccount').val(countsec);

        $.ajax({

            type: "post",

            // cache: false,

            // async: false,

            url: "{{url('/admin/casetabpragraph-list')}}",

            data: {

                '_token': token,
                'countsec': countsec

            },

            success: function(result) {

                $("#addtabparagraph" + rand).append(result);

            },

            complete: function() {},

        });

    }



    function editthreepragraph(rand, countsec) {

        var token = $("#token").val();

        //   alert(servicelist)

        // $('#totalseccount').val(countsec);

        $.ajax({

            type: "post",

            // cache: false,

            // async: false,

            url: "{{url('/admin/casethreepragraph-list')}}",

            data: {

                '_token': token,
                'countsec': countsec

            },

            success: function(result) {

                $("#editthreeparagraph" + rand).append(result);

            },

            complete: function() {},

        });

    }





    function orderbyservicesec(rand, secid) {

        var order_by = $("#orderby" + secid).val();

        // alert(order_by);

        $.ajax({

            type: "post",

            // cache: false,

            // async: false,

            url: "{{url('/admin/case-studies-section-order')}}",

            data: {

                'post': 'ok',

                'secid': secid,

                'order_by': order_by

            },

            success: function(result) {



            },

            complete: function() {},

        });

    }



    function removesecrvice(rand, secid) {

        // var tabserial = $("#tabserial"+rand).val();

        // var no = 1;

        // var serial = parseInt(tabserial) + parseInt(no);

        // alert(serial);

        if (confirm("Do you really want to delete record?") == true) {

            $.ajax({

                type: "post",

                // cache: false,

                // async: false,

                url: "{{url('/admin/case-studies-section-delete')}}",

                data: {

                    'post': 'ok',

                    'secid': secid

                },

                success: function(result) {

                    // alert(result); 



                    window.location.href = "{{url('/admin/add-case-studies')}}"

                },

                complete: function() {},

            });

        }

    }



    function removethreepragraph(rand) {

        $('#removethreesec' + rand).remove();

    }



    function buttontype(rand) {

        var button_type = $('#button_type' + rand).val();

        // alert(button_type);

        if (button_type == 'Yes') {

            $('.showtype' + rand).show();

        } else if (button_type == 'No') {

            $('.showtype' + rand).hide();

        }

    }



    function bgtypeimage(rand) {

        var bg_type = $('#bg_type' + rand).val();

        // alert(button_type);

        if (bg_type == 'bgcolor') {

            $('.bgcolor' + rand).show();

            $('.bgimagetype' + rand).hide();

        } else if (bg_type == 'bgimage') {

            $('.bgcolor' + rand).hide();

            $('.bgimagetype' + rand).show();

        } else if (bg_type == 'white') {

            $('.bgcolor' + rand).hide();

            $('.bgimagetype' + rand).hide();

        }

    }



    function removesec(rand, keydata) {

        var token = $("#token").val();

        // alert(order_by);

        if (confirm("Do you really want to delete record?") == true) {

            $.ajax({

                type: "post",

                // cache: false,

                // async: false,

                url: "{{url('/admin/case-studies-removesec')}}",

                data: {

                    '_token': token,

                    'keydata': keydata

                },

                success: function(result) {

                    // alert(result); 

                    window.location.href = "{{url('/admin/add-case-studies')}}"

                    // $('#removesec'+rand).remove();



                },

                complete: function() {},

            });

        }



    }
</script>

@endsection