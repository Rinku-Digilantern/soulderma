@extends('admin.master')
@section('content')
<section id="main-content">
  <section class="wrapper">
<!--     @php-->
<!--echo '<pre>';print_r(session('userinfo'));echo '</pre>';-->
<!--@endphp-->
    <div class="page-title">
      <div>
        <h1>Dashboard</h1>
        <ul class="breadcrumb side">
          <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="active">Dashboard List</li>
        </ul>
      </div>
    </div>
    <div class="row mt-30">
      <div class="col-lg-3 col-md-6">
        <div class="iq-card-body">
          <div class="iq-dlfex">
            <div class="text-left">
              <span class="title-txt total-tit"><small>Total</small>Appointment</span>
              <span class="number-txt">0</span>
            </div>
            <div class="rounded-circle iq-card-icon bg-primary"><i class="fa fa-user fontsmall"></i></div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="iq-card-body">
          <div class="iq-dlfex">
            <div class="text-left">
              <span class="title-txt schedule-tit"><small>Schedule</small>Appointment</span>
              <span class="number-txt">0</span>
            </div>
            <div class="rounded-circle iq-card-icon schedule-bg bg-primary"><i class="fa fa-user fontsmall"></i></div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="iq-card-body">
          <div class="iq-dlfex">
            <div class="text-left">
              <span class="title-txt tomorrow-tit"><small>Tomorrow</small>Appointment</span>
              <span class="number-txt">0</span>
            </div>
            <div class="rounded-circle iq-card-icon tomorrow-bg bg-primary"><i class="fa fa-user fontsmall"></i></div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="iq-card-body p20">
          <img src="{{ url('/admin/images/hospital-tab.jpg') }}" />
        </div>
      </div>
    </div>
  </section>
</section>

<!-- profile page -->



<!--<div class="profile_page">-->
<!--  <section class="vh-100 ">-->
<!--    <div class="container py-5 h-100">-->
<!--      <div class="row d-flex justify-content-center align-items-center h-100">-->
<!--        <div class="col user_card_desing mb-4 mb-lg-0">-->
<!--          <div class="card user_card " style="border-radius: 25px 0;">-->
<!--            <div class="row g-0">-->
<!--              <div class="col-md-4 gradient-custom  text-center ">-->
<!--                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava1-bg.webp" alt="Avatar" class="img-fluid my-5" style="width: 80px;" />-->
<!--                <div class="user_name">Marie Horwitz</div>-->
<!--                <a href="#"><i class="fa fa-pencil-square-o mb-5"></i></a>-->
<!--              </div>-->
<!--              <div class="col-md-8">-->
<!--                <div class="card-body p-4">-->
<!--                  <div class="info">-->
<!--                    Information-->
<!--                  </div>-->
<!--                  <hr class="mt-0 mb-4">-->
<!--                  <div class=" pt-1 profile_detail">-->
<!--                    <div class="col-6 ">-->
<!--                      <h6>Email</h6>-->
<!--                      <p class="text-muted">info@example.com</p>-->
<!--                    </div>-->
<!--                    <div class="col-6 ">-->
<!--                      <h6>Phone</h6>-->
<!--                      <p class="text-muted">123 456 0789</p>-->
<!--                    </div>-->
<!--                  </div>-->
<!--                </div>-->
<!--              </div>-->
<!--            </div>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--    </div>-->
<!--  </section>-->
<!--</div>-->





@endsection