<?php 
// print_r($data);
// print_r($users);
// die;
?>
@extends('admin.master')
@section('content')
<!--main content start-->
<style>
  .selet-user-style {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 10px;
}

.selet-user-style label {
    width: 20%;
}

.selet-user-style select {
    width: 50%;
}
</style>

<section id="main-content">
  <section class="wrapper">
    <div class="page-title">
      <div>
        <h1>Login Activity List</h1>
        <ul class="breadcrumb side">
          <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/admin/log')}}">Login Activity List</a></li>
          <li class="active"></li>
        </ul>
      </div>
      <div><button id="export" class="btn btn-primary">Export</button></div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="selet-user-style">
        <label>Select User</label>
        <select  class="form-control"  onchange="loadLogData(this.value)">
          <option value=""> Select User</option>
          @foreach($users as $user)
          <option value="{{ $user->usr_id }}" data-id="{{ $user->usr_id }}">{{$user->usr_email}}</option>
          @endforeach
        </select>
        </div>
      </div>
     
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <table class="table table-hover table-bordered" id="sampleTable">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>URL</th>
                  <th>Remarks</th>
                  <th>Created Date</th>
                </tr>
              </thead>
              <tbody id="jsontable">
                  
              </tbody>
            </table>
           
          </div>
        </div>
      </div>
    </div>
  </section>
</section>

<script type="text/javascript">
  $("button").click(function(){
  $("#sampleTable").table2excel({
    // exclude CSS class
    exclude: ".noExl",
    name: "Worksheet Name",
    filename: "Contact.xls" //do not include extension
  }); 
});
</script>
<!--main content end-->
@endsection