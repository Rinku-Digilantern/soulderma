@extends('admin.master')
@section('content')
@php
$primeid = session('primeid');
@endphp


<div id="main">
    <section id="main-content">
        <section class="wrapper">
            <div class="card cardsec">
                <div class="card-body">
                    <div class="page-title">
                        <div>
                            <h1>Add Doctor Certificate Image</h1>
                            <ul class="breadcrumb side">
                                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
                                <li><a href="{{url('/admin/doctor-certificate')}}">DoctorCertificate List</a></li>
                                <li class="active">Add Doctor Certificate Image</li>
                            </ul>
                        </div>
                        <div>
                            <a href="{{url('/admin/doctor-certificate')}}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <!-- Start form here -->
                    <form action="{{URL::to('/admin/create_doctor_certificate')}}" id="myForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                        <input type="hidden" name="site_id" value="{{session('useradmin')['site_id']}}">
                        <input type="hidden" name="org_id" value="{{session('useradmin')['org_id']}}">
                        <input type="hidden" name="created_by" value="{{session('useradmin')['usr_id']}}">
                        <input type="hidden" name="certificate_id" id="certificate_id" value="@if(isset($primeid)){{$primeid}}@else 0 @endif">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="form-group col-md-4 col s4">
                                    <label class="control-label">Show Type</label>
                                    <select class="form-control valid_name" name="certificate_show_type">
                                        <option value="">Select Type</option>
                                        <option value="inside" @if(isset($view->
                                            certificate_show_type))@if($view->certificate_show_type=='inside') selected
                                            @endif @else selected @endif>Inside</option>
                                        <option value="outside" @if(isset($view->
                                            certificate_show_type))@if($view->certificate_show_type=='outside') selected
                                            @endif @endif>Outside</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4  col s4">
                                    <label class="control-label">Doctor List</label>
                                    <select class="form-control valid_name" name="doctor_id">
                                        <option value="">Select Doctor List</option>
                                        @foreach($doctorlist as $doctorlistsec)
                                        <option value="{{ $doctorlistsec->doc_id }}" @if(isset($view->
                                            doctor_id))@if($view->doctor_id==$doctorlistsec->doc_id) selected @endif
                                            @endif>{{$doctorlistsec->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group  col-md-4  col s4">
                                    <label class="control-label">Image Alt Tag</label>
                                    <input class="form-control" name="alt_tag" value="@if(isset($view->alt_tag)) {{$view->alt_tag}} @endif" type="text">
                                </div>
                                <div class="form-group  col-md-4  col s4">
                                    <div class="admin_img_card">

                                        @if(isset($view->certificate_image))
                                        @if($view->certificate_image!='')
                                        <img src="{{url('/'.session('useradmin')['site_url'].'backend/certificate/'.$view->certificate_image)}}" style="width: 85px;height: 85px;"><br>
                                        @else
                                        <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">
                                        @endif
                                        @else
                                        <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;"><br>
                                        @endif
                                        <div class="form-group">
                                            <label class="control-label">Image</label>
                                            <input class="form-control" name="certificate_image" type="file">
                                            <input class="form-control" name="oldcertificate_image" value="@if(isset($view->certificate_image)){{$view->certificate_image}}@endif" type="hidden">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group  col-md-4  col s4">
                                    <div class="admin_img_card">
                                        @if(isset($view->full_image))
                                        @if($view->full_image!='')
                                        <img src="{{url('/'.session('useradmin')['site_url'].'backend/certificate/'.$view->full_image)}}" style="width: 85px;height: 85px;"><br>
                                        @else
                                        <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;">
                                        @endif
                                        @else
                                        <img src="{{url('/images/no_image.jpg')}}" style="width:85px;height:85px;"><br>
                                        @endif
                                        <div class="form-group">
                                            <label class="control-label">Image</label>
                                            <input class="form-control" name="full_image" type="file">
                                            <input class="form-control" name="oldfull_image" value="@if(isset($view->full_image)){{$view->full_image}}@endif" type="hidden">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12  col s12" style="padding-top: 20px; padding-bottom: 20px;">
                                    <button id="myBtn" class="btn btn-primary icon-btn marginT38 submit_button" type="button">
                                        <i class="fa fa-fw fa-lg fa-check-circle"></i>Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- End form -->
                </div>
            </div>
        </section>
    </section>
</div>



<script>
    CKEDITOR.replace('textArea');
</script>

@endsection