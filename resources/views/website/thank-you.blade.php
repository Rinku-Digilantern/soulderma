@include('website.include.header')

<link rel="stylesheet" href="{{ url('website') }}/css/thankyou.css">

<section id="topcommonbanner" class="thank_you_page">
  <div class="commonbannerclinic">
    <div class="ban"></div>
  </div>
</section>



<section id="thank_you_design" class="thank_you_page">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-12">
        <div class="headingandbredcrumb">
          <div class="commonheading notofamily commonheadingfontsize">Thank You!</div>
          <p class="lead">We have received your request. We will get in touch with you shortly.</p>
          <p class="viewalltestimonial">
            <a class="commonanchor" href="{{ url('/') }}" role="button"> Home</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>


@include('website.include.footer')