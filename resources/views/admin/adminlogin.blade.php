@include('admin.include.header')

<div class="New_login">
  <div class=" main_form_div">
    <div class="hospital_img">
      <img src="https://i.pinimg.com/originals/57/1f/d2/571fd28ce80b7902bfc61ca7e6119fe9.jpg" alt="Hospital Image" class="img-fluid ">
    </div>
    <div class="Form_container">
      <div class="left_form">
        <div class="login_logo">
          <img class="f-logo" src="{{ url('/admin/images/logo.png') }}" alt="logo">
        </div>
        <form class="form_fields">
          <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" id="uniq" name="uncode">
          <input type="email" class="ggg" autocomplete="off" id="email" onkeyup="validateEmail()" name="email" placeholder="Email">
          <div id="showemailmsg" class="error"></div>
          <input type="password" class="ggg" autocomplete="off" id="password" onkeyup="validatePassword()" name="password" placeholder="Password">
          <span id="togglePassword" onclick="togglePasswordVisibility()">
            <i id="toggleIcon" class="fa fa-eye"></i>
          </span>
          <div id="password_valid" class="error"></div>
          <div class="clearfix"></div>
          <div class="form-input">
            <div class="captcha-box d-flex align-items-center">
              <div class="w-50 d-flex">
                <div class="req-captcha-box">
                  <div class="captchacodeleftpic" id="capt"></div>
                </div>
                <div class="button-box">
                  <button type="button" class="btnBtnSubmits btn btn-lg" onClick="captchareload()"><i class="fa fa-refresh"></i></button>
                </div>
              </div>
              <div class="w-50">
                <input type="text" class="" formcontrolname="captcha" id="captchacode" autocomplete="off" placeholder="Captcha*" maxlength="6">
                <div id="validcaptcha" class="error"></div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="submit_btn">
            <button id="submit" type="button" class="btn btn-primary" onclick="checkvalidation()">Submit</button>
          </div>
        </form>
        <div id="validuser"></div>
        <div><a href="{{ url('admin/forgot-password') }}" class="frgt_pswd">Forgot Password</a></div>
      </div>
    </div>
  </div>
</div>

<!-- <div class="log-w3">
  <div class="w3layouts-main">
    <div class="logo-block login_logo" style="text-align: center;">
      <img class="f-logo" src="{{ url('/admin/images/Arete_logo.png') }}" alt="logo">
    </div>

    <form>
      <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" id="uniq" name="uncode">
      <input type="email" class="ggg" autocomplete="off" id="email" onkeyup="validateEmail()" name="email" placeholder="Email">
      <div id="showemailmsg" class="error"></div>
      <input type="password" class="ggg" autocomplete="off" id="password" onkeyup="validatePassword()" name="password" placeholder="Password">
      <span id="togglePassword" onclick="togglePasswordVisibility()">
        <i id="toggleIcon" class="fa fa-eye"></i>
      </span>
      <div id="password_valid" class="error"></div>
      <div class="clearfix"></div>
      <div class="form-input">
        <div class="captcha-box d-flex align-items-center">
          <div class="w-50 d-flex">
            <div class="req-captcha-box">
              <div class="captchacodeleftpic" id="capt"></div>
            </div>
            <div class="button-box">
              <button type="button" class="btnBtnSubmits btn btn-lg" onClick="captchareload()"><i class="fa fa-refresh"></i></button>
            </div>
          </div>
          <div class="w-50">
            <input type="text" class="" formcontrolname="captcha" id="captchacode" autocomplete="off" placeholder="Captcha*" maxlength="6">
            <div id="validcaptcha" class="error"></div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>

      <div class="submit_btn">
        <button id="submit" type="button" class="btn btn-primary" onclick="checkvalidation()">Submit</button>
      </div>
    </form>
    <div id="validuser"></div>
    <div><a href="{{ url('admin/forgot-password') }}" class="frgt_pswd">Forgot Password</a></div>
  </div>

</div> -->

<style>
  #togglePassword {
    cursor: pointer;
    color: #000;
  }
</style>



<script>
  function togglePasswordVisibility() {
    var passwordInput = document.getElementById("password");
    var toggleIcon = document.getElementById("toggleIcon");
    if (passwordInput.type === "password") {
      passwordInput.type = "text";
      toggleIcon.classList.remove("fa-eye");
      toggleIcon.classList.add("fa-eye-slash");
    } else {
      passwordInput.type = "password";
      toggleIcon.classList.remove("fa-eye-slash");
      toggleIcon.classList.add("fa-eye");
    }
  }

  captchareload();

  function captchareload() {
    fetch("{{url('/googlecaptcha')}}", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          post: 'ok'
        })
      })
      .then(response => response.json()) // Parse the JSON response
      .then(result => {
        // Update the HTML content of the 'capt' and 'uniq' elements
        document.getElementById('capt').innerHTML = result.captchashows;
        document.getElementById('uniq').value = result.uniqid;
      })
      .catch(error => console.error(error)); // Handle any errors that occur
  }
  // Login Validation
  function checkvalidation(getid) {
    var code = document.getElementById("uniq").value
    var codecaptcha = document.getElementById("captchacode").value
    var email = document.getElementById("email").value
    var password = document.getElementById("password").value
    var _token = document.getElementById("token").value
    var capminlenth = 6;
    var checkcode = false;
    var checkemail = false;
    var checkpassword = false;
    if (codecaptcha == '') {
      document.getElementById("validcaptcha").innerHTML = 'This field is required.';
      document.getElementById("captchacode").classList.add("errorsection");
      document.getElementById("captchacode").classList.remove("validsection");
      // e.preventDefault();
    } else if (codecaptcha.length != capminlenth) {
      document.getElementById("validcaptcha").innerHTML = 'Please Enter Valid Captcha.';
      document.getElementById("captchacode").classList.add("errorsection");
      document.getElementById("captchacode").classList.remove("validsection");
      // e.preventDefault();
    } else {
      var checkcode = true;
      document.getElementById("validcaptcha").innerHTML = ' ';
      document.getElementById("captchacode").classList.add("validsection");
      document.getElementById("captchacode").classList.remove("errorsection");
    }
    var emailExp = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    emailExp.test(email);
    if (email == '') {
      document.getElementById("showemailmsg").innerHTML = 'This field is required.';
      document.getElementById("email").classList.add("errorsection");
      document.getElementById("email").classList.remove("validsection");
      // e.preventDefault();
    } else if (!emailExp.test(email)) {
      document.getElementById("showemailmsg").innerHTML = 'Please Enter Valid Email ID.';
      document.getElementById("email").classList.add("errorsection");
      document.getElementById("email").classList.remove("validsection");
      // e.preventDefault();
    } else {
      var checkemail = true;
      document.getElementById("showemailmsg").innerHTML = ' ';
      document.getElementById("email").classList.add("validsection");
      document.getElementById("email").classList.remove("errorsection");
    }
    var lowerCaseLetters = /[a-z]/;
    var numbers = /[0-9]/;
    var upperCaseLetters = /[A-Z]/;
    var specialchar = /[!@#$%^&*()\-=_+[\]{}|;:<>/?]/;
    if (password == '') {
      document.getElementById("password_valid").innerHTML = 'This field is required.';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
      // e.preventDefault();
    } else if (!lowerCaseLetters.test(password)) {
      document.getElementById("password_valid").innerHTML =
        'The password must contain at least one lowercase letter.';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
    }
    // Validate capital letters
    else if (!upperCaseLetters.test(password)) {
      document.getElementById("password_valid").innerHTML =
        'The password must contain at least one uppercase letter.';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
    }
    // Validate numbers
    else if (!numbers.test(password)) {
      document.getElementById("password_valid").innerHTML = 'The password must contain at least one digit.';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
    } else if (!specialchar.test(password)) {
      document.getElementById("password_valid").innerHTML =
        'The password must contain at least one special character';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
    }
    // Validate length
    else if (!password.length >= 8) {
      document.getElementById("password_valid").innerHTML = 'The password must be at least 8 characters long.';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
    } else {
      var checkpassword = true;
      document.getElementById("password_valid").innerHTML = ' ';
      document.getElementById("password").classList.add("validsection");
      document.getElementById("password").classList.remove("errorsection");
    }
    if (checkcode == true && checkemail == true && checkpassword == true) {
      var xhr = new XMLHttpRequest();
      // set up the request
      xhr.open("POST", "{{ url('admin/user-login') }}");
      xhr.setRequestHeader("Content-Type", "application/json");
      // handle the response
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            window.location.href = "{{url('/admin/dashboard')}}"; //redirection
          } else {
            if (xhr.status == 400) {
              document.getElementById("submit").disabled = false;
              document.getElementById("submit").innerText = "Submit";
              document.getElementById("validcaptcha").innerHTML = 'Please Enter Valid Captcha.';
            }

            if (xhr.status == 401) {
              document.getElementById("submit").disabled = false;
              document.getElementById("submit").innerText = "Submit";
              document.getElementById("validcaptcha").innerHTML = 'Please Enter Valid Captcha.';
            }

            if (xhr.status == 404) {
              document.getElementById("submit").disabled = false;
              document.getElementById("submit").innerText = "Submit";
              document.getElementById("validuser").innerHTML = 'User Id is not valid';
            }
            document.getElementById("submit").disabled = false;
          }
        }
      };

      var data = {
        'email': email,
        'password': password,
        'uncode': code,
        'captcha': codecaptcha,
        '_token': _token,
      };

      // send the request
      document.getElementById("submit").disabled = true;
      document.getElementById("submit").innerText = "Submiting..";
      xhr.send(JSON.stringify(data));
      // }
    }
  }
  // for contact and appointment
  function validateEmail() {
    var emailInput = document.getElementById('email');
    var email = emailInput.value;
    var errorSpan = document.getElementById('showemailmsg');
    // Regular expression pattern for email validation
    var pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (pattern.test(email)) {
      errorSpan.textContent = ""; // Clear any existing error message
      // Do something with the valid email, such as submitting the form
    } else {
      errorSpan.textContent = "Please enter a valid email address";
    }
  }
  // For Contact and appointment function validate

  function validatePassword() {
    var passInput = document.getElementById('password');
    var password = passInput.value;
    var errorSpan = document.getElementById('password_valid');
    // Check if the password is at least 8 characters long
    if (password.length < 8) {
      errorSpan.textContent = "The password must be at least 8 characters long";
    } else
      // Check if the password contains at least one lowercase letter
      if (!/[a-z]/.test(password)) {
        errorSpan.textContent = "The password must contain at least one lowercase letter";
      } else
        // Check if the password contains at least one uppercase letter
        if (!/[A-Z]/.test(password)) {
          errorSpan.textContent = "The password must contain at least one uppercase letter";
        } else
          // Check if the password contains at least one special character
          if (!/[!@#$%^&*()\-=_+[\]{}|;':"<>/?]/.test(password)) {
            errorSpan.textContent = "The password must contain at least one special character";
          } else {
            errorSpan.textContent = "";
          }
  }
</script>



@include('admin.include.footer')
