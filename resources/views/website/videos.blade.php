@include('website.include.header')


    <link rel="stylesheet" href="{{ url('website') }}/css/commoncategory.css">


    <section id="topcommonbanner">
        <div class="commonbannerclinic">
            <img src="{{ url('website') }}/img/about-clinic/about-clinic.jpg" class="w-100">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="headingandbredcrumb">
                        <div class="commonheading notofamily commonheadingfontsize">videos Category</div>
                        <div class="breadcrumb commonbredcurmb">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a>videos Category</a></li>
                            </ul>
                        </div>
                        <div class="commonrequestform">
                            @include('website.include.requestcallback')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="realresultswrapper">
        <div class="container">
            <div class="row">
                @foreach($video as $vid)
                <div class="col-12 col-md-4">
                    <div class="realcatwrap">
                        <img src="{{ url('/backend/service_video/image') }}/{{ $vid->image }}" class="w-100" alt="{{ $vid->alt_tag }}">
                        <a href="{{ url('video') }}/{{ $vid->url }}" class="realcatheadingwrap">
                            <div class="realheading">{{ $vid->name }}</div>
                            <button class="commongradientbutton">View All</button>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    @include('website.include.requestanappointment')
    @include('website.include.footer')
