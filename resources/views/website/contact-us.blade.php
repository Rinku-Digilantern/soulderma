@include('website.include.header')

<section id="topcommonbanner" class="contact_page_banner">
    <div class="commonbannerclinic">
        <img src="{{ url('website') }}/img/about-clinic/about-clinic.jpg" class="w-100">
    </div>
    <div class="container page_heading">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="headingandbredcrumb">
                    <div class="commonheading notofamily commonheadingfontsize">Contact us</div>
                    <div class="breadcrumb commonbredcurmb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a>Contact us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="appointmentwrapper " class="for-desktop contact_form_address">
    <div class="container">
        <!-- <div class="homeappointmentheading notofamily commonheadingfontsize"></div> -->
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="appointaddresswrap">
                    <div class="apadressicon">
                        <img src="{{ url('website') }}/img/location-icon.png">
                    </div>
                    <div class="aplocationcontentwrap">
                        <div class="aplocationheading">ADDRESS:</div>
                        <div class="detailaddress">
                            <p>Ground Floor W-58, Greater Kailash I, New Delhi, Delhi 110048 India</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="appointaddresswrap">
                    <div class="apadressicon">
                        <img src="{{ url('website') }}/img/content-icon.png">
                    </div>
                    <div class="aplocationcontentwrap">
                        <div class="aplocationheading">MOBILE NO.:</div>
                        <div class="apmobilenumber">
                            <a href="tel:+91-8595941529">+91-8595941529</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="appointaddresswrap">
                    <div class="apadressicon">
                        <img src="{{ url('website') }}/img/email-icon.png">
                    </div>
                    <div class="aplocationcontentwrap">
                        <div class="aplocationheading">EMAIL ID:</div>
                        <div class="detailaddress">
                            <p><a href="mailto:info@souldermaclinic.com">info@souldermaclinic.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="bigappointwrap" class="Conttact_form_main">
    <div class="container">
        <!-- <div class="homeappointmentheading notofamily commonheadingfontsize for-mobile"></div> -->
        <form method="post" class="bottom_form">
            <input type="hidden" id="uniq_app" name="uncode">
            <input type="hidden" name="_token" id="token_app" value="{{ csrf_token() }}">
            <div class="row appointbg">
                <div class="col-12 col-md-6">
                    <div class="bigapform">
                        <input type="text" name="name" class="form-control" autocomplete="off" id="name_app" onkeyup="validateUsernameapp()" placeholder="Name*" required="">
                        <div id="validname_app" class="error"></div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="bigapform">
                        <input type="text" class="form-control" name="phone" autocomplete="off" minlength="10" onkeyup="validatePhoneNumberapp()" maxlength="10" id="phone_app" placeholder="Phone*" required="">
                        <div id="validphone_app" class="error"></div>
                        <input type="hidden" name="request_url" id="request" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
                        <input type="hidden" id="referrer" name="referrer">
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="bigapform">
                        <input type="email" class="form-control" name="email" autocomplete="off" id="email_app" onkeyup="validateEmail()" placeholder="Email*" required="">
                        <div id="showemailmsg_app" class="error"></div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="bigapform">
                        <textarea class="form-control" name="message" autocomplete="off" rows="1" id="message" placeholder="Message" required=""></textarea>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="big-apsubmitwrapper">
                        <div class="bigcaptchawrap">
                            <div class="captcgawrap">
                                <div class="req-captcha-box">
                                    <div class="captchacodeleftpic" id="capt_app"></div>
                                </div>
                                <div class="button-box apcaptchabutton">
                                    <button type="button" class="btnBtnSubmits" onClick="captchareload_app()"><i class="fa fa-refresh"></i></i></button>
                                </div>

                            </div>
                            <div class="inputfillcaptcha">
                                <input type="text" class="w-100 form-control" formcontrolname="captcha" id="captchacode_app" autocomplete="off" placeholder="Captcha*" maxlength="6">
                                <div id="validcaptcha_app" class="error"></div>
                            </div>
                        </div>
                        <div class="bgcaptchasubmit">
                            <button id="submitapp" type="button" onclick="checkvalidation_contact()">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>

<div class="contact_map">
    <div class="contact_form_map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.634595750514!2d77.22862708172144!3d28.550700913078252!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce22cb7cb7bf7%3A0x6feea16d7281ffbc!2sSoul%20Derma%20Clinic%20by%20Dr.Anika%20Goel!5e0!3m2!1sen!2sin!4v1689163198182!5m2!1sen!2sin" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
</div>



<div class="contact_page_footer">
    @include('website.include.footer')
</div>