<!DOCTYPE html>
<html>
<head>
@include('admin.include.header')
</head>
<body>
@include('admin.include.side_bar')
@yield('content')
@include('admin.include.footer')
</body>
</html>
