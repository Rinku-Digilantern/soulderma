@include('website.include.header')

    <link rel="stylesheet" href="{{ url('website') }}/css/aboutclinic.css">


    <section id="topcommonbanner">
        <div class="commonbannerclinic">
            <img src="{{ url('website') }}/img/about-clinic/about-clinic.jpg" class="w-100">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="headingandbredcrumb">
                        <div class="commonheading notofamily commonheadingfontsize">{{ $cat->name }}</div>
                        <div class="breadcrumb commonbredcurmb">
                            <ul>
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="{{ url('') }}/results">Results</a></li>
                            <li>{{ $cat->name }}</li>
                            </ul>
                        </div>
                        <div class="commonrequestform">
                            @include('website.include.requestcallback')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section id="clinicgallerypage" class="mt-5">
<div class="container">
    <div class="row">
        @foreach($result as $res)
            <a href="{{ url('/backend/service_result/inner')}}/{{ $res->beforeimg }}" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-4">
                <img src="{{ url('/backend/service_result/inner')}}/{{ $res->beforeimg }}" class="img-fluid" alt="{{ $res->alt_img }}">
            </a>
        @endforeach
    </div>
</div>
</section>
@include('website.include.requestanappointment')

@include('website.include.footer')
