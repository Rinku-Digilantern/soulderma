@include('website.include.header')

<link rel="stylesheet" href="{{ url('website') }}/css/aboutdr.css">
<link rel="stylesheet" href="{{ url('website') }}/css/sliderfive.css">


<section id="topcommonbanner" class="about_doctor_page">
    <div class="commonbannerclinic">
        <img src="{{ url('website') }}/img/aboutdr/abouttopbanner.jpg" class="w-100" alt="Best Skin Doctor in Greater kailash - Dr. Anika Goel">
    </div>
    <div class="container managemargin">
        <div class="breadcrumb commonbredcurmb for-mobile">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a>About Doctor</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6 col-md-7 bottom_mrg">
                <div class="headingandbredcrumb">
                    <div class="commonheading notofamily commonheadingfontsize">DR. ANIKA GOEL</div>
                    <div class="bannersubheading">The Chief Consultant Dermatologist of <span>Soul Derma Clinic</span> in Greater Kailash, South Delhi</div>
                    <div class="bannertext">
                        <!-- <p>Dr. Anika Goel, the Chief Consultant Dermatologist of Soul Derma Clinic, is a qualified dermatologist, venereologist, and leprologist (M.D.) from Sri Guru Ram Das Institute of Medical Science and Research, Amritsar. She has received continual training, whenever she got the opportunity, to keep herself abreast of the advancements and trends in dermatology. She holds expertise in treating patients with all kinds of skin, nail, and hair disorders and is a master of cosmetic dermatology services including hair transplant surgery.</p> -->
                    </div>
                    <!-- <div class="breadcrumb commonbredcurmb">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a>About clinic</a></li>
                            </ul>
                        </div> -->
                    <div class="commonrequestform doctor_page_form">
                        @include('website.include.requestcallback')
                    </div>
                    <div class="breadcrumb commonbredcurmb for-desktop">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li><a>About Doctor</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-md-5 dr_ban_img">
                <div class="topbannersubbanner">
                    <img src="{{ url('website') }}/img/aboutdr/drimage.png" class="w-100" alt="Best Skin Specialist in Greater Kailash, South Delhi: Dr. Anika Goel is the Best Skin Doctor in Greater Kailash and has expertise in treating of skin, nail, and hair disorders.
">
                    <div class="playbuttondfv">
                        <div class="playbutton vid">
                            <!-- Button trigger modal -->
                            <!-- <button type="button" class="btn btn-primary video-btn" data-bs-toggle="modal"
                                    data-src="https://www.youtube.com/embed/Qxd8hiwbrFo" data-bs-target="#myModal">
                                    <span class="fa fa-play"></span>
                                </button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="dermawrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="dermacontentwrap">
                    <h1 class="dermacontentheading notofamily commonheadingfontsize mb-2">Best Skin Specialist in South Delhi</h1>
                    <div class="dermacontenttext">
                        <p>Dr. Anika Goel, the Chief Consultant Dermatologist of Soul Derma Clinic, is a qualified dermatologist, venereologist, and leprologist (M.D.) from Sri Guru Ram Das Institute of Medical Science and Research, Amritsar. She is also regarded as the <b>best skin specialist in South Delhi</b>. She has received continual training, whenever she got the opportunity, to keep herself abreast of the advancements and trends in dermatology. She holds expertise in treating patients with all kinds of skin, nail, and hair disorders and is a master of cosmetic dermatology services including hair transplant surgery.</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-5">
                <div class="dreducationwrap">
                    <div class="aducationheading notofamily">Education</div>
                    <div class="eduspcailtion">
                        <div class="firsteduwrap commoneduerap">
                            <div class="imgeduwrap">
                                <img src="{{ url('website') }}/img/aboutdr/ramdas.png" alt="Sri Guru Ram Das Institute of Medical Science and Research, Amritsar">
                            </div>
                            <div class="educontentdetail">
                                <p><span>M.D. in Dermatology, Venereology, and Leprosy </span></p>
                                <li>Sri Guru Ram Das Institute of Medical Science and Research, Amritsar (Baba Farid University of Health Sciences) - 2019</li>
                            </div>
                        </div>

                        <div class="secodeduwrap commoneduerap">
                            <div class="imgeduwrap">
                                <img src="{{ url('website') }}/img/aboutdr/patil.png" alt="D.Y. Patil Medical College, Navi Mumbai, India">
                            </div>
                            <div class="educontentdetail">
                                <p><span>MBBS</span></p>
                                <li>D.Y. Patil Medical College, Navi Mumbai, India. (D Y Patil University) – 2015</li>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="dermacontenttext">
                    <p>Dr. Anika Goel is one of the finest, certified dermatologists in Greater Kailash, South Delhi. She is considered as the most premium Aesthetic Physician in Delhi. She has a specialization in cosmetic procedures and clinical treatments of skin and hair concerns, as well as genital rejuvenation procedures. She is also an expert in performing hair transplant surgery and laser hair removal treatment. With extensive experience in general, cosmetic, and surgical dermatology for about a decade, she has successfully treated all kinds of patients by tailoring her services to the needs and goals of her patients.</p>
                    <p>So far, more than 5,000 patients have undergone diagnosis and treatment of their concerns by her and have reported a boost in their self-confidence. Thanks to her immense, comprehensive knowledge of the complexities of her field and her patient-centric approach, Dr. Anika Goel, the <b> best skin specialist in Greater Kailash</b>, continues to transform the lives of her patients.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="clnicalexpertieawrapper">
    <div class="container">
        <div class="commonmainheadingwrap clnicalexpertieaborder">
            <div class="clinicalheading notofamily commonheadingfontsize">Clinical Interests And Expertise</div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="clinicalcontentwrap commonclinnicalcontentwrap">
                    <p>Dr. Anika Goel is a renowned dermatologist and aesthetician with a special interest in injectables, non-surgical anti-ageing treatments, and laser hair removal. She is the best dermatologist consultant who ensures her patients are well-informed about their concerns and treatment plan. She has answers to all your queries related to her field.</p>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="clinicalcontentwrap commonclinnicalcontentwrap">
                    <p>What makes her the <b>best skin specialist in Greater Kailash</b>, or what sets her apart from other skin specialists, is her flexibility in handling all kinds of cases and the freedom she offers to her clients. She is persistent in advocating for her patients to be open-minded.</p>
                </div>
            </div>

            <!-- <div class="col-12 col-md-4">
                    <div class="clinicalcontentwrap commonclinnicalcontentwrap">
                        <p>Undergoing skincare treatments at Soul Derma is highly rewarding. With considerable years of
                            experience in dermatology and hair transplant, Dr Anika Goel has established herself as one
                            of the <span> best dermatologist in South Delhi</span>. Here are some top things to know
                            about the
                            clinical expertise and interests of Dr Anika Goel.</p>
                    </div>
                </div> -->
        </div>
    </div>
</section>

<section id="populartreatment" class="popular_treatment">
    <div class="container">
        <div class="commonmainheadingwrap clnicalexpertieaborder">
            <div class="clinicalheading notofamily commonheadingfontsize">Popular Treatments</div>
            <p>The skincare experience at Soul Derma by Dr. Anika Goel is highly rewarding. With significant experience in performing all kinds of dermatology services. Dr. Anika Goel has established herself as the best dermatologist in South Delhi. Her clinical interests and expertise are in:</p>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="d-flex align-items-start mobilechangeview">

                    <div class="tab-content aboutdrtab" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                            <div class="abdractivewrap abdrcommonactive">
                                <div class="imagetab">
                                    <img src="{{ url('website') }}/img/aboutdr/Clinical-dermatology.jpg" class="w-100" alt="Clinical Dermatology">
                                </div>
                                <div class="playbuttoncontentwrap">
                                    <div class="playbutton vid">
                                        <!-- Button trigger modal -->
                                        <!-- <button type="button" class="btn btn-primary video-btn" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/Qxd8hiwbrFo" data-bs-target="#myModal"> <span class="fa fa-play"></span>
                                        </button> -->
                                    </div>
                                    <div class="imagedrheading">Clinical Dermatology</div>
                                    <p>Clinical dermatology is all about the identification, treatment, management, and prevention of an entire range of skin, hair, and nail conditions. Dr. Anika Goel is one of the premier clinical dermatologists in Greater Kailash, South Delhi. She offers highly accurate diagnosis, professional counselling, and top-quality treatments for various skin, hair, and nail conditions.</p>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                            <div class="abdractivewrap abdrcommonactive">
                                <div class="imagetab">
                                    <img src="{{ url('website') }}/img/aboutdr/cosmetic-dermatologist.jpg" class="w-100" alt="Cosmetic Dermatologist">
                                </div>
                                <div class="playbuttoncontentwrap">
                                    <div class="playbutton vid">
                                        <!-- Button trigger modal -->
                                        <!-- <button type="button" class="btn btn-primary video-btn" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/Qxd8hiwbrFo" data-bs-target="#myModal"> <span class="fa fa-play"></span>
                                        </button> -->
                                    </div>
                                    <div class="imagedrheading">Cosmetic Dermatology</div>
                                    <p>It is a subspecialty of dermatology that encompasses procedures to improve the appearance of the skin, particularly the face and neck. Dr. Anika Goel is a highly reputed cosmetic dermatologist in Greater Kailash, South Delhi. She offers advanced aesthetic skin treatments at reasonable prices such as dermal fillers, mesotherapy, chemical peels, and many more to help her clients achieve their desired looks.</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                            <div class="abdractivewrap abdrcommonactive">
                                <div class="imagetab">
                                    <img src="{{ url('website') }}/img/aboutdr/hair-transplant.jpg" class="w-100" alt="Hair Transplant">
                                </div>
                                <div class="playbuttoncontentwrap">
                                    <div class="playbutton vid">
                                        <!-- Button trigger modal -->
                                        <!-- <button type="button" class="btn btn-primary video-btn" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/Qxd8hiwbrFo" data-bs-target="#myModal"> <span class="fa fa-play"></span>
                                    </button> -->
                                    </div>
                                    <div class="imagedrheading">Hair Transplant</div>
                                    <p>It is a surgical hair restoration solution wherein healthy hair follicles are transferred from hair-dense areas to balding/thinning regions of the scalp. Dr. Anika Goel is the best hair transplant surgeon in Greater Kailash, South Delhi. She relies on advanced hair transplant techniques: Follicular Unit Extraction to help her patients achieve a head full of healthy, thick hair.</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                            <div class="abdractivewrap abdrcommonactive">
                                <div class="imagetab">
                                    <img src="{{ url('website') }}/img/aboutdr/laser--treatments.jpg" class="w-100" alt="Laser Treatments">
                                </div>
                                <div class="playbuttoncontentwrap">
                                    <div class="playbutton vid">
                                        <!-- Button trigger modal -->
                                        <!-- <button type="button" class="btn btn-primary video-btn" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/Qxd8hiwbrFo" data-bs-target="#myModal"> <span class="fa fa-play"></span>
                                        </button> -->
                                    </div>
                                    <div class="imagedrheading">Laser Treatments</div>
                                    <p>Lasers have long been in use in the aesthetic industry to address various skin concerns like scars, pigmentation, hair removal, and others. If searching for the best laser treatment centre in Greater Kailash, Delhi, end your search at Soul Derma Clinic and have a consultation with Dr. Anika Goel. She is the best laser expert who has considerable years of experience in treating various skin concerns with ablative and non-ablative lasers such as CO2 lasers and Q-switched laser devices.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav nav-pills aboutdrtabbutton" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <button class="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">
                            <div class="buttonimagetext">
                                <img src="{{ url('website') }}/img/clinicalderma.png" alt="Clinical Dermatology">
                                <p>Clinical <span> Dermatology</span></p>
                            </div>
                        </button>
                        <button class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                            <div class="buttonimagetext">
                                <img src="{{ url('website') }}/img/cosmeticderma.png" alt="cosmeticderma">
                                <p>Cosmetic <span> Dermatology</span></p>
                            </div>
                        </button>
                        <button class="nav-link" id="v-pills-messages-tab" data-bs-toggle="pill" data-bs-target="#v-pills-messages" type="button" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                            <div class="buttonimagetext">
                                <img src="{{ url('website') }}/img/hairtransp.png" alt="Hair Transplant">
                                <p>Hair <span> Transplant</span></p>
                            </div>
                        </button>
                        <button class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill" data-bs-target="#v-pills-settings" type="button" role="tab" aria-controls="v-pills-settings" aria-selected="false">
                            <div class="buttonimagetext">
                                <img src="{{ url('website') }}/img/lasertreat.png" alt="Laser Treatments">
                                <p>Laser <span> Treatments</span></p>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="commonmainheadingwrap mt-2">
            <p>She is the epitome of a medical professional who goes above and beyond to achieve exceptional outcomes due to her unmatched abilities, commitment to continuing education, and genuine concern for her patients. Experience the transformative power of her expertise and embark on a journey towards healthy, radiant skin under the care of Dr. Anika Goel, the <b>best skin doctor in Greater Kailash</b>.</p>
        </div>
    </div>
</section>
<section id="aboutclinicschoosewrap" class="Doc_contents">
    <div class="container">
        <div class="row">
            <div class="whychoosaboutwrap">
                <div class="commonmainheadingwrap">
                    <div class="meshotheropyheading notofamily commonheadingfontsize">Dr. Anika Goel</div>
                    <h2 class="meshotheropyheading notofamily commonheadingfontsize">Best Dermatologist In South Delhi</h2>
                    <div class="meshotheropyheading notofamily commonheadingfontsize">The Epitome Of Excellence In Dermatology</div>
                </div>
                <div class="whychoosimagewrap img_centre">
                    <img src="{{ url('website') }}/img/about-clinic/aboutclinicimage.jpg" class="w-100" alt="At Soul Derma Clinic, Dr. Anika Goel is the Best Dermatologist in South Delhi and offers her patients the most cutting-edge treatments. Consult now.">
                </div>
                <div class="mesotherpycontentwrap">
                    <ul>
                        <li><i class="fa fa-check-circle"></i> When it comes to choosing a dermatologist who can truly transform your skin and restore your confidence, Dr. Anika Goel stands head and shoulders above the rest. Renowned for her exceptional skills and unwavering dedication to her patients, she has rightfully earned her place as the <b style="display: contents;"> Best Dermatologist In South Delhi</b> and an expert aesthetic doctor. </li>
                        <li> <i class="fa fa-check-circle"></i> With a specialisation in injectables and fillers, Dr. Anika Goel possesses a unique talent for artfully enhancing natural beauty. Her keen eye for detail, coupled with her precise techniques, allows her to create stunning results that leave her patients feeling rejuvenated and self-assured. Whether it's smoothing out wrinkles, restoring volume, or contouring facial features, Dr. Anika Goel's expertise is second to none. </li>
                        <li> <i class="fa fa-check-circle"></i> She is also recognized as an expert in hair transplantation, providing innovative solutions for individuals experiencing hair loss. Additionally, Dr. Anika Goel is highly skilled in non-surgical genital rejuvenation, offering a wide range of innovative procedures to enhance the genital area. </li>
                        <li class="display-in"> <i class="fa fa-check-circle"></i> What truly sets Dr. Anika Goel, <strong>best skin doctor in Greater Kailash</strong>, apart is her unwavering commitment to staying at the forefront of her field. To ensure that she offers her patients the most cutting-edge treatments, she regularly attends prestigious conferences and embarks on global tours, seeking out the latest advancements and techniques. Notably, her participation in conferences worldwide has enriched her expertise, allowing her to provide unparalleled care to her patients.</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>


<!-- <section id="meshotheropywrapper">
    <div class="container meshotheropywrapperborder">
        <div class="commonmainheadingwrap">
            <div class="meshotheropyheading notofamily commonheadingfontsize">BEST SKIN DOCTOR IN <br> GREATER KAILASH FOR MESOTHERAPY</div>
            <div class="meshotheropyheading notofamily commonheadingfontsize">DR. ANIKA GOEL <br> THE EPITOME OF EXCELLENCE IN DERMATOLOGY</div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="mesotherpycontentwrap">
                    <ul>
                        <li><i class="fa fa-check-circle"></i> When it comes to choosing a dermatologist who can truly transform your skin and restore your confidence, Dr. Anika Goel stands head and shoulders above the rest. Renowned for her exceptional skills and unwavering dedication to her patients, she has rightfully earned her place as a top dermatologist and an expert aesthetic doctor. </li>
                        <li> <i class="fa fa-check-circle"></i> With a specialisation in injectables and fillers, Dr. Anika Goel possesses a unique talent for artfully enhancing natural beauty. Her keen eye for detail, coupled with her precise techniques, allows her to create stunning results that leave her patients feeling rejuvenated and self-assured. Whether it's smoothing out wrinkles, restoring volume, or contouring facial features, Dr. Anika Goel's expertise is second to none. </li>
                        <li> <i class="fa fa-check-circle"></i> She is also recognized as an expert in hair transplantation, providing innovative solutions for individuals experiencing hair loss. Additionally, Dr. Anika Goel is highly skilled in non-surgical genital rejuvenation, offering a wide range of innovative procedures to enhance the genital area. </li>
                   
                    </ul>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="mesoimage">
                    <img src="{{ url('website') }}/img/aboutdr/aboutdrbanner.jpg" class="w-100">
                </div>
            </div>
            <div class="col-12">
                <div class="mesotherpycontentwrap">
                    <ul>
                        <li> <i class="fa fa-check-circle"></i> What truly sets Dr. Anika Goel apart is her unwavering commitment to staying at the forefront of her field. To ensure that she offers her patients the most cutting-edge treatments, she regularly attends prestigious conferences and embarks on global tours, seeking out the latest advancements and techniques. Notably, her participation in conferences worldwide has enriched her expertise, allowing her to provide unparalleled care to her patients.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section> -->

<section id="clnicalexpertieawrapper">
    <div class="container">
        <div class="commonmainheadingwrap clnicalexpertieaborder">
            <div class="clinicalheading notofamily commonheadingfontsize">Compassion & Care</div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="clinicalcontentwrap">
                    <p>In addition to Dr. Anika’s clinical practice, she actively contributes to charitable endeavours in the realm of dermatology. Driven by her compassionate nature, she provides free medicine and dermatological treatments to those in need. Her selflessness and commitment to making a positive impact on society have earned her immense respect and admiration.</p>
                    <p class="mt-1">Choosing Dr. Anika Goel as your dermatologist means choosing excellence, compassion, and innovation.</p>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- <section id="homeblogwrapper">
        <div class="container">
            <div class="homeblogheading notofamily commonheadingfontsize">LATEST BLOGS</div>
            <div class="row manage-mbwidthparent">
                <div class="col-12 col-md-4 manage-mbwidth">
                    <div class="homeblogcontentwrapper">
                        <div class="homeblogimagewrap">
                            <div class="homeblogimage">
                                <img src="{{ url('website') }}/img/homeblog-1.jpg" class="w-100">
                            </div>
                            <div class="blogcalender">
                                <p>05 <span>JAN</span></p>
                            </div>
                        </div>
                        <div class="homeblogcontenttext">
                            <div class="homeblogcontentheading">LOREM IPSUM DOLOR SIT</div>
                            <div class="homeblogtextp">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras iaculis enim eget urna
                                    lobortis laoreet.</p>
                            </div>
                            <div class="homblogview">
                                <a href="#">read more</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-4 manage-mbwidth">
                    <div class="homeblogcontentwrapper">
                        <div class="homeblogimagewrap">
                            <div class="homeblogimage">
                                <img src="{{ url('website') }}/img/homeblog-2.jpg" class="w-100">
                            </div>
                            <div class="blogcalender">
                                <p>05 <span>JAN</span></p>
                            </div>
                        </div>
                        <div class="homeblogcontenttext">
                            <div class="homeblogcontentheading">LOREM IPSUM DOLOR SIT</div>
                            <div class="homeblogtextp">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras iaculis enim eget urna
                                    lobortis laoreet.</p>
                            </div>
                            <div class="homblogview">
                                <a href="#">read more</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-4 manage-mbwidth">
                    <div class="homeblogcontentwrapper">
                        <div class="homeblogimagewrap">
                            <div class="homeblogimage">
                                <img src="{{ url('website') }}/img/homeblog-3.jpg" class="w-100">
                            </div>
                            <div class="blogcalender">
                                <p>05 <span>JAN</span></p>
                            </div>
                        </div>
                        <div class="homeblogcontenttext">
                            <div class="homeblogcontentheading">LOREM IPSUM DOLOR SIT</div>
                            <div class="homeblogtextp">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras iaculis enim eget urna
                                    lobortis laoreet.</p>
                            </div>
                            <div class="homblogview">
                                <a href="#">read more</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="homeblogviewmore for-desktop">
                <a href="#">view more</a>
            </div>
        </div>
        </div>
    </section> -->

@include('website.include.requestanappointment')

@include('website.include.footer')