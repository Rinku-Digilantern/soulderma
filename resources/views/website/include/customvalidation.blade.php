<script >
captchareload();
captchareload_app();
function captchareload() {
     // Send a POST request to the server endpoint
    fetch("{{url('/googlecaptcha')}}", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ post: 'ok' })
    })
    .then(response => response.json()) // Parse the JSON response
    .then(result => {
      // Update the HTML content of the 'capt' and 'uniq' elements
      document.getElementById('capt').innerHTML = result.captchashows;
      document.getElementById('uniq').value = result.uniqid;
    })
    .catch(error => console.error(error)); // Handle any errors that occur
  }

function captchareload_app() {
    // alert('hi');
    fetch("{{url('/googlecaptcha_app')}}", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ post: 'ok' })
    })
    .then(response => response.json()) // Parse the JSON response
    .then(result => {
      // Update the HTML content of the 'capt' and 'uniq' elements
      document.getElementById('capt_app').innerHTML = result.captchashows;
      document.getElementById('uniq_app').value = result.uniqid;
    })
    .catch(error => console.error(error)); // Handle any errors that occur
}

// Form Appointment and Contact Form
function checkvalidation(getid) {

    var code = document.getElementById("uniq_app").value
    var codecaptcha = document.getElementById("captchacode_app").value
    var name = document.getElementById("name_app").value
    var email = document.getElementById("email_app").value
    var phone = document.getElementById("phone_app").value
    var date = document.getElementById("date_app").value
    var request_url = document.getElementById("request").value
    var referrer_url = document.getElementById("referrer").value
    var _token = document.getElementById("token_app").value
    var message = document.getElementById("message").value

    var capminlenth = 6;
    var phoneminLength = 10;
    var phonemaxLength = 10;
    var checkcode = false;
    var checkname = false;
    var checkemail = false;
    var checkphone = false;
    var checkdate = false;
    var checksername = false;

    if (codecaptcha == '') {
        document.getElementById("validcaptcha_app").innerHTML = 'This field is required.';
        document.getElementById("captchacode_app").classList.add("errorsection");
        document.getElementById("captchacode_app").classList.remove("validsection");
        // e.preventDefault();
    } else if (codecaptcha.length != capminlenth) {
        document.getElementById("validcaptcha_app").innerHTML = 'Please Enter Valid Captcha.';
        document.getElementById("captchacode_app").classList.add("errorsection");
        document.getElementById("captchacode_app").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkcode = true;
        document.getElementById("validcaptcha_app").innerHTML = ' ';
        document.getElementById("captchacode_app").classList.add("validsection");
        document.getElementById("captchacode_app").classList.remove("errorsection");
    }

    //var regName = /^[a-zA-Z]+/;
    var regName=/^[a-zA-Z ]*$/;
    if (name == '') {
        document.getElementById("validname_app").innerHTML = 'This field is required.';
        document.getElementById("name_app").classList.add("errorsection");
        document.getElementById("name_app").classList.remove("validsection");
        // e.preventDefault();
    } else if (!regName.test(name)) {
        document.getElementById("validname_app").innerHTML = 'Please Enter Valid Name.';
        document.getElementById("name_app").classList.add("errorsection");
        document.getElementById("name_app").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkname = true;
        document.getElementById("validname_app").innerHTML = ' ';
        document.getElementById("name_app").classList.add("validsection");
        document.getElementById("name_app").classList.remove("errorsection");
    }

    var emailExp = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    emailExp.test(email);
    if (email == '') {
        document.getElementById("showemailmsg_app").innerHTML = 'This field is required.';
        document.getElementById("email_app").classList.add("errorsection");
        document.getElementById("email_app").classList.remove("validsection");
        // e.preventDefault();
    } else if (!emailExp.test(email)) {
        document.getElementById("showemailmsg_app").innerHTML = 'Please Enter Valid Email ID.';
        document.getElementById("email_app").classList.add("errorsection");
        document.getElementById("email_app").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkemail = true;
        document.getElementById("showemailmsg_app").innerHTML = ' ';
        document.getElementById("email_app").classList.add("validsection");
        document.getElementById("email_app").classList.remove("errorsection");
    }

    var phoneno = /^[0-9]+$/;
    if (phone == '') {
        document.getElementById("validphone_app").innerHTML = 'This field is required.';
        document.getElementById("phone_app").classList.add("errorsection");
        document.getElementById("phone_app").classList.remove("validsection");
        // e.preventDefault();
    } else if (phone.length < phoneminLength) {
        document.getElementById("validphone_app").innerHTML = 'Please Enter Valid Phone Number.';
        document.getElementById("phone_app").classList.add("errorsection");
        document.getElementById("phone_app").classList.remove("validsection");
        // e.preventDefault();
    } else if (!(phoneno.test(phone))) {
        document.getElementById("validphone_app").innerHTML = 'Please Enter Valid Phone Number.';
        document.getElementById("phone_app").classList.add("errorsection");
        document.getElementById("phone_app").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkphone = true;
        document.getElementById("validphone_app").innerHTML = ' ';
        document.getElementById("phone_app").classList.add("validsection");
        document.getElementById("phone_app").classList.remove("errorsection");
    }


    // var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    if (date == '') {
        document.getElementById("validdate_app").innerHTML = 'This field is required.';
        document.getElementById("date_app").classList.add("errorsection");
        document.getElementById("date_app").classList.remove("validsection");
        // e.preventDefault();
    // } else if (!(date_regex.test(date))) {
    //     document.getElementById("validdate_app").innerHTML = 'Please Select Valid Date.';
    //     document.getElementById("date_app").classList.add("errorsection");
    //     document.getElementById("date_app").classList.remove("validsection");
    //     // e.preventDefault();
    } else {
        var checkdate = true;
        document.getElementById("validdate_app").innerHTML = '';
        document.getElementById("date_app").classList.add("validsection");
        document.getElementById("date_app").classList.remove("errorsection");
    }

    if (checkcode == true && checkname == true && checkemail == true && checkphone == true && checkdate == true) {
        // function sendRequest(email,name,phone,date,message,code,request_url,referral_url,codecaptcha,_token) {
          // create a new XMLHttpRequest object
          var xhr = new XMLHttpRequest();

          // set up the request
          xhr.open("POST", "{{ url('appointmentsave') }}");
          xhr.setRequestHeader("Content-Type", "application/json");

          // handle the response
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        window.location.href="{{ url('/') }}/thank-you";//redirection
                    } else {
                        if (xhr.status == 400) {
                            document.getElementById("submitapp").disabled = false;
                            document.getElementById("submitapp").innerText = "Submit";
                            document.getElementById("validcaptcha_app").innerHTML = 'Please Enter Valid Captcha.';
                        }
                        if (xhr.status == 401) {
                            document.getElementById("submitapp").disabled = false;
                            document.getElementById("submitapp").innerText = "Submit";
                            document.getElementById("validcaptcha_app").innerHTML = 'Please Enter Valid Captcha.';
                        }
                        document.getElementById("submitapp").disabled = false;
                    }
                }
            };
            var data = {
                'email': email,
                'name': name,
                'phone': phone,
                'date': date,
                'message': message,
                'uncode': code,
                'request_url': request_url,
                'referral_url': referrer_url,
                'captcha': codecaptcha,
                '_token':_token,
            };
            // send the request
            document.getElementById("submitapp").disabled = true;
            document.getElementById("submitapp").innerText = "Submitting...";
            xhr.send(JSON.stringify(data));
        // }
    }
}

function checkvalidation_contact(getid) {

    var code = document.getElementById("uniq_app").value
    var codecaptcha = document.getElementById("captchacode_app").value
    var name = document.getElementById("name_app").value
    var email = document.getElementById("email_app").value
    var phone = document.getElementById("phone_app").value
    var request_url = document.getElementById("request").value
    var referrer_url = document.getElementById("referrer").value
    var _token = document.getElementById("token_app").value
    var message = document.getElementById("message").value
    // alert(codecaptcha);
    // console.log(code);
    // console.log(codecaptcha);
    var capminlenth = 6;
    var phoneminLength = 10;
    var phonemaxLength = 10;
    var checkcode = false;
    var checkname = false;
    var checkemail = false;
    var checkphone = false;
    var checksername = false;

    if (codecaptcha == '') {
        document.getElementById("validcaptcha_app").innerHTML = 'This field is required.';
        document.getElementById("captchacode_app").classList.add("errorsection");
        document.getElementById("captchacode_app").classList.remove("validsection");
        // e.preventDefault();
    } else if (codecaptcha.length != capminlenth) {
        document.getElementById("validcaptcha_app").innerHTML = 'Please Enter Valid Captcha.';
        document.getElementById("captchacode_app").classList.add("errorsection");
        document.getElementById("captchacode_app").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkcode = true;
        document.getElementById("validcaptcha_app").innerHTML = ' ';
        document.getElementById("captchacode_app").classList.add("validsection");
        document.getElementById("captchacode_app").classList.remove("errorsection");
    }

    //var regName = /^[a-zA-Z]+/;
    var regName=/^[a-zA-Z ]*$/;
    if (name == '') {
        document.getElementById("validname_app").innerHTML = 'This field is required.';
        document.getElementById("name_app").classList.add("errorsection");
        document.getElementById("name_app").classList.remove("validsection");
        // e.preventDefault();
    } else if (!regName.test(name)) {
        document.getElementById("validname_app").innerHTML = 'Please Enter Valid Name.';
        document.getElementById("name_app").classList.add("errorsection");
        document.getElementById("name_app").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkname = true;
        document.getElementById("validname_app").innerHTML = ' ';
        document.getElementById("name_app").classList.add("validsection");
        document.getElementById("name_app").classList.remove("errorsection");
    }

    var emailExp = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    emailExp.test(email);
    if (email == '') {
        document.getElementById("showemailmsg_app").innerHTML = 'This field is required.';
        document.getElementById("email_app").classList.add("errorsection");
        document.getElementById("email_app").classList.remove("validsection");
        // e.preventDefault();
    } else if (!emailExp.test(email)) {
        document.getElementById("showemailmsg_app").innerHTML = 'Please Enter Valid Email ID.';
        document.getElementById("email_app").classList.add("errorsection");
        document.getElementById("email_app").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkemail = true;
        document.getElementById("showemailmsg_app").innerHTML = ' ';
        document.getElementById("email_app").classList.add("validsection");
        document.getElementById("email_app").classList.remove("errorsection");
    }

    var phoneno = /^[0-9]+$/;
    if (phone == '') {
        document.getElementById("validphone_app").innerHTML = 'This field is required.';
        document.getElementById("phone_app").classList.add("errorsection");
        document.getElementById("phone_app").classList.remove("validsection");
        // e.preventDefault();
    } else if (phone.length < phoneminLength) {
        document.getElementById("validphone_app").innerHTML = 'Please Enter Valid Phone Number.';
        document.getElementById("phone_app").classList.add("errorsection");
        document.getElementById("phone_app").classList.remove("validsection");
        // e.preventDefault();
    } else if (!(phoneno.test(phone))) {
        document.getElementById("validphone_app").innerHTML = 'Please Enter Valid Phone Number.';
        document.getElementById("phone_app").classList.add("errorsection");
        document.getElementById("phone_app").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkphone = true;
        document.getElementById("validphone_app").innerHTML = ' ';
        document.getElementById("phone_app").classList.add("validsection");
        document.getElementById("phone_app").classList.remove("errorsection");
    }

    if (checkcode == true && checkname == true && checkemail == true && checkphone == true) {

        var xhr = new XMLHttpRequest();

          // set up the request
          xhr.open("POST", "{{ url('contactussave') }}");
          xhr.setRequestHeader("Content-Type", "application/json");

          // handle the response
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        window.location.href="{{ url('/') }}/thank-you";//redirection
                    } else {
                        if (xhr.status == 400) {
                            document.getElementById("submitapp").disabled = false;
                            document.getElementById("submitapp").innerText = "Submit";
                            document.getElementById("validcaptcha_app").innerHTML = 'Please Enter Valid Captcha.';
                        }
                        if (xhr.status == 401) {
                            document.getElementById("submitapp").disabled = false;
                            document.getElementById("submitapp").innerText = "Submit";
                            document.getElementById("validcaptcha_app").innerHTML = 'Please Enter Valid Captcha.';
                        }
                        document.getElementById("submitapp").disabled = false;
                    }
                }
            };
            var data = {
                'email':email,
                'name':name,
                'phone':phone,
                'message':message,
                'uncode':code,
                'request_url':request_url,
                'referral_url':referrer_url,
                'captcha':codecaptcha,
                '_token':_token,
            };
            // send the request
            document.getElementById("submitapp").disabled = true;
            document.getElementById("submitapp").innerText = "Submitting...";
            xhr.send(JSON.stringify(data));
    }
}

// Callback form
function checkvalidationcallback() {

    var code = document.getElementById("uniq").value
    var codecaptcha = document.getElementById("captchacode").value
    var name = document.getElementById("name").value
    var request_url = document.getElementById("request_call").value
    var referrer_url = document.getElementById("referrer_call").value
    var phone = document.getElementById("mobile").value
    var _token = document.getElementById("token").value
    // console.log(request_url,referrer_url);
    var capminlenth = 6;
    var phoneminLength = 10;
    var phonemaxLength = 10;
    var checkcode = false;
    var checkname = false;
    // var checkemail = false;
    var checkphone = false;
    // var checkdate = false;
    // var checksername = false;

    if (codecaptcha == '') {
        document.getElementById("validcaptcha").innerHTML = 'This field is required.';
        document.getElementById("captchacode").classList.add("errorsection");
        document.getElementById("captchacode").classList.remove("validsection");
        // e.preventDefault();
    } else if (codecaptcha.length != capminlenth) {
        document.getElementById("validcaptcha").innerHTML = 'Please Enter Valid Captcha.';
        document.getElementById("captchacode").classList.add("errorsection");
        document.getElementById("captchacode").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkcode = true;
        document.getElementById("validcaptcha").innerHTML = ' ';
        document.getElementById("captchacode").classList.add("validsection");
        document.getElementById("captchacode").classList.remove("errorsection");
    }

    //var regName = /^[a-zA-Z]+/;
    var regName=/^[a-zA-Z ]*$/;
    if (name == '') {
        document.getElementById("validname").innerHTML = 'This field is required.';
        document.getElementById("name").classList.add("errorsection");
        document.getElementById("name").classList.remove("validsection");
        // e.preventDefault();
    } else if (!regName.test(name)) {
        document.getElementById("validname").innerHTML = 'Please Enter Valid Name.';
        document.getElementById("name").classList.add("errorsection");
        document.getElementById("name").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkname = true;
        document.getElementById("validname").innerHTML = ' ';
        document.getElementById("name").classList.add("validsection");
        document.getElementById("name").classList.remove("errorsection");
    }

   var phoneno = /^[0-9]+$/;
    if (phone == '') {
        document.getElementById("showmobilemsg").innerHTML = 'This field is required.';
        document.getElementById("mobile").classList.add("errorsection");
        document.getElementById("mobile").classList.remove("validsection");
        // e.preventDefault();
    } else if (phone.length < phoneminLength) {
        document.getElementById("showmobilemsg").innerHTML = 'Please Enter Valid Phone Number.';
        document.getElementById("mobile").classList.add("errorsection");
        document.getElementById("mobile").classList.remove("validsection");
        // e.preventDefault();
    } else if (!(phoneno.test(phone))) {
        document.getElementById("showmobilemsg").innerHTML = 'Please Enter Valid Phone Number.';
        document.getElementById("mobile").classList.add("errorsection");
        document.getElementById("mobile").classList.remove("validsection");
        // e.preventDefault();
    } else {
        var checkphone = true;
        document.getElementById("showmobilemsg").innerHTML = ' ';
        document.getElementById("mobile").classList.add("validsection");
        document.getElementById("mobile").classList.remove("errorsection");
    }


    if (checkcode == true && checkname == true && checkphone == true) {
        var xhr = new XMLHttpRequest();

          // set up the request
          xhr.open("POST", "{{ url('callback') }}");
          xhr.setRequestHeader("Content-Type", "application/json");

          // handle the response
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        window.location.href="{{ url('/') }}/thank-you";//redirection
                    } else {
                        if (xhr.status == 400) {
                            document.getElementById("submitcall").disabled = false;
                            document.getElementById("submitcall").innerText = "Submit";
                            document.getElementById("validcaptcha").innerHTML = 'Please Enter Valid Captcha.';
                        }
                        if (xhr.status == 401) {
                            document.getElementById("submitcall").disabled = false;
                            document.getElementById("submitcall").innerText = "Submit";
                            document.getElementById("validcaptcha").innerHTML = 'Please Enter Valid Captcha.';
                        }
                        document.getElementById("submitcall").disabled = false;
                    }
                }
            };
            var data = {
                'mobile':phone,
                'name':name,
                'request_url':request_url,
                'referral_url':referrer_url,
                'uncode':code,
                'captcha':codecaptcha,
                '_token':_token,
            };
            document.getElementById("submitcall").disabled = true;
            document.getElementById("submitcall").innerText = "Submitting...";
            // send the request
            xhr.send(JSON.stringify(data));
    }
}

// For Call Back function validate

// Name onclick validation
function validateUsername() {
  var username = document.getElementById('name').value;
  var errorSpan = document.getElementById('validname');
  var regName=/^[a-zA-Z ]*$/;
  if (!regName.test(username)) {
    errorSpan.textContent = "Please enter a valid name (only letters, spaces, and hyphens are allowed)";
  }else if (username.length < 3) {
    errorSpan.textContent = "Name must be at least 3 characters long";
  } else if (username.length > 40) {
    errorSpan.textContent = "Name can't exceed 40 characters";
  } else {
    errorSpan.textContent = "";
  }
}

// mobile no. onclick validation
function validatePhoneNumber() {
  var phoneNumber = document.getElementById('mobile').value;
  var errorSpan = document.getElementById('showmobilemsg');

  // Regular expression pattern for a 10-digit mobile number
  var pattern = /^\d{10}$/;

  if (pattern.test(phoneNumber)) {
    errorSpan.textContent = "";
  } else {
    errorSpan.textContent = "Please enter a valid 10-digit phone number";
  }
}
// For Call Back function validate

// For Contact and appointment function validate
// Name onclick validation
function validateUsernameapp() {
  var username = document.getElementById('name_app').value;
  var errorSpan = document.getElementById('validname_app');
  var regName=/^[a-zA-Z ]*$/;
  if (!regName.test(username)) {
    errorSpan.textContent = "Please enter a valid name (only letters, spaces, and hyphens are allowed)";
  }else if (username.length < 3) {
    errorSpan.textContent = "Name must be at least 3 characters long";
  } else if (username.length > 40) {
    errorSpan.textContent = "Name can't exceed 40 characters";
  } else {
    errorSpan.textContent = "";
  }
}

// mobile no. onclick validation
function validatePhoneNumberapp() {
  var phoneNumber = document.getElementById('phone_app').value;
  var errorSpan = document.getElementById('validphone_app');

  // Regular expression pattern for a 10-digit mobile number
  var pattern = /^\d{10}$/;

  if (pattern.test(phoneNumber)) {
    errorSpan.textContent = "";
  } else {
    errorSpan.textContent = "Please enter a valid 10-digit phone number";
  }
}

// for contact and appointment
function validateEmail() {
  var emailInput = document.getElementById('email_app');
  var email = emailInput.value;
  var errorSpan = document.getElementById('showemailmsg_app');

  // Regular expression pattern for email validation
  var pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  if (pattern.test(email)) {
    errorSpan.textContent = ""; // Clear any existing error message
    // Do something with the valid email, such as submitting the form
  } else {
    errorSpan.textContent = "Please enter a valid email address";
  }
}
// For Contact and appointment function validate
</script>
