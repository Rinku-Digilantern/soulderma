<form class="row g-3  from-wrap banner_form">
    <input type="hidden" id="uniq" name="uncode">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div class=" col-auto col_auto_1">
        <input type="text" name="name" autocomplete="off" class="form-control" id="name" onkeyup="validateUsername()" placeholder="Name*" required="">
        <div id="validname" class="error"></div>
    </div>
    <div class="col_auto_1 col-auto ">
        <input type="text" class="form-control" name="mobile" autocomplete="off" id="mobile" onkeyup="validatePhoneNumber()" minlength="10" maxlength="10" placeholder="Phone*" required="">
        <div id="showmobilemsg" class="error"></div>
    </div>

    <input type="hidden" class="form-control" name="request_url" id="request_call" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <input type="hidden" class="form-control" id="referrer_call" name="referrer">
    <div class=" col-auto">
        <div class="commoncaptcawrap commoncaptcawrap_img">
            <div class="req-captcha-box  captcha">
                <div class="captchacodeleftpic" id="capt">
                </div>
            </div>
            <div class="button-box">
                <button type="button" class="btnBtnSubmits" onClick="captchareload()"><i class="bi bi-arrow-counterclockwise fa fa-refresh"></i></button>
            </div>
        </div>
    </div>
    <div class="col-auto">
        <input type="text" class="form-control captcha_field" name="captcha" id="captchacode" autocomplete="off" placeholder="Captcha*" maxlength="6">
        <div id="validcaptcha" class="error"></div>

    </div>
    <div class="col-auto">
        <button id="submitcall" type="button" onclick="checkvalidationcallback()" class="showsubmitbutton">SUBMIT</button>
    </div>
</form>

<!-- Backup old design -->

<!-- <form class="row g-3 from-wrap">
    <input type="hidden" id="uniq" name="uncode">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div class="col-auto">
        <input type="text" name="name" autocomplete="off" class="form-control" id="name" onkeyup="validateUsername()" placeholder="Name*" required="">
        <div id="validname" class="error"></div>
    </div>
    <div class="col-auto">
        <input type="text" class="form-control" name="mobile" autocomplete="off" id="mobile" onkeyup="validatePhoneNumber()" minlength="10" maxlength="10" placeholder="Phone*" required="">
        <div id="showmobilemsg" class="error"></div>
    </div>
    <input type="hidden" class="form-control" name="request_url" id="request_call" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <input type="hidden" class="form-control" id="referrer_call" name="referrer">
    <div class="col-auto">
        <div class="d-flex align-items-center commoncaptcawrap ">
            <div class="req-captcha-box  captcha">
                <div class="captchacodeleftpic" id="capt">
                </div>
            </div>
            <div class="button-box">
                <button type="button" class="btnBtnSubmits" onClick="captchareload()"><i class="bi bi-arrow-counterclockwise fa fa-refresh"></i></button>
            </div>
        </div>

    </div>
    <div class="col-auto">

        <input type="text" class="form-control captcha_field" name="captcha" id="captchacode" autocomplete="off" placeholder="Captcha*" maxlength="6">
        <div id="validcaptcha" class="error"></div>

    </div>
    <div class="col-auto">
        <button id="submitcall" type="button" onclick="checkvalidationcallback()" class="showsubmitbutton">SUBMIT</button>
    </div>
</form> -->