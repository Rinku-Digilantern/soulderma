$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $(".main-headerwrap").addClass("headernewclass");
    } else {
        $(".main-headerwrap").removeClass("headernewclass");
    }
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $("header").addClass("headerfixtop");
        $(".animate-menu").addClass("side_menubar");
    } else {
        $("header").removeClass("headerfixtop");
        $(".animate-menu").removeClass("side_menubar");
    }
});

/*--------------------sidebar--------------*/

$.sidebarMenu($(".sidebar-menu"));

$.sidebarMenu($(".sidebar-menu-rtl"));

$("#showRight").click(function () {
    $(".animate-menu-right").toggleClass("animate-menu-open");
    $(".second-button").addClass("close_icon");
});

$("#hideRight").click(function () {
    $(".animate-menu-right").removeClass("animate-menu-open");
    $(".second-button").removeClass("close_icon");
});

$(document).ready(function () {
    $(".second-button").on("click", function () {
        $(".animated-icon2").toggleClass("open");
    });
});

$(document).ready(function () {
    $(".nav-toggle").click(function () {
        $("body").toggleClass("bodybga");
    });
});

/*------------------end--sidebar--------------*/

document.addEventListener("DOMContentLoaded", function () {
    window.addEventListener("scroll", function () {
        if (window.scrollY > 50) {
            document.getElementById("navbar_top").classList.add("fixed-top");

            // add padding top to show content behind navbar

            navbar_height = document.querySelector(".navbar").offsetHeight;
        } else {
            document.getElementById("navbar_top").classList.remove("fixed-top");

            // remove padding top from body

            document.body.style.paddingTop = "0";
        }
    });
});

// DOMContentLoaded  end


$(document).ready(function () {
    $(".firstowl").owlCarousel({
        loop: false,

        margin: 30,

        stagePadding: 20,

        nav: true,

        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>",
        ],

        dots: false,

        autoplay: false,

        responsive: {
            0: {
                items: 1,

                nav: false,

                margin: 0,

                stagePadding: 0,
            },

            600: {
                items: 1,

                nav: false,
            },

            1000: {
                items: 1,
            },
        },
    });

    $(".second-owl").owlCarousel({
        loop: false,

        margin: 30,

        stagePadding: 20,

        nav: false,

        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>",
        ],

        dots: true,

        autoplay: false,

        responsive: {
            0: {
                items: 1,

                nav: false,

                margin: 10,

                stagePadding: 0,
            },

            600: {
                items: 1,

                nav: false,
            },

            1000: {
                items: 1,
            },
        },
    });

    $(".aboutecowl").owlCarousel({
        loop: true,
        margin: 30,
        stagePadding: 20,
        nav: false,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>",
        ],

        dots: true,
        autoplay: true,
        autoplayTimeout: 4000,
        responsive: {
            0: {
                items: 1,

                nav: false,

                margin: 10,

                stagePadding: 0,
            },
            600: {
                items: 3,
                nav: false,
            },
            1000: {
                items: 4,
            },
        },
    });

    $(".clienttestimonial").owlCarousel({
        loop: true,

        margin: 30,

        stagePadding: 20,

        nav: true,

        navText: [
            "<i class='fa  fa-long-arrow-left'></i>",
            "<i class='fa  fa-long-arrow-right'></i>",
        ],

        dots: false,

        autoplay: true,

        autoplaySpeed: false,

        responsive: {
            0: {
                items: 1,

                nav: false,

                margin: 10,

                stagePadding: 0,
            },

            600: {
                items: 2,

                nav: true,
            },

            1000: {
                items: 2,
            },
        },
    });

    $(".impserviceslider").owlCarousel({

        stagePadding: 200,

        loop: true,

        margin: 10,

        nav: false,

        items: 1,

        lazyLoad: true,

        responsive: {
            0: {
                items: 1,

                stagePadding: 60,
            },

            600: {
                items: 1,

                stagePadding: 100,
            },

            1000: {
                items: 1,

                stagePadding: 200,
            },

            1200: {
                items: 1,

                stagePadding: 250,
            },

            1400: {
                items: 1,

                stagePadding: 200,

                margin: 0,
            },

            1600: {
                items: 1,

                stagePadding: 350,
            },

            1800: {
                items: 1,

                stagePadding: 400,
            },
        },
    });
});

/*-----------------vide-popup------------------------*/

$(document).ready(function () {
    // Gets the video src from the data-src on each button

    var $videoSrc;

    $(".video-btn").click(function () {
        $videoSrc = $(this).data("src");
    });

    // console.log($videoSrc);

    // when the modal is opened autoplay it

    $("#myModal").on("shown.bs.modal", function (e) {
        // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get

        $("#video").attr(
            "src",
            $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0"
        );
    });

    // stop playing the youtube video when I close the modal

    $("#myModal").on("hide.bs.modal", function (e) {
        // a poor man's stop video

        $("#video").attr("src", $videoSrc);
    });

    // document ready
});

/*-----------------end-vide-popup------------------------*/

function expandColumn(element) {
    element.classList.add("expanded");

    var columns = document.getElementsByClassName("box-expand");

    for (var i = 0; i < columns.length; i++) {
        if (columns[i] !== element) {
            columns[i].classList.add("other-column");
        }
    }
}

function collapseColumns() {
    var columns = document.getElementsByClassName("box-expand");

    for (var i = 0; i < columns.length; i++) {
        columns[i].classList.remove("expanded");

        columns[i].classList.remove("other-column");
    }
}

/*-----------------------end----hover effect------------------------*/

const nav = document.querySelector(".nav-container");
if (nav) {
    const toggle = nav.querySelector(".nav-toggle");

    if (toggle) {
        toggle.addEventListener("click", () => {
            if (nav.classList.contains("is-active")) {
                nav.classList.remove("is-active");
            } else {
                nav.classList.add("is-active");
            }
        });

        // nav.addEventListener("blur", () => {

        //   nav.classList.remove("is-active");

        // });
    }
}

/*-------------------------------------------------------------------*/

/*-------------------------------------------------------------------*/

/*---------------------backto top--------------------*/

var btn = $("#button");

$(window).scroll(function () {
    if ($(window).scrollTop() > 100) {
        btn.addClass("show");
    } else {
        btn.removeClass("show");
    }
});

btn.on("click", function (e) {
    e.preventDefault();

    $("html, body").animate({ scrollTop: 0 }, "200");
});

/*-----------------end----backto top--------------------*/
