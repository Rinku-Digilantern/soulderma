<style type="text/css">/* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
          margin: 0 auto !important;
          padding: 0 !important;
          height: 100% !important;
          width: 100% !important;
        }
        a{
          text-decoration: none;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
          -ms-text-size-adjust: 100%;
          -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*='margin: 16px 0'] {
          margin:0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
          mso-table-lspace: 0pt !important;
          mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
          border-spacing: 0 !important;
          border-collapse: collapse !important;
          table-layout: fixed !important;
          margin: 0 auto !important;
        }
        table table table {
          table-layout: auto;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
          -ms-interpolation-mode:bicubic;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        *[x-apple-data-detectors],  /* iOS */
        .x-gmail-data-detectors,  /* Gmail */
        .x-gmail-data-detectors *,
        .aBn {
          border-bottom: 0 !important;
          cursor: default !important;
          color: inherit !important;
          text-decoration: none !important;
          font-size: inherit !important;
          font-family: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
        .a6S {
           display: none !important;
           opacity: 0.01 !important;
        }
        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img + div {
           display:none !important;
          }

        /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
          text-decoration: none !important;
        }


        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
          .email-container {
            min-width: 375px !important;
          }
        }
    </style>
    <!-- Progressive Enhancements -->
    <style type="text/css">/* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
          transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
          background: #555555 !important;
          border-color: #555555 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

          .email-container {
            width: 100% !important;
            margin: auto !important;
          }

          /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
          .fluid {
            max-width: 100% !important;
            height: auto !important;
            margin-left: auto !important;
            margin-right: auto !important;
          }

          /* What it does: Forces table cells into full-width rows. */
          .stack-column,
          .stack-column-center {
            display: block !important;
            width: 100% !important;
            max-width: 100% !important;
            direction: ltr !important;
          }
          /* And center justify these ones. */
          .stack-column-center {
            text-align: center !important;
          }

          /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
          .center-on-narrow {
            text-align: center !important;
            display: block !important;
            margin-left: auto !important;
            margin-right: auto !important;
            float: none !important;
          }
          table.center-on-narrow {
            display: inline-block !important;
          }

          /* What it does: Adjust typography on small screens to improve readability */
          .email-container p {
            font-size: 17px !important;
            line-height: 22px !important;
          }

        }
    </style>
    <!-- What it does: Makes background images in 72ppi Outlook render at correct size. --><!--[if gte mso 9]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
    <center style="width: 100%; background: #bbbbbb; text-align: left;"><!-- Email Header : BEGIN -->
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="email-container" role="presentation" style="margin: auto;" width="600">
      <tbody>
        <tr>
          <td style="padding:5px 0; text-align: center;background: #666666;"><img alt="alt_text" border="0" src="https://www.souldermaclinic.com/website/img/logo.png" style="height: auto; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;width: 150px;" /></td>
        </tr>
      </tbody>
    </table>
    <!-- Email Header : END --><!-- Email Body : BEGIN -->

    <table align="center" border="0" cellpadding="0" cellspacing="0" class="email-container" role="presentation" style="margin: auto;" width="600"><!-- Hero Image, Flush : BEGIN -->
      <tbody>
        <tr>
          <td bgcolor="#ffffff"><img align="center" alt="alt_text" border="0" class="g-img" height="" src="https://www.souldermaclinic.com/website/img/about-clinic/about-clinic.jpg" style="width: 100%; max-width: 600px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;" width="600" /></td>
        </tr>
        <!-- Hero Image, Flush : END --><!-- 1 Column Text + Button : BEGIN -->
        <tr>
          <td bgcolor="#ffffff" style="padding: 40px 40px 20px; text-align: center;">
          <h1 style="margin: 0; font-family: sans-serif; font-size: 24px; line-height: 27px; color: #333333; font-weight: normal;">Thank you for Contacting us!</h1>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ffffff" style="padding: 0 40px 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: center;">
          <p style="margin: 0;">One of our team members shall get in touch with you shortly.If you would like to speak with someone immediately, connect with us on:<br />
          <a href="tel:+91 8595941529">+91 8595941529</a> or <a href="mailto:info@souldermaclinic.com">info@souldermaclinic.com  </a></p>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ffffff" style="padding: 0 40px 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: center;">
          <p style="margin: 0;">Stay connected with us for updates!</p>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ffffff" style="padding: 0 40px 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="margin: auto">
            <tbody>
              <tr>
                <td class="button-td" style="border-radius: 3px; background: #222222; text-align: center;"><a class="button-a" href="https://www.souldermaclinic.com" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" target="_blank"><span style="color:#ffffff;">Go to Website</span></a></td>
              </tr>
              <!-- Button : END --><!-- 1 Column Text + Button : END --><!-- Clear Spacer : BEGIN -->
              <tr>
                <td align="center" class="social_container" style="padding:20px 0px;" valign="top">

                </td>
              </tr>
              <!-- Clear Spacer : END -->
            </tbody>
          </table>
          </td>
        </tr>
      </tbody>
    </table>
    <!-- Email Body : END --></center>
