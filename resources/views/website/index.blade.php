@include('website.include.header')
<link rel="stylesheet" href="{{ url('website') }}/css/home.css">
<link rel="stylesheet" href="{{ url('website') }}/css/sliderfive.css">
<!-- <link rel="stylesheet" href="{{ url('website') }}/css/written-testimonials.css"> -->
<section class="bannersection">
    <div class="header-banner">
        <img src="{{ url('website') }}/img/banner.jpg" class="w-100 img-fluid" alt="Soulderma">
    </div>
    <div class="bannertextwrap extra-class">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-12 col-md-6">
                    <div class="imgsign">
                        <img src="{{ url('website') }}/img/drsign.png" class="img-fluid" alt="Soulderma">
                    </div>
                    <div class="mdtext">m.d.</div>
                    <div class="mainserviceslinkbanner">
                        <ul>
                            <li><a href="{{ url('skin') }}">SKIN</a></li>
                            <li><a href="{{ url('aesthetics') }}">AESTHETIC</a></li>
                            <li><a href="{{ url('hair') }}">HAIR</a></li>
                        </ul>
                    </div>
                    <div class="readmoreservices for_mob">
                        <a href="tel:+918595941529">CALL US NOW</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="bannerpointerborder">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="bannerpointwrap rigitborder">
                            <div class="bannerpointheading">EXPERTISE</div>
                            <div class="bannerpoint">
                                <p>10+ years of experience and expertise of treating all dermatological conditions.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="bannerpointwrap rigitborder">
                            <div class="bannerpointheading">TECHNOLOGY</div>
                            <div class="bannerpoint">
                                <p>Utilizes US-FDA approved advanced technologies and techniques.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="bannerpointwrap">
                            <div class="bannerpointheading">CERTIFICATIONS</div>
                            <div class="bannerpoint">
                                <p>Holds a number of national and international certifications on the latest advances.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pointers_for_mobile">
            <div class="bannerpointerborder">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="bannerpointwrap rigitborder">
                                <div class="bannerpointheading">10,000+ <span class="bannerpoint">
                                        Happy Patient
                                    </span></div>

                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="bannerpointwrap rigitborder">
                                <div class="bannerpointheading">US FDA <span class="bannerpoint">
                                        Approved Technology
                                    </span></div>

                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="bannerpointwrap">
                                <div class="bannerpointheading">10+ Years <span class="bannerpoint">
                                        of Experience
                                    </span></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="commonrequestform pt-3">
            @include('website.include.requestcallback')
        </div>
    </div>
</section>



<section id="requestacallbackwrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="requestwrapper">
                    <div class="requestheading">Soul Derma's Featured Services</div>
                </div>
            </div>
        </div>
    </div>
</section>


<main id="main" class="content-wrap on-canvas for-desktop service_tabs">
    <div class="container-fluid mod-home_services_slider">
        <div class="row tab_section">
            <div class="wrapper">

                <div class="item left">
                    <div class="image">
                        <img src="{{ url('website') }}/img/services-5.jpg" class="w-100" alt="MIRA PEEL">
                    </div>
                    <div class="inner">
                        <div class="innerheading notofamily commonheadingfontsize">MIRA PEEL</div>
                        <div class="content">
                            <div class="innersubheading">Mira Peel: Reveal Your True Glow</div>
                            <p>MIRA peel is a customised treatment, a perfect add-on technology to routine skin therapies for aging skin. This technology uses Fractional Transbrasion™ to assist in desquamation of dead cells along with delivery of specialised solutions...</p> <!--  deep into the skin layers for optimum rejuvenation and results. It aids in collagen production and elastin synthesis, which in turn tightens and lifts the skin. If you want to make your skin look flawless, receive MIRA peel at Soul Derma Clinic. -->
                        </div>
                        <a href="{{ url('/mirapeel') }}" class="btn commonanchor">View More</a>
                    </div>
                </div>


                <div class="item center">
                    <div class="image">
                        <img src="{{ url('website') }}/img/services-1.jpg" class="w-100" alt="Hair Treatment">
                    </div>
                    <div class="inner">
                        <h1 class="innerheading notofamily commonheadingfontsize">HAIR TRANSPLANT</h1>
                        <div class="content">
                            <h2 class="innersubheading">Unleash Your Best Self- Hair Transplant</h2>
                            <p>Hair transplantation is a surgical technique to harvest healthy, permanent hair-bearing skin or directly individual hair follicular units using a micro punch device from the back or side of the scalp to bald or thinning sections of the scalp.</p> <!-- If you are seeking a tailor-made, advanced FUE hair transplantation, visit now at Soul Derma Clinic. -->
                        </div>
                        <a href="{{ url('/hair-transplant') }}" class="btn commonanchor">View More</a>
                    </div>
                </div>
                <div class="item right">
                    <div class="image">
                        <img src="{{ url('website') }}/img/services-3.jpg" class="w-100" alt="BRIDE & GROOM">
                    </div>
                    <div class="inner">
                        <div class="innerheading notofamily commonheadingfontsize">BRIDE & GROOM</div>
                        <div class="content">
                            <div class="innersubheading">Love Your Skin: Skincare for the Big Day</div>
                            <p>The wedding is one of the most memorable days for both bride and groom and so they deserve to look their best. Soul Derma clinic offers varieties of pre-wedding treatments for bride and groom that include laser hair removal and skin lightening rejuvenating treatments such as mesotherapy, laser toning, and chemical peel- all services in affordable packages.</p><!--/ -->
                        </div>
                        <!-- <a href="#" class="btn commonanchor">View More</a> -->
                    </div>
                </div>
                <div class="item hiddenright">
                    <div class="image">
                        <img src="{{ url('website') }}/img/services-4.jpg" class="w-100" alt="LASER HAIR REMOVAL">
                    </div>
                    <div class="inner">
                        <div class="innerheading notofamily commonheadingfontsize">LASER HAIR REMOVAL</div>
                        <div class="content">
                            <div class="innersubheading">Smooth, Confident, Carefree: Laser Hair Removal</div>
                            <p>Laser hair removal is an excellent cosmetic method to get rid of undesirable facial or body hair on any skin type. If you want to stay hair-free for a long time, contact Soul Derma Clinic for the best laser hair removal at cost-effective rates, today!</p>
                        </div>
                        <!-- <a href="#" class="btn commonanchor">View More</a> -->
                    </div>
                </div>

            </div>
            <div class="tabs-wrapper">
                <div class="tabs">

                    <div class="tab">
                        <img src="{{ url('website') }}/img/meerapee.png" alt="MIRA PEEL">
                        <p>MIRA PEEL</p>
                    </div>
                    <div class="tab active">
                        <img src="{{ url('website') }}/img/hairtp.png" alt="HAIR TRANSPLANT">
                        <p>HAIR TRANSPLANT</p>
                    </div>

                    <div class="tab">
                        <img src="{{ url('website') }}/img/bridegroom.png" alt="BRIDE & GROOM">
                        <p>BRIDE & GROOM</p>
                    </div>

                    <div class="tab">
                        <img src="{{ url('website') }}/img/lhr.png" alt="LASER HAIR REMOVAL">
                        <p>LASER HAIR REMOVAL</p>
                    </div>
                </div>
            </div>

        </div>

    </div> <!-- Quote Module -->


</main>

<!--
    <section id="impserviceswraper" class="for-desktop">
        <div class=" impserviceslider owl-carousel owl-theme">
            <div class="item">
                <div class="itemcontentwrap">
                    <div class="itmeimageleftwrap">
                        <img src="img/iv-therapy.jpg" class="w-100 img-fluid">
                    </div>
                    <div class="itmeimagerightwrap">
                        <div class="ivicontentwrap commonfivecontentstyle">
                            <div class="notofamily htpheading commonheadingfontsize">HAIR TRANSPLANT</div>
                            <div class="htpsubheading">SOUL DERMA CLINIC WAS FOUNDED</div>
                            <div class="htp-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla posuere
                                    aliquam nib sit amet convallis lacus pretium vitae orci varius natoque
                                    penatibus et magnis dis parturient montes nascetur ridiculus mus
                                    suspendisse finebus velit. vitae mattis placerat, massa tellus dignissim
                                    magna, id egestas mi magna nec nisi praesent congue ligula vitae massa
                                    pharetra, vitae. maximus metus lacinia. Quisque id sodales lacus.
                                    Vivamus sed lobortis diam.</p>
                            </div>
                            <div class="readmorebutton"><a href="#">READ MORE</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="itemcontentwrap">
                    <div class="itmeimageleftwrap">
                        <img src="img/hairtp.jpg" class="w-100 img-fluid">
                    </div>
                    <div class="itmeimagerightwrap">
                        <div class="ivicontentwrap commonfivecontentstyle">
                            <div class="notofamily htpheading commonheadingfontsize">HAIR TRANSPLANT</div>
                            <div class="htpsubheading">SOUL DERMA CLINIC WAS FOUNDED</div>
                            <div class="htp-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla posuere
                                    aliquam nib sit amet convallis lacus pretium vitae orci varius natoque
                                    penatibus et magnis dis parturient montes nascetur ridiculus mus
                                    suspendisse finebus velit. vitae mattis placerat, massa tellus dignissim
                                    magna, id egestas mi magna nec nisi praesent congue ligula vitae massa
                                    pharetra, vitae. maximus metus lacinia. Quisque id sodales lacus.
                                    Vivamus sed lobortis diam.</p>
                            </div>
                            <div class="readmorebutton"><a href="#">READ MORE</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="itemcontentwrap">
                    <div class="itmeimageleftwrap">
                        <img src="img/meerapeal.jpg" class="w-100 img-fluid">
                    </div>
                    <div class="itmeimagerightwrap">
                        <div class="ivicontentwrap commonfivecontentstyle">
                            <div class="notofamily htpheading commonheadingfontsize">HAIR TRANSPLANT</div>
                            <div class="htpsubheading">SOUL DERMA CLINIC WAS FOUNDED</div>
                            <div class="htp-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla posuere
                                    aliquam nib sit amet convallis lacus pretium vitae orci varius natoque
                                    penatibus et magnis dis parturient montes nascetur ridiculus mus
                                    suspendisse finebus velit. vitae mattis placerat, massa tellus dignissim
                                    magna, id egestas mi magna nec nisi praesent congue ligula vitae massa
                                    pharetra, vitae. maximus metus lacinia. Quisque id sodales lacus.
                                    Vivamus sed lobortis diam.</p>
                            </div>
                            <div class="readmorebutton"><a href="#">READ MORE</a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="itemcontentwrap">
                    <div class="itmeimageleftwrap">
                        <img src="img/meerapeal.jpg" class="w-100 img-fluid">
                    </div>
                    <div class="itmeimagerightwrap">
                        <div class="ivicontentwrap commonfivecontentstyle">
                            <div class="notofamily htpheading commonheadingfontsize">HAIR TRANSPLANT</div>
                            <div class="htpsubheading">SOUL DERMA CLINIC WAS FOUNDED</div>
                            <div class="htp-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla posuere
                                    aliquam nib sit amet convallis lacus pretium vitae orci varius natoque
                                    penatibus et magnis dis parturient montes nascetur ridiculus mus
                                    suspendisse finebus velit. vitae mattis placerat, massa tellus dignissim
                                    magna, id egestas mi magna nec nisi praesent congue ligula vitae massa
                                    pharetra, vitae. maximus metus lacinia. Quisque id sodales lacus.
                                    Vivamus sed lobortis diam.</p>
                            </div>
                            <div class="readmorebutton"><a href="#">READ MORE</a></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="item">
                <div class="itemcontentwrap">
                    <div class="itmeimageleftwrap">
                        <img src="img/meerapeal.jpg" class="w-100 img-fluid">
                    </div>
                    <div class="itmeimagerightwrap">
                        <div class="ivicontentwrap commonfivecontentstyle">
                            <div class="notofamily htpheading commonheadingfontsize">HAIR TRANSPLANT</div>
                            <div class="htpsubheading">SOUL DERMA CLINIC WAS FOUNDED</div>
                            <div class="htp-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla posuere
                                    aliquam nib sit amet convallis lacus pretium vitae orci varius natoque
                                    penatibus et magnis dis parturient montes nascetur ridiculus mus
                                    suspendisse finebus velit. vitae mattis placerat, massa tellus dignissim
                                    magna, id egestas mi magna nec nisi praesent congue ligula vitae massa
                                    pharetra, vitae. maximus metus lacinia. Quisque id sodales lacus.
                                    Vivamus sed lobortis diam.</p>
                            </div>
                            <div class="readmorebutton"><a href="#">READ MORE</a></div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </section> -->


<section id="fivemainservices" class="for-mobile">
    <div class="containerminmax">

        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item border_bottom" role="presentation">
                <button class="nav-link active" id="pills-ivtherapy-tab" data-bs-toggle="pill" data-bs-target="#pills-ivtherapy" type="button" role="tab" aria-controls="pills-ivtherapy" aria-selected="true">
                    <img src="{{ url('website') }}/img/ivtherapy.png" alt="IV THERAPY">
                    <p>IV THERAPY</p>
                </button>
            </li>
            <li class="nav-item border_bottom" role="presentation">
                <button class="nav-link" id="pills-hairtransplant-tab" data-bs-toggle="pill" data-bs-target="#pills-hairtransplant" type="button" role="tab" aria-controls="pills-hairtransplant" aria-selected="false">
                    <img src="{{ url('website') }}/img/hairtp.png" alt="HAIR TRANSPLANT">
                    <p>HAIR TRANSPLANT</p>
                </button>
            </li>
            <li class="nav-item border_bottom" role="presentation">
                <button class="nav-link" id="pills-mirapeel-tab" data-bs-toggle="pill" data-bs-target="#pills-mirapeel" type="button" role="tab" aria-controls="pills-mirapeel" aria-selected="false"> <img src="{{ url('website') }}/img/meerapee.png">
                    <p>MIRA PEEL</p>
                </button>
            </li>

            <li class="nav-item border_bottom" role="presentation">
                <button class="nav-link" id="pills-laserhairremoval-tab" data-bs-toggle="pill" data-bs-target="#pills-laserhairremoval" type="button" role="tab" aria-controls="pills-laserhairremoval" aria-selected="false"><img src="{{ url('website') }}/img/lhr.png">
                    <p>LASER HAIR REMOVAL</p>
                </button>
            </li>

            <li class="nav-item border_bottom" role="presentation">
                <button class="nav-link" id="pills-bridegroom-tab" data-bs-toggle="pill" data-bs-target="#pills-bridegroom" type="button" role="tab" aria-controls="pills-bridegroom" aria-selected="false"><img src="{{ url('website') }}/img/bridegroom.png">
                    <p>BRIDE & GROOM</p>
                </button>
            </li>
        </ul>
        <div class="mainfivservicesscrollwrap">
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-ivtherapy" role="tabpanel" aria-labelledby="pills-ivtherapy-tab">
                    <div class="ivthaerapy-wrap">
                        <div class="ivimagewrap commonfivestyle">
                            <img src="{{ url('website') }}/img/services-2.jpg" class="w-100 img-fluid" alt="IV THERAPY">
                        </div>
                        <div class="ivicontentwrap commonfivecontentstyle">
                            <div class="notofamily htpheading">IV THERAPY</div>
                            <div class="htpsubheading">Infuse Radiance: IV Therapy for Glowing Skin</div>
                            <div class="htp-content">
                                <p>IV (Intravenous) therapy is a customised approach used to directly administer fluids, essential nutrients, blood, and medicines into the bloodstream through a vein. If you want glowing, rejuvenated skin without surgeries at a reasonable price, undergo advanced IV therapy at Soul Derma Clinic.</p>
                                <!--  -->
                            </div>
                            <div class="readmoreservices">
                                <!-- <a href="#">READ MORE</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="pills-hairtransplant" role="tabpanel" aria-labelledby="pills-hairtransplant-tab">
                    <div class="ivthaerapy-wrap">
                        <div class="ivimagewrap commonfivestyle">
                            <img src="{{ url('website') }}/img/services-1.jpg" class="w-100 img-fluid" alt="Hair Treatment">
                        </div>
                        <div class="ivicontentwrap commonfivecontentstyle">
                            <div class="notofamily htpheading">HAIR TRANSPLANT</div>
                            <div class="htpsubheading">Unleash Your Best Self- Hair Transplant</div>
                            <div class="htp-content">
                                <p>Hair transplantation is a surgical technique to harvest healthy, permanent hair-bearing skin or directly individual hair follicular units using a micro punch device from the back or side of the scalp to bald or thinning sections of the scalp.</p> <!-- If you are seeking a tailor-made, advanced FUE hair transplantation, visit now at Soul Derma Clinic. -->
                            </div>
                            <div class="readmoreservices">
                                <a href="{{ url('hair-transplant') }}" class="commonanchor">READ MORE</a>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="tab-pane fade" id="pills-mirapeel" role="tabpanel" aria-labelledby="pills-mirapeel-tab">
                    <div class="ivthaerapy-wrap">
                        <div class="ivimagewrap commonfivestyle">
                            <img src="{{ url('website') }}/img/services-5.jpg" class="w-100 img-fluid" alt="Mira Peel">
                        </div>
                        <div class="ivicontentwrap commonfivecontentstyle">
                            <div class="notofamily htpheading">MIRA PEEL</div>
                            <div class="htpsubheading">Mira Peel: Reveal Your True Glow</div>
                            <div class="htp-content">
                                <p>MIRA peel is a customised treatment, a perfect add-on technology to routine skin therapies for aging skin. This technology uses Fractional Transbrasion™ to assist in desquamation of dead cells along with delivery of specialised solutions...</p> <!-- deep into the skin layers for optimum rejuvenation and results.  It aids in collagen production and elastin synthesis, which in turn tightens and lifts the skin. If you want to make your skin look flawless, receive MIRA peel at Soul Derma Clinic. -->
                            </div>
                            <div class="readmoreservices">
                                <a href="{{ url('/mirapeel') }}" class="commonanchor">READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="pills-laserhairremoval" role="tabpanel" aria-labelledby="pills-laserhairremoval-tab">
                    <div class="ivthaerapy-wrap">
                        <div class="ivimagewrap commonfivestyle">
                            <img src="{{ url('website') }}/img/services-4.jpg" class="w-100 img-fluid" alt="Laser hair removal">
                        </div>
                        <div class="ivicontentwrap commonfivecontentstyle">
                            <div class="notofamily htpheading">LASER HAIR REMOVAL</div>
                            <div class="htpsubheading">Smooth, Confident, Carefree: Laser Hair Removal</div>
                            <div class="htp-content">
                                <p>Laser hair removal is an excellent cosmetic method to get rid of undesirable facial or body hair on any skin type. If you want to stay hair-free for a long time, contact Soul Derma Clinic for the best laser hair removal at cost-effective rates, today!</p>
                            </div>
                            <div class="readmoreservices">
                                <!-- <a href="#" class="commonanchor">READ MORE</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="pills-bridegroom" role="tabpanel" aria-labelledby="pills-bridegroom-tab">
                    <div class="ivthaerapy-wrap">
                        <div class="ivimagewrap commonfivestyle">
                            <img src="{{ url('website') }}/img/services-3.jpg" class="w-100 img-fluid" alt="BRIDE & GROOM">
                        </div>
                        <div class="ivicontentwrap commonfivecontentstyle">
                            <div class="notofamily htpheading">BRIDE & GROOM</div>
                            <div class="htpsubheading">Love Your Skin: Skincare for the Big Day</div>
                            <div class="htp-content">
                                <p>The wedding is one of the most memorable days for both bride and groom and so they deserve to look their best. Soul Derma clinic offers varieties of pre-wedding treatments for bride and groom that include laser hair removal and skin lightening rejuvenating treatments such as mesotherapy, laser toning, and chemical peel- all services in affordable packages.</p><!--/ -->
                            </div>
                            <div class="readmoreservices">
                                <!-- <a href="#" class="commonanchor">READ MORE</a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>


<section id="homedoctorwrap">
    <div class="container">
        <div class="row forcenteritem">
            <div class="col-12 col-md-6">
                <div class="homedoctorcontentwrap">
                    <h3 class="homedrname notofamily commonheadingfontsize">DR. ANIKA GOEL</h3>
                    <div class="homeprofsion">M.D. DERMATOLOGY</div>
                    <div class="homedrcontent">
                        <p>Dr. Anika Goel is one of the finest, certified dermatologists in Greater Kailash, South Delhi. She is considered as the most premium Aesthetic Physician in Delhi. She has specialisation in cosmetic procedures and clinical treatments of skin, hair, and nail concerns. She is an expert in hair transplant surgery and even the best laser hair removal specialist in Delhi.</p>
                        <p>So far, more than 5,000 patients have undergone diagnosis and treatment of their concerns by her and have reported a boost in their self-confidence.</p>

                        <ul>
                            <li><i class="fa fa-check-circle"></i>M.D. in Dermatology, Venereology, and Leprosy- Sri Guru Ram Das Institute of Medical Science and Research, Amritsar (Baba Farid University of Health Sciences) - 2019</li>
                            <li><i class="fa fa-check-circle"></i>MBBS- D.Y. Patil Medical College, Navi Mumbai, India. (D Y Patil University) – 2015</li>
                        </ul>
                    </div>
                    <div class="homedrreadmore">
                        <a href="{{ url('skin-specialist') }}" class="commonanchor">Read More</a>
                        <img src="{{ url('website') }}/img/homedrsign.png" alt="Dr. Anika Goel is one of the finest, certified dermatologists in Greater Kailash, South Delhi.">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 img_end">
                <div class="homedrpngimage">
                    <img src="{{ url('website') }}/img/dr-image.png"  alt="Dr. Anika Goel is one of the finest, certified dermatologists in Greater Kailash, South Delhi.">
                </div>
            </div>
        </div>
    </div>
</section>



<section id="homeaboutdoctorwrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <h4 class="homeaboutdoctr notofamily commonheadingfontsize">ABOUT SOUL DERMA</h4>
                <div class="homeaboutdrcontent">
                    <p><strong>Soul Derma Clinic</strong>: Your Trusted Destination for Skin & Hair Care in South Delhi</p>

                    <p>Soul Derma Clinic, located in the prime area of Greater Kailash, South Delhi, is a highly regarded general and cosmetic skin clinic. Founded by renowned dermatologist Dr. Anika Goel, the clinic offers a comprehensive range of high-quality, customised, and affordable treatments for skin, hair, and aesthetics.</p>

                    <p class="displaynone">With decades of experience as a Chief Consultant Dermatologist and Dermal Clinician, Dr. Anika Goel ensures that Soul Derma Clinic stays up to date with FDA-approved cutting-edge dermal technology and utilises top-notch therapeutic products to deliver flawless results.</p>

                    <p class="displaynone">At Soul Derma Clinic, a dedicated team of well-trained and highly experienced experts takes pride in providing holistic treatments while prioritising patient comfort and safety. The staff consists of both male and female professionals who are known for their expertise and humanity.</p>

                    <!-- <p>Technologies like Radio cautery, Micro needling Radiofrequency, Vagi-nal Tightening, Q-Switched Nd: YAG laser, Microdermabrasion Hydra Medi facial, Milesman quadruple wavelength Diode Laser, and Dermapen 4 are offered at the clinic.</p> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section id="clinicslider">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="managesliderwidth">
                    <div class="firstowl owl-carousel owl-theme">
                        <div class="item">
                            <img src="{{ url('website') }}/img/clinic1.jpg" alt="Clinic Image">
                        </div>
                        <div class="item">
                            <img src="{{ url('website') }}/img/clinic2.jpg" alt="Clinic Image">
                        </div>
                        <div class="item">
                            <img src="{{ url('website') }}/img/clinic3.jpg" alt="Clinic Image">
                        </div>
                        <div class="item">
                            <img src="{{ url('website') }}/img/clinic4.jpg" alt="Clinic Image">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="sliderreadmore">
            <a href="{{ url('skin-clinic') }}" class="commonanchor">Read more</a>
        </div>
    </div>
</section>

<section id="imageexpandwrap" class="for-desktop">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="container-box">
                    <div class="box-expand" onmouseover="expandColumn(this)" onmouseout="collapseColumns()">
                        <div class="boxcontentheading notofamily commonheadingfontsize smallwidthheading">HAIR</div>
                        <div class="box-content">
                            <div class="boxcontentheading notofamily commonheadingfontsize">HAIR</div>
                            <div class="box-content-inner">
                                <p>Soul Derma offers innovative solutions for hair loss, hair fall, baldness, thinning hair, and scalp conditions.</p>
                                <p class="displayonhover">Their services include hair transplantation, growth factor therapy, mesotherapy, and advanced hair loss control therapy. These treatments stimulate new hair growth, manage baldness, strengthen hair follicles, and promote a healthy scalp environment.</p>
                                <div class="readexpand">
                                    <a href="{{ url('/hair') }}">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="expan-imagewrap">
                            <img src="{{ url('website') }}/img/hair-expand.png" alt="HAIR TREATMENTS">

                        </div>
                        <div class="expandservicesextandwrapper">
                            <div class="expandservicesheading">HAIR TREATMENTS</div>
                            <div class="expendservices">
                                <div class="expandserviceswrap">
                                    <div class="expandservicesclickwrap">
                                        <a href="{{ url('/hair-concerns') }}">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/Hair-concern-sm.jpg" alt="Hair Concern">
                                            </div>
                                            <p>Hair Concern</p>
                                        </a>
                                    </div>
                                    <!-- <div class="expandservicesclickwrap">
                                        <a href="#">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/dark-circle.jpg">
                                            </div>
                                            <p>Dark circle</p>
                                        </a>
                                    </div> -->
                                </div>
                                <div class="expandserviceswrap">
                                    <div class="expandservicesclickwrap">
                                        <a href="{{ url('/hair-growth') }}">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/Hair-growth-sm.jpg" alt="Hair Growth">
                                            </div>
                                            <p>Hair Growth</p>
                                        </a>
                                    </div>
                                    <!-- <div class="expandservicesclickwrap">
                                        <a href="#">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/dark-circle.jpg">
                                            </div>
                                            <p>Dark circle</p>
                                        </a>
                                    </div> -->
                                </div>
                            </div>
                            <!-- <div class="expendservicesviewmore">
                                <a href="#" class="commonanchor">View more</a>
                            </div> -->
                        </div>
                        <div class="godestination">
                            <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>

                    <div class="box-expand" onmouseover="expandColumn(this)" onmouseout="collapseColumns()">
                        <div class="boxcontentheading notofamily commonheadingfontsize smallwidthheading">SKIN</div>
                        <div class="box-content">
                            <div class="boxcontentheading notofamily commonheadingfontsize">SKIN</div>
                            <div class="box-content-inner">
                                <p>Soul Derma offers the most advanced and personalized skincare solutions for a wide range of dermatological concerns, focusing on both general and cosmetic issues.</p>
                                <p class="displayonhover">From acne and acne scars to hyperpigmentation and aging skin, their skincare experts in Delhi offer treatments like chemical peels, laser toning, microdermabrasion, RF, medi facial, injectable, and dermal fillers. </p>
                                <!-- These procedures help rejuvenate the skin, improve texture, reduce dark spots, boost collagen production, remove congestion, and restore a youthful appearance. -->
                                <div class="readexpand">
                                    <a href="{{ url('/skin') }}" class="commonanchor">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="expan-imagewrap">
                            <img src="{{ url('website') }}/img/skin-expand.png" alt="SKIN TREATMENTS">

                        </div>
                        <div class="expandservicesextandwrapper">
                            <div class="expandservicesheading">SKIN TREATMENTS</div>
                            <div class="expendservices">
                                <div class="expandserviceswrap">
                                    <div class="expandservicesclickwrap">
                                        <a href="{{ url('/acne') }}">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/acne-sm.jpg" alt="Acne">
                                            </div>
                                            <p>Acne</p>
                                        </a>
                                    </div>
                                    <div class="expandservicesclickwrap">
                                        <a href="{{ url('/pigmentation') }}">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/pigmentation-sm.jpg" alt="Pigmentation">
                                            </div>
                                            <p>Pigmentation</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="expandserviceswrap">
                                    <div class="expandservicesclickwrap">
                                        <a href="{{ url('/skin-growth') }}">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/Skin-growth-sm.jpg" alt="Skin Growth">
                                            </div>
                                            <p>Skin Growth</p>
                                        </a>
                                    </div>
                                    <div class="expandservicesclickwrap">
                                        <a href="{{ url('/marks-and-spots') }}">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/Marks-&-spots-sm.jpg" alt="Marks & Spots">
                                            </div>
                                            <p>Marks & Spots</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="expendservicesviewmore">
                                <a href="#" class="commonanchor">View more</a>
                            </div> -->
                        </div>
                        <div class="godestination">
                            <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>

                    <div class="box-expand" onmouseover="expandColumn(this)" onmouseout="collapseColumns()">
                        <div class="boxcontentheading notofamily commonheadingfontsize smallwidthheading">AESTHETIC
                        </div>
                        <div class="box-content">
                            <div class="boxcontentheading notofamily commonheadingfontsize">AESTHETIC</div>
                            <div class="box-content-inner">
                                <p>Soul Derma offers the best aesthetic treatments, such as injectables, dermal fillers, thread lifts, laser hair removal, non-surgical facelifts and skin tightening procedures.</p>
                                <p class="displayonhover"> These services help enhance facial features, reduce premature aging lines like fine lines and wrinkles, and provide a more youthful and vibrant look. </p>
                                <!-- The clinic ensures that each treatment is tailored to individual needs, providing effective and long-lasting results. -->
                                <div class="readexpand">
                                    <a href="{{ url('/aesthetics') }}" class="commonanchor">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="expan-imagewrap">
                            <img src="{{ url('website') }}/img/aesthetic-expand.png" alt="AESTHETIC TREATMENTS">

                        </div>
                        <div class="expandservicesextandwrapper">
                            <div class="expandservicesheading">AESTHETIC TREATMENTS</div>
                            <div class="expendservices">
                                <div class="expandserviceswrap">
                                    <div class="expandservicesclickwrap">
                                        <a href="{{ url('/facials') }}">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/facial-sm.jpg" alt="Facials">
                                            </div>
                                            <p>Facials</p>
                                        </a>
                                    </div>
                                    <div class="expandservicesclickwrap">
                                        <a href="{{ url('/facial-rejuvenation') }}">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/facial-rejuvenation-sm.jpg" alt="Facial Rejuvenation">
                                            </div>
                                            <p>Facial Rejuvenation</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="expandserviceswrap">
                                    <div class="expandservicesclickwrap">
                                        <a href="{{ url('/anti-aging') }}">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/Anti-aging-sm.jpg" alt="Anti Aging">
                                            </div>
                                            <p>Anti Aging</p>
                                        </a>
                                    </div>
                                    <!-- <div class="expandservicesclickwrap">
                                        <a href="#">
                                            <div class="expendservicesimage">
                                                <img src="{{ url('website') }}/img/dark-circle.jpg">
                                            </div>
                                            <p>Dark circle</p>
                                        </a>
                                    </div> -->
                                </div>
                            </div>
                            <!-- <div class="expendservicesviewmore">
                                <a href="#" class="commonanchor">View more</a>
                            </div> -->
                        </div>
                        <div class="godestination">
                            <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="imageexpandmobilewrap" class="for-mobile">
    <div class=" container-fluid">
        <div class="row manage-mbwidthparent">
            <div class="col-12 manage-mbwidth">
                <div class="mbswrap">
                    <div class="mbsheading notofamily ">HAIR</div>
                    <div class="mbscontent">
                        <p>Soul Derma offers innovative solutions for hair loss, hair fall, baldness, thinning hair, and scalp conditions.</p>
                    </div>
                    <div class="expan-imagewrap">
                        <img src="{{ url('website') }}/img/hair-expand.png" alt="HAIR">

                    </div>
                    <div class="godestination">
                        <a href="{{ url('/hair') }}"><i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-12 manage-mbwidth">
                <div class="mbswrap">
                    <div class="mbsheading notofamily">SKIN</div>
                    <div class="mbscontent">
                        <p>Soul Derma offers the most advanced and personalized skincare solutions for a wide range of dermatological concerns, focusing on both general and cosmetic issues.</p>
                    </div>
                    <div class="expan-imagewrap">
                        <img src="{{ url('website') }}/img/skin-expand.png" alt="SKIN">

                    </div>
                    <div class="godestination">
                        <a href="{{ url('/skin') }}"><i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>


            <div class="col-12 manage-mbwidth">
                <div class="mbswrap">
                    <div class="mbsheading notofamily">AESTHETIC</div>
                    <div class="mbscontent">
                        <p>Soul Derma offers the best aesthetic treatments, such as injectables, dermal fillers, thread lifts, laser hair removal, non-surgical facelifts, non-surgical vag-inal tightening, and skin tightening procedures.</p>
                    </div>
                    <div class="expan-imagewrap">
                        <img src="{{ url('website') }}/img/aesthetic-expand.png" alt="AESTHETIC">

                    </div>
                    <div class="godestination">
                        <a href="{{ url('/aesthetics') }}"><i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="combindtestimonialswrapper">
    <div class="container">
        <div class="testimonialheading notofamily commonheadingfontsize">LET OUR PATIENTS TALK</div>
        <div class="row">
            <!-- start of testimonials for desktop -->
            <div class="col-12 col-md-6 col-lg-4 col-lg-4 managewrittenspacing">
                <div class="written-contentwrap">
                    <div class="sorcewrap">
                        <div class="nameandstar">
                            <div class="candidate-name">KHUSHBOO MAAN</div>
                            <div class="stars-wrap">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                        <div class="source-image">
                            <img src="{{ url('website') }}/img/googleicon.png" class="img-fluid" width="65" height="22" alt="google">
                        </div>
                    </div>
                    <div class="testi-content-wrap">
                        <p>Dr. Anika Goel is ready good in her work and sweet to her patients too, even the staff working in Soul Derma Clinic is good.</p>
                        <div class="warning-content">
                            <span>✱ Opinions/Results may vary from person to person.</span>
                        </div>
                    </div>
                    <div class="qoutesimage">
                        <img src="{{ url('website') }}/img/qutosimage.png" alt="quots">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-lg-4 managewrittenspacing">
                <div class="written-contentwrap">
                    <div class="sorcewrap">
                        <div class="nameandstar">
                            <div class="candidate-name">Sahil Bhardwaj</div>
                            <div class="stars-wrap">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                        <div class="source-image">
                            <img src="{{ url('website') }}/img/googleicon.png" class="img-fluid" width="65" height="22" alt="google">
                        </div>
                    </div>
                    <div class="testi-content-wrap">
                        <p>Dr. Anika Goel’s clinic has a team of extremely dexterous professionals who cater to the needs of your skin meticulously. I was quite satisfied with my black head removal service recently and have been a constant client/ patient at ‘Soul Derma Clinic’ since the past couple of years for various treatments/ skin care routines.<br>
                            I have even availed their various facial services, underarm laser hair removal and laser corn removal from the foot. I highly recommend this clinic.<br>
                            Dr. Anika is very calm and humble with her approach, which makes her patients feel at ease to discuss their skin problems and get productive advice for its betterment.</p>
                        <div class="warning-content">
                            <span>✱ Opinions/Results may vary from person to person.</span>
                        </div>
                    </div>
                    <div class="qoutesimage">
                        <img src="{{ url('website') }}/img/qutosimage.png" alt="quots">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-lg-4 managewrittenspacing ipadviewnone">
                <div class="written-contentwrap">
                    <div class="sorcewrap">
                        <div class="nameandstar">
                            <div class="candidate-name">Anchal Jaiswal</div>
                            <div class="stars-wrap">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                        <div class="source-image">
                            <img src="{{ url('website') }}/img/googleicon.png" class="img-fluid" width="65" height="22" alt="google">
                        </div>
                    </div>
                    <div class="testi-content-wrap">
                        <p>If the words you spoke appeared on your skin would you still. Be beautiful I had a great experience to visit her clinic dr anika goel.she is such a sweet person</p>
                        <div class="warning-content">
                            <span>✱ Opinions/Results may vary from person to person.</span>
                        </div>
                    </div>
                    <div class="qoutesimage">
                        <img src="{{ url('website') }}/img/qutosimage.png" alt="quots">
                    </div>
                </div>
            </div>
            <!-- <div class="viewalltestimonial">
                <a href="{{ url('written-testimonials') }}" class="commonanchor">Read More</a>
            </div> -->

            <!-- START OF TESTIMONIALCOLUMN Mobile Screen -->
            <div class="col-12 d-md-none d-lg-none d-xl-none d-block disp_up">
                <div class="wrapper_testi">
                    <!-- START OF TESTIMONIALCOLUMN -->
                    <div class="written-contentwrap">
                        <div class="sorcewrap">
                            <div class="nameandstar">
                                <div class="candidate-name">KHUSHBOO MAAN</div>
                                <div class="stars-wrap">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                            <div class="source-image">
                                <img src="{{ url('website') }}/img/googleicon.png" class="img-fluid" width="65" height="22" alt="google">
                            </div>
                        </div>
                        <div class="testi-content-wrap">
                            <p>Dr. Anika Goel is ready good in her work and sweet to her patients too, even the staff working in Soul Derma Clinic is good.</p>
                            <div class="warning-content">
                                <span>✱ Opinions/Results may vary from person to person.</span>
                            </div>
                        </div>
                        <div class="qoutesimage">
                            <img src="{{ url('website') }}/img/qutosimage.png" alt="quots">
                        </div>
                    </div>
                    <!-- // End OF TESTIMONIALCOLUMN -->

                    <!-- START OF TESTIMONIALCOLUMN -->
                    <div class="written-contentwrap">
                        <div class="sorcewrap">
                            <div class="nameandstar">
                                <div class="candidate-name">Sahil Bhardwaj</div>
                                <div class="stars-wrap">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                            <div class="source-image">
                                <img src="{{ url('website') }}/img/googleicon.png" class="img-fluid" width="65" height="22" alt="google">
                            </div>
                        </div>
                        <div class="testi-content-wrap">
                            <p>Dr. Anika Goel’s clinic has a team of extremely dexterous professionals who cater to the needs of your skin meticulously. I was quite satisfied with my black head removal service recently and have been a constant client/ patient at ‘Soul Derma Clinic’ since the past couple of years for various treatments/ skin care routines.<br>
                                I have even availed their various facial services, underarm laser hair removal and laser corn removal from the foot. I highly recommend this clinic.<br>
                                Dr. Anika is very calm and humble with her approach, which makes her patients feel at ease to discuss their skin problems and get productive advice for its betterment.</p>
                            <div class="warning-content">
                                <span>✱ Opinions/Results may vary from person to person.</span>
                            </div>
                        </div>
                        <div class="qoutesimage">
                            <img src="{{ url('website') }}/img/qutosimage.png" alt="quots">
                        </div>
                    </div>
                    <!-- // End OF TESTIMONIALCOLUMN -->

                    <!-- START OF TESTIMONIALCOLUMN -->
                    <div class="written-contentwrap">
                        <div class="sorcewrap">
                            <div class="nameandstar">
                                <div class="candidate-name">Anchal Jaiswal</div>
                                <div class="stars-wrap">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                            <div class="source-image">
                                <img src="{{ url('website') }}/img/googleicon.png" class="img-fluid" width="65" height="22" alt="google">
                            </div>
                        </div>
                        <div class="testi-content-wrap">
                            <p>If the words you spoke appeared on your skin would you still. Be beautiful I had a great experience to visit her clinic dr anika goel.she is such a sweet person</p>
                            <div class="warning-content">
                                <span>✱ Opinions/Results may vary from person to person.</span>
                            </div>
                        </div>
                        <div class="qoutesimage">
                            <img src="{{ url('website') }}/img/qutosimage.png" alt="quots">
                        </div>
                    </div>
                    <!-- // End OF TESTIMONIALCOLUMN -->
                </div>
                <!-- // End OF Mobile Screen -->
            </div>
            <div class="viewalltestimonial">
                <a href="{{ url('written-testimonials') }}" class="commonanchor">Read More</a>
            </div>
        </div>
    </div>
</section>
<!-- <section id="combindtestimonialswrapper">
    <div class="container">
        <div class="testimonialheading notofamily commonheadingfontsize">LET OUR PATIENTS TALK</div>
        <div class="row">
            <div class="col-12 col-md-12 cutwidth">
                <div class="second-owl owl-carousel owl-theme">
                    <div class="item">
                        <div class="combindtestimonialsmetterwrap">
                            <div class="vidosidetestimonialwrap">
                                <div class="vidosidetestimonialimage">
                                    <div class="video-imagetestimona">
                                        <img src="{{ url('website') }}/img/videotestimonial.jpg">
                                    </div>
                                    <div class="playbutton vid"> -->
<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary video-btn" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/Qxd8hiwbrFo" data-bs-target="#myModal"> <span class="fa fa-play"></span>
                                        </button> -->
<!-- </div>
                                    <div class="videorealresult">
                                        <a href="#" class="d-block"><img src="{{ url('website') }}/img/onvideoreal.jpg"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="combindwrittentestimonial">
                                <div class="combindwrittentestimonialheading notofamily">HAIR TRANSPLANT</div>
                                <div class="combindwrittencontent">
                                    <p>I am thoroughly impressed by the Dr. Anika and her team at Soul Derma. She's
                                        been nothing but patient and for starters helped me change my attitude
                                        towards my skin. She's also helped me get a basic skin routine in place.</p>
                                </div>
                                <div class="combideclientname">
                                    <ul>
                                        <li><img src="{{ url('website') }}/img/user.png"></li>
                                        <li>Banwari Agnihotri</li>
                                    </ul>
                                </div>
                                <div class="quotsimage">
                                    <img src="{{ url('website') }}/img/qutosimage.png">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="combindtestimonialsmetterwrap">
                            <div class="vidosidetestimonialwrap">
                                <div class="vidosidetestimonialimage">
                                    <div class="video-imagetestimona">
                                        <img src="{{ url('website') }}/img/videotestimonial.jpg">
                                    </div>
                                    <div class="playbutton vid"> -->
<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary video-btn" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/Qxd8hiwbrFo" data-bs-target="#myModal"> <span class="fa fa-play"></span>
                                        </button>
                                    </div>
                                    <div class="videorealresult">
                                        <a href="#" class="d-block"><img src="{{ url('website') }}/img/onvideoreal.jpg"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="combindwrittentestimonial">
                                <div class="combindwrittentestimonialheading notofamily">HAIR TRANSPLANT</div>
                                <div class="combindwrittencontent">
                                    <p>I am thoroughly impressed by the Dr. Anika and her team at Soul Derma. She's
                                        been nothing but patient and for starters helped me change my attitude
                                        towards my skin. She's also helped me get a basic skin routine in place.</p>
                                </div>
                                <div class="combideclientname">
                                    <ul>
                                        <li><img src="{{ url('website') }}/img/user.png"></li>
                                        <li>Banwari Agnihotri</li>
                                    </ul>
                                </div>
                                <div class="quotsimage">
                                    <img src="{{ url('website') }}/img/qutosimage.png">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="combindtestimonialsmetterwrap">
                            <div class="vidosidetestimonialwrap">
                                <div class="vidosidetestimonialimage">
                                    <div class="video-imagetestimona">
                                        <img src="{{ url('website') }}/img/videotestimonial.jpg">
                                    </div>
                                    <div class="playbutton vid"> -->
<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary video-btn" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/Qxd8hiwbrFo" data-bs-target="#myModal"> <span class="fa fa-play"></span>
                                        </button>
                                    </div>
                                    <div class="videorealresult">
                                        <a href="#" class="d-block"><img src="{{ url('website') }}/img/onvideoreal.jpg"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="combindwrittentestimonial">
                                <div class="combindwrittentestimonialheading notofamily">HAIR TRANSPLANT</div>
                                <div class="combindwrittencontent">
                                    <p>I am thoroughly impressed by the Dr. Anika and her team at Soul Derma. She's
                                        been nothing but patient and for starters helped me change my attitude
                                        towards my skin. She's also helped me get a basic skin routine in place.</p>
                                </div>
                                <div class="combideclientname">
                                    <ul>
                                        <li><img src="{{ url('website') }}/img/user.png"></li>
                                        <li>Banwari Agnihotri</li>
                                    </ul>
                                </div>
                                <div class="quotsimage">
                                    <img src="{{ url('website') }}/img/qutosimage.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="viewalltestimonial">
            <a href="#" class="commonanchor">view all</a>
        </div>
    </div>
</section> -->


<section id="smallbanner" class="mob-bgimg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="smallbannerheadingbg">UPCOMING PRODUCTS</div>
                <div class="smallbannerheading notofamily">UPCOMING PRODUCTS</div>
                <div class="smallbannercontent">
                    <p>Give your skin the love and attention it deserves with Dermatologist-approved products.</p>
                    <!-- <p>ELIT MAECENAS ACCUMSAN VELIT IPSUM.</p> -->

                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section id="homeblogwrapper">
    <div class="container">
        <div class="homeblogheading notofamily commonheadingfontsize">LATEST BLOGS</div>
        <div class="row manage-mbwidthparent">
            <div class="col-12 col-md-4 manage-mbwidth">
                <div class="homeblogcontentwrapper">
                    <div class="homeblogimagewrap">
                        <div class="homeblogimage">
                            <img src="{{ url('website') }}/img/homeblog-1.jpg" class="w-100">
                        </div>
                        <div class="blogcalender">
                            <p>05 <span>JAN</span></p>
                        </div>
                    </div>
                    <div class="homeblogcontenttext">
                        <div class="homeblogcontentheading">LOREM IPSUM DOLOR SIT</div>
                        <div class="homeblogtextp">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras iaculis enim eget urna
                                lobortis laoreet.</p>
                        </div>
                        <div class="homblogview">
                            <a href="#" class="commonanchor">read more</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-12 col-md-4 manage-mbwidth">
                <div class="homeblogcontentwrapper">
                    <div class="homeblogimagewrap">
                        <div class="homeblogimage">
                            <img src="{{ url('website') }}/img/homeblog-2.jpg" class="w-100">
                        </div>
                        <div class="blogcalender">
                            <p>05 <span>JAN</span></p>
                        </div>
                    </div>
                    <div class="homeblogcontenttext">
                        <div class="homeblogcontentheading">LOREM IPSUM DOLOR SIT</div>
                        <div class="homeblogtextp">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras iaculis enim eget urna
                                lobortis laoreet.</p>
                        </div>
                        <div class="homblogview">
                            <a href="#" class="commonanchor">read more</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-12 col-md-4 manage-mbwidth">
                <div class="homeblogcontentwrapper">
                    <div class="homeblogimagewrap">
                        <div class="homeblogimage">
                            <img src="{{ url('website') }}/img/homeblog-3.jpg" class="w-100">
                        </div>
                        <div class="blogcalender">
                            <p>05 <span>JAN</span></p>
                        </div>
                    </div>
                    <div class="homeblogcontenttext">
                        <div class="homeblogcontentheading">LOREM IPSUM DOLOR SIT</div>
                        <div class="homeblogtextp">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras iaculis enim eget urna
                                lobortis laoreet.</p>
                        </div>
                        <div class="homblogview">
                            <a href="#" class="commonanchor">read more</a>
                        </div>
                    </div>
                </div>

            </div>

        </div> 
            <div class="homeblogviewmore for-desktop">
                <a href="#" class="commonanchor">view more</a>
            </div>
    </div>
    </div>
</section> -->

<section id="requestpopup">
    <!-- Modal -->
    <div class="modal fade" id="requestPop" tabindex="-1" aria-labelledby="requestPopLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="requestPopLabel">Request a Callback</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body popfrom_homepage">
                    @include('website.include.requestcallback')
                </div>
            </div>
        </div>
    </div>
</section>
@include('website.include.requestanappointment')

@include('website.include.footer')