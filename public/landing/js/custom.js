// header fixed
$(document).ready(function () {
  $(window).scroll(function () {
    var sticky = $('header'),
      scroll = $(window).scrollTop();
    if (scroll >= 50) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
  });
});
// Mobile Menu
(function ($) {
  "use strict";
  if ($('.main-nav').length) {
    var $mobile_nav = $('.main-nav').clone().prop({
      class: 'mobile-nav d-lg-none'
    });
    $('body').append($mobile_nav);
    $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="fa fa-bars"></i></button>');
    $('body').append('<div class="mobile-nav-overly"></div>');

    $(document).on('click', '.mobile-nav-toggle', function (e) {
      $('body').toggleClass('mobile-nav-active');
      $('.mobile-nav-toggle i').toggleClass('fa-times fa-bars');
      $('.mobile-nav-overly').toggle();
    });
    $(document).on('click', '.mobile-nav .drop-down > a', function (e) {
      e.preventDefault();
      $(this).next().slideToggle(300);
      $(this).parent().toggleClass('active');
    });
    $(document).click(function (e) {
      var container = $(".mobile-nav, .mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('.mobile-nav-overly').fadeOut();
        }
      }
    });
  } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
    $(".mobile-nav, .mobile-nav-toggle").hide();
  }
})(jQuery);
// Menu 
$(document).ready(function () {
  $(window).resize(function () {
    if ($(window).width() >= 980) {
      $(".navbar .dropdown-toggle").hover(function () {
        $(this).parent().toggleClass("show");
        $(this).parent().find(".dropdown-menu").toggleClass("show");
      });
      $(".navbar .dropdown-menu").mouseleave(function () {
        $(this).removeClass("show");
      });
    }
  });
});

/*---- scroll to top----*/
$(document).ready(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $('#scroll').fadeIn();
    } else {
      $('#scroll').fadeOut();
    }
  });
  $('#scroll').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  });
});

// Start Videos section
$(document).ready(function () {
  var $videoSrc;
  $('.video-btn').click(function () {
    $videoSrc = $(this).data("src");
  });
  console.log($videoSrc);
  $('#myModal').on('shown.bs.modal', function (e) {
    $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
  })
  $('#myModal').on('hide.bs.modal', function (e) {
    $("#video").attr('src', $videoSrc);
  })
});
// End Videos section

//  Faq section script 

$(document).ready(function () {
  $(".accordions").click(function () {
    // $('.accordion').removeClass('active');
    $(this).toggleClass("actives");
    $(this).next().slideToggle(200);
    if ($(this).children("i").hasClass("fa-angle-down")) {
      $(this).children("i").removeClass("fa-angle-down");
      $(this).children("i").addClass("fa-angle-up");
    } else {
      $(this).children("i").removeClass("fa-angle-up");
      $(this).children("i").addClass("fa-angle-down");
    }
  });
});


function addService(service) {
  const $select = document.querySelector('#mobBody #mySelect');
  $select.value = service;
}