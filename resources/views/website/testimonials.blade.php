@include('website.include.header')

    <link rel="stylesheet" href="{{ url('website') }}/css/written-testimonials.css">


    <section id="topcommonbanner" class="mob_view">
        <div class="commonbannerclinic">
            <img src="{{ url('website') }}/img/about-clinic/about-clinic.jpg" class="w-100">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="headingandbredcrumb">
                        <div class="commonheading notofamily commonheadingfontsize">Written Testimonials</div>
                        <div class="breadcrumb commonbredcurmb">
                            <ul>
                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li><a>Written Testimonials</a></li>
                            </ul>
                        </div>
                        <div class="commonrequestform">
                            @include('website.include.requestcallback')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="writtentestimonialwrap">
        <div class="container">
            <div class="row">
                @foreach($testimonial as $testi)
                <div class="col-12 col-lg-4 col-md-6 managewrittenspacing">
                    <div class="written-contentwrap">
                        <div class="sorcewrap">
                            <div class="nameandstar">
                                <div class="candidate-name">{{ $testi->name }}</div>
                                <div class="stars-wrap">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                            <div class="source-image">
                                @if($testi->source == 'google')
                                    <img src="{{ url('website') }}/img/googleicon.png" class="img-fluid" width="65" height="22" alt="google" />
                                    @elseif($testi->source == 'practo')
                                    <img src="{{ url('website') }}/ima/practo.png" class="img-fluid" width="66" height="18" alt="practo" />
                                    @endif
                            </div>
                        </div>
                        <div class="content-wrap">
                            {!! $testi->description !!}
                            <div class="warning-content">
                                <span>✱ Opinions/Results may vary from person to person.</span>
                            </div>
                        </div>
                        <div class="qoutesimage">
                            <img src="{{ url('website') }}/img/qutosimage.png" alt="quots">
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="pageination">
                {{ $testimonial->links() }}
                </div>
            </div>
        </div>
    </section>

  @include('website.include.requestanappointment')
  @include('website.include.footer')
