@include('admin.include.header')
<div class="New_login">
  <div class=" main_form_div">
    <div class="hospital_img">
      <img src="https://i.pinimg.com/originals/57/1f/d2/571fd28ce80b7902bfc61ca7e6119fe9.jpg" alt="Hospital Image" class="img-fluid ">
    </div>
    <div class="Form_container">
      <div class="left_form">
        <div class="login_logo">
          <img class="f-logo" src="{{ url('/admin/images/logo.png') }}" alt="logo">
        </div>
        <form class="form_fields change_pswd">
          <input type="hidden" name="email" id="email" value="{{ request()->get('requestid') }}">
          <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
          <input type="password" class="ggg" autocomplete="off" id="password" onkeyup="validatePassword()" name="password" placeholder="New Password">
          <span id="togglePassword" onclick="togglePasswordVisibility()">
            <i id="toggleIcon" class="fa fa-eye"></i>
          </span>
          <div id="password_valid" class="error"></div>
          <input type="password" class="ggg" autocomplete="off" id="cnfpassword" onkeyup="cnfvalidatePassword()" name="cnfpassword" placeholder="Confirm Password">
          <span id="togglePassword1" onclick="togglePasswordVisibility1()">
            <i id="toggleIcon1" class="fa fa-eye"></i>
          </span>
          <div id="cnf_password_valid" class="error"></div>
          <div class="clearfix"></div>
          <div class="submit_btn">
            <button id="submit" type="button" class="btn btn-primary" onclick="checkvalidation()">Submit</button>
          </div>
        </form>
        <div id="validuser"></div>
        <div class="w3layouts-main" id="messagecnf" style="display:none;">
          <div id="msg"></div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- <div class="log-w3">
  <div class="w3layouts-main" id="forgot">
    <div class="logo-block login_logo" style="text-align: center;">
      <img class="f-logo" src="{{ url('/admin/images/Arete_logo.png') }}" alt="logo">
    </div>
    <form>
      <input type="hidden" name="email" id="email" value="{{ request()->get('requestid') }}">
      <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
      <input type="password" class="ggg" autocomplete="off" id="password" onkeyup="validatePassword()" name="password" placeholder="New Password">
      <span id="togglePassword" onclick="togglePasswordVisibility()">
        <i id="toggleIcon" class="fa fa-eye"></i>
      </span>
      <div id="password_valid" class="error"></div>
      <input type="password" class="ggg" autocomplete="off" id="cnfpassword" onkeyup="cnfvalidatePassword()" name="cnfpassword" placeholder="Confirm Password">
      <span id="togglePassword1" onclick="togglePasswordVisibility1()">
        <i id="toggleIcon1" class="fa fa-eye"></i>
      </span>
      <div id="cnf_password_valid" class="error"></div>
      <div class="clearfix"></div>
      <div class="submit_btn">
        <button id="submit" type="button" class="btn btn-primary" onclick="checkvalidation()">Submit</button>
      </div>
    </form>
    <div id="validuser"></div>
  </div>
  <div class="w3layouts-main" id="messagecnf" style="display:none;">
    <div id="msg"></div>
  </div>
</div> -->

<style>
  #togglePassword {
    cursor: pointer;
    color: #000;
  }

  #togglePassword1 {
    cursor: pointer;
    color: #000;
  }
</style>

<script>
  // For Password Visiblity
  function togglePasswordVisibility() {
    var passwordInput = document.getElementById("password");
    var toggleIcon = document.getElementById("toggleIcon");

    if (passwordInput.type === "password") {
      passwordInput.type = "text";
      toggleIcon.classList.remove("fa-eye");
      toggleIcon.classList.add("fa-eye-slash");
    } else {
      passwordInput.type = "password";
      toggleIcon.classList.remove("fa-eye-slash");
      toggleIcon.classList.add("fa-eye");
    }
  }

  // For CNFPassword Visiblity
  function togglePasswordVisibility1() {
    var passwordInput = document.getElementById("cnfpassword");
    var toggleIcon = document.getElementById("toggleIcon1");

    if (passwordInput.type === "password") {
      passwordInput.type = "text";
      toggleIcon.classList.remove("fa-eye");
      toggleIcon.classList.add("fa-eye-slash");
    } else {
      passwordInput.type = "password";
      toggleIcon.classList.remove("fa-eye-slash");
      toggleIcon.classList.add("fa-eye");
    }
  }

  // Login Validation
  function checkvalidation(getid) {
    var email = document.getElementById("email").value
    var password = document.getElementById("password").value
    var _token = document.getElementById("token").value
    var checkpassword = false;

    var lowerCaseLetters = /[a-z]/;
    var numbers = /[0-9]/;
    var upperCaseLetters = /[A-Z]/;
    var specialchar = /[!@#$%^&*()\-=_+[\]{}|;:<>/?]/;
    if (!lowerCaseLetters.test(password)) {
      ldocument.getElementById("password_valid").innerHTML = 'The password must contain at least one lowercase letter.';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
    }
    // Validate capital letters
    else if (!upperCaseLetters.test(password)) {
      document.getElementById("password_valid").innerHTML = 'The password must contain at least one uppercase letter.';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
    }

    // Validate numbers
    else if (!numbers.test(password)) {
      document.getElementById("password_valid").innerHTML = 'The password must contain at least one digit.';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
    } else if (!specialchar.test(password)) {
      document.getElementById("password_valid").innerHTML = 'The password must contain at least one special character';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
    }

    // Validate length
    else if (!password.length >= 8) {
      document.getElementById("password_valid").innerHTML = 'The password must be at least 8 characters long.';
      document.getElementById("password").classList.add("errorsection");
      document.getElementById("password").classList.remove("validsection");
    } else {
      var checkpassword = true;
      document.getElementById("password_valid").innerHTML = ' ';
      document.getElementById("password").classList.add("validsection");
      document.getElementById("password").classList.remove("errorsection");
    }

    if (checkpassword == true) {
      var xhr = new XMLHttpRequest();

      // set up the request
      xhr.open("POST", "{{ url('admin/submitchangepassword') }}");
      xhr.setRequestHeader("Content-Type", "application/json");

      // handle the response
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            document.getElementById("forgot").style.display = "none";
            document.getElementById("messagecnf").style.display = "block";
            document.getElementById("msg").innerHTML = "Password updated successfully!";
          } else {
            if (xhr.status == 400) {
              document.getElementById("submit").disabled = false;
              document.getElementById("submit").innerText = "Submit";
              document.getElementById("validuser").innerHTML = 'Email Not exist in our database ';
            }
            if (xhr.status == 404) {
              document.getElementById("submit").disabled = false;
              document.getElementById("submit").innerText = "Submit";
              document.getElementById("validuser").innerHTML = 'Email Not exist in our database ';
            }
            document.getElementById("submit").disabled = false;
          }
        }
      };
      var data = {
        'email': email,
        'password': password,
        '_token': _token,
      };
      // send the request
      document.getElementById("submit").disabled = true;
      document.getElementById("submit").innerText = "Submiting..";
      xhr.send(JSON.stringify(data));
      // }
    }
  }

  // for contact and appointment
  function validateEmail() {
    var emailInput = document.getElementById('email');
    var email = emailInput.value;
    var errorSpan = document.getElementById('showemailmsg');

    // Regular expression pattern for email validation
    var pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (pattern.test(email)) {
      errorSpan.textContent = ""; // Clear any existing error message
      // Do something with the valid email, such as submitting the form
    } else {
      errorSpan.textContent = "Please enter a valid email address";
    }
  }
  // For Contact and appointment function validate

  function validatePassword() {
    var passInput = document.getElementById('password');
    var password = passInput.value;
    var errorSpan = document.getElementById('password_valid');
    // Check if the password is at least 8 characters long
    if (password.length < 8) {
      errorSpan.textContent = "The password must be at least 8 characters long";
    } else

      // Check if the password contains at least one lowercase letter
      if (!/[a-z]/.test(password)) {
        errorSpan.textContent = "The password must contain at least one lowercase letter";
      } else

        // Check if the password contains at least one uppercase letter
        if (!/[A-Z]/.test(password)) {
          errorSpan.textContent = "The password must contain at least one uppercase letter";
        } else

          // Check if the password contains at least one special character
          if (!/[!@#$%^&*()\-=_+[\]{}|;':"<>/?]/.test(password)) {
            errorSpan.textContent = "The password must contain at least one special character";
          } else {
            errorSpan.textContent = "";
          }
  }

  function cnfvalidatePassword() {
    var passInput = document.getElementById('password');
    var password = passInput.value;
    var cnfpassInput = document.getElementById('cnfpassword');
    var cnfpassword = cnfpassInput.value;
    var errorSpan = document.getElementById('cnf_password_valid');
    if (password !== cnfpassword) {
      errorSpan.textContent = "Passwords do not match.";
    } else {
      errorSpan.textContent = "";
    }
  }
</script>

@include('admin.include.footer')
