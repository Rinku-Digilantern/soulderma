<?php

namespace App\Http\Controllers;

use App\Mail\OwnerMail;
use App\Mail\UserMail;
use App\Models\AppointmentlpModel;
use App\Models\AppointmentModel;
use App\Models\CallbackModel;
use App\Models\CaptchaModel;
use App\Models\RequestAcallbackModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class LandingController extends Controller
{
    public function acne()
    {
        return view('landing.acne');
    }

    public function thankYou()
    {
        return view('landing.thank-you');
    }
    
    public function doctor()
    {
        return view('landing.doctor');
    }

    public function wedding()
    {
        return view('landing.wedding');
    }

    public function lhr()
    {
        return view('landing.lhr');
    }

    public function appointmentlp(Request $request){
       
        $validator = Validator::make($request->input(), [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'date' => 'required',
        ]);

        //Will get redirected if validator is not matched
        if ($validator->fails()) {
            return Redirect::back()->withErrors($request->input())->withInput($request->input());
        }
        //new data will be create in menu table
        AppointmentModel::create($request->input());

        $url = url('/');
        $backendurl = env('BACK_FRONT');

        $detailsowner = [
            'name' => $request->input('name'),
            'orgname' => 'SoulDemra',
            'emailsubject' => 'New Book Appointment',
            'clientphone' => $request->input('phone'),
            'clientemail' => $request->input('email'),
            'date' => $request->input('date'),
            'page' => 'appointment-owner',
            'message' => $request->input('message'),
        ];

        $detailsuser = [
            'name' => $request->input('name'),
            'heading' => 'Thank you for booking an appointment with us',
            'orgname' => 'SoulDemra',
            'primarycountrycode' => '91',
            'primaryphone' => '8595941529',
            'secondaryphone' => '',
            'secondarycountrycode' => '91',
            'email' => 'info@souldermaclinic.com',
            'url' => $url,
            'page' => 'appointment-user',
            'logo' => 'https://www.souldermaclinic.com/website/img/logo.png',
        ];

        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $message = $request->input('message');
        $location = '';
        $date = $request->input('date');
        $refer = $request->input('referer');
        $source = $request->input('source');


        Mail::to($request->input('email'))->send(new UserMail($detailsuser));
        Mail::to('testing.digilantern@gmail.com')->send(new OwnerMail($detailsowner));
                
    }

    public function callbacklp(Request $request){
        $validator = Validator::make($request->input(), [
            'name' => 'required',
            'phone ' => 'required',
            'captcha' => 'required',
        ]);
    
        if ($validator->fails()) {
            return response()->json(['success' => false, 'data' => 'validerror'], 400);
        }
    
        $cap = $request->input('captcha');
        $capuid = $request->input('uncode');
        $cp = CaptchaModel::where('capchacode', $cap)->where('uid', $capuid);
    
        if ($cp->count() < 1) {
            return response()->json(['success' => false, 'data' => $cp->count()], 400);
        }
    
        try {
            RequestAcallbackModel::create($request->input());
    
            $url = url('/');
            $backendurl = env('BACK_FRONT');
    
            $detailsowner = [
                'name' => $request->input('name'),
                'emailsubject' => 'New Request Call Back',
                'orgname' => 'SoulDemra',
                'clientphone' => $request->input('phone'),
                'page' => 'request-owner',
                //'message' => $request->input('message'),
            ];
            
            // Mail::to('info@souldermaclinic.com')->send(new OwnerMail($detailsowner));
            Mail::to('testing.digilantern@gmail.com')->send(new OwnerMail($detailsowner));
    
            return response()->json(['success' => true], 200);
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            return response()->json(['success' => false, 'data' => $errorInfo], 400);
        }
    }

    public function callbacklist()
    {
        $select_table = ['id', 'name', 'phone', 'service', 'request_url', 'created_at'];
        $data = RequestAcallbackModel::select($select_table)->get();
        return view('admin.landingpage.list-callback', ['view' => $data]);
    }

    public function landingappointmentlist()
    {
        $select_table = ['id', 'name','phone', 'email','message','request_url', 'service', 'created_at'];
        $data = AppointmentModel::select($select_table)->get();
        return view('admin.landingpage.list-landing-appointment', ['view' => $data]);
    }
}