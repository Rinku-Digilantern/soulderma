<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- bs icon -->
    <link rel="icon" href="{{url('/website/img/favicon.ico')}}" type="image/x-icon" />
    <!-- Font Awesome -->
    <link rel="preload" as="style"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        onload="this.rel='stylesheet'">
    <link rel="preload" as="font" type="font/woff2" crossorigin
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Noto+Serif:wght@400;700&family=Poppins:wght@300;400;500&display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="css/bt.css" />
    <link rel="stylesheet" href="css/owl.carousel.min.css" />
    <link rel="stylesheet" href="css/owl.theme.default.min.css" />
    <link rel="stylesheet" href="css/date-picker.css" rel="preload" type="text/css" as="style"
        onload="this.rel='stylesheet'" />
    <link rel="stylesheet" href="css/lightbox.css">
    <link rel="stylesheet" href="css/sidebar-menu.css">
    <link rel="stylesheet" href="css/common.css">
    <!-- <link rel="preload" as="image" fetchpriority="high" type="image/jpg" href="{{url('/website/img/home-topbanner.webp')}}" /> -->
    <link rel="preload" as="image" fetchpriority="high" type="image/jpg" href="img/home-banner.webp" />
    <title>Soul Derma</title>
 

</head>


    <link rel="stylesheet" href="css/aboutdr.css">
    <link rel="stylesheet" href="css/sliderfive.css">



    <body>
        <main id="main" class="content-wrap on-canvas">
            <div class="container mod-home_services_slider">
                <div class="row">
                    <div class="wrapper">
                        <div class="item left">
                            <div class="image">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                    data-lazyload="img"
                                    data-lazyload-src="https://www.datocms-assets.com/31333/1595354524-homepage7-active.jpg?auto=format,compress"
                                    alt>
                            </div>
                            <div class="inner">
                                <h2>Tummy <span class="block">Tuck</span></h2>
                                <div class="content">
                                    <p>If you struggle with a tummy bulge that no diet or exercise can resolve, you can
                                        achieve a firmer, trimmer figure with a custom tummy tuck. Excess skin, fat, and
                                        tissue will disappear, the muscle structure tightened, creating a dramatic
                                        improvement in your overall look.</p>
                                </div>
                                <a href="/body-contouring/tummy-tuck/" class="btn">Learn more</a>
                            </div>
                        </div>
                        <div class="item center">
                            <div class="image">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                    data-lazyload="img"
                                    data-lazyload-src="https://www.datocms-assets.com/31333/1595354530-homepage8-active.jpg?auto=format,compress"
                                    alt>
                            </div>
                            <div class="inner">
                                <h2>Mommy <span class="block">Makeover</span></h2>
                                <div class="content">
                                    <h3>Restore your body after baby</h3>
                                    <p>Pregnancy, childbirth, and breastfeeding can significantly change your body and
                                        breasts. Dr. Paul Glat performs custom mommy makeover procedures to restore a fit,
                                        firm, lifted look to breasts, tummy, butt, thighs, or other areas that have been
                                        affected. You’ll appear young, fit, and sculpted, and back to your best self.</p>
                                </div>
                                <a href="/body-contouring/mommy-makeover/" class="btn">Learn more</a>
                            </div>
                        </div>
                        <div class="item right">
                            <div class="image">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                    data-lazyload="img"
                                    data-lazyload-src="https://www.datocms-assets.com/31333/1595354544-homepage9-active.jpg?auto=format,compress"
                                    alt>
                            </div>
                            <div class="inner">
                                <h2>Liposuction</h2>
                                <div class="content">
                                    <p>Liposuction is, and always will be, one of the most effective ways to remove excess
                                        fat and create a more appealing figure. When under the care of Dr. Paul Glat in his
                                        Philadelphia area clinic, you can achieve refined body contour, with excess fat gone
                                        forever.</p>
                                </div>
                                <a href="/body-contouring/liposuction/" class="btn">Learn more</a>
                            </div>
                        </div>
    
                    </div>
                    <div class="tabs-wrapper">
                        <div class="tabs">
                            <div class="tab">
                                <p>Tummy <span class="block">Tuck</span></p>
                            </div>
                            <div class="tab active">
                                <p>Mommy <span class="block">Makeover</span></p>
                            </div>
                            <div class="tab">
                                <p>Liposuction</p>
                            </div>
                        </div>
                        <a href="/body-contouring/" class="btn">View all</a>
                    </div>
    
                </div>
    
            </div> <!-- Quote Module -->
    
    
        </main>

    


    <section id="homeblogwrapper">
        <div class="container">
            <div class="homeblogheading notofamily commonheadingfontsize">LATEST BLOGS</div>
            <div class="row manage-mbwidthparent">
                <div class="col-12 col-md-4 manage-mbwidth">
                    <div class="homeblogcontentwrapper">
                        <div class="homeblogimagewrap">
                            <div class="homeblogimage">
                                <img src="img/homeblog-1.jpg" class="w-100">
                            </div>
                            <div class="blogcalender">
                                <p>05 <span>JAN</span></p>
                            </div>
                        </div>
                        <div class="homeblogcontenttext">
                            <div class="homeblogcontentheading">LOREM IPSUM DOLOR SIT</div>
                            <div class="homeblogtextp">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras iaculis enim eget urna
                                    lobortis laoreet.</p>
                            </div>
                            <div class="homblogview">
                                <a href="#">read more</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-4 manage-mbwidth">
                    <div class="homeblogcontentwrapper">
                        <div class="homeblogimagewrap">
                            <div class="homeblogimage">
                                <img src="img/homeblog-2.jpg" class="w-100">
                            </div>
                            <div class="blogcalender">
                                <p>05 <span>JAN</span></p>
                            </div>
                        </div>
                        <div class="homeblogcontenttext">
                            <div class="homeblogcontentheading">LOREM IPSUM DOLOR SIT</div>
                            <div class="homeblogtextp">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras iaculis enim eget urna
                                    lobortis laoreet.</p>
                            </div>
                            <div class="homblogview">
                                <a href="#">read more</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-4 manage-mbwidth">
                    <div class="homeblogcontentwrapper">
                        <div class="homeblogimagewrap">
                            <div class="homeblogimage">
                                <img src="img/homeblog-3.jpg" class="w-100">
                            </div>
                            <div class="blogcalender">
                                <p>05 <span>JAN</span></p>
                            </div>
                        </div>
                        <div class="homeblogcontenttext">
                            <div class="homeblogcontentheading">LOREM IPSUM DOLOR SIT</div>
                            <div class="homeblogtextp">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras iaculis enim eget urna
                                    lobortis laoreet.</p>
                            </div>
                            <div class="homblogview">
                                <a href="#">read more</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="homeblogviewmore for-desktop">
                <a href="#">view more</a>
            </div>
        </div>
        </div>
    </section>
    <section id="appointmentwrapper" class="for-desktop">

        <div class="container">
            <div class="homeappointmentheading notofamily commonheadingfontsize">BOOK AN APPOINTMENT</div>
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="appointaddresswrap">
                        <div class="apadressicon">
                            <img src="img/location-icon.png">
                        </div>
                        <div class="aplocationcontentwrap">
                            <div class="aplocationheading">ADDRESS:</div>
                            <div class="detailaddress">
                                <p>Ground Floor W-58, Greater Kailash, New Delhi, Delhi 110048 India</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="appointaddresswrap">
                        <div class="apadressicon">
                            <img src="img/content-icon.png">
                        </div>
                        <div class="aplocationcontentwrap">
                            <div class="aplocationheading">MOBILE NO.:</div>
                            <div class="apmobilenumber">
                                <a href="tel:+91-8595941529">+91-8595941529</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="appointaddresswrap">
                        <div class="apadressicon">
                            <img src="img/email-icon.png">
                        </div>
                        <div class="aplocationcontentwrap">
                            <div class="aplocationheading">EMAIL ID:</div>
                            <div class="detailaddress">
                                <p><a href="mailto:souldermaclinic@gmail .com">souldermaclinic@gmail .com</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="bigappointwrap">
        <div class="container">
            <div class="homeappointmentheading notofamily commonheadingfontsize for-mobile">BOOK AN APPOINTMENT</div>
            <div class="row appointbg">
                <div class="col-12 col-md-4">
                    <div class="bigapform">
                        <input type="text" class="form-control" id="apone" placeholder="Name*">
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="bigapform">
                        <input type="text" class="form-control" id="aptwo" placeholder="Phone Number*">
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="bigapform">
                        <input type="email" class="form-control" id="apthree" aria-describedby="emailHelp"
                            placeholder="Email Address *">
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="bigapform bigapformrelative">
                        <input type="text" id="date" name="date" class="dateselect form-control for-icon"
                            placeholder="Date*" />
                        <span class="fa fa-calendar"></span>
                    </div>
                </div>

                <div class="col-12 col-md-8">
                    <div class="bigapform">
                        <textarea rows="1" class="form-control" placeholder="Type Message"></textarea>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="big-apsubmitwrapper">
                        <div class="bigcaptchawrap">
                            <div class="captcgawrap">
                                <div class="captchbgimg"><img src="img/captcha.jpg"></div>
                                <div class="apcaptchabutton">
                                    <button><i class="fa fa-refresh"></i></button>
                                </div>

                            </div>
                            <div class="inputfillcaptcha">
                                <input type="text">
                            </div>
                        </div>
                        <div class="bgcaptchasubmit">
                            <button>submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="popupappointmentform">
        <!-- Modal -->
        <div class="modal fade" id="appointmentPop" tabindex="-1" aria-labelledby="appointmentPopLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="appointmentPopLabel">Book An Appointment</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="popappointment">
                                <input type="text" class="form-control" id="exone" placeholder="Name*">
                            </div>
                            <div class="popappointment">
                                <input type="text" class="form-control" id="extwo" placeholder="Phone Number*">
                            </div>
                            <div class="popappointment">
                                <input type="email" class="form-control" id="exthree" aria-describedby="emailHelp"
                                    placeholder="Email*">
                            </div>
                            <div class="popappointment psrelative">
                                <input type="text" id="date" name="date" class="dateselect form-control for-icon"
                                    placeholder="Date*" />
                                <span class="fa fa-calendar"></span>
                            </div>
                            <div class="popappointment captchawrap">
                                <div class="captchaimagewrap">
                                    <div class="captchaimage">
                                        <img src="img/captcha.jpg" class="img-fluid">
                                    </div>
                                    <div class="captcharefreshbutton">
                                        <button><span class="fa fa-refresh"></span></button>
                                    </div>
                                </div>

                                <input type="text" class="form-control" id="exfour" aria-describedby="emailHelp">
                            </div>
                            <button type="submit" class="popupappointmentsubmit commonsubmitform">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="requestpopup">
        <!-- Modal -->
        <div class="modal fade" id="requestPop" tabindex="-1" aria-labelledby="requestPopLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="requestPopLabel">Request a Callback</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="popappointment">
                                <input type="text" class="form-control" id="exreq1" placeholder="Name*">
                            </div>
                            <div class="popappointment">
                                <input type="text" class="form-control" id="exreq2" placeholder="Phone Number*">
                            </div>
                            <button type="submit" class="requestsubmitsubmit commonsubmitform">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--common Modal for all video in this page-->
    <section id="commonmodalvideopopup" class="fade-in-out">
        <div class="modal fade modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">x</button>
                        <!-- 16:9 aspect ratio -->
                        <div class="ratio ratio-16x9">
                            <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always"
                                allow="autoplay"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end--common Modal for all video in this page-->


    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 commonborderright">
                    <div class="footerlogocontent commonfooterpadding">
                        <div class="footerlogo">
                            <a href="index.html"><img src="img/footerlogo.png"></a>
                        </div>
                        <div class="footertext">
                            <p>Dr Anika Goel is a renowned cosmetic dermatologist and hair transplant surgeon. She has
                                extensive experience of more than 5 years. She has done her M.D. Dermatology,
                                Venereology
                                and Leprosy - Sri Guru Ram Das Institute of Medical Science and Research, Amritsar.</p>
                        </div>
                        <div class="fotersocialmedia for-desktop">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a> </li>
                                <li> <a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-2 commonborderright">
                    <div class="footerlinkwrap commonfooterpadding">
                        <div class="footerlinksheading notofamily">SKIN SERVICES</div>
                        <ul>
                            <li><a href="#">ACNE</a></li>
                            <li><a href="#">PIGMENTATION</a></li>
                            <li><a href="#">SKIN GROWTH</a></li>
                            <li><a href="#">SKIN ISSUES</a></li>
                            <li><a href="#">MARKS & SPOTS</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-6 col-md-2 commonborderright">
                    <div class="footerlinkwrap commonfooterpadding">
                        <div class="footerlinksheading notofamily">AESTHETICS</div>
                        <ul>
                            <li><a href="#">FACIAL REJUVENATION</a></li>
                            <li><a href="#">FACIALS</a></li>
                            <li><a href="#">ANTI AGING</a></li>
                        </ul>
                    </div>
                </div>


                <div class="col-12 col-md-2 commonborderright">
                    <div class="footerlinkwrap commonfooterpadding">
                        <div class="footerlinksheading notofamily">HAIR SERVICES</div>
                        <ul>
                            <li><a href="#">HAIR CONCERNS</a></li>
                            <li><a href="#">HAIR GROWTH</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="footerlinks">
                        <ul>
                            <li><a href="#">ABOUT DOCTOR</a></li>
                            <li><a href="#">ABOUT CLINIC</a></li>
                            <li><a href="#">BLOGS</a></li>
                            <li><a href="#">TESTIMONIALS</a></li>
                            <li><a href="#">CONTACT US</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <section id="footercontent" class="for-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="appointaddresswrap">
                                <div class="apadressicon">
                                    <img src="img/location-icon.png">
                                </div>
                                <div class="aplocationcontentwrap">
                                    <div class="aplocationheading">ADDRESS:</div>
                                    <div class="detailaddress">
                                        <p>Ground Floor W-58, Greater Kailash, New Delhi, Delhi 110048 India</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="appointaddresswrap">
                                <div class="apadressicon">
                                    <img src="img/content-icon.png">
                                </div>
                                <div class="aplocationcontentwrap">
                                    <div class="aplocationheading">MOBILE NO.:</div>
                                    <div class="apmobilenumber">
                                        <a href="tel:+91-8595941529">+91-8595941529</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="appointaddresswrap">
                                <div class="apadressicon">
                                    <img src="img/email-icon.png">
                                </div>
                                <div class="aplocationcontentwrap">
                                    <div class="aplocationheading">EMAIL ID:</div>
                                    <div class="detailaddress">
                                        <p><a href="mailto:souldermaclinic@gmail .com">souldermaclinic@gmail .com</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="row managebottomfooter">
                <div class="col-12 col-md-6">
                    <div class="fotersocialmedia for-mobile">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a> </li>
                            <li> <a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                    <div class="copyrightwrap">
                        <p>© 2023, Soul Derma. All Rights Reserved. Powered by <a href="#">DigiLantern</a></p>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="treamsconditionwrap">
                        <ul>
                            <li><a href="#">PRIVACY POLICY</a></li>
                            <li><a href="#">TERMS AND CONDITIONS</a></li>
                            <li><a href="#">DISCLAIMER</a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-------------------------------------------end--Cta----------------------------->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/bs.js" defer></script>
    <script type="text/javascript" src="js/date-picker.js" defer></script>
    <script type="text/javascript" src="js/owl.carousel.min.js" defer></script>
    <script type="text/javascript" src="js/lightbox.js" defer></script>
    <script src="js/sidebar-menu.js"></script>
    <script src="js/fiveservices.js"></script>
    <script type="text/javascript" src="js/custom.js" defer></script>
    <script type="text/javascript">
        (() => { var p = t => { let o = document.readyState; if (typeof $ != "function") { setTimeout(() => { p(t) }, 100); return } o === "complete" || o !== "loading" && !document.documentElement.doScroll ? t() : document.addEventListener("DOMContentLoaded", t) }, m = p; var g = () => { [...document.querySelectorAll("video")].forEach((t, o) => { let e = t.getAttribute("data-lazyload"), n = t.getAttribute("data-lazyload-loaded"); if (!e || n) { let a = t.getAttribute("src"), s = window.innerWidth, r = t.getAttribute("data-desktop-vid"), l = t.getAttribute("data-tablet-vid"), u = t.getAttribute("data-mobile-vid"), d = s <= 640 ? u : s <= 920 ? l : r; d !== null && d !== a && t.setAttribute("src", d) } }) }, E = t => "scrollTop" in t ? t.scrollTop : t.pageYOffset, y = t => { let o = t.getBoundingClientRect(); return { top: o.top + window.scrollY, left: o.left + window.scrollX } }, C = t => { for (var o = [], e = t.parentNode.firstChild; e;)e.nodeType === 1 && e !== t && o.push(e), e = e.nextSibling; return o }, c = { run() { g(), [...document.querySelectorAll("[data-lazyload]")].forEach(function (t, o) { var e = t.getAttribute("data-lazyload"), n = t.getAttribute("data-lazyload-src"); let a = (window.tresioConfig || {}).lazyload || {}; var s = a.homeOffset !== void 0 ? a.homeOffset : 300, r = a.offset !== void 0 ? a.offset : 300; e === "bg" && (r = a.backgroundOffset !== void 0 ? a.backgroundOffset : 300); var l = E(window), u = t.nodeName === "SOURCE" ? y(t.parentNode).top : y(t).top; window.location.pathname === "/" && s !== void 0 && l < 300 && (r = s); var d = u - r, v = d + t.getBoundingClientRect().height, h = l + document.querySelector("html").clientHeight, w = u - l, A = d < -r, S = v > l && d < h || w < r; let z = t.style.display; if (S && z !== "none" && t.getAttribute("data-lazyload-loaded") !== "true" && !A) { if (e === "bg" && (t.style.backgroundImage = `url(${n})`), e === "img" && (t.getAttribute("src") !== n && t.setAttribute("src", n), C(t).forEach((i, x) => { if (i.nodeName === "SOURCE") { let f = i.getAttribute("data-lazyload-srcset"), T = i.getAttribute("srcset"); f && f !== T && i.setAttribute("srcset", f) } })), e === "video") { if (t.getAttribute("data-desktop-vid")) t.setAttribute("data-lazyload-loaded", "true"), g(); else if (t.setAttribute("src", n), t.nodeName === "SOURCE") { let i = t.parentNode; i.load && i.load() } } t.setAttribute("data-lazyload-loaded", "true") } }) }, init() { document.readyState === "loading" && c.run(), m(() => { window.addEventListener("resize", c.run, !1), window.addEventListener("scroll", c.run, !1), c.run() }) } }, b = c; b.init(); })();

    </script>
</body>

</html>