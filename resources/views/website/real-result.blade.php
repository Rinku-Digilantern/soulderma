@include('website.include.header')


    <link rel="stylesheet" href="{{ url('website') }}/css/commoncategory.css">


    <section id="topcommonbanner">
        <div class="commonbannerclinic">
            <img src="{{ url('website') }}/img/about-clinic/about-clinic.jpg" class="w-100">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="headingandbredcrumb">
                        <div class="commonheading notofamily commonheadingfontsize">Real Result</div>
                        <div class="breadcrumb commonbredcurmb">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a>Real Result</a></li>
                            </ul>
                        </div>
                        <div class="commonrequestform">
                            @include('website.include.requestcallback')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="realresultswrapper">
        <div class="container">
            <div class="row">
                @foreach($result as $res)
                <div class="col-12 col-md-4">
                    <div class="realcatwrap">
                        <img src="{{ url('/backend/service_result/image') }}/{{ $res->image }}" class="w-100" alt="{{ $res->alt_tag }}">
                        <a href="{{ url('result') }}/{{ $res->url }}" class="realcatheadingwrap">
                            <div class="realheading">{{ $res->name }}</div>
                            <button class="commongradientbutton" href="{{ url('result') }}/{{ $res->url }}">View All</button>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    @include('website.include.requestanappointment')

@include('website.include.footer')
