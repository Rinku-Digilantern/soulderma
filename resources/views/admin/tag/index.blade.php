@extends('admin.master')
@section('content')
<!--main content start-->
<div id="main">
<section id="main-content">
   
  <section class="wrapper">
    @if ($errors->any())
    <div class="alert alert-danger">
       <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
       </ul>
    </div>
    @endif
    @if(Session()->has('message'))
    <div class="alert alert-success">
       {{Session()->get('message')}}
    </div>
    @endif
    @if(Session()->has('Error'))
    <div class="alert alert-danger">
       {{Session()->get('Error')}}
    </div>
    @endif
    <div class="page-title">
  
          
        @if(in_array('1',$permission) || session('useradmin')['super_org_id']=='1')
            <a href="{{url('/admin/create-tag')}}" class="btn btn-primary">Add Tag</a>
        @endif
 
  
  
      <div>
        <h1>Tag List</h1>
        <ul class="breadcrumb side">
          <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="active">Tag Listt</li>
        </ul>
      </div>
      <div>
        @if(in_array('1',$permission) || session('useradmin')['super_org_id']=='1')
        <a href="{{url('/admin/create-tag')}}" class="btn btn-primary">Add Tag</a>
        @endif
      </div>
    </div>
  <div class="">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <table class="table table-hover table-bordered" id="sampleTable">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Type</th>
                  <th>Head Tag</th>
                  <th>Body Tag</th>
                  <th>Status</th>
                  <th>Created Date </th>
                  <th>Updated date </th>
                  <th style="width:15%">Action</th>
                </tr>
              </thead>
              <tbody>
                <!-- Start foreach loop -->
                @foreach($view as $viewlist)
                <tr>
                  <td>
                    {{$viewlist->id}}
                  </td>
                  <td>
                    {{$viewlist->type}}
                  </td>
                  <td>
                    {{$viewlist->headtag}}
                  </td>
                  <td>
                    {{$viewlist->bodytag}}
                  </td>
                  <td>
                    {{$viewlist->status}}
                  </td>
                  
                  <td>
                    {{$viewlist->created_at}}
                  </td>
                  <td>
                    {{$viewlist->updated_at}}
                  </td>


                  <td>
                    <input type="hidden" name="_token" id="token{{$viewlist->id}}" value="{{ csrf_token() }}">
                    @if($viewlist->status==1)
                    <button type="button" class="btn btn-info btn-lg" onclick="getactive('{{$viewlist->id}}','0')">Active</button>
                    @else
                    <button type="button" class="btn btn-danger btn-lg" onclick="getactive('{{$viewlist->id}}','1')">Inactive</button>
                    @endif
                    {{-- @if($viewlist->seo_status == 'preview') --}}
                    @if(in_array('2',$permission) || session('useradmin')['super_org_id']=='1')
                    <a href="{{url('/admin/edit-tag/'.$viewlist->id)}}" class="btn btn-warning  btn-lg"><i class="fa fa-edit"></i></a>&nbsp;
                    @endif
                    {{-- @else --}}
                    {{-- <a data-toggle="modal" data-target="#seo{{$viewlist->id}}" class="btn btn-success  btn-lg"><i class="fa fa-eye"></i></a>&nbsp; --}}
                    {{-- @endif --}}
                    @if(in_array('4',$permission) || session('useradmin')['super_org_id']=='1')
                    <a onclick="return confirm('Are you sure you want to delete?')" href="{{url('/admin/delete-tag/'.$viewlist->id)}}" class="btn btn-danger  btn-lg"><i class="fa fa-trash"></i></a>
                    @endif

                    <!-- Modal -->
                    <div id="seo{{$viewlist->seo_id}}" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{$viewlist->page_name}}</h4>
                          </div>
                          <div class="modal-body">
                            <table class="table table-hover table-bordered" id="sampleTable">
                              <tbody>
                                <tr>
                                  <td>Id</td>
                                  <td>{{$viewlist->seo_id}}</td>
                                </tr>
                                <tr>
                                  <td style="width: 20%;">Page Name</td>
                                  <td>{{$viewlist->page_name}}</td>
                                </tr>
                                <tr>
                                  <td style="width: 20%;">Title Tag</td>
                                  <td>{{ $viewlist->title_tag}}</td>
                                </tr>
                                <tr>
                                  <td style="width: 20%;">Keyword Tag</td>
                                  <td>{{$viewlist->keyword_tag}}</td>
                                </tr>
                                <tr>
                                  <td style="width: 20%;">Description Tag</td>
                                  <td>{{$viewlist->description_tag}}</td>
                                </tr>
                                <tr>
                                  <td style="width: 20%;">Canonical</td>
                                  <td>{{$viewlist->canonical_tag}}</td>
                                </tr>
                                <tr>
                                  <td style="width: 20%;">Image</td>
                                  <td><img src="{{url('/'.session('useradmin')['site_url'].'backend/seo/'.$viewlist->image)}}" class="imgwidth"></td>
                                </tr>
                                <tr>
                                  <td style="width: 20%;">Type</td>
                                  <td>{{$viewlist->type}}</td>
                                </tr>
                                <tr>
                                  <td style="width: 20%;">Site Name</td>
                                  <td>{{$viewlist->site_name}}</td>
                                </tr>
                                <tr>
                                  <td style="width: 20%;">Url</td>
                                  <td>{{$viewlist->url}}</td>
                                </tr>
                              </tbody>
                  </td>
                </tr>
            </table>
          </div>

        </div>

      </div>
    </div>

    </td>
    </tr>
    @endforeach
    <!-- End foreach loop -->
    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
   </div>
  </section>
</section>
</div>
<script>
function getactive(id,status){
   var token = $('#token' + id).val();
      // alert(token);return false;
       $.ajax({
        url : "{{url('/admin/tag-getactive')}}",
        type : "post",
       // dataType : "json",
        data : {"id":id,"status" : status,"_token":token},
        success : function(data) {
            console.log(data);
location.href= "{{url('/admin/index-tag')}}";
        },
    });
   }
  </script>

<!--main content end-->
@endsection