<!-- Header -->
<!doctype html>
<html lang="en">

<head>
    <meta name="facebook-domain-verification" content="wam750j9i0jm83wg99h2q04ctwtamn" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="<?php echo base_url() ?>assets/soulderma/images/favicon.png">
    <title>Dr. Anika Goel</title>
    <link rel="preload" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/soulderma/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="preload" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" as='font' onload="this.onload=null;this.rel='stylesheet'" crossorigin>
    <link rel="preload" href="<?php echo base_url() ?>assets/soulderma/css/datepicker.css" as="style" onload="this.rel='stylesheet'">
    <!-- <link rel="preload" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" crossorigin /> -->

    <!-- Owl Carousel links  -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/soulderma/css/owl.carousel.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/soulderma/css/owl.theme.green.css" />
    
    
<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1319675791941517');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1319675791941517&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TCDGGBV');</script>
<!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TCDGGBV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <header class="hdrnobg">
        <div class="row align-items-center">
            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                <a><img src="<?php echo base_url() ?>assets/soulderma/images/lazypic.jpg" data-src="<?php echo base_url() ?>assets/soulderma/images/logo.png" class="logoicn lazyload" alt="logo" /></a>
            </div>
            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6" style="text-align:right;">
                <a class="topCallBtn" href="Tel:+91-8595941529">Call Now</a>

            </div>
        </div>
    </header>

    <div class="container-fluid myContPad">
        <div class="row souldermaDesk justify-content-end align-items-center">
            <div class="col-lg-5 col-md-6 col-sm-12 souldermaMob">
                <div class="bannerContent">
                    <div class="docName">Sparkle from <br class="mobBrShow"/> within <span class="glow">This Diwali!</span></div>
                    <!-- <span class="topSpan">Best Method to get rid of unwanted Hair</span> -->
                    <!-- <ul class="myUl">
                        <li class="docExp">Widely accepted and proven treatment</li>
                        <li class="docExp">Quickly and effective with long lasting results</li>
                        <li class="docExp">Painless and is suited for various skin tones</li>
                        <li class="docExp">An effective treatment for ingrown hair as well</li>
                    </ul> -->
                    <div class="offerList mnofferList">
                        <li class="offerTag  mnOffertag">offer</li>
                        <!-- <li class="offTag">50% off</li> -->
                        <li class="offTag mnOffTag">@ 12,999/-</li>
                        <!-- <li class="oldPriceTag">₹ 1,19,000</li> -->
                        <!-- <li class="newPriceTag">₹ 12,999</li> -->
                        <li class="sessionTag mnStag">3 Illuminating Procedures  <br>
                            <span>Hydra facial</span> <br>
                            <span>Glow Medi Facial</span> <br>
                            <span>Glow Drips </span><br>
                        </li>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 myTopForm">

                <div class="topFormBx bnrFrm">
                    <div class="text-center frmHd  myHd">Request a Call Back</div>
                    <?php include('call-back-form.php') ?>
                </div>
            </div>
            <div class="btnDiv desplay-mob text-center"><a href="Tel:+91-8595941529">Call Now for Cost Estimation</a></div>
        </div>
    </div>


    <section class="topStatisticStrip">
        <div class="container-fluid">
            <div class="container">
                <div class="topStrip mnLine">
                    <div class="row">
                        <div class="col-lg-4 lineDiv">
                            <li>
                                <span>7+</span>
                                <span>Yrs of experience</span>
                            </li>
                            <span class="stripLine"></span>
                        </div>
                        <div class="col-lg-4 lineDiv">
                            <li>
                                <span>2000+</span>
                                <span>Happy <br class="mobBrShow"> patients</span>
                            </li>
                            <span class="stripLine"></span>
                        </div>
                        <div class="col-lg-4 lineDiv">
                            <li>
                                <span>25+</span>
                                <span>World class treatments</span>
                            </li>
                            <span class="stripLine"></span>
                        </div>
                        <!-- <div class="col-lg-3 lineDiv">
                            <li>
                                <span>10K</span>
                                <span>Lorem Ipsum</span>
                            </li>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Why choose us section start here  -->
    <!-- <section class="whyChooseSec">
        <div class="container">
            <div class="tstmnlHD text-center mainHead">Why Choose us</div>
            <div class="row">
                <li>
                    <img src="<?php echo base_url() ?>assets/soulderma/images/reason-3.png" alt="">
                    <div class="whyHd">Natural Results</div>
                    <p>The experts at Soul Derma work in minute details to provide the most natural looking hairline and youthful rejuvenated skin.</p>
                </li>
                <li>
                    <img src="<?php echo base_url() ?>assets/soulderma/images/reason-2.png" alt="">
                    <div class="whyHd">Smooth Recovery</div>
                    <p>The hair and skin specialists at Soul Derma are highly skilled in doing the procedures with great precision and accuracy. This facilitates a fast and smooth recovery.</p>
                </li>
                <li>
                    <img src="<?php echo base_url() ?>assets/soulderma/images/reason-1.png" alt="">
                    <div class="whyHd">Covid Safety </div>
                    <p>The entire team is accustomed to follow strict safety measures as per WHO Guidelines.</p>
                </li>
            </div>
        </div>
    </section> -->


    <section class="contentSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="secHDg">Hydra Facial</div>
                    <p>The HydraFacial is a unique facial treatment that combines the benefits of a traditional facial with the power of hydraulics. This innovative facial treatment uses a special device to cleanse, exfoliate and hydrate the skin.
                        The HydraFacial is ideal for those who want to improve the appearance of their skin without having to undergo surgery or other invasive procedures. This luxurious facial treatment can help improve the texture and tone of the skin and reduce the appearance of fine lines and wrinkles.
                    </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="secHDg">Benefits of Hydrafacial treatment?</div>
                    <li>Improves the appearance of fine lines and wrinkles</li>
                    <li>Improves the texture and tone of the skin</li>
                    <li>Reduces the appearance of enlarged pores</li>
                    <li>Enhances the production of collagen and elastin</li>
                    <li>Improves circulation</li>
                    <li>Reduces the appearance of brown spots and pigmentation</li>
                    <li>Helps to fight against free radical damage</li>
                </div>
            </div>
        </div>
    </section>

    <section class="gloMediSec">
        <div class="container">
            <div class="secHDg">Glow Medi Facial</div>
            <p>Medi-facials are medical grade facials done in order to deeply cleanse, nourish and exfoliate the skin. It focuses more on nurturing the skin and making it glow and healthy. It helps in tightening and moisturising the skin and relaxing the mood by driving stress away. It also helps in reducing ageing signs and smoothening the texture of the skin. Glow Medi-facial also comes with anti-inflammatory and anti-bacterial properties which helps in exfoliation.</p>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <li>Exfoliates the skin with hydrodermabrasion and removes dead and damaged skin cells</li>
                    <li>Restores a youthful, glowing, and hydrated look</li>
                    <li>Resolves sun damage, pigmentation and signs of ageing</li>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <li>Exfoliated, soft, and supple skin</li>
                    <li>Suitable for All skin types</li>
                    <li>Provides hydration and unclogs pores</li>
                    <li>Improves skin tone</li>
                </div>
            </div>
        </div>
    </section>


    <section class="contentSection" style="background-color:#fff1f6;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="secHDg">Glow Drips</div>
                    <p>IV Glow drips help replenish the body with essential vitamins and nutrients in a much more rapid manner than taking supplements.These Skin-targeted drips contain minerals like magnesium and zinc, B-complex vitamins, and vitamin C, all of which support healthy skin function. Besides delivering skin-boosting nutrients in larger amounts, the fact that makes this procedure stand out is that the results are pretty immediate.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="secHDg">Benefits of Glow Drips?</div>
                    <li>provides an inside out glow</li>
                    <li>Gives glass-like glowing skin</li>
                    <li>Provides nutritional boost</li>
                    <li>Improves appearance almost immediately</li>
                    <li>Visible difference in skin texture and tone</li>
                    <li>promotes younger and brighter looking skin</li>
                    <li> and long-lasting results</li>
                </div>
            </div>
        </div>
    </section>
    <!-- Why choose us section end here  -->

    <!-- Testimonial section start here  -->
    <section class="sctnTestimonials gry">
        <div class="container-fluid">
            <div class="tstmnlHD text-center myTesti">Testimonials</div>
            <div id="TestimonialsSlider" class="custmTstmnal carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators tstmnlCrcl">
                    <button type="button" data-bs-target="#TestimonialsSlider" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#TestimonialsSlider" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#TestimonialsSlider" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    <button type="button" data-bs-target="#TestimonialsSlider" data-bs-slide-to="3" aria-label="Slide 4"></button>
                </div>
                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="testItem">
                                    <div class="row">
                                        <div class="col-auto me-auto"><span class="cname">Google</span></div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                    <p>It was yet again a wonderful experience at soul derma .Getting my hair back and also the Medi facial and pico laser have shown amazing results.</p>
                                    <div class="uname">Pulkit maan</div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="testItem">
                                    <div class="row">
                                        <div class="col-auto me-auto"><span class="cname">Google</span></div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                    <p>
                                        Trust me, if you’re thinking of having a clear young skin, Dr. Anika Goel is the person to meet.

                                        I visited her clinic right before my wedding.
                                        Big shoutout to the doc & her team for making my experience so comfortable, while soothing music played in the background.

                                        I had laser hair removal for my high shave line, which was painless and effective. I also had medifacials done to get ready for my big day, and I couldn’t be happier with the result. My skin looked clear and bright.

                                        Expertise: 6/5
                                        Ambience: 5/5
                                        Value for money: 5/5

                                        Thank you for this amazing experience Dr. Anika. Will be visiting again for sure. 🙌🏻
                                    </p>
                                    <div class="uname">Rahul Malhotra</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="testItem">
                                    <div class="row">
                                        <div class="col-auto me-auto"><span class="cname">Google</span></div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                    <p>I visited Dr. Anika for skin issue. I have given very late reviews to Dr.Anika just becuase i want to see the results first. I have been visiting the clinic since sept of last year. So I'll give genuine reviews here. First of all Dr. Anika is an amazing doctor and it's an amazing experience with soul derma clinic. She explained the things and procedure pretty well and she took her maximum time to explain about my skin condition and tried to make it better through the advance technology they have in their clinic. The doctor is very humble and polite. Extremely knowledgeable about various skin and hair conditions. I would suggest everyone to stop looking for other skin specialists and go to her straight away. Her domain expertise is quite evident, the explanation of the diagnosis is detailed and clear. I have always had positive results from the advised course of treatment. Overall very nice experience. The doctors team are very cordial,experienced and made me very comfortable. All the staffs are very friendly, genuine, very cooperative and polite. The clinic is very neat and clean. Everything is hygienic. And I have heared from lot of people that in other dermatologist clinic Dr. do not even like to meet the patients. Randomly any staff member handles the patients and in few clinics non medical staff attend the patients which is so harmful that's why Dr. Anika goel is the best dermatologist because she gives proper time to her patients and she has medical staff. Technician is very nice and knowledgeable. I'm quite happy with my results. Go for it without any hesitation. she counsels very well and gives you best suggestion. This clinic is amazing for skin and hair treatments. Please do consider coming here for any skin/hair related treatment. Dr. Anika is such a kind soul and helped me a lot during all the procedures and I am able to see the changes in my skin texture after treatments. I would highly recommend and In last DR. ANIKA GOEL IS BEST DERMATOLOGIST IN DELHI. ❤️❤️
                                    </p>
                                    <div class="uname">Kavita Prajapati</div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="testItem">
                                    <div class="row">
                                        <div class="col-auto me-auto"><span class="cname">Google</span></div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                    <p>I had a great experience to visit her clinic. I am very glad to meet Dr anika goeal..she is such a sweet & humble person. She's extremely patient. I am completely satisfied with her treatment. l am able to see the changes in my looks after treatment 🥰</p>
                                    <div class="uname">Usha Singh</div>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="testItem">
                                    <div class="row">
                                        <div class="col-auto me-auto"><span class="cname">Google</span></div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                    <p>I had an excellent experience here. Dr. Goel was amazing. Extremely attentive and caring. She explained everything in great detail and answered all my questions. I had the vampire glow treatment, which was very reasonably priced - cant wait to see the results! The service and professionalism was at a high standard from start to finish. If i was staying in Delhi longer i would definitely come back to soul derma for more treatments. Thank you soul derma!</p>
                                    <div class="uname">Manuel Spiropoulos</div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="testItem">
                                    <div class="row">
                                        <div class="col-auto me-auto"><span class="cname">Google</span></div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                    <p>Lovely clinic! Anika has been a massive help in clearing my skin--I get compliments from everyone about how drastically it has changed. She's extremely patient and listened to my problems well and made a treatment plan suitable to my needs and my budget, and was always open to modifying it according to what I wanted (without compromising on what was needed). I've recommended her to most friends and would do the same to a stranger! The clinic is also very resourceful and they deliver products to me when I run out, which has been a massive help.</p>
                                    <div class="uname">Malayka Shirazi</div>
                                </div>
                            </div>
                        </div>
                    </div>





                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="testItem">
                                    <div class="row">
                                        <div class="col-auto me-auto"><span class="cname">Google</span></div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                    <p>Treatment at Soul Derma helped me overcome pigmentation and even removal of fine lines. My skin lookes rejuvenated. Follow up by Dr. Anika post the treatment is very reassuring.</p>
                                    <div class="uname">Meenu Chopra</div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="testItem">
                                    <div class="row">
                                        <div class="col-auto me-auto"><span class="cname">Google</span></div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                    <p>I am so happy with the result of hydra facial . Staffs are so humble and polite . And I have no words for Dr. Anika she is amazing . I have also consulted regarding my hair thinning issue . Will be adding more review after all of my meso hair therapy session gets over .</p>
                                    <div class="uname">Sweta Poudel</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev prevbtn" type="button" data-bs-target="#TestimonialsSlider" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next prevbtn nextbtn" type="button" data-bs-target="#TestimonialsSlider" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
                <p class="opinion myTesti">*Opinions / Results may vary from person to person.</p>
            </div>
        </div>
    </section>
    <!-- Testimonial section end here  -->


<!-- Videos section start here  -->
    <section class="asFacultyMain sctnTestimonials myVidBG">
        <div class="container">
            <div class="tstmnlHD text-center">Videos</div>
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-4 col-md-4 d-sm-6 d-xs-6 mb-3">
                    <div class="landingvideos" data-bs-toggle="modal" data-bs-target="#instagram" onClick="setInstaVideo('https://www.instagram.com/p/CjYM6qyJFv-/embed/')">
                        <!-- <div class="ldplay">
                            <i class="fa fa-play"></i>
                        </div> -->
                        <img src="<?php echo base_url() ?>assets/soulderma/images/vid1.jpg" class="img-fluid" alt="" />

                    </div>
                    <!-- <span class="text-white">*Results may vary person to person</span> -->

                </div>
                <div class="col-xl-6 col-lg-4 col-md-4 d-sm-6 d-xs-6 mb-3">
                    <div class="landingvideos" data-bs-toggle="modal" data-bs-target="#instagram" onClick="setInstaVideo('https://www.instagram.com/p/CZ6FOKLpjlk/embed/')">
                        <!-- <div class="ldplay">
                            <i class="fa fa-play"></i>
                        </div> -->
                        <img src="<?php echo base_url() ?>assets/soulderma/images/vid2.jpg" class="img-fluid" alt="" />

                    </div>
                    <!-- <span class="text-white">*Results may vary person to person</span> -->

                </div>
                <!-- <div class="col-lg-4 col-md-4 col-sm-12 my-3">
                    <img onclick="setVid('https://www.youtube.com/embed/DTF3ZvAQwRU')" data-bs-toggle="modal" data-bs-target="#youtubemodal" src="<?php echo base_url() ?>assets/soulderma/images/vid1.jpg" class="w-100" alt="video1">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 my-3">
                    <img onclick="setVid('https://https://www.youtube.com/embed/DTF3ZvAQwRU')" data-bs-toggle="modal" data-bs-target="#youtubemodal" src="<?php echo base_url() ?>assets/soulderma/images/vid1.jpg" class="w-100" alt="video1">
                </div> -->
            </div>
        </div>
    </section>
    <!-- Videos section end here  -->

    <!-- Videos section start here  -->
    <!--<section class="asFacultyMain sctnTestimonials myVidBG">-->
    <!--    <div class="container">-->
    <!--        <div class="tstmnlHD text-center">Videos</div>-->
    <!--        <div class="row align-items-center">-->
    <!--            <div class="col-lg-4 col-md-4 col-sm-12 my-3">-->
    <!--                <img onclick="setVid('https://www.youtube.com/embed/DTF3ZvAQwRU')" data-bs-toggle="modal" data-bs-target="#youtubemodal" src="<?php echo base_url() ?>assets/soulderma/images/vid1.jpg" class="w-100" alt="video1">-->
    <!--            </div>-->
    <!--            <div class="col-lg-4 col-md-4 col-sm-12 my-3">-->
    <!--                <img onclick="setVid('https://www.youtube.com/embed/DTF3ZvAQwRU')" data-bs-toggle="modal" data-bs-target="#youtubemodal" src="<?php echo base_url() ?>assets/soulderma/images/vid1.jpg" class="w-100" alt="video1">-->
    <!--            </div>-->
    <!--            <div class="col-lg-4 col-md-4 col-sm-12 my-3">-->
    <!--                <img onclick="setVid('https://https://www.youtube.com/embed/DTF3ZvAQwRU')" data-bs-toggle="modal" data-bs-target="#youtubemodal" src="<?php echo base_url() ?>assets/soulderma/images/vid1.jpg" class="w-100" alt="video1">-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!-- Videos section end here  -->



    <!-- Videos section start here  -->
    <!-- <section class="asFacultyMain sctnTestimonials ">
        <div class="container">
            <div class="tstmnlHD text-center">Testimonials</div>
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-12 my-3">
                    <img onclick="setVid('https://www.youtube.com/embed/DTF3ZvAQwRU')" data-bs-toggle="modal" data-bs-target="#youtubemodal" src="<?php echo base_url() ?>assets/soulderma/images/vid1.jpg" class="w-100" alt="video1">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 my-3">
                    <div class="testItem">
                        <div class="row">
                            <div class="col-auto me-auto"><span class="cname">Google</span></div>
                            <div class="col-auto">
                            </div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A officiis atque doloremque dolorem, porro minus vel est. Officia nesciunt, eius quibusdam mollitia dolores quisquam a minima dolore nihil, delectus harum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati eius, magnam unde esse iste doloribus. Possimus esse nihil nesciunt dolore maiores obcaecati asperiores expedita beatae. Nostrum, aliquid similique. Accusantium, reiciendis!
                        </p>
                        <div class="uname">Anonymous</div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- Videos section end here  -->




    <!-- Doctor Section Start here -->
    <div class="docSection">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4">
                    <img src="<?php echo base_url() ?>assets/soulderma/images/docImage.png" class="w-100" alt="docImage">
                </div>
                <div class="col-lg-8">
                    <div class="docContent">
                        <div class="docName">Dr. Anika Goel</div>
                        <p>Dr Anika Goel is a renowned cosmetic dermatologist and hair transplant surgeon. She has extensive experience of more than 5 years. She has done her M.D. Dermatology, Venereology and Leprosy- Sri Guru Ram Das Institute of Medical Science and Research, Amritsar. She has completed her MBBS- D.Y. Patil Medical College Navi Mumbai, India University- Padmashree Dr. D.Y. Patil University</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Doctor Section end here -->



    <!-- Videos section start here  -->
    <!-- <section class="asFacultyMain sctnTestimonials ">
        <div class="container">
            <div class="tstmnlHD text-center">Results</div>
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-12 my-3">
                    <img class="w-100" src="<?php echo base_url() ?>assets/soulderma/images/result1.jpg" alt="">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 my-3">
                    <img onclick="setVid('https://www.youtube.com/embed/DTF3ZvAQwRU')" data-bs-toggle="modal" data-bs-target="#youtubemodal" src="<?php echo base_url() ?>assets/soulderma/images/vid1.jpg" class="w-100" alt="video1">
                </div>

            </div>
        </div>
    </section> -->
    <!-- Videos section end here  -->


    <div id="AppForm">&nbsp;</div>
    <section class="bookAppointment serviceForm">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-8 col-lg-8 col-md-10 formSpace">
                    <div class="appointmntHD text-center">Book An Appointment</div>
                    <?php include('book-an-appointment.php'); ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Results section start here  -->





    <!-- Footer -->
    <footer class="blck">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 my-3">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.6345934417313!2d77.23130931500711!3d28.550700982450234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce3dfd43e3265%3A0x2bed17d86139c92d!2sSoul%20Derma%20Clinic%20by%20Dr.Anika%20Goel!5e0!3m2!1sen!2sin!4v1665042571080!5m2!1sen!2sin" style="border:0;width:100%;height:100%" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div class="col-lg-6 my-3">
                    <ul class="footer-ul bg-light">
                        <li><i class="fa fa-envelope" aria-hidden="true"></i>souldermaclinic@gmail.com</li>
                        <li><i class="fa fa-phone" aria-hidden="true"></i> +91-8595941529</li>
                        <li><i class="fa fa-address-book" aria-hidden="true"></i>Ground Floor W-58,
                            Greater Kailash I, New Delhi,
                            Delhi 110048 India</li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <div class="container-fluid disclaimer-section px-0 blck">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mydis">
                    Disclaimer: All the general information, graphics, and videos provided on the website
                    (www.soulderma.com) are solely to create awareness among people and is not intended to be
                    taken as medical advice. The results also vary from patient to patient, depending upon their health
                    condition and body anatomy. If you are interested in finding out more, please contact us today for a
                    personal consultation.
                </div>
            </div>
        </div>
    </div>
    <div class="txtCprgt text-center">© Copyright 2022 | All Rights Reserved</div>
    <div class="d-md-none d-lg-none d-xl-none col-sm-block ftrFixMenu">
        <div class="row align-items-center">
            <div class="col"><a href="tel:+91-8595941529"><i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Call Us</p>
                </a></div>
            <div class="col d-none"><a href="mailto:info@delhiurologyhospital.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                    <p>Email Us</p>
                </a></div>
            <div class="col"><a href="#AppForm"><i class="fa fa-calendar" aria-hidden="true"></i>
                    <p>Appointment</p>
                </a></div>
        </div>
    </div>

<div class="modal fade" id="instagram" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content video">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">

                <iframe id="InstamyFrame" class="instagram-media instagram-media-rendered" id="instagram-embed-0"
                    src=" " allowtransparency="true" allowfullscreen="true" frameborder="0" height="400"
                    data-instgrm-payload-id="instagram-media-payload-0" scrolling="no"
                    style="background: white;  border-radius: 3px; border: 1px solid rgb(219, 219, 219); box-shadow: none; display: block; margin:0px auto; padding: 0px;"></iframe>
            </div>

        </div>
    </div>
</div>
    <!-- Youtube Modal  -->
    <!-- Modal -->
    <div class="modal fade" id="youtubemodal" tabindex="-1" aria-labelledby="youtubemodal1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <iframe height="400" width="100%" id="YouTubeFrame" src="" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>


    <script>
        function setVid(val) {
            document.getElementById("YouTubeFrame").setAttribute("src", val);
        }
        document.querySelector('#youtubemodal .btn-close').addEventListener('click', function() {
            document.getElementById("YouTubeFrame").setAttribute("src", '');
        })
        document.querySelector('#youtubemodal').addEventListener('click', function() {
            document.getElementById("YouTubeFrame").setAttribute("src", '');
        })
        function setInstaVideo(val) {
        document.getElementById("InstamyFrame").src = val;
        document.getElementById('InstamyFrame').style.color = 'auto';
    }
    </script>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" id="mobBody">
                    <div class="topFormBx bnrFrm">
                        <div class="text-center frmHd  myHd">Book an Appointment</div>
                        <?php include('call-back-form.php') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Whatsapp Button  -->
    <a href="https://api.whatsapp.com/send?phone=8595941529&amp;text=Hi" class="whatsapp sticky-bot">
        <i class="fa fa-whatsapp" aria-hidden="true"></i>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <a href="#" id="scroll" style="display: none;"><span></span></a>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/soulderma/js/bootstrap-datepicker.js" defer></script>
        <script src="<?php echo base_url() ?>assets/soulderma/js/custom.js"></script>

        <!-- Owl Carousel  -->
        <script src="<?php echo base_url() ?>assets/soulderma/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/soulderma/js/owl.carousel.min.js"></script>


        <script type="text/javascript" src="<?php echo base_url() ?>assets/soulderma/js/lazysizes.min.js" defer></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>


        <style type="text/css">
            .loadme {
                display: none;
                text-align: center;
            }
        </style>
        <script type="text/javascript">
            var $ = jQuery;
            $(function() {
                $("#date").datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    startDate: '-0m',
                    daysOfWeekDisabled: [0]
                })
            });
        </script>
        <script type="text/javascript">
            // When the browser is ready...
             jQuery.validator.addMethod("lettersonly", function(value, element) 
            {
            return this.optional(element) || /^[a-z," "]+$/i.test(value);
            }, "Letters and spaces only please"); 
            $(function() {

                // Setup form validation on the #register-form element
                $("#appointment_form").validate({
                    // Specify the validation rules
                    rules: {

                       name: {
                             required: true,
                             lettersonly:true,
                        },
                        phone: {
                            required: true,
                            number: true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        date: "required"
                    },

                    // Specify the validation error messages
                    messages: {

                        name: "<font color='red'>Please Enter Your  Name</font>",
                        phone: "<font color='red'>Please Enter Your  Mobile Number</font>",
                        email: "<font color='red'>Please Enter Your valid Email Address</font>",
                        date: "<font color='red'>Please Select Appointment Date</font>",
                    },

                    submitHandler: function(form) {
                        $.ajax({
                            url: form.action,
                            type: form.method,
                            data: $(form).serialize(),
                            beforeSend: function() {
                                $(".loadme").show();
                                $("#submit_btn").prop('disabled', true);
                            },
                            success: function(data) {
                                if (data == 1) {
                                    $(".loadme").show();
                                    $("#submit_btn").prop('disabled', true);
                                    window.location.replace('/thank-you');
                                } else if (data == 0) {
                                    $("#submit_btn").prop('enabled', true);
                                    $(".loadme").hide();
                                    $('#errormsgap').html(
                                        'Value is not Currect. Please insert Currect Details'
                                    );
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                $(".loadme").hide();
                                $("#submit_btn").removeAttr("disabled");
                            }
                        });
                    }
                });
            });
        </script>

        <script type="text/javascript">
            // When the browser is ready...
           
            $(function() {

                // Setup form validation on the #register-form element
                $("#callback").validate({

                    // Specify the validation rules
                    rules: {

                        name: {
                             required: true,
                             lettersonly:true,
                        },
                        phone: {
                            required: true,
                            number: true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                    },

                    // Specify the validation error messages
                    messages: {

                        name: "<font color='red'>Please Enter Your  Name</font>",
                        phone: "<font color='red'>Please Enter Your  Mobile Number</font>",
                        email: "<font color='red'>Please Enter Your valid Email Address</font>",
                    },

                    submitHandler: function(form) {
                        $.ajax({
                            url: form.action,
                            type: form.method,
                            data: $(form).serialize(),
                            beforeSend: function() {
                                $(".loadme").show();
                                $("#call_submit").prop('disabled', true);
                            },
                            success: function(data) {
                                if (data == 1) {
                                    $(".loadme").show();
                                    $("#call_submit").prop('disabled', true);
                                    window.location.replace('/thank-you');
                                } else if (data == 0) {
                                    $(".loadme").hide();
                                    $("#call_submit").prop('enabled', true);
                                    $('#errormsg').html(
                                        'Value is not Currect. Please insert Currect Details'
                                    );
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                $(".loadme").hide();
                                $("#call_submit").removeAttr("disabled");
                            }
                        });
                    }
                });

            });
        </script>
        <script>
            function setInstaVideo(val) {
                // console.log(val);
                document.getElementById("InstamyFrame").src = val;
                document.getElementById('InstamyFrame').style.color = 'auto';
            }
        </script>

        <script>
            $('#instagram .btn-close').click(function() {
                $("iframe#InstamyFrame").attr("src", $("iframe#InstamyFrame").attr("src"));
            });

            $("#instagram").click(function() {
                $("iframe#InstamyFrame").attr("src", $("iframe#InstamyFrame").attr("src"));
            });
        </script>
</body>

</html>