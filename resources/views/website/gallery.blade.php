@include('website.include.header')

    <link rel="stylesheet" href="{{ url('website') }}/css/aboutclinic.css">


    <section id="topcommonbanner" class="gallery_page_banner">
        <div class="commonbannerclinic">
            <img src="{{ url('website') }}/img/about-clinic/about-clinic.jpg" class="w-100" alt="Soulderma Clinic Gallery">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="headingandbredcrumb">
                        <div class="commonheading notofamily commonheadingfontsize">Clinic Gallery</div>
                        <div class="breadcrumb commonbredcurmb">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a>Clinic Gallery</a></li>
                            </ul>
                        </div>
                        <div class="commonrequestform">
                            @include('website.include.requestcallback')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section id="clinicgallerypage" class="mt-5">
<div class="container">
    <div class="row">
        @foreach($gallery as $glr)
        <div class="col-lg-4 col-md-6 col-12">
            <div class="gallery_box">
                <div class="mid_img">
                    <a href="{{ url('/backend/gallery') }}/{{ $glr->gallery_image }}" data-toggle="lightbox" data-gallery="example-gallery">
                        <img src="{{ url('/backend/gallery') }}/{{ $glr->gallery_image }}" class="img-fluid"  alt="{{ $glr->alt_tag }}">
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
</section>

@include('website.include.requestanappointment')

@include('website.include.footer')
