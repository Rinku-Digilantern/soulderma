@include('website.include.header')

    <link rel="stylesheet" href="{{ url('website') }}/css/services.css">
    <link rel="stylesheet" href="{{ url('website') }}/css/videos.css">


    <section id="topcommonbanner" class="serviceinner_page_banner">
        <div class="commonbannerclinic service_banner">
            <img src="{{url('/backend/service/banner')}}/{{$servicedetail->service_banner_image}}" class="w-100" alt="{{ $servicedetail->alt_tag }}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="headingandbredcrumb">
                        <div class="commonheading notofamily commonheadingfontsize">{{ $servicedetail->service_name }}</div>
                        <div class="breadcrumb commonbredcurmb">
                            <ul>
                                <li><a href="{{ url('/') }}">Home</a></li>
                                @if($sub->service_name != 'General' )
                                <li><a href="{{ url('/').'/'.$sub->url }}">{{ $sub->service_name }}</a></li>
                                @endif
                                <li><a href="{{ url('') }}/{{ $cat->url }}">{{ $cat->service_name }}</a></li>
                                <li><a>{{ $servicedetail->service_name }}</a></li>
                            </ul>
                        </div>
                        <div class="commonrequestform">
                            @include('website.include.requestcallback')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(isset($servicedetail->section))
    @php
    $service_sec = json_decode($servicedetail->section);
    $service_sec  = collect($service_sec)->sortBy('secorderby');
    @endphp
    @endif
    <!-- Start Foreach -->
    @foreach($service_sec as $ser_sec)
    <!-- Left Image Section -->
    @if($ser_sec->type=='imagetext')
    <section class="leftheadingimagewrap @if(isset($ser_sec->class_add)) {{$ser_sec->class_add}} @endif">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    @if(isset($ser_sec->heading_tag))
                        <{{$ser_sec->heading_tag}} class="commonservicesheading"> @if(isset($ser_sec->section_heading)) {{ $ser_sec->section_heading }} @endif</{{$ser_sec->heading_tag}}>
                    @else
                    <div class="commonservicesheading">
                        @if(isset($ser_sec->section_heading))
                        {{ $ser_sec->section_heading }}
                        @endif
                    </div>
                    @endif
                    <div class="imageleft">
                        <img src="{{url('/backend/service/section')}}/{{$ser_sec->image}}" class="w-100" alt="{{ $ser_sec->alttag }}">
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="commonpointers">
                        {!! $ser_sec->section1 !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    <!-- Left Image Section End -->

    <!-- Full Text Section -->
    @if($ser_sec->type=='fulltext')
    <section class="centerhighlight centercontent-wrap @if(isset($ser_sec->class_add)) {{$ser_sec->class_add}} @endif">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="centerheighcontentwrap">
                        @if(isset($ser_sec->heading_tag))
                            @if(isset($ser_sec->section_heading))
                                <{{$ser_sec->heading_tag}} class="commonservicesheading">@if(isset($ser_sec->section_heading)) {{ $ser_sec->section_heading }} @endif</{{$ser_sec->heading_tag}}>
                            @endif
                        @else
                            <div class="commonservicesheading"> @if(isset($ser_sec->section_heading)) {{ $ser_sec->section_heading }} @endif</div>
                        @endif
                        {!!$ser_sec->section1 !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    <!-- Full Text Section End-->

    <!-- Two Pragraph Section -->
    @if($ser_sec->type=='twoparagraph')
    <section class="twoheadingwrap @if(isset($ser_sec->class_add)) {{$ser_sec->class_add}} @endif">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                @if(isset($ser_sec->heading_tag))
                    @if(isset($ser_sec->section_heading))
                        <{{$ser_sec->heading_tag}} class="commonservicesheading"> @if(isset($ser_sec->section_heading)) {{ $ser_sec->section_heading }} @endif</{{$ser_sec->heading_tag}}>
                    @endif
                @else
                <div class="commonservicesheading">
                    @if(isset($ser_sec->section_heading))
                    {{ $ser_sec->section_heading }}
                    @endif
                </div>
                @endif
                </div>
                <div class="col-12 col-md-6">
                @if(isset($ser_sec->heading_tag))
                    @if(isset($ser_sec->service_heading))
                        <{{$ser_sec->heading_tag}} class="twosubheading"> @if(isset($ser_sec->service_heading)) {{ $ser_sec->service_heading }} @endif</{{$ser_sec->heading_tag}}>
                    @endif
                @else
                <div class="twosubheading">
                    @if(isset($ser_sec->service_heading))
                    {{ $ser_sec->service_heading }}
                    @endif
                </div>
                @endif
                    <div class="commonpointers">
                        {!!$ser_sec->section1!!}
                    </div>

                </div>
                <div class="col-12 col-md-6">
                    @if(isset($ser_sec->heading_tag))
                        @if(isset($ser_sec->service_heading1))
                            <{{$ser_sec->heading_tag}} class="twosubheading"> @if(isset($ser_sec->service_heading1)) {{ $ser_sec->service_heading1 }} @endif</{{$ser_sec->heading_tag}}>
                        @endif
                    @else
                    <div class="twosubheading">
                        @if(isset($ser_sec->service_heading1))
                        {{ $ser_sec->service_heading1 }}
                        @endif
                    </div>
                    @endif
                    <div class="commonpointers">
                        {!!$ser_sec->section2!!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    <!-- Two Pragraph Section End -->

     <!-- Start of Twoparagraphdifflayot Section -->
    @if($ser_sec->type=='twoparagraphdifflayout')
    <div class="twoparadiffrentwrap @if(isset($ser_sec->class_add)) {{$ser_sec->class_add}} @endif">
        <div class="container">
                @if(isset($ser_sec->heading_tag))
                    @if(isset($ser_sec->section_heading))
                        <{{$ser_sec->heading_tag}} class="commonservicesheading"> @if(isset($ser_sec->section_heading)) {{ $ser_sec->section_heading }} @endif</{{$ser_sec->heading_tag}}>
                    @endif
                @else
                    <div class="commonservicesheading">
                        @if(isset($ser_sec->section_heading))
                        {{ $ser_sec->section_heading }}
                        @endif
                    </div>
                @endif
            {!! $ser_sec->service_comman !!}
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="commonleftheadingparadiffrentwrap">
                        @if(isset($ser_sec->heading_tag))
                            @if(isset($ser_sec->service_heading))
                                <{{$ser_sec->heading_tag}} class="twoparadifheading"> @if(isset($ser_sec->service_heading)) {{ $ser_sec->service_heading }} @endif</{{$ser_sec->heading_tag}}>
                            @endif
                        @else
                            @if(isset($ser_sec->service_heading))
                            <div class="twoparadifheading">
                                {{ $ser_sec->service_heading }}
                            </div>
                            @endif
                        @endif
                        {!!$ser_sec->section1!!}
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="commonleftheadingparadiffrentwrap">
                        @if(isset($ser_sec->heading_tag))
                            @if(isset($ser_sec->service_heading1))
                                <{{$ser_sec->heading_tag}} class="twoparadifheading"> @if(isset($ser_sec->service_heading1)) {{ $ser_sec->service_heading1 }} @endif</{{$ser_sec->heading_tag}}>
                            @endif
                        @else
                            @if(isset($ser_sec->service_heading1))
                            <div class="twoparadifheading">
                                {{ $ser_sec->service_heading1 }}
                            </div>
                            @endif
                        @endif
                        {!!$ser_sec->section2!!}
                    </div>
                </div>
            </div>
            {!! $ser_sec->service_comman2 !!}
        </div>
    </div>
    @endif
    <!-- Start of Twoparagraphdifflayot Section End-->


    <!-- Start of Sectionthreeparagraph -->
    @if($ser_sec->type=='sectionthreeparagraph')
    <div class="threeparasection centercontent-wrap @if(isset($ser_sec->class_add)) {{$ser_sec->class_add}} @endif">
        <div class="container">
            @if(isset($ser_sec->heading_tag))
                <{{$ser_sec->heading_tag}} class="commonservicesheading"> @if(isset($ser_sec->section_heading)) {{ $ser_sec->section_heading }} @endif</{{$ser_sec->heading_tag}}>
            @else
                <div class="commonservicesheading">
                    @if(isset($ser_sec->section_heading))
                        {{ $ser_sec->section_heading }}
                    @endif
                </div>
            @endif
            
            <div class="row">
                @if($ser_sec->section1)
                <div class="col-12">
                    <div class="commonthreepara mb-2">
                    {!! $ser_sec->section1 !!}
                    </div>
                </div>
                @endif
                @if($ser_sec->threepragraph->threeparagraphdata)
                    @for($i=0;$i<count($ser_sec->threepragraph->threeparagraphdata);$i++)
                        <div class="col-12 col-lg-4 col-md-6">
                            <div class="firstthreeparasection commonthreepara">
                                <div class="threeparaheadingsmall">@if(isset($ser_sec->threepragraph->sec_heading[$i])) {!!$ser_sec->threepragraph->sec_heading[$i]!!}@endif</div>
                                <div class="thereeparacontent">
                                    {!!$ser_sec->threepragraph->threeparagraphdata[$i]!!}
                                </div>
                            </div>
                        </div>
                    @endfor
                @endif
                @if($ser_sec->section2)
                <div class="col-12">
                    <div class="commonthreepara mt-2">
                    {!! $ser_sec->section2 !!}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    @endif
    <!-- Sectionthreeparagraph  End-->

    <!-- Right Image Section Start -->
    @if($ser_sec->type=='rightimagetext')
    <div class="rightimagecontentwrap @if(isset($ser_sec->class_add)) {{$ser_sec->class_add}} @endif">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 col-md-12">
                    @if(isset($ser_sec->heading_tag))
                        <{{$ser_sec->heading_tag}} class="rightimageheading commonservicesheading"> @if(isset($ser_sec->section_heading)) {{ $ser_sec->section_heading }} @endif</{{$ser_sec->heading_tag}}>
                    @else
                        <div class="rightimageheading commonservicesheading">
                            @if(isset($ser_sec->section_heading))
                            {{ $ser_sec->section_heading }}
                            @endif
                        </div>
                    @endif
                    {!! $ser_sec->section1 !!}
                </div>
                <div class="col-12 col-lg-6 col-md-12">
                    <div class="rightimagewraper">
                        <img src="{{url('/backend/service/section')}}/{{$ser_sec->image}}" class="w-100" alt="{{ $ser_sec->alttag }}">
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endif
    <!-- Right Image Section End -->
@endforeach
<!-- End foreach -->

 <!-- Start faqsection Section -->
 @if($servicefaqs)
 @if(count($servicefaqs)>0)
 <section class="faqsection">
     <div class="container">
         <div class="row">
             <div class="col-12 col-md-12 col-lg-12">
                 <h4 class="faqsectionheading">Frequently Asked Questions</h4>
             </div>
             <div class="col-12 col-md-12 col-lg-12">
                 <!-- Start of faqbox -->
                 @foreach($servicefaqs as $faq)
                 <div class="faqbox">
                     <div class="faqboxheading">{!! $faq->question !!}</div>
                     <div class="marginspace">
                     {!!$faq->answer!!}
                     </div>
                 </div>
                 @endforeach
                 <!-- // End of faqbox -->
             </div>
         </div>
     </div>
 </section>
 @endif
@endif
 <!--faqsection Section End-->

 <!-- Start Real Result Section -->
 @if($result->isNotEmpty())
 <section class="seventhsection">
     <div class="container-fluid">
         <div class="row">
             <div class="col-12 col-md-12 col-lg-12">
                 <h4 class="seventhsectionheading">Before and After</h4>
             </div>
             @foreach($result as $real)
             <div class="col-12 col-md-4 col-lg-4">
                 <div class="serviceresultcolumn">
                     <img src="{{ url('/backend/service_result/inner') }}/{{ $real->afterimg }}" class="img-fluid" alt="{{ $real->alt_img }}" />
                     <p>*Opinion / Results may vary person to person.</p>
                 </div>
             </div>
             @endforeach
             @if($real_cat)
             <div class="col-12 col-md-12 col-lg-12">
                 <div class="resulttopdiv">
                     <a href="{{ url('result') }}/{{ $real_cat->url }}" class="servicevideobtn">View More Results</a>
                 </div>
             </div>
             @endif
         </div>
     </div>
 </section>
 @endif
 <!-- // End Real Result Section -->


<!-- Start Video Section -->
@if($video->isNotEmpty())
<section class="eightsection">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <h4 class="eightsectionheading commonservicesheading text-center">Videos</h4>
            </div>
            @foreach($video as $vid)
            <div class="col-12 col-md-4">
                <div class="videoinnerbox">
                    <div class="testimonialpagevideowrap">
                        <div class="video-imagetestimona">
                            <img src="{{ url('/backend/service_video/inner') }}/{{ $vid->image }}" class="w-100" alt="{{ $vid->alt_img }}">
                        </div>
                        <div class="playbutton vid forabsl">
                            <!-- Button trigger modal -->
                            <button type="button"  class="btn btn-primary video-btn" onclick="openVideo('{{$vid->video}}')"> <span class="fa fa-play"></span></button>
                        </div>
                    </div>
                    <div class="videohd">
                        {{$vid->name}}
                    </div>
                </div>
            </div>
             @endforeach
             @if($video_cat)
            <div class="col-12 col-md-12 col-lg-12">
                <div class="resulttopdiv">
                    <a href="{{ url('video') }}/{{ $video_cat->url }}" class="servicevideobtn">View More Videos</a>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
 @endif
 <!-- Video Model box -->
<div class="video-modal-overlay "></div>
<div class="video-modal testimonial_video ">
    <div class="video-box">
        <button class="modal-btn video_close" fdprocessedid="hnipnw"><i class="fa fa-close"></i></button>
        <iframe src="" id="videoIframe" title="YouTube video player" frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowfullscreen=""></iframe>
    </div>
</div>
<style>
    .video-modal-overlay {
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.445);
    z-index: 9;
    display: none;
}
.video-modal {
    max-width: 800px;
    width: 100%;
    background: white;
    position: fixed;
    z-index: 111;
    z-index: 99;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    padding: 10px;
    box-shadow: rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px;
    text-align: left;
    display: none;
}
.video-modal .video-box {
    position: relative;
}
.video-modal .video-box .modal-btn {
    position: absolute;
    right: 0px;
    top: -1px;
    width: 50px;
    border: none;
    line-height: 0;
    height: 35px;
    padding: 0px;
    background: white;
}
.video-modal .video-box iframe {
    height: 480px;
    width: 100%;
}
.video-modal.activeVideo {
    display: block;
}
</style>
<!-- Video Section -->
<script async type="text/javascript">
    let play = document.getElementById("videoIframe");
    let closevideo = document.querySelector(".video_close");
    let overlay = document.querySelector(".video-modal-overlay");
    let model_box = document.querySelector(".video-modal");
    function openVideo(val) {
        overlay.classList.add("activeVideo");
        model_box.classList.add("activeVideo");
        play.src = val;
        document.body.classList.add("stop_scroll");
    }
    closevideo.addEventListener("click", function() {
        overlay.classList.remove("activeVideo");
        model_box.classList.remove("activeVideo");
        play.src = "";
        document.body.classList.remove("stop_scroll");
    });
    overlay.addEventListener("click", function() {
        overlay.classList.remove("activeVideo");
        model_box.classList.remove("activeVideo");
        play.src = "";
        document.body.classList.remove("stop_scroll");
    });
</script>
 @include('website.include.requestanappointment')

@include('website.include.footer')
