<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td style="padding-top: 20px;" valign="top" bgcolor="#ffffff" width="100%"><!-- One Column -->
<table class="deviceWidth" style="font-family: Arial, sans-serif; font-size: 13px; padding: 0;" border="0" width="580" cellspacing="0" cellpadding="0" align="center" bgcolor="#ECE4E1">
<tbody>
<tr>
<td style="width: 100%; background: #fff;">
<table border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td style="padding: 10px 0; test-align: centre;"><a href="#" target="_blank"><img style="padding-left: 13px;" src="#" alt="" border="0" /></a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td height="10">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0 15px;">Dear {{$details['name']}},</td>
</tr>
<tr>
<td height="10">&nbsp;</td>
</tr>
<tr>
<td height="10">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0 15px;">
<h2>Forgot your password?</h2>
<p>We received a request to reset the password for your account.<br /><br />To reset your password, click below link:</p>
<a href="{{$details['url']}}"> Click here </a></td>
</tr>
<tr>
<td height="10">&nbsp;</td>
</tr>
<tr>
<td height="20">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0 15px;">Thank You,</td>
</tr>
<tr>
<td style="padding: 0 15px;">Digilantern</td>
</tr>
<tr>
<td height="20">&nbsp;</td>
</tr>
<tr>
<td style="width: 100%; background: #a11e1e;"><!-- Logo -->
<table border="0" width="580" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td class="center" style="padding: 25px 0px 9px 0px;">
<h2 style="color: #fff; font-size: 18px; padding: 0px 10px 0px 20px;">Follow Us on:</h2>
</td>
<td style="padding: 9px 0px 9px 228px;"><a style="padding-right: 5px;" href="#" target="_blank"> <img src="#" /> </a> <a style="padding-right: 5px;" href="#" target="_blank"> <img src="" /> </a> <a style="padding-right: 5px;" href="#" target="_blank"> <img src="#" /> </a></td>
</tr>
</tbody>
</table>
<!-- End Logo --></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
