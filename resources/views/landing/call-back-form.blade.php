<form id="callback" class="formBox" action="{{ url('callback-form') }}" method="post">
    <p id="errormsg" class="text-center" style="color: red; font-weight:bold;"></p>
    <input name="name" autocomplete="off" type="text" id="name" class="infldNew" placeholder="Name*" required="">
    <input name="phone" autocomplete="off" id="phone" type="text" class="infldNew" placeholder="Phone*" minlength="10" maxlength="10" required="">
    <!-- <textarea class="infldNew" rows="4" name="message" placeholder="Message"></textarea> -->
           <!-- <select class="infldNew" name="" id="mySelect">
                <option value="">Select a Treatment</option>
                <option value="kidney-stone">Kidney Stone</option>
                <option value="prostate-surgery">Prostate Surgery</option>
                <option value="bladder-stone">Bladder Stone</option>
                <option value="ureteric-stone">Ureteric Stone</option>
                <option value="prostate-cancer">Prostate Cancer</option>
                <option value="circumcision-surgery">Circumcision Surgery</option>
                <option value="kidney-transplant">Kidney Transplant</option>
                <option value="stricture-urethra">Stricture Urethra</option>
            </select> -->
    <input type="hidden" name="request_url" value="{{ $_SERVER['REQUEST_URI'] }}">
    <input type="hidden" name="referral_url" value="">
    <input type="hidden" name="honey">
    <div class="d-grid">
        <button type="submit" id="call_submit" class="newbtnSubmit mt-2">SUBMIT</button>
        <div class="loadme"><img src="{{ url('assets/images/loading_icon.gif') }}" width="40" height="40"></div>
    </div>
</form>