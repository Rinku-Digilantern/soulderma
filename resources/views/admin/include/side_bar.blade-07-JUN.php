    <header class="header fixed-top clearfix">
        <div class="row header_content">
            <!--logo start-->
            <div class="col-md-4">
                <div class="brand">
                    <a href="{{url('admin/dashboard')}}" class="logo">
                        <img src="{{ url('/admin/images/Arete_logo.png') }}" style="height:50px;">
                    </a>
                    <div class="sidebar-toggle-box">
                        <div class="fa fa-bars"></div>
                    </div>
                </div>
            </div>
            <!--logo end-->
            <!-- search_bar -->
            <div class="col-md-4">
                <div class="search_bar">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    <input type="text" class="form-control" placeholder="Search">
                </div>
            </div>

            <div class="col-md-4">
                <div class="top-nav clearfix ">
                    <ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item nav-profile dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                                <div class="admin_login_profile">
                                    <div class="admin_logo">
                                        <!-- <img src="{{url('/images/Arete_logo.png')}}" alt="profile" /> -->
                                        <img src="{{ url('/admin/images/Arete_logo.png') }}" alt="profile" />
                                    </div>
                                    <div class="menu">
                                        <span class="nav-profile-name">Admin</span>
                                        <span class="online-status"></span>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                                <a class="dropdown-item" href="{{url('/admin/logout')}}">
                                    <i class="mdi mdi-logout fa fa-sign-out text-primary" aria-hidden="true"></i>Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>



    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse">
            <!-- sidebar menu start-->
            <div class="leftside-navigation">
                <ul class="sidebar-menu" id="nav-accordion">
                    
                    @foreach( Session::get('userinfo')['user_menu_permissions'] as $menu)
                    @if($menu['mnu_dropdown']=='No')
                    <li class="<?php if(in_array($menu['mnu_url'],request()->segments())) { echo 'sbactive'; } ?>">
                        <a class="" href="{{url('/admin/'.$menu['mnu_url'])}}">
                            <i class="{{ $menu['mnu_icon']}}"></i>
                            <span>{{ $menu['mnu_name']}}</span>
                        </a>
                    </li>
                    @else
                    <li class="bold mnactive"><a class="" href="{{ url('/admin/'.$menu['mnu_url']) }}"><i class="{{ $menu['mnu_icon']}}"></i><span> {{ $menu['mnu_name']}} </span></a>
                        <ul class="sub">
                            @foreach( Session::get('userinfo')['user_operation_permissions'] as $operation)
                            @if( $menu['mnu_id'] == $operation['cfg_mun_id'] )
                            <li class="<?php if(in_array($operation['op_link'],request()->segments())) { echo 'sbactive'; } ?>"> <a href="{{ url('/admin/'.$operation['op_link'])}}"> {{ $operation['op_name']}} </a></li>
                            @endif
                            @endforeach
                        </ul>
                    </li>
                    @endif
                    @endforeach
                </ul>
            </div>
            <!-- sidebar menu end-->
        </div>
    </aside>
    
    <style>
        .mnactive .sbactive{
            color:red;
        }
    </style>