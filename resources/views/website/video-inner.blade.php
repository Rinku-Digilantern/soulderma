@include('website.include.header')

<link rel="stylesheet" href="{{ url('website') }}/css/videos.css">


<section id="topcommonbanner">
    <div class="commonbannerclinic">
        <img src="{{ url('website') }}/img/about-clinic/about-clinic.jpg" class="w-100">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="headingandbredcrumb">
                    <div class="commonheading notofamily commonheadingfontsize">{{$cat->name}}</div>
                    <div class="breadcrumb commonbredcurmb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a>videos</a></li>
                            <li>{{$cat->name}}</li>
                        </ul>
                    </div>
                    <div class="commonrequestform">
                        @include('website.include.requestcallback')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="videoteastimonialwrap">
    <div class="container">
        <div class="row">
            @foreach($video as $vid)
            <div class="col-12 col-md-4">
                <div class="videoinnerbox">
                    <div class="testimonialpagevideowrap">
                        <div class="video-imagetestimona">
                            <img src="{{ url('/backend/service_video/inner') }}/{{ $vid->image }}" class="w-100" alt="{{ $vid->alt_img }}">
                        </div>
                        <div class="playbutton vid forabsl">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary video-btn" data-bs-toggle="modal" data-src="{{ $vid->video }}" data-bs-target="#myModal"> <span class="fa fa-play"></span>
                            </button>
                        </div>

                    </div>
                    <div class="videohd">
                        {{$vid->name}}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>


<!--common Modal for all video in this page-->
<section id="commonmodalvideopopup" class="fade-in-out">
    <div class="modal fade modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <!-- 16:9 aspect ratio -->
                    <div class="ratio ratio-16x9">
                        <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always" allow="autoplay"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end--common Modal for all video in this page-->
@include('website.include.requestanappointment')
@include('website.include.footer')