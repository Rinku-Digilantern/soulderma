<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmailJob;
use App\Mail\OwnerMail;
use App\Mail\ContactUsMail;
use App\Mail\UserMail;
use App\Models\AppointmentModel;
use App\Models\CaptchaModel;
use App\Models\ContactUsModel;
use App\Models\GetAQuotesModel;
use App\Models\CallBackModel;
use App\Models\GoogleRecaptchaModel;
use App\Models\MasterOrgModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;


class AppointmentController extends Controller
{
    public function index()
    {
        $select_table = ['app_id', 'name', 'email', 'phone', 'date', 'request_url', 'referral_url', 'source_type', 'message', 'created_at'];
        $data = AppointmentModel::select($select_table)->orderBy('app_id','DESC')->get();
        return view('admin.appointment.list-appointment', ['view' => $data]);
    }

    public function call_back_form()
    {
        $select_table = ['id', 'name', 'mobile', 'request_url', 'referral_url', 'source_type', 'created_at'];
        $data = CallBackModel::select($select_table)->orderBy('id','desc')->get();
        return view('admin.appointment.list-call-back', ['view' => $data]);
    }

    public function getaquote()
    {
        $select_table = ['quotes_id', 'name', 'email', 'phone', 'message', 'service_name','quotes', 'created_at'];
        $data = GetAQuotesModel::select($select_table)->orderBy('quotes_id','desc')->get();
        return view('admin.appointment.list-getaqoute', ['view' => $data]);
    }

    public function add_appointment_mail()
    {
        return view('admin.appointment.add-appointment');
    }

	public function appointmentsave(Request $request){
// 	          echo    Session::get('favcolor');
// dd(session()->all());
// die('sss');

        $validator = Validator::make($request->input(), [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'date' => 'required',
            'captcha' => 'required',
        ]);

        //Will get redirected if validator is not matched
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => 'validation failed'], 400);
        }
        $cap = $request->input('captcha');
        $capuid = $request->input('uncode');

        $cp = CaptchaModel::where('capchacode',$cap)->where('uid',$capuid);

        if($cp->count() < 1){
            return response()->json(['success' => false, 'data' => []], 401);
        }

        //new data will be create in menu table
        $callId = AppointmentModel::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'date' => date('d-m-Y',strtotime($request->input('date'))),
            'message' => $request->input('message'),
            'treatment' => $request->input('treatment'),
            'request_url' => $request->input('request_url'),
            'referral_url' => $request->input('referral_url'),
            'source_type' => $request->input('source_type'),
        ]);

        $url = url('/');
        $backendurl = env('BACK_FRONT');

        $detailsowner = [
            'name' =>  $request->input('name'),
            'orgname' =>  '',
            'clientphone' =>  $request->input('phone'),
            'clientemail' =>  $request->input('email'),
            'date' => date('d-m-Y',strtotime($request->input('date'))),
            'treatment' => $request->input('treatment'),
            'page' => 'appointment-owner',
            'request_url' => $request->input('request_url'),
            'referral_url' => $request->input('referral_url'),
            'message' => $request->input('message'),
        ];
        $detailsuser = [
            'name' =>  $request->input('name'),
            'orgname' =>  'Soulderma',
            'primarycountrycode' => '91',
            'primaryphone' =>  '8595941529',
            'secondaryphone' =>  null,
            'secondarycountrycode' => null,
            'email' =>  $request->input('email'),
            'url' => $url,
            'page' => 'appointment-user',
            'logo' => ''
        ];
        // if($siteview->count() > 0){
        Mail::to($request->input('email'))->send(new UserMail($detailsuser));

        Mail::to('info@souldermaclinic.com')->send(new OwnerMail($detailsowner));
        // dispatch(new SendEmailJob($detailsuser));
        // dispatch(new SendEmailJob($detailsowner));
        // }
        return response()->json(['success' => true], 200);
    }

    public function contactussave(Request $request){

        $validator = Validator::make($request->input(), [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'captcha' => 'required',
            'message'=>'required'
        ]);

        //Will get redirected if validator is not matched
        if ($validator->fails()) {
            return response()->json(['success' => false, 'data' => []], 400);
        }
        $cap = $request->input('captcha');
        $capuid = $request->input('uncode');

        $cp = CaptchaModel::where('capchacode',$cap)->where('uid',$capuid);

        if($cp->count() < 1){
            return response()->json(['success' => false, 'data' => []], 401);
        }

        //new data will be create in menu table
        $callId = ContactUsModel::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'message' => $request->input('message'),
        ]);

        // $org = MasterOrgModel::find($request->input('org_id'));
        $url = url('/');
        $backendurl = env('BACK_FRONT');

        $detailsowner = [
            'name' =>  $request->input('name'),
            'orgname' =>  'Soulderma',
            'phone' =>  $request->input('phone'),
            'email' =>  $request->input('email'),
            'page' => 'contact-owner',
            'message' => $request->input('message'),
        ];
        $detailsuser = [
            'name' =>  $request->input('name'),
            'orgname' =>  'Soulderma',
            'primarycountrycode' => '91',
            'primaryphone' =>  '8595941529',
            'secondaryphone' =>  null,
            'secondarycountrycode' => null,
            'email' =>  $request->input('email'),
            'url' => $url,
            'page' => 'contact-user',
            'logo' => ''
        ];

        Mail::to($request->input('email'))->send(new UserMail($detailsuser));

        Mail::to('info@souldermaclinic.com')->send(new OwnerMail($detailsowner));

        // dispatch(new SendEmailJob($detailsuser));
        // dispatch(new SendEmailJob($detailsowner));

        return response()->json(['success' => true], 200);
    }


    public function callbacksave(Request $request){
        $validator = Validator::make($request->input(), [
            'name' => 'required',
            'mobile' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => 'validation failed'], 400);
        }


        $cap = $request->input('captcha');
        $capuid = $request->input('uncode');
        $cp = CaptchaModel::where('capchacode',$cap)->where('uid',$capuid);

        if($cp->count() < 1){
            return response()->json(['success' => false, 'data' => []], 401);
        }

       $data=[
           'name'=>$request->input('name'),
            'mobile'=>$request->input('mobile'),
            'request_url' => $request->input('request_url'),
            'referral_url' => $request->input('referral_url'),
           ];
        //new data will be create in menu table
        $callId =  CallBackModel::create($data);

        // $org = MasterOrgModel::find($request->input('org_id'));
        $url = url('/');
        $backendurl = env('BACK_FRONT');

        $detailsowner = [
            'name' =>  $request->input('name'),
            'orgname' =>  'Soulderma',
            'mobile' =>  $request->input('mobile'),
            'request_url' => $request->input('source'),
            'referral_url' => $request->input('referer'),
            'page' => 'callback-owner',
        ];

        $detailsuser = [
                    'name' =>  $request->input('name'),
                    'orgname' =>  'Soulderma',
                    'primarycountrycode' => '91',
                    'primaryphone' =>  '8595941529',
                    'secondaryphone' =>  null,
                    'secondarycountrycode' => null,
                    'mobile' =>  $request->input('mobile'),
                    'url' => $url,
                    'page' => 'callback-user',
                    'logo' => ''
                ];

        // dispatch(new SendEmailJob($detailsowner));
        // Mail::to($request->input('email'))->send(new UserMail($detailsuser));
        Mail::to('info@souldermaclinic.com')->send(new OwnerMail($detailsowner));
        return response()->json(['success' => true], 200);
    }

}
