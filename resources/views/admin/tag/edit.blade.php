<?php
// echo "<pre>";
//     print_r($tag);
// echo "</pre>";
?>

@extends('admin.master')
@section('content')
@php 
$primeid = session('primeid');
@endphp
<div id="main">
<section id="main-content">
  <section class="wrapper">
  
  
<div class="card cardsec">
      <div class="card-body">
        <div class="page-title pagetitle">
          <h1>Edit Tag</h1>
          <ul class="breadcrumb side">
            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
            <li><a href="{{url('/admin/index-tag')}}">Tag Lists</a></li>
            <li class="active">Edit Tag</li>
          </ul>
        </div>
      </div>
    </div>

    <!-- Start form here -->
    <form action="{{route('update_tag',$tag)}}" id="myForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
      <input type="hidden" value="{{ csrf_token() }}" name="_token">
      <input type="hidden" name="site_id" value="{{session('useradmin')['site_id']}}">
      <input type="hidden" name="org_id" value="{{session('useradmin')['org_id']}}">
      <input type="hidden" name="created_by" value="{{session('useradmin')['usr_id']}}">
      <input type="hidden" name="seo_id" id="seo_id" value="@if(isset($primeid)){{$primeid}}@else 0 @endif">
      <!-- @csrf -->
      <div class="card">
        <div class="card-body">
		<div class="container">
          <div class="row">
      

            <div class="form-group col-md-6">
              <label class="control-label">Tag type</label>
              <select class="form-control" name="type">
                  <option value="">Select Tag Type</option>
                  <option value="googletagmanager" 
                  @if(isset($tag->type))
                    @if($tag->type=='googletagmanager')
                     selected
                    @endif
                  @endif>
                Google Tag Manager</option>
                  <option value="fbpixel"
                  @if(isset($tag->type))
                  @if($tag->type=='fbpixel')
                   selected
                  @endif
                @endif
                  >Facebook Pixel</option>
                  <option value="schema"
                  @if(isset($tag->type))
                  @if($tag->type=='schema')
                   selected
                  @endif
                @endif
                  >Schema</option>

               </select>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label">Status</label>
                <select class="form-control" name="status">
                    <option value="">Select Status</option>
                    <option value="1"
                    @if(isset($tag->status))
                        @if($tag->status==1)
                            selected
                        @endif
                    @endif
                    >Active</option>
                    <option value="0"
                    @if(isset($tag->status))
                    @if($tag->status==0)
                        selected
                    @endif
                @endif
                    >Inactive</option>
                </select>
              </div>
            <div class="form-group col-md-6">
                <label class="control-label">Head Part</label>
                <textarea class="form-control height600" name="headtag" cols="12" rows="12">
                    @if(isset($tag->headtag))
                       <?= $tag->headtag ?>
                    @endif
                </textarea>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label">Body Part</label>
                <textarea class="form-control height600" name="bodytag" cols="12" rows="12">
                    @if(isset($tag->bodytag))
                    <?= $tag->bodytag ?>
                 @endif
                </textarea>
            </div>
            
          


           

            <div class="col-sm-12 marginT30 col s12" style="padding-top: 20px; padding-bottom: 20px;">
              <button type="submit" class="btn btn-primary icon-btn" onclick="validatesec()">
                <i class="fa fa-fw fa-lg fa-check-circle"></i>Submit
              </button>&nbsp;&nbsp;&nbsp;
              <a class="btn btn-default icon-btn" href="{{url('/admin/seo')}}">
                <i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel
              </a>
            </div> 
          </div>
		  </div>
        </div>
      </div>
    </form>
    <!-- End form -->

  </section>
</section>
</div>

<script>
  CKEDITOR.replace('textArea');
</script>
@endsection