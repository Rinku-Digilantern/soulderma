@include('website.include.header')


<link rel="stylesheet" href="{{ url('website') }}/css/blog-detail.css">


<section id="topcommonbanner">
    <div class="commonbannerclinic">
        <img src="{{ url('backend/blog') }}/{{ $bloglist->blog_image_inner }}" class="w-100">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="headingandbredcrumb">
                    <div class="commonheading notofamily commonheadingfontsize">{{ $bloglist->blog_name }}</div>
                    <div class="breadcrumb commonbredcurmb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="{{ url('blogs') }}">Blogs</a></li>
                            <li><a>{{ $bloglist->blog_name }}</a></li>
                        </ul>
                    </div>
                    <div class="commonrequestform blog_design_form">
                        @include('website.include.requestcallback')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="blogdetailwrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="detaildwrap">
                    <div class="detailedwrap-heading">{{ $bloglist->blog_name }}</div>
                    <ul>
                        <li><a href="#"><i class="fa fa-user"></i>Soul derma</a></li>
                        <li><a href="#"><i class="fa fa-calendar"></i>{{ $bloglist->date }}</a></li>
                    </ul>

                </div>

                <div class="detailedcontentwrap">
                    {!! $bloglist->blog_description !!}
                </div>
            </div>
            <!-- <div class="col-12 col-md-4">
                <div class="recentpostwrap">
                    <div class="recentpostheading">Popular Post</div>
                    @foreach($blog as $blg)
                    <a href="#" class="recentlinkwrap">
                        <div class="recentpostimage">
                            <img src="{{ url('backend/blog/') }}/{{ $blg->blog_image }}" class="w-100" alt="{{ $blg->alt_image_name }}">
                        </div>
                        <div class="recentpostcontent">
                            <div class="recentcontentheading"><a href="{{ url('blogs') }}/{{ $blg->url }}">{{ $blg->blog_name }}</a></div>
                            <p class="shordic">{!! Str::limit($blg->short_desc,75) !!}....</p>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div> -->
        </div>
    </div>
</section>

@include('website.include.requestanappointment')
@include('website.include.footer')