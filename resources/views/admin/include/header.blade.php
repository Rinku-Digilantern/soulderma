<title>Soulderma | Admin Panel</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<!-- <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script> -->
<!-- bootstrap-css -->
<link rel="stylesheet" href="{{ url('/admin/css/bootstrap.min.css')}}">
<link href="{{ url('/admin/dropdown/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ url('/admin/css/main.css')}}">
<link href="{{ url('/admin/css/style.css')}}" rel='stylesheet' type='text/css' />
<link href="{{ url('/admin/css/style-responsive.css')}}" rel="stylesheet" />
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ url('/admin/css/font.css')}}" type="text/css" />
<link href="{{ url('/admin/css/font-awesome.css')}}" rel="stylesheet">
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->

<script src="{{ url('/admin/js/jquery2.0.3.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="{{ url('/admin/ckeditor/ckeditor.js')}}"></script>
<script src="{{ url('/admin/js/raphael-min.js')}}"></script>
<script src="{{ url('/admin/js/morris.js')}}"></script>
<script src="{{ url('/admin/ckeditor/samples/js/sample.js')}}"></script>

<!-- Start for date range  -->
<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" /> -->
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<!-- End for date range  -->

<!-- <script src="{{ url('/admin/dropdown/select2.min.js')}}"></script>
    <script src="{{ url('/admin/dropdown/form-select2.js')}}"></script> -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<script>
    //     $('select').select2({
    //   closeOnSelect: false
    // });
</script>

<?php //print_r(session('useradmin'));
?>
